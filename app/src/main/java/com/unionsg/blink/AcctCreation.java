package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class AcctCreation extends AppCompatActivity {

    private Spinner spinner1;
    private Spinner spinner2;
    private EditText nameEditText;
    private Button confirmButton;
    private EditText accNameEditText;
    private EditText acctEditText;
    TextView titleTextView;
    ImageButton fab;
    Button search_button;
    Dialog dialog;
    ProgressDialog progress;
    String brCode = null;
    String usrName = null;

    String mbAccDetails = null;
    String[] accDetail = null;

    String aliasAccNum = null;
    String aliasName = null;
    String accTypes = "Choose type of account~Current~Savings";

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String telcos = null;
    String aliasAcc = null;

    String accNum = null;
    String accName = null;

    ProgressDialog progress1;
    ProgressDialog progress2;
    int pos;
    String amount = null;
    String topAmt = null;

    String desc = null;
    String legalForm = null;
    String prodCode = null;

    String sendDesc = null;
    String sendLegalForm = null;
    String sendProdCode = null;

    String currCode = null;
    String curr = null;

    String alias = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;

    String response = null;
    String branch = null;
    String brName = null;
    String accType = null;
    String sendAccType = null;
    String sendCurr = null;
    String sendCurrCode = null;

//    Handler handler;
//    Runnable r;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acct_creation);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
//		accDetail = mbAccDetails.split(",");


        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new currencyFireMan().execute();
        } else {
            showAlertDialog(AcctCreation.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }

        nameEditText = (EditText) findViewById(R.id.nameedittext);
        nameEditText.setEnabled(false);
        spinner1 = (Spinner) findViewById(R.id.spinner1);
        acctEditText = (EditText) findViewById(R.id.acctedittext);
        acctEditText.setEnabled(false);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        confirmButton = (Button) findViewById(R.id.confirm_button);

        nameEditText.setHint("eg. MARK SAVINGS ACCOUNT");
        nameEditText.setText(usrName);


        populateSpinner1();


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accType = ((TextView) view.findViewById(R.id.acctype)).getText().toString().trim();
                switch (accType) {
                    case "Current":
                        sendAccType = "1";
                        System.out.println(sendAccType);
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new searchFireMan().execute();
                        } else {
                            showAlertDialog(AcctCreation.this, "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                        break;
                    case "Savings":
                        sendAccType = "2";
                        cd = new ConnectionDetector(getApplicationContext());
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new searchFireMan().execute();
                        } else {
                            showAlertDialog(AcctCreation.this, "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                        break;
                    case "Choose type of account":
                        break;
                }


            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });

        spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                sendCurr = ((TextView) view.findViewById(R.id.curr)).getText().toString().trim();
                sendCurrCode = ((TextView) view.findViewById(R.id.currcode)).getText().toString().trim();
//							sendDesc = ((TextView) v.findViewById(R.id.desc)).getText().toString().trim();
                System.out.println(sendCurr);
                System.out.println(sendCurrCode);

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                accName = nameEditText.getText().toString();
                if (accName.equals("")) {
                    nameEditText.setError("Please enter account name");
                }
//
                else {

                    System.out.println("brCode is " + brCode);

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new confirmFireMan().execute();
                    } else {
                        showAlertDialog(AcctCreation.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }

//					Toast.makeText(Ach.this, accountNum + "\n" + amount + "\n" + brchName + "\n" + crAcc + "\n" + transDet, Toast.LENGTH_LONG).show();
//					Intent confirmTopupIntent = new Intent(AcctCreation.this,ConfirmAch.class);
//					confirmTopupIntent.putExtra("crAcc", aliasAccNum);
//					confirmTopupIntent.putExtra("crAccName", aliasName);
//					confirmTopupIntent.putExtra("brName", brName);
//					confirmTopupIntent.putExtra("token", token);
//					startActivity(confirmTopupIntent);
                }

            }
        });

        //call in onCreate
        //  setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(AcctCreation.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(AcctCreation.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//        //Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }

    public void populateSpinner1() {

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

//						System.out.println(aliasAccDetails.length);
        StringTokenizer accDetToken = new StringTokenizer(accTypes, "~");

        for (int a = 0; accDetToken.hasMoreTokens(); a++) {

            System.out.println(accDetToken.countTokens());

            telcos = accDetToken.nextToken().trim();
//							beneAcc = accDetToken.nextToken().trim();

            System.out.println(telcos);
//
            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("telconame", telcos);
//							hm.put("aliasaccnum", beneAcc "**********"+ .lastIndexOf(0, -3)  );

            theList.add(hm);

        }

        String[] from = {"telconame"};
        // Ids of views in listview_layout
        int[] to = {R.id.acctype};

        SimpleAdapter adapter2 = new SimpleAdapter(AcctCreation.this, theList, R.layout.list_acctypes, from, to);

        spinner1.setAdapter(adapter2);

    }


    public String banksJSON(String sendAcType, String authToken) {
        return "accType=" + sendAcType +
                "&authToken=" + authToken;
    }


    public String myBanksResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = banksJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "legalform", json);

        System.out.println("response is " + response);

        return response;

    }


    public class searchFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress1 = new ProgressDialog(AcctCreation.this);
            progress1.setCancelable(false);
            progress1.setTitle("Please wait");
            progress1.setMessage("Retrieving account types...");
            progress1.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myBanksResponse(sendAccType, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress1.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);

                    progress1.cancel();
                    new ResponseDialog(AcctCreation.this, "Not Found", mbAcc);
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress1.cancel();
                    /*if(mbAcc.isEmpty()){
                        new ResponseDialog(TopUp.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds");
					}
					else{*/
                    System.out.println("mbAcc: " + mbAcc);

                    //					StringTokenizer accDetToken = new StringTokenizer(string)
//						aliasAccDetails = mbAcc.split("");

//					aliasAccDetails = mbAcc.split(",");

                    dialog = new Dialog(AcctCreation.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setTitle("Account Type");
                    dialog.setContentView(R.layout.selectbank_dialog);
                    dialog.setCancelable(true);
                    titleTextView = (TextView) dialog.findViewById(R.id.titleText);
                    titleTextView.setText(accType + " Accounts");
                    ListView list = (ListView) dialog.findViewById(R.id.ach_banklist);
                    aliasAccDetails = mbAcc.split(",");


                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

//					System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
                        prodCode = myAccToken.nextToken().trim();
                        legalForm = myAccToken.nextToken().trim();
                        desc = myAccToken.nextToken().trim();

                        System.out.println(prodCode);
                        System.out.println(legalForm);
                        System.out.println(desc);


                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("prodCode", prodCode);
                        hm.put("legalForm", legalForm);
                        hm.put("desc", desc);
//						hm.put("ledgerbal", "   " + accLedgerBal);

                        theList.add(hm);

                        System.out.println("a is " + a);

                    }

                    String[] from = {"prodCode", "legalForm", "desc"};
                    int[] to = {R.id.prodcode, R.id.legalform, R.id.desc};


                    SimpleAdapter adapter2 = new SimpleAdapter(AcctCreation.this, theList, R.layout.list_products, from, to);
//
//
                    list.setAdapter(adapter2);
//
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                                long arg3) {
                            // TODO Auto-generated method stub
                            //send legal form and product code
                            sendLegalForm = ((TextView) v.findViewById(R.id.legalform)).getText().toString().trim();
                            sendProdCode = ((TextView) v.findViewById(R.id.prodcode)).getText().toString().trim();
                            sendDesc = ((TextView) v.findViewById(R.id.desc)).getText().toString().trim();
                            acctEditText.setText(sendDesc);
                            System.out.println(sendLegalForm);
                            System.out.println(sendProdCode);
//							bName = ((TextView) v.findViewById(R.id.bankname)).getText().toString().trim();

                            dialog.dismiss();


                        }
                    });

                    dialog.show();


                }

            }

        }

    }


    public String currJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String currResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = currJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "curr", json);

        System.out.println("response is " + response);

        return response;

    }


    public class currencyFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress1 = new ProgressDialog(AcctCreation.this);
            progress1.setCancelable(false);
            progress1.setTitle("Please wait");
            progress1.setMessage("Retrieving account types...");
            progress1.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = currResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    progress1.cancel();
                    e.printStackTrace();
//                    System.out.println("Exception e: " + e.toString());
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                progress1.cancel();
                e.printStackTrace();
                System.out.println("Exception e: " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

//			if(e != null){
//				progress.cancel();
//				showAlertDialog(LoginActivity.this, "Error!","No response from server. Please check your Internet connection and try again",false);
//			} else {
//				super.onPostExecute(result);
//
//				if (mb_response == null){
//					progress.cancel();
//					showAlertDialog(LoginActivity.this, "Error!","Connection timed out. Please check your Internet connection and try again",false);
//
//				}

            if (e != null) {
//                System.out.println("Exception e: " + e.toString());
                progress1.cancel();
                showAlertDialog(AcctCreation.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress1.cancel();
//                    showAlertDialog(AcctCreation.this, "Error!", "Connection timed out. Please check your Internet connection and try again", false);

                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);

                    progress1.cancel();
                    new ResponseDialog(AcctCreation.this, "Not Found", mbAcc).showDialog();

                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "No response", "Please check internet connection").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress1.cancel();
					/*if(mbAcc.isEmpty()){
						new ResponseDialog(TopUp.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds");
					}
					else{*/
                    System.out.println("mbAcc: " + mbAcc);

                    //					StringTokenizer accDetToken = new StringTokenizer(string)
//						aliasAccDetails = mbAcc.split("");

//					aliasAccDetails = mbAcc.split(",");

//					dialog = new Dialog(AcctCreation.this);
//					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////                    dialog.setTitle("Long press to select recipient's bank");
//					dialog.setContentView(R.layout.selectbank_dialog);
//					dialog.setCancelable(true);
//					ListView list = (ListView) dialog.findViewById(R.id.ach_banklist);
                    aliasAccDetails = mbAcc.split(",");

//					StringTokenizer myToken = new StringTokenizer(mbAcc,",");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

//					System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
//						bankName = myAccToken.nextToken().trim();
                        currCode = myAccToken.nextToken().trim();
                        curr = myAccToken.nextToken().trim();
//						desc = myAccToken.nextToken().trim();

                        System.out.println(currCode);
                        System.out.println(curr);


                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("currCode", currCode);
                        hm.put("curr", curr);
//						hm.put("ledgerbal", "   " + accLedgerBal);

                        theList.add(hm);

                        System.out.println("a is " + a);

                    }

                    String[] from = {"currCode", "curr"};
                    int[] to = {R.id.currcode, R.id.curr};


                    SimpleAdapter adapter2 = new SimpleAdapter(AcctCreation.this, theList, R.layout.list_curr, from, to);
//
//
                    spinner2.setAdapter(adapter2);
//
//					list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//						@Override
//						public void onItemClick(AdapterView<?> arg0, View v, int arg2,
//												long arg3) {
//							// TODO Auto-generated method stub
//							//send legal form and product code
//							sendCurr = ((TextView) v.findViewById(R.id.curr)).getText().toString().trim();
//							sendCurrCode = ((TextView) v.findViewById(R.id.currcode)).getText().toString().trim();
////							sendDesc = ((TextView) v.findViewById(R.id.desc)).getText().toString().trim();
//							System.out.println(sendCurr);
//							System.out.println(sendCurrCode);
////							bName = ((TextView) v.findViewById(R.id.bankname)).getText().toString().trim();
//
//							dialog.dismiss();
//
//
//						}
//					});

//					dialog.show();


                }

            }

        }

    }


    public String confirmJSON(String acName, String legForm, String acType, String curCode, String prdCode, String token) {
        return "loanProdCode=" + acName +
                "&legalForm=" + legForm +
                "&accType=" + acType +
                "&currCode=" + curCode +
                "&prodCode=" + prdCode +
                "&authToken=" + token;


    }


    public String confirmResponse(String a1, String a2, String a3, String a4, String a5, String a6) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON(a1, a2, a3, a4, a5, a6);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "openAcc", json);

        System.out.println(response);

        return response;
    }


    public class confirmFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(AcctCreation.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
//				System.out.println(from+","+aliasAccNum+","+crAccName+","+amt+","+token);
                response = confirmResponse(accName, sendLegalForm, sendAccType, sendCurrCode, sendProdCode, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        //					creationTime = jsonObject.getString("creationTime");
                        //					lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        //					System.out.println(mb_userId);
                        System.out.println("mbAcc is + " + mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                new ResponseDialog(AcctCreation.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("21")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "Transaction unsuccessful", mb_response).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("20")) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "Transaction unsuccessful", mb_response).showDialog();

                } else if (mb_response.trim().equals("11")) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "Sorry", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("18")) {
                    progress.cancel();
                    new ResponseDialog(AcctCreation.this, "Transaction unsuccessful", mb_response).showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(AcctCreation.this, "Success", mbAcc).showDialog();

                }

            }

        }

    }


    // add items into spinner dynamically
	/*public void addItemsOnSpinner1() {

		aList = new ArrayList<HashMap<String,String>>();

		for(int a=0;a<accDetail.length;a++){
			StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

			if (a==0){
				loanProdCode = "Transfer From";
				loanProdCode = "";
				accAvailBal = "";
				accLedgerBal = "";
			}else{

			accNum = myAccToken.nextToken().trim();
			loanProdCode = myAccToken.nextToken().trim();
			accAvailBal = myAccToken.nextToken().trim();
			accLedgerBal = myAccToken.nextToken().trim();
			//			String d = myAccToken.nextToken().trim();
			System.out.println(accNum);
			System.out.println(loanProdCode);
			//			System.out.println(accAvailBal);
			//			System.out.println(accLedgerBal);

			HashMap<String, String> hm = new HashMap<String,String>();

			hm.put("accname", loanProdCode);
			hm.put("accnum",  accNum  );
			hm.put("bal", accAvailBal);
			hm.put("ledgerbal", "   " + accLedgerBal);
			//			hm.put("next", Integer.toString(next[0]) );

			aList.add(hm);


		}		


	}*/


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    //    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        menu.findItem(R.id.refresh).setVisible(false);
//        return true;
//    }
//
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);
                AcctCreation.this.finish();
                return true;
        }
//		return super.onOptionsItemSelected(item);
        return false;
    }

    public void onBackPressed() {
        //		moveTaskToBack(true);
        AcctCreation.this.finish();
    }

	/*@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		MainMenu menuObject = new MainMenu();
		menuObject.countDownTimer.cancel();
		menuObject.countDownTimer.start();
	}*/


}

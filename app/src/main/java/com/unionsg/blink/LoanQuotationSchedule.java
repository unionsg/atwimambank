package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.otherservices.GlobalCodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class LoanQuotationSchedule extends AppCompatActivity {

    ListView listView;

    String mbAccDetails = null;
    String[] accDetail = null;

    String mbAcc = null;
    String[] loanScheduleArray = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String repaymentDate = null;
    String interest = null;
    String installment = null;
    String principalRepayAmt = null;
    String interestRepayAmt = null;
    String totalRepaymentAmt = null;

    TextView productTV;
    TextView amtTV;
    TextView tenureTV;
    TextView interestTypeTV;
    TextView principalRepayFreqTV;
    TextView interestFreqTV;
    TextView intRatePerMonthOrAnnumTV;


    ProgressDialog progress;
    ProgressDialog progress2;
    ProgressDialog progress4;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    String remittanceAmt = null;
    String respCode = null;
    String resp = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;
    String usrId = null;

    String response = null;

    String tenureInMonths = null;
    String freqDesc = null;
    String intFreqDesc = null;
    String intDesc = null;
    String loanProd = null;
    String amount = null;


    String question = null;
    String loanSchedule = null;
    String testQuestions = "Select a test question~What is the name of your first boss?~What is the name of your favourite car?~What is the name of your pet?";

    Dialog dialog;
    String balFromFile = null;
    int flag = 0;

    String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    Pattern pattern;

    int[] colorList = new int[]{R.color.green_400, R.color.green_500, R.color.green_600, R.color.green_700, R.color.green_800,
            R.color.green_900, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue,
            R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_quotation_schedule);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras().getString("token").trim();
        loanSchedule = getIntent().getExtras().getString("loanSchedule").trim();
        interest = getIntent().getExtras().getString("interest").trim();

        tenureInMonths = getIntent().getExtras().getString("tenureInMonths").trim();
        freqDesc = getIntent().getExtras().getString("freqDesc").trim();
        intFreqDesc = getIntent().getExtras().getString("intFreqDesc").trim();
        intDesc = getIntent().getExtras().getString("intDesc").trim();
        loanProd = getIntent().getExtras().getString("loanProd").trim();
        amount = getIntent().getExtras().getString("amount").trim();

        listView = (ListView) findViewById(R.id.loan_schedule_list);
        productTV = (TextView) findViewById(R.id.prod);
        amtTV = (TextView) findViewById(R.id.amt);
        tenureTV = (TextView) findViewById(R.id.tenureMonths);
        interestTypeTV = (TextView) findViewById(R.id.int_type);
        principalRepayFreqTV = (TextView) findViewById(R.id.prin_freq);
        interestFreqTV = (TextView) findViewById(R.id.int_freq);
        intRatePerMonthOrAnnumTV = (TextView) findViewById(R.id.int_per_month_or_annum);

        amount = GlobalCodes.FormatToMoney(amount);

        tenureTV.setText(tenureInMonths);
        productTV.setText(loanProd);
        amtTV.setText(amount);
        interestTypeTV.setText(intDesc);
        principalRepayFreqTV.setText(freqDesc);
        interestFreqTV.setText(intFreqDesc);
        intRatePerMonthOrAnnumTV.setText(interest);

        populateSchedule(loanSchedule);

    }


    public void populateSchedule(String schedule) {

        loanScheduleArray = schedule.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(loanScheduleArray.length);

        for (int a = 0; a < loanScheduleArray.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(loanScheduleArray[a], "~");
            repaymentDate = myAccToken.nextToken().trim();
            principalRepayAmt = myAccToken.nextToken().trim();//principal repay amount
            interestRepayAmt = myAccToken.nextToken().trim();//interest repay amt
            totalRepaymentAmt = myAccToken.nextToken().trim();//total repayment amt

            principalRepayAmt = GlobalCodes.FormatToMoney(principalRepayAmt);
            interestRepayAmt = GlobalCodes.FormatToMoney(interestRepayAmt);
            totalRepaymentAmt = GlobalCodes.FormatToMoney(totalRepaymentAmt);

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("repaymentDate", repaymentDate);
            hm.put("principalRepayAmt", principalRepayAmt);
            hm.put("interestRepayAmt", interestRepayAmt);
            hm.put("totalRepaymentAmt", totalRepaymentAmt);
            hm.put("myColor", Integer.toString(colorList[3]));

            theList.add(hm);


        }

        String[] from = {"repaymentDate", "principalRepayAmt", "interestRepayAmt", "totalRepaymentAmt", "myColor"};
        int[] to = {R.id.repayment_date, R.id.principal_amt, R.id.interest_amt, R.id.total_amt, R.id.my_color};

        SimpleAdapter adapter = new SimpleAdapter(LoanQuotationSchedule.this, theList, R.layout.list_loanquotation_schedule, from, to);

        listView.setAdapter(adapter);


    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    LoanQuotationSchedule.this.finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                LoanQuotationSchedule.this.finish();  //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //		menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
//                flag = 1;
                LoanQuotationSchedule.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        flag = 1;
        LoanQuotationSchedule.this.finish();
    }


}

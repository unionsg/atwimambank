package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class TermsAndConditions extends AppCompatActivity {

	private Spinner spinner1, spinner2;
	private EditText amtEditText;
	private Button confirmButton;
	Button okButton;
//	private Button addAliasButton;
	String mbAccDetails = null;
	String[] accDetail = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	//	String accDetails = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String alias = null;
	String aliasAcc = null;

	String accNum = null;
	String accLedgerBal = null;
	String accAvailBal = null;
	String accName = null;

	String accountNum = null;
	String accountName = null;

	ProgressDialog progress;
	ProgressDialog progress2;
	int pos;
	String aliasAccNum = null;
	String aliasName = null;
	String transAmt = null;

	int[] next = new int[]{	R.drawable.more };
	List<HashMap<String,String>> aList;
	View view;

	String token = null;

	String accnumText;

	String response = null;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.terms_and_conditions);

		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();

		okButton = (Button) findViewById(R.id.ok_button);

		okButton.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {
				SharedPreferences preferences = getSharedPreferences("myprefs", MODE_PRIVATE);
				if (preferences.getBoolean("firstLogin", true)) {
//			setContentView(R.layout.main);
//			System.out.println("firstLogin " + firstLogin);
					SharedPreferences.Editor editor1 = preferences.edit();
					editor1.putBoolean("firstLogin", false);
					editor1.apply();
					Intent loginIntent = new Intent(TermsAndConditions.this, LoginActivity.class);
					finish();
					startActivity(loginIntent);


				}



			}
		});





	
	}
	
	
	
	public String acctsJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myacctsResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = acctsJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getacc", json);

		System.out.println(response);

		return response;
	}



	public class myAcctsFireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress2 = new ProgressDialog(TermsAndConditions.this);
			progress2.setCancelable(true);
			progress2.setTitle("Please wait");
			progress2.setMessage("Retrieving accounts...");
			progress2.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myacctsResponse(token);
//				System.out.println(response);
				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId"); 
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress2.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress2.cancel();
					new ResponseDialog(TermsAndConditions.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				}
				else if(mb_response.trim().equals("11") ){
					progress2.cancel();
					new ResponseDialog(TermsAndConditions.this, "Error !", "Unknown Error");
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress2.cancel();
					
					System.out.println(mbAcc);
					
					aliasAccDetails = mbAcc.split("`");

					List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

					System.out.println(aliasAccDetails.length);
						
						for(int a=0;a<aliasAccDetails.length-1;a++){
							StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 , 
							accNum = myAccToken.nextToken().trim();
							accName = myAccToken.nextToken().trim();
							accAvailBal = myAccToken.nextToken().trim();
							accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
							accLedgerBal = myAccToken.nextToken().trim();
							accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
							
							System.out.println(accNum);
							System.out.println(accName);
							
							HashMap<String, String> hm = new HashMap<String,String>();

							hm.put("accname", accName);
							hm.put("accnum",  accNum  );
							hm.put("bal", accAvailBal
									.replace("SLL","Le ")
									.replace("GHS","¢")
									.replace("USD","$")
									.replace("GBP","£")
									.replace("EUR","€")
									.replace("CNY","¥")
									.replace("JPY","¥"));
							hm.put("ledgerbal", "   " + accLedgerBal
									.replace("SLL","Le ")
									.replace("GHS","¢")
									.replace("USD","$")
									.replace("GBP","£")
									.replace("EUR","€")
									.replace("CNY","¥")
									.replace("JPY","¥"));
							
							theList.add(hm);

						}		
						
						String[] from = { "next","accname", "bal", "accnum", "ledgerbal" };
						int[] to = { R.id.flag,R.id.acc_name,R.id.avail_bal,R.id.acc_num, R.id.ledger_bal};

						SimpleAdapter adapter = new SimpleAdapter(TermsAndConditions.this, theList, R.layout.list_bills_from , from , to);

						spinner1.setAdapter(adapter);
					

					//				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
					//				loginIntent.putExtra("token", mb_token);
					//				loginIntent.putExtra("userName", mb_userName);
					//				loginIntent.putExtra("mb_acc_details", mb_acc_details); 
					//				startActivity(transDetailsIntent);
				}

			}
			new fireMan().execute();

		}

	}



	public String fundsTransJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = fundsTransJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getalias", json);

		System.out.println(response);

		return response;
	}



	public class fireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(TermsAndConditions.this);
			progress.setCancelable(false);
			progress.setTitle("Please wait");
			progress.setMessage("Retrieving beneficiaries...");
			progress.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myResponse(token);

				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress.cancel();
					new ResponseDialog(TermsAndConditions.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				}
				else if(mb_response.trim().equals("19") ){
					progress.cancel();
					new ResponseDialog(TermsAndConditions.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds");
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress.cancel();

					System.out.println(mbAcc);

					//					StringTokenizer accDetToken = new StringTokenizer(string)
					aliasAccDetails = mbAcc.split(",");

					List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

					System.out.println(aliasAccDetails.length);

					for(int a=0;a<aliasAccDetails.length-1;a++){
						StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

						System.out.println(accDetToken.countTokens());

						alias = accDetToken.nextToken().trim();
						aliasAcc = accDetToken.nextToken().trim();

						System.out.println(alias);
						System.out.println(aliasAcc);

						HashMap<String, String> hm = new HashMap<String,String>();

						hm.put("aliasname", alias);
						hm.put("aliasaccnum", aliasAcc /*"**********"+*/ /*.lastIndexOf(0, -3) */ );

						theList.add(hm);

					}

					String[] from = { "aliasname", "aliasaccnum" };

					// Ids of views in listview_layout
					int[] to = { R.id.alias_name,R.id.alias_acc_num};

					SimpleAdapter adapter2 = new SimpleAdapter(TermsAndConditions.this, theList, R.layout.list_to , from , to);

					spinner2.setAdapter(adapter2);


				}

			}


		}

	}











	// add items into spinner dynamically
	/*public void addItemsOnSpinner1() {

		aList = new ArrayList<HashMap<String,String>>();

		for(int a=0;a<accDetail.length;a++){
			StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

			if (a==0){
				loanProdCode = "Transfer From";
				loanProdCode = "";
				accAvailBal = "";
				accLedgerBal = "";
			}else{

			accNum = myAccToken.nextToken().trim();
			loanProdCode = myAccToken.nextToken().trim();
			accAvailBal = myAccToken.nextToken().trim();
			accLedgerBal = myAccToken.nextToken().trim();
			//			String d = myAccToken.nextToken().trim();
			System.out.println(accNum);
			System.out.println(loanProdCode);
			//			System.out.println(accAvailBal);
			//			System.out.println(accLedgerBal);

			HashMap<String, String> hm = new HashMap<String,String>();

			hm.put("accname", loanProdCode);
			hm.put("accnum",  accNum  );
			hm.put("bal", accAvailBal);
			hm.put("ledgerbal", "   " + accLedgerBal);
			//			hm.put("next", Integer.toString(next[0]) );

			aList.add(hm);


		}		


	}*/



	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
					//					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
					//					startActivity(loginIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findItem(R.id.refresh).setVisible(false);
//		return true;
//	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			//	         NavUtils.navigateUpFromSameTask(this);S
			TermsAndConditions.this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		//		moveTaskToBack(true); 
		TermsAndConditions.this.finish();
	}



}

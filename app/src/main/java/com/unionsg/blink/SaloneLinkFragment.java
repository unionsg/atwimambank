package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.liststuff.BeneficiaryAdapter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class SaloneLinkFragment extends Fragment {

	private Context context;

	public SaloneLinkFragment() {
		// Required empty public constructor
	}


	ListView list;
	TextView firstLetterTV;
	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	String beneCode = null;
	String mbAcc = null;
	String mbAcc2 = null;
	String[] aliasAccDetails = null;
	String beneName = null;
	String beneAcc = null;

	String beneFromFile = null;
	ProgressDialog progress;
	String token = null;
	String internalBeneficiary = null;
	String externalBeneficiary = null;
	String response = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		token = getActivity().getIntent().getExtras().getString("token").trim();



//		externalBeneficiary = getActivity().getIntent().getExtras().getString("externalBeneficiary").trim();

//		mbAcc = getActivity().getIntent().getExtras().getString("mbAcc").trim();
		System.out.println("token: " + token);
		System.out.println("1111111111");



	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

//		token = getActivity().getIntent().getExtras().getString("token").trim();
//		System.out.println("token: " + token);

//		Bundle bundle = this.getArguments();
//		String myValue = bundle.getString("key");
//		System.out.println(myValue);

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.same_bank_beneficiaries, container, false);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

		list = (ListView) view.findViewById(R.id.bene_list);
//		firstLetterTV = (TextView) view.findViewById(R.id.first_letter);


		beneFromFile = readFromFile(getActivity());
		if (!beneFromFile.equals("")) {
			System.out.println("not empty");
			internalBeneficiary = beneFromFile;
			populateList1();
//			username.setEnabled(true);

		} else {
//			internalBeneficiary = getActivity().getIntent().getExtras().getString("internalBeneficiary").trim();
			System.out.println("empty");
			Toast.makeText(getActivity(), "Sorry you do not have any same bank beneficiaries. Tap on the plus button to create one",Toast.LENGTH_LONG).show();
//			new ResponseDialog(getActivity(), "Sorry", "You do not have any beneficiaries" ).showDialog();
		}

//		new fireMan().execute();

//		TextView transferFrom = (TextView) view.findViewById(R.id.from);
//		TextView transferTo = (TextView) view.findViewById(R.id.to);
//		TextView amount = (TextView) view.findViewById(R.id.amount);
////		TextView narration = (TextView) view.findViewById(R.id.trans_details);
//
//		transferFrom.setTypeface(tf, Typeface.BOLD);
//		transferTo.setTypeface(tf, Typeface.BOLD);
//		amount.setTypeface(tf, Typeface.BOLD);
//		narration.setTypeface(tf, Typeface.BOLD);

		return view;
	}









	public void populateList1(){
        ImageView textDrawable;
		System.out.println("internalBeneficiary: " + internalBeneficiary);

		aliasAccDetails = internalBeneficiary.split("`");

		List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

		System.out.println(aliasAccDetails.length);

		for(int a=0;a<aliasAccDetails.length-1;a++){
			StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

			System.out.println(accDetToken.countTokens());

//			beneCode = accDetToken.nextToken().trim();
			beneName = accDetToken.nextToken().trim();
			beneAcc = accDetToken.nextToken().trim();

			System.out.println(beneName);
			System.out.println(beneAcc);

			HashMap<String, String> hm = new HashMap<String,String>();

//			hm.put("benecode", beneCode);
			hm.put("benename", beneName);
			hm.put("beneacc", beneAcc );
			hm.put("firstletter", beneName.substring(0,1).toUpperCase());

			theList.add(hm);

		}

		String[] from = { /*"benecode",*/ "benename", "beneacc", "firstletter" };

		// Ids of views in listview_layout
		int[] to = { /*R.id.bene_code,*/ R.id.service_type, R.id.bene_acc, R.id.first_letter};

		BeneficiaryAdapter adapter2 = new BeneficiaryAdapter(getActivity(), theList, R.layout.list_beneficiary , from , to);

		list.setAdapter(adapter2);

//		Random r = new Random();
//		int red=r.nextInt(255 - 0 + 1)+0;
//		int green=r.nextInt(255 - 0 + 1)+0;
//		int blue=r.nextInt(255 - 0 + 1)+0;
//
//		GradientDrawable draw = new GradientDrawable();
//		draw.setShape(GradientDrawable.OVAL);
//		draw.setColor(Color.rgb(red,green,blue));
//		firstLetterTV.setBackground(draw);


//		Drawable circle = ResourcesCompat.getDrawable(getResources(), R.drawable.circle_c4, null);
//		circle.setColorFilter(getRandomMaterialColor(), PorterDuff.Mode.SRC_ATOP);
//		firstLetterTV.setBackground(circle);

	}


    private int getRandomMaterialColor() {
        int returnColor = Color.GRAY;
        int arrayId = getActivity().getResources().getIdentifier("list_item_colors", "array", getActivity().getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getActivity().getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }





	private String readFromFile(Context context) {

		String ret = "";

		try {
			InputStream inputStream = context.openFileInput("intB.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("Login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("Login activity", "Cannot read file: " + e.toString());
		}

		return ret;
	}





//	public String fundsTransJSON(String authToken){
//		return "authToken="+authToken;
//	}
//
//
//	public String myResponse (String a1) throws IOException {
//		SendRequest sendReqObj = new SendRequest();
//
//		String json = fundsTransJSON(a1);
//		String response = sendReqObj.postReq(GlobalCodes.myUrl+"viewbeneficiaries", json);
//
//		System.out.println(response);
//
//		return response;
//	}
//
//
//
//	public class fireMan extends AsyncTask<Void , Void, Void>{
//
//		private Exception e = null;
//
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//			progress = new ProgressDialog(getActivity());
//			progress.setCancelable(false);
//			progress.setTitle("Please wait");
//			progress.setMessage("Retrieving beneficiaries...");
//			progress.show();
//		}
//
//		@Override
//		protected Void doInBackground(Void ... params) {
//
//
//
//			try {
//				response = myResponse(token);
//
//				try {
//					JSONObject jsonObject = new JSONObject(response);
//					id = jsonObject.getString("id");
//					creationTime = jsonObject.getString("creationTime");
//					lastModificationDate = jsonObject.getString("lastModificationDate");
//					mb_token = jsonObject.getString("tokenId");
//					mb_response = jsonObject.getString("mb_response");
//					mbAcc = jsonObject.getString("mb_accDetails");
//					mbAcc2 = jsonObject.getString("mb_transDetails");
//
//
//					System.out.println(mbAcc);
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			return null;
//		}
//		@Override
//		protected void onPostExecute(Void result) {
//			// TODO Auto-generated method stub
//
//			if(e != null){
//				progress.cancel();
//				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
//			} else {
//				super.onPostExecute(result);
//
//				if (mb_response.trim().equals("12")){
//					System.out.println("Received response code > "+mb_response);
//
//					progress.cancel();
//					new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again");
////					finish();
//				}
//				else if(mb_response.trim().equals("19") ){
//					progress.cancel();
//					new ResponseDialog(getActivity(), "No Beneficiary Found", "Please create a beneficiary to transfer funds");
//
//				}
//				else if(mb_response.trim().equals("00") ){
//					progress.cancel();
//
//					System.out.println(mbAcc);
//
//					populateList1();
//
//
//				}
//
//			}
//
//
//		}
//
//	}




	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
//					getActivity().finish();
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}





}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.otherservices.GlobalCodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class LoanSchedule extends AppCompatActivity {

    ListView listView;
    TextView currentBalTextView;
    TextView loanTypeTextView;
    TextView arrearsTextView;
    TextView facilityNumTextView;


    String mbAccDetails = null;
    String[] accDetail = null;

    //	String accDetails = null;
    String mbAcc = null;
    String successMsg = null;
    String[] loanScheduleArray = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String facilityNum = null;
    String repaymentDate = null;
    String interestAmt = null;
    String installment = null;
    String principalAmt = null;
    String interestPaid = null;
    String principalPaid = null;


    ProgressDialog progress;
    ProgressDialog progress2;
    ProgressDialog progress4;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    String remittanceAmt = null;
    String respCode = null;
    String resp = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;
    String usrId = null;

    String response = null;

    String loanType = null;
    String currentBal = null;
    String arrears = null;
//    String facilityNum = null;

    String question = null;
    String loanSchedule = null;
    String testQuestions = "Select a test question~What is the name of your first boss?~What is the name of your favourite car?~What is the name of your pet?";

    Dialog dialog;
    String balFromFile = null;
    int flag = 0;

    String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    Pattern pattern;

    int[] colorList = new int[]{R.color.green_400, R.color.green_500, R.color.green_600, R.color.green_700, R.color.green_800,
            R.color.green_900, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue,
            R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loan_schedule);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loanSchedule = getIntent().getExtras().getString("schedule").trim();
        facilityNum = getIntent().getExtras().getString("facilityNum").trim();
        loanType = getIntent().getExtras().getString("loanType").trim();
        currentBal = getIntent().getExtras().getString("currentBal").trim();
        arrears = getIntent().getExtras().getString("arrears").trim();
//        flag = getIntent().getExtras().getInt("flag");

        facilityNumTextView = (TextView) findViewById(R.id.facility_no);
        arrearsTextView = (TextView) findViewById(R.id.arrears_amt);
        loanTypeTextView = (TextView) findViewById(R.id.type_of_loan);
        currentBalTextView = (TextView) findViewById(R.id.currentbal);

//        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

        listView = (ListView) findViewById(R.id.loan_schedule_list);

        facilityNumTextView.setText(facilityNum);
        arrearsTextView.setText(arrears);
        loanTypeTextView.setText(loanType);
        currentBalTextView.setText(currentBal);


        populateSchedule(loanSchedule);

    }



    public void populateSchedule(String schedule) {

        loanScheduleArray = schedule.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(loanScheduleArray.length);

//        Random r = new Random();
//        int red=r.nextInt(255 + 1);
//        int green=r.nextInt(255 + 1);
//        int blue=r.nextInt(255 + 1);
//
//        GradientDrawable draw = new GradientDrawable();
//        draw.setColor(Color.rgb(red,green,blue));
//        ImageView imageView = (ImageView) view.findViewById(R.id.my_color);

//
// Color.argb(255, rand.nextInt(256), rand.nextInt(256), rand.nextInt(256) );

        for (int a = 0; a < loanScheduleArray.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(loanScheduleArray[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            repaymentDate = myAccToken.nextToken().trim();
            interestAmt = myAccToken.nextToken().trim();
            principalAmt = myAccToken.nextToken().trim();
            installment = myAccToken.nextToken().trim();
            interestPaid = myAccToken.nextToken().trim();
            principalPaid = myAccToken.nextToken().trim();

            interestAmt = GlobalCodes.FormatToMoney(interestAmt);
            principalAmt = GlobalCodes.FormatToMoney(principalAmt);
            installment = GlobalCodes.FormatToMoney(installment);
            interestPaid = GlobalCodes.FormatToMoney(interestPaid);
            principalPaid = GlobalCodes.FormatToMoney(principalPaid);

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("repaymentDate", repaymentDate);
            hm.put("interestAmt", interestAmt);
            hm.put("principalAmt", principalAmt);
            hm.put("installment", installment);
            hm.put("interestPaid", interestPaid);
            hm.put("principalPaid", principalPaid);
            hm.put("myColor", Integer.toString(colorList[3]));

            theList.add(hm);


        }

        String[] from = { "interestAmt", "principalAmt", "installment", "interestPaid", "principalPaid", "repaymentDate", "myColor"};
        int[] to = {R.id.int_amt, R.id.prin_amt, R.id.inst_amt, R.id.int_paid, R.id.prin_paid, R.id.repay_date, R.id.my_color};

        SimpleAdapter adapter = new SimpleAdapter(LoanSchedule.this, theList, R.layout.list_loanschedule, from, to);

        listView.setAdapter(adapter);


    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    LoanSchedule.this.finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                LoanSchedule.this.finish();  //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //		menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
//                flag = 1;
                LoanSchedule.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        flag = 1;
        LoanSchedule.this.finish();
    }


}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class QRPayments extends AppCompatActivity {

    private Spinner spinner1;
    private EditText amtEditText;
    private TextView merchantAccTextView;
    private TextView merchantAccNameTV;
    private EditText narrationEditText;
    Button scanButton;
    private Button confirmButton;
    private EditText pinEditText;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;
    String beneTel = null;
    String beneCode = null;

    String accNum = null;
    String accLedgerBal = null;
    String accType = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;
    String curr = null;
    String pin = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    int pos;
    static String merchantDetails = "";
    String merchantAccNum = null;
    //    String merchant
    String aliasName = null;
    String transAmt = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String accnumText;

    String response = null;
    String balFromFile = null;
    String beneFromFile = null;
    String narration = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String questions = null;
    String codeFlag = null;
    String merchantAccName = null;

    Dialog dialog;
    TextView titleTextView;

    static String staticAccDetails = null;
    static int staticAccNumPosition = 10000;
    static String staticAmount = null;
    static String staticNarration = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_payments);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        scanButton = (Button) findViewById(R.id.scan_button);
        amtEditText = (EditText) findViewById(R.id.amtedittext);
        narrationEditText = (EditText) findViewById(R.id.narrationedittext);
        merchantAccTextView = (TextView) findViewById(R.id.merchant_acc_textview);
        merchantAccNameTV = (TextView) findViewById(R.id.merchant_accname);


//        if (!merchantAccNum.length() == 0) {
//
//        } else {
//            merchantAccTextView.setText(merchantAccNum);
//        }

        confirmButton = (Button) findViewById(R.id.confirm_button);

        balFromFile = readBalancesFromFile(QRPayments.this);

        if (!balFromFile.equals("")) {
            System.out.println("balances not empty");
            mbAcc = balFromFile;
            System.out.println("MBACC balances: " + mbAcc);
            populateAccounts();

        } else {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myAcctsFireMan().execute();
            } else {
                showAlertDialog(QRPayments.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        }


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
                accAvailBal = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();

                staticAccNumPosition = position;

                if (accAvailBal.contains("Le")) {
                    curr = "Le";
                } else if (accAvailBal.contains("¢")) {
                    curr = "¢";
                } else if (accAvailBal.contains("$")) {
                    curr = "$";
                } else if (accAvailBal.contains("£")) {
                    curr = "£";
                } else if (accAvailBal.contains("€")) {
                    curr = "€";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });


        scanButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                merchantAccNum = merchantAccTextView.getText().toString();

                staticAmount = amtEditText.getText().toString();
                staticNarration = narrationEditText.getText().toString();

                Intent scanIntent = new Intent(QRPayments.this, SendQRCash.class);
                scanIntent.putExtra("token", token);
                scanIntent.putExtra("userName", usrName);
                scanIntent.putExtra("userId", usrId);
                scanIntent.putExtra("mb_acc_details", custType);
                scanIntent.putExtra("balances", balances);
                scanIntent.putExtra("creationTime", creationTime);
                scanIntent.putExtra("beneInternal", beneInternal);
                scanIntent.putExtra("beneExternal", beneExternal);
                scanIntent.putExtra("questions", questions);
                scanIntent.putExtra("codeFlag", codeFlag);
                startActivity(scanIntent);
                finish();


            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                transAmt = amtEditText.getText().toString();
                narration = narrationEditText.getText().toString();
               merchantAccNum = merchantAccTextView.getText().toString();

                if (transAmt.trim().isEmpty()) {
                    amtEditText.setError("Enter transfer amount");
                } else if (merchantAccNum.trim().isEmpty()) {
                    Toast.makeText(QRPayments.this, "Tap on the button above to scan the QR code", Toast.LENGTH_LONG).show();
                } else {
                    if (narration.trim().isEmpty()) {
                        narration = "QR PAYMENT";
                    }

                    System.out.println("from: " + accountNum);
                    System.out.println("to: " + merchantAccNum);
                    System.out.println("amt: " + transAmt);
                    System.out.println("narr: " + narration);

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new getNamefireMan().execute();
                    } else {
                        showAlertDialog(QRPayments.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }

//                    Intent confirmTransIntent = new Intent(QRPayments.this, ConfirmTrans.class);
//                    System.out.println(accountNum);
//                    System.out.println(merchantAccNum);
//                    System.out.println(curr);
//                    confirmTransIntent.putExtra("curr", curr);
//                    confirmTransIntent.putExtra("dbAcc", accountNum);
//                    confirmTransIntent.putExtra("crAcc", merchantAccNum);
//                    confirmTransIntent.putExtra("crAccName", aliasName);
//                    confirmTransIntent.putExtra("amt", transAmt);
//                    confirmTransIntent.putExtra("narration", narration.toUpperCase());
//                    confirmTransIntent.putExtra("token", token);
//                    confirmTransIntent.putExtra("userName", usrName);
//                    confirmTransIntent.putExtra("userId", usrId);
//                    confirmTransIntent.putExtra("mb_acc_details", custType);
//                    confirmTransIntent.putExtra("balances", balances);
//                    confirmTransIntent.putExtra("creationTime", creationTime);
//                    confirmTransIntent.putExtra("beneInternal", beneInternal);
//                    confirmTransIntent.putExtra("beneExternal", beneExternal);
//                    confirmTransIntent.putExtra("questions", questions);
//                    confirmTransIntent.putExtra("codeFlag", codeFlag);
//                    startActivity(confirmTransIntent);
                }

            }
        });


        //call in onCreate
        //  setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(QRPayments.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(QRPayments.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }


    private void buildDialog(int animationSource) {

        dialog = new Dialog(QRPayments.this);
        dialog.setContentView(R.layout.confirm_qr);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.from);
        TextView toTV = (TextView) dialog.findViewById(R.id.to);
        TextView amtTV = (TextView) dialog.findViewById(R.id.amt);
        TextView narrationTV = (TextView) dialog.findViewById(R.id.narration);

        pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);

        accountNumTV.setText(accountNum);
        toTV.setText(merchantAccName);
        amtTV.setText(transAmt);
        narrationTV.setText(narration);
//        toTV.setText(to);


//        questionTV.setText(question);
//        answerTV.setText(answer);
//
        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pin = pinEditText.getText().toString();
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
                if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new confirmFireMan().execute();
                    } else {
                        showAlertDialog(QRPayments.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                    dialog.dismiss();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    private String readBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public void populateAccounts() {

        System.out.println(mbAcc);

        aliasAccDetails = mbAcc.split(" ` ");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
            accType = myAccToken.nextToken().trim();

            System.out.println(accNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

//							hm.put("accname", loanProdCode);
//			if (loanProdCode.length() > 20) {
////							accNameDisp = loanProdCode.substring(0, 18) + "...";
//				hm.put("accname", loanProdCode.substring(0, 17) + "...");
//			} else {
//				hm.put("accname", loanProdCode);
//			}
            hm.put("accnum", accNum);
            hm.put("accname", accName);
            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            theList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        SimpleAdapter adapter = new SimpleAdapter(QRPayments.this, theList, R.layout.list_bills_from, from, to);

        spinner1.setAdapter(adapter);

        if (merchantDetails.length() == 0) {

        } else {
            spinner1.setSelection(staticAccNumPosition);
            StringTokenizer merchantToken = new StringTokenizer(merchantDetails, "~");
            merchantAccNum = merchantToken.nextToken();
            merchantAccName = merchantToken.nextToken();
            merchantAccTextView.setText(merchantAccNum);
            merchantAccNameTV.setText(merchantAccName);
            amtEditText.setText(staticAmount);
            narrationEditText.setText(staticNarration);
        }

    }


    public String confirmsendJSON(String accnumber) {
        return "accnum=" + accnumber;


    }


    public String confirmAcctResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmsendJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getAcctName", json);

        System.out.println(response);

        return response;
    }


    public class getNamefireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(QRPayments.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
//                System.out.println(beneAccNum);
                response = confirmAcctResponse(merchantAccNum);
                if (response.isEmpty()) {
                    response = "66";
                }

//				try {
//					JSONObject jsonObject = new JSONObject(response);
//					id = jsonObject.getString("id");
//					//					creationTime = jsonObject.getString("creationTime");
//					//					lastModificationDate = jsonObject.getString("lastModificationDate");
//					mb_token = jsonObject.getString("tokenId");
//					mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
//					mbAcc = jsonObject.getString("accDetails");
//
//					//					System.out.println(mb_userId);
////					System.out.println(mbAcc);
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (response.trim().equals("11")) {

                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Sorry", "The merchant's account number is wrong").showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "No response", "Please try again").showDialog();
                } else {
                    progress.cancel();
                    merchantAccName = response;
                    System.out.println("beneficiaryName: " + merchantAccName);
                    buildDialog(R.style.DialogAnimation);


                }

            }

        }

    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(QRPayments.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(QRPayments.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(QRPayments.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(QRPayments.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(QRPayments.this, "Error !", "Unknown Error").showDialog();
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    aliasAccDetails = mbAcc.split(" ` ");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 , 
                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
                        accType = myAccToken.nextToken().trim();

                        System.out.println(accNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

//							hm.put("accname", loanProdCode);
//							if (loanProdCode.length() > 20) {
////							accNameDisp = loanProdCode.substring(0, 18) + "...";
//								hm.put("accname", loanProdCode.substring(0, 17) + "...");
//							} else {
//								hm.put("accname", loanProdCode);
//							}
                        hm.put("accnum", accNum);
                        hm.put("accname", accName);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(QRPayments.this, theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);


                }


            }

        }

    }


    private void writeBalancesToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bal.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String confirmJSON(String accNumDr, String accNumCr, String crAccName, String amt, String authToken, String narr, String myPin) throws UnsupportedEncodingException {
        return "accNumDr=" + accNumDr +
                "&accNumCr=" + accNumCr +
                "&aliasName=" + crAccName +
                "&amt=" + amt +
                "&authToken=" + authToken +
                "&narration=" + narration +
                "&sec=" + myPin +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");


    }


    public String confirmResponse(String a1, String a2, String a3, String a4, String a5, String a6, String a7) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON(a1, a2, a3, a4, a5, a6, a7);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "qrfundtrans", json);

        System.out.println(response);

        return response;
    }


    public class confirmFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(QRPayments.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
//                System.out.println(from + "," + aliasAccNum + "," + crAccName + "," + transAmt + "," + token);
                response = confirmResponse(accountNum, merchantAccNum, merchantAccName, transAmt, token, narration, pin);

                try {
                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        //					creationTime = jsonObject.getString("creationTime");
                        //					lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");

                    } else {
                        mb_response = "66";
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("21")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Transaction unsuccessful", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("20")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Insufficient funds", mbAcc).showDialog();

                } else if (mb_response.trim().equals("18")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Transaction unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("99")) {
                    progress.cancel();
                    new WrongPinResponseDialog(QRPayments.this, "Transaction unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("00")) {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new updateMyAcctsFireMan().execute();
                    } else {
                        showAlertDialog(QRPayments.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }

                }

            }

        }

    }


    public String updateAcctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String updateMyacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = updateAcctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class updateMyAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = updateMyacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        balances = jsonObject.getString("mb_accDetails");

                        System.out.println(balances);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Success", mbAcc).showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Success", mbAcc).showDialog();
                } else if (mb_response.trim().equals("30")) {
                    progress.cancel();
                    new ResponseDialog(QRPayments.this, "Unsuccessful", mbAcc).showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    System.out.println(balances);
                    writeBalancesToFile(balances, QRPayments.this);
                    new updateResponseDialog(QRPayments.this, "Success", mbAcc).showDialog();

                }

            }

        }

    }


    public class updateResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public updateResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public updateResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    staticAccNumPosition = 10000;
                    merchantAccNum = "";
                    staticAmount = "";
                    staticNarration = "";
                    Intent intent = new Intent(QRPayments.this, MainMenu.class);
                    intent.putExtra("token", token);
                    intent.putExtra("userName", usrName);
                    intent.putExtra("userId", usrId);
                    intent.putExtra("mb_acc_details", custType);
                    intent.putExtra("balances", balances);
                    intent.putExtra("creationTime", creationTime);
                    intent.putExtra("beneInternal", beneInternal);
                    intent.putExtra("beneExternal", beneExternal);
                    intent.putExtra("questions", questions);
                    intent.putExtra("codeFlag", codeFlag);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    QRPayments.this.finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    staticAccNumPosition = 10000;
                    merchantAccNum = "";
                    staticAmount = "";
                    staticNarration = "";

                    Intent intent = new Intent(QRPayments.this, MainMenu.class);
                    intent.putExtra("token", token);
                    intent.putExtra("userName", usrName);
                    intent.putExtra("userId", usrId);
                    intent.putExtra("mb_acc_details", custType);
                    intent.putExtra("balances", balances);
                    intent.putExtra("creationTime", creationTime);
                    intent.putExtra("beneInternal", beneInternal);
                    intent.putExtra("beneExternal", beneExternal);
                    intent.putExtra("questions", questions);
                    intent.putExtra("codeFlag", codeFlag);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    QRPayments.this.finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}


    public class WrongPinResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public WrongPinResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public WrongPinResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    pinEditText.setText("");
                    pinEditText.setError("You entered a wrong PIN code");
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    //	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                staticAccNumPosition = 10000;
                merchantAccNum = "";
                staticAmount = "";
                staticNarration = "";
                merchantDetails = "";
                Intent intent = new Intent(QRPayments.this, MainMenu.class);
                intent.putExtra("token", token);
                intent.putExtra("userName", usrName);
                intent.putExtra("userId", usrId);
                intent.putExtra("mb_acc_details", custType);
                intent.putExtra("balances", balances);
                intent.putExtra("creationTime", creationTime);
                intent.putExtra("beneInternal", beneInternal);
                intent.putExtra("beneExternal", beneExternal);
                intent.putExtra("questions", questions);
                intent.putExtra("codeFlag", codeFlag);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                QRPayments.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        staticAccNumPosition = 10000;
        merchantAccNum = "";
        staticAmount = "";
        staticNarration = "";
        merchantDetails = "";
        Intent intent = new Intent(QRPayments.this, MainMenu.class);
        intent.putExtra("token", token);
        intent.putExtra("userName", usrName);
        intent.putExtra("userId", usrId);
        intent.putExtra("mb_acc_details", custType);
        intent.putExtra("balances", balances);
        intent.putExtra("creationTime", creationTime);
        intent.putExtra("beneInternal", beneInternal);
        intent.putExtra("beneExternal", beneExternal);
        intent.putExtra("questions", questions);
        intent.putExtra("codeFlag", codeFlag);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        QRPayments.this.finish();
    }


}

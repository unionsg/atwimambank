package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class MobileMoneyToAccount extends AppCompatActivity {

    private Spinner  spinner2;
    private EditText amtEditText;
    private EditText transferToEditText;
    private EditText phoneEditText;
    private EditText narrationEditText;
    private EditText voucherEditText;
    private Button confirmButton;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = "";
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;
    String beneTel = null;
    String beneCode = null;
    String voucher = null;

    String accNum = null;
    String accLedgerBal = null;
    String accType = null;
    String accAvailBal = null;
    String accName = null;
    String phone = null;

    String accountNum = null;
    String accountName = null;
    String curr = null;
    String network = null;
    String chosenNetwork = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String accnumText;

    String response = null;
    String balFromFile = null;
    String beneFromFile = null;
    String narration = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String codeFlag = null;
    String questions = null;

    JSONArray jsonArray;
//    Handler handler;
//    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobilemoney_to_acc);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

        transferToEditText = (EditText) findViewById(R.id.transfer_to_edittext);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        amtEditText = (EditText) findViewById(R.id.amtedittext);
        phoneEditText = (EditText) findViewById(R.id.sender_phone_edittext);
        voucherEditText = (EditText) findViewById(R.id.voucher_edittext);
        narrationEditText = (EditText) findViewById(R.id.narrationedittext);

        confirmButton = (Button) findViewById(R.id.confirm_button);

        phoneEditText.setEnabled(false);

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new getCustPhoneFireMan().execute();
        } else {
            showAlertDialog(MobileMoneyToAccount.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }





        spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                chosenNetwork = ((TextView) view.findViewById(R.id.telco_name)).getText().toString().trim();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                accountNum = transferToEditText.getText().toString();
                transAmt = amtEditText.getText().toString();
                narration = narrationEditText.getText().toString();
                phone = phoneEditText.getText().toString();
                voucher = voucherEditText.getText().toString();

                System.out.println("debit wallet: " + phone);

                if (phone.trim().isEmpty()) {
                    phoneEditText.setError("Enter phone number to send from");
                } else if (transAmt.trim().isEmpty()) {
                    amtEditText.setError("Enter transfer amount");
                } else if (accountNum.trim().isEmpty()) {
                    transferToEditText.setError("Enter account to send to");
                } else {
                    if (narration.trim().isEmpty()) {
                        narration = "MB WALLET TO ACCOUNT";
                    }

                    Intent confirmTransIntent = new Intent(MobileMoneyToAccount.this, ConfirmMobileMoneyToAcc.class);
                    System.out.println(accountNum);
                    System.out.println(curr);
                    confirmTransIntent.putExtra("curr", curr);
                    confirmTransIntent.putExtra("crAcc", accountNum);
                    confirmTransIntent.putExtra("phone", phone);
                    confirmTransIntent.putExtra("amt", transAmt);
                    confirmTransIntent.putExtra("voucher", voucher);
                    confirmTransIntent.putExtra("network", chosenNetwork);
                    confirmTransIntent.putExtra("narration", narration.toUpperCase());
                    confirmTransIntent.putExtra("token", token);
                    confirmTransIntent.putExtra("userName", usrName);
                    confirmTransIntent.putExtra("userId", usrId);
                    confirmTransIntent.putExtra("mb_acc_details", custType);
                    confirmTransIntent.putExtra("balances", balances);
                    confirmTransIntent.putExtra("creationTime", creationTime);
                    confirmTransIntent.putExtra("beneInternal", beneInternal);
                    confirmTransIntent.putExtra("beneExternal", beneExternal);
                    confirmTransIntent.putExtra("questions", questions);
                    confirmTransIntent.putExtra("codeFlag", codeFlag);
                    startActivity(confirmTransIntent);
                }

            }
        });

        //call in onCreate
        //   setAppIdleTimeout();
    }





    public String getCustPhoneJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myCustPhoneResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getCustPhoneJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getcustphone", json);

        System.out.println(response);

        return response;
    }


    public class getCustPhoneFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
			progress = new ProgressDialog(MobileMoneyToAccount.this);
			progress.setCancelable(false);
			progress.setTitle("Please wait");
			progress.setMessage("Retrieving phone number...");
			progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myCustPhoneResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
//                progress.cancel();
                new ResponseDialog(MobileMoneyToAccount.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
//                    progress.cancel();
                    new ResponseDialog(MobileMoneyToAccount.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("66")) {
//                    progress.cancel();
                    new ResponseDialog(MobileMoneyToAccount.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

//                    progress.cancel();
                    new ResponseDialog(MobileMoneyToAccount.this, "You do not have a registered phone number. Please visit the nearest branch to register your phone number to access this service", "Please login again").showDialog();

                } else if (mb_response.trim().equals("00")) {
//                    progress.cancel();

                    phoneEditText.setText(mbAcc);
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new getNetworksFireMan().execute();
                    } else {
                        showAlertDialog(MobileMoneyToAccount.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                }

            }

//			new searchBanksFireMan().execute();

        }

    }











    public String getNetworksJSON( /*String accNum, String msisdn, String amount, String network, String narration*/) {
//        return "accountNum=" + accNum +
//                "&msisdn=" + msisdn +
//                "&amount=" + amount +
//                "&network=" + network +
//                "&narration=" + narration;
        return "";
    }


    public String getNetworksResponse() throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getNetworksJSON();
        String response = sendReqObj.getReq(GlobalCodes.myUrl + "getAllNetworks");

        System.out.println(response);

        return response;
    }

    String[] networkArray = {""};
    List<HashMap<String, String>> networkList = new ArrayList<HashMap<String, String>>();

    public class getNetworksFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//            progress = new ProgressDialog(MobileMoneyToAccount.this);
//            progress.setCancelable(false);
//            progress.setTitle("Please wait");
//            progress.setMessage("Retrieving networks...");
//            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = getNetworksResponse();//(accountNum, accountNum, accountNum, accountNum, accountNum);

                try {
                    if (!response.isEmpty()) {
                        jsonArray = new JSONArray(response);
                        System.out.println("jsonArray length: " + jsonArray.length());
                        mb_response = "00";

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            System.out.println("11111111111111111111111");

            if (e != null) {
                progress.cancel();
                showAlertDialog(MobileMoneyToAccount.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                System.out.println("222222222222222222222222222222");
                super.onPostExecute(result);

                if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(MobileMoneyToAccount.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")){
                    System.out.println("333333333333333333333333333333333333");
                    progress.cancel();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            System.out.println("network: " + jsonArray.getString(i));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                        System.out.println("i: " + i);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        try {
                            hm.put("name", jsonArray.getString(i));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                        networkList.add(hm);


                    }

//                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

//                    populateNetworks(theList);

                    String[] from = {"name"};
                    // Ids of views in listview_layout
                    int[] to = {R.id.telco_name};

                    SimpleAdapter adapter2 = new SimpleAdapter(MobileMoneyToAccount.this, networkList, R.layout.list_telcos, from, to);

                    spinner2.setAdapter(adapter2);

                }


            }


        }

    }






    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                MobileMoneyToAccount.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        MobileMoneyToAccount.this.finish();
    }


}

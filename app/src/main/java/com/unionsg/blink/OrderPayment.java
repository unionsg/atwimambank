package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;

public class OrderPayment extends Fragment {

    public OrderPayment() {
        // Required empty public constructor
    }

    private Spinner spinner1, spinner2;
    private EditText amtEditText;
    private Button confirmButton;
    private Button getFeesButton;
    //    EditText nameNameEditText;
    EditText emailEditText;
    EditText confirmEmailEditText;
    EditText amountEditText;
    EditText phoneEditText;


    String name = null;
    String email = null;
    String email2 = null;
    String amount = null;
    String phone = null;

    private TextView feesTextView;
    String mbAccDetails = null;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String successMsg = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;
    String curr = null;
    String pin = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    ProgressDialog progress4;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    String remittanceAmt = null;
    String respCode = null;
    String resp = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;
    String usrId = null;

    String response = null;

    String question = null;
    String chosenQuestion = null;
    String testQuestions = "Select a test question~What is the name of your first boss?~What is the name of your favourite car?~What is the name of your pet?";

    Dialog dialog;
    String balFromFile = null;

    String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    Pattern pattern;

//    Handler handler;
//    Runnable r;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        token = getActivity().getIntent().getExtras().getString("token").trim();
        usrId = getActivity().getIntent().getExtras().getString("usrId").trim();

        pattern = Pattern.compile(regex);

        System.out.println("token: " + token);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//		token = getActivity().getIntent().getExtras().getString("token").trim();
//		System.out.println("token: " + token);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.order_payment, container, false);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

        emailEditText = (EditText) view.findViewById(R.id.email_edittext);
        confirmEmailEditText = (EditText) view.findViewById(R.id.confirm_email_edittext);
        amountEditText = (EditText) view.findViewById(R.id.amount_edittext);
        phoneEditText = (EditText) view.findViewById(R.id.phone_edittext);

        emailEditText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });


        confirmEmailEditText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

        spinner1 = (Spinner) view.findViewById(R.id.spinner1);

        confirmButton = (Button) view.findViewById(R.id.proceed_button);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new myAcctsFireMan().execute();
        } else {

            showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection", false);

        }


//        populateSpinner1();


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();

//                accountName = ((TextView) view.findViewById(R.id.acc_name)).getText().toString().trim();
                curr = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();

                if (accAvailBal.contains("Le")) {
                    curr = "Le";
                } else if (accAvailBal.contains("¢")) {
                    curr = "¢";
                } else if (accAvailBal.contains("$")) {
                    curr = "$";
                } else if (accAvailBal.contains("£")) {
                    curr = "£";
                } else if (accAvailBal.contains("€")) {
                    curr = "€";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                name = nameNameEditText.getText().toString();
                email = emailEditText.getText().toString();
                email2 = confirmEmailEditText.getText().toString();
                amount = amountEditText.getText().toString();
                phone = phoneEditText.getText().toString();


                if (email.trim().isEmpty()) {
                    emailEditText.setError("Enter recipient's email address");
                } else if (email2.trim().isEmpty()) {
                    confirmEmailEditText.setError("Confirm recipient's email address");
                } else if (!checkEmail(email)) {
                    emailEditText.setError("Enter a valid email address");
                } else if (!email.trim().equals(email2.trim())) {
                    confirmEmailEditText.setError("Email address does not match first one");
                } else if (amount.trim().isEmpty()) {
                    amountEditText.setError("Enter amount");
                } else if (phone.trim().isEmpty()) {
                    phoneEditText.setError("Enter recipient's phone number");
                } else {
//                    Intent confirmTransIntent = new Intent(SaloneLinkExtra.this, ConfirmSaloneLink.class);
//                    System.out.println(name);
                    System.out.println(email);
                    System.out.println(email2);
                    System.out.println(amount);
                    System.out.println(phone);
                    System.out.println(token);


                    buildDialog(R.style.DialogAnimation);


                }
            }
        });


        return view;
    }


    public boolean checkEmail(String email) {
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    private void buildDialog(int animationSource) {

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.confirm_order_payment);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
        TextView emailTV = (TextView) dialog.findViewById(R.id.email);
        TextView senderPhoneTV = (TextView) dialog.findViewById(R.id.phone_number);
        TextView amtTV = (TextView) dialog.findViewById(R.id.amt);

        final EditText pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);

        accountNumTV.setText(accountNum);
//        senderNameTV.setText(name);
        emailTV.setText(email);
        senderPhoneTV.setText(phone);
        amtTV.setText("Le" + GlobalCodes.FormatToMoney(amount));

//
        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pin = pinEditText.getText().toString();
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
                if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {

                    ConnectionDetector cd = new ConnectionDetector(getActivity());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(getActivity(), "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "accounts", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(getActivity());
            progress2.setCancelable(false);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);

                try {

                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                new ResponseDialog(getActivity(), "Error!", "No response from server. Please check your Internet connection and try again").showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error!", "Connection timed out. Please check your Internet connection and try again").showDialog();

                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
                    getActivity().finish();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error !", "Unknown Error").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

//                    staticAccDetails = mbAcc;

                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        accountNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accountName = accName;
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
                        //			String d = myAccToken.nextToken().trim();
                        System.out.println(accountNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

//                        if (loanProdCode.length() > 20) {
////							accNameDisp = loanProdCode.substring(0, 18) + "...";
//                            hm.put("accname", loanProdCode.substring(0, 17) + "...");
//                        } else {
//                            hm.put("accname", loanProdCode);
//                        }
                        hm.put("accname", accName);
                        hm.put("accnum", accountNum);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(getActivity(), theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);

                }

            }

        }

    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String blinkPayJSON(String userId, String emailAddress, String accnum, String acctDesc, String phoneNumber, String amt, String authToken, String myPin) throws UnsupportedEncodingException {

        return "username=" + acctDesc +
                "&bene_email=" + emailAddress +
                "&customer_acc=" + accnum +
                "&account_desc=" + acctDesc +
                "&bene_tel=" + phoneNumber +
                "&oamount=" + amt +
                "&authToken=" + authToken +
                "&sec=" + myPin+
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");

    }


    public String myResponse(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = blinkPayJSON(a1, a2, a3, a4, a5, a6, a7, a8);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "orderPayment", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending details...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(usrId, email, accountNum, accountName, phone, amount, token, pin);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        successMsg = jsonObject.getString("mb_accDetails");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");

                        System.out.println(mb_response);
                        System.out.println(successMsg);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub


            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);
                System.out.println(mb_response);
                System.out.println(successMsg);

                if (mb_response.trim().equals("21")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "Transaction unsuccessful", successMsg).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("20")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "Insufficient funds", successMsg).showDialog();

                } else if (mb_response.trim().equals("18")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "Transaction unsuccessful", successMsg).showDialog();

                } else if (mb_response.trim().equals("99")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "Transaction unsuccessful", successMsg).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
//					progress.cancel();
                    new ResponseDialog(getActivity(), "Successful", successMsg).showDialog();


                }

            }


        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    getActivity().finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().finish();  //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


}

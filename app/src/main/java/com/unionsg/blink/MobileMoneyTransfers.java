package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.otherservices.GlobalCodes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MobileMoneyTransfers extends AppCompatActivity {

    int[] arrow = new int[]{R.drawable.more};
    int[] alias_pic = new int[]{R.drawable.ic_action_alias};
    int[] next_pic = new int[]{R.drawable.more};

    Dialog dialog;
    TextView titleTextView;
    TextView transferRate;
    TextView noteRate;

    TextView sameBank;
    TextView otherBank;

    String oldPassword = null;
    String newPassword1 = null;
    String newPassword2 = null;
    String[] menus = {"Account to Mobile Wallet", "Mobile Wallet to Account"};

    String rateType = null;
    String beneficiaryType = null;
    String chosenRateType = null;
    String chosenBeneficiaryType = null;
    List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
    ListView list;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_transDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    //	String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    ProgressDialog progress;
    String response = null;
    String title = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String codeFlag = null;
    String questions = null;

    int[] pics = new int[]{
            R.drawable.ic_bank_to_mobile1,
            R.drawable.ic_mobile_to_bank

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.mobilemoney);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

        System.out.println(menus.length);

        for (int a = 0; a < menus.length; a++) {

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("menuitem", menus[a]);
            hm.put("pix", Integer.toString(arrow[0]));
            hm.put("icons", Integer.toString(pics[a]));

            aList.add(hm);

        }

        String[] from = {"menuitem", "pix", "icons"};
        int[] to = {R.id.menu_item, R.id.flag, R.id.icon};

        SimpleAdapter adpt = new SimpleAdapter(this, aList, R.layout.list_requests, from, to);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adpt);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                String dataToCompare = ((TextView) v.findViewById(R.id.menu_item)).getText().toString().trim();

                switch (dataToCompare) {
                    case ("Account to Mobile Wallet"): {
                        Intent intent = new Intent(MobileMoneyTransfers.this, AccountToMobileMoney.class);
                        intent.putExtra("token", token);
                        intent.putExtra("userName", usrName);
                        intent.putExtra("userId", usrId);
                        intent.putExtra("mb_acc_details", custType);
                        intent.putExtra("balances", balances);
                        intent.putExtra("creationTime", creationTime);
                        intent.putExtra("beneInternal", beneInternal);
                        intent.putExtra("beneExternal", beneExternal);
                        intent.putExtra("questions", questions);
                        intent.putExtra("codeFlag", codeFlag);
                        intent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                        startActivity(intent);
                        break;
                    }
                    case ("Mobile Wallet to Account"): {
                        Intent intent = new Intent(MobileMoneyTransfers.this, MobileMoneyToAccount.class);
                        intent.putExtra("token", token);
                        intent.putExtra("userName", usrName);
                        intent.putExtra("userId", usrId);
                        intent.putExtra("mb_acc_details", custType);
                        intent.putExtra("balances", balances);
                        intent.putExtra("creationTime", creationTime);
                        intent.putExtra("beneInternal", beneInternal);
                        intent.putExtra("beneExternal", beneExternal);
                        intent.putExtra("questions", questions);
                        intent.putExtra("codeFlag", codeFlag);
                        intent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                        startActivity(intent);
                        break;
                    }


                }
            }


        });

        //call in onCreate
        //  setAppIdleTimeout();
    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();
                    //					Intent confirmTransIntent = new Intent(ConfirmTrans.this,MainMenu.class);
                    //					startActivity(confirmTransIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }




    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //		menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                MobileMoneyTransfers.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        MobileMoneyTransfers.this.finish();
    }

}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class EditExtBeneficiary extends AppCompatActivity {

    String beneAccNum = null, beneName = null, beneTel = null;
    String beneBank = null;
    String beneCode = null;
    String beneAcc = null;
    String response = null;
    String token = null;
    String accountNum = null;
    String accountName = null;
    Dialog dialog;
    String bankName = null;
    ProgressDialog progress4;

    String id = null;
    String creationTime = null;
    String lastModificationDate = null;
    String mb_token = null;
    String mb_userId = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mbAcc = null;
    String mbAcc2 = null;
    String[] aliasAccDetails = null;

    EditText beneAccEditText, beneNameEditText, beneTelEditText;
    EditText edtUserAccName;
    TextView aliasNameTextView;
    ProgressDialog progress;
    Spinner spinBankData;
    List<HashMap<String, String>> myList;
    Button addButton;
    String aloo = null;
//    Handler handler;
//    Runnable r;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_ext_beneficiary);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras().getString("token").trim();
        beneAcc = getIntent().getExtras().getString("beneacc").trim();
        beneName = getIntent().getExtras().getString("benename").trim();
        beneTel = getIntent().getExtras().getString("benetel").trim();
        beneCode = getIntent().getExtras().getString("benecode").trim();
        System.out.println("CreateExtBeneficiary: " + token);

        beneAccEditText = (EditText) findViewById(R.id.bene_acc);
        beneNameEditText = (EditText) findViewById(R.id.bene_name);
        beneTelEditText = (EditText) findViewById(R.id.bene_tel);

        beneAccEditText.setText(beneAcc);
        beneNameEditText.setText(beneName);
        beneTelEditText.setText(beneTel);

        addButton = (Button) findViewById(R.id.add_button);


        addButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                beneAccNum = beneAccEditText.getText().toString();
                beneName = beneNameEditText.getText().toString();
                beneTel = beneTelEditText.getText().toString();

                if (beneAccNum.trim().isEmpty()) {
                    beneAccEditText.setError("Enter beneficiary's account number");
                } else if (beneName.trim().isEmpty()) {
                    beneNameEditText.setError("Enter beneficiary's name");
                } else if (beneTel.trim().isEmpty()) {
                    beneTelEditText.setError("Enter beneficiary's mobile number");
                } else {
                    beneBank = beneAccNum.substring(0, 3);
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new searchBanksFireMan().execute();
                    } else {
                        showAlertDialog(EditExtBeneficiary.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                    System.out.println("beneBank: " + beneBank);
                }
            }
        });


        //call in onCreate
        //   setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(EditExtBeneficiary.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(EditExtBeneficiary.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//      //  Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }

    private void buildDialog(int animationSource) {

        dialog = new Dialog(EditExtBeneficiary.this);
        dialog.setContentView(R.layout.confirm_beneficiary_ext);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
        TextView bankNameTV = (TextView) dialog.findViewById(R.id.bankname);
        TextView beneNameTV = (TextView) dialog.findViewById(R.id.bene_name);
        TextView benePhoneTV = (TextView) dialog.findViewById(R.id.bene_phone);

        accountNumTV.setText(beneAccNum);
        bankNameTV.setText(bankName);
        beneNameTV.setText(beneName);
        benePhoneTV.setText(beneTel);


        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new fireMan().execute();
                dialog.dismiss();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    public String bankListJSON(String bankCode, String authToken) {
        return "bankCode=" + bankCode +
                "&authToken=" + authToken;
    }


    public String myBanksListResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = bankListJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getBankName", json);

        System.out.println(response);

        return response;

    }


    public class searchBanksFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress4 = new ProgressDialog(EditExtBeneficiary.this);
            progress4.setCancelable(false);
            progress4.setTitle("Please wait");
            progress4.setMessage("Retrieving bank name...");
            progress4.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myBanksListResponse(beneBank, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress4.cancel();
                new ResponseDialog(EditExtBeneficiary.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress4.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("66")) {
                    progress4.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);

                    progress4.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Not Found", mbAcc).showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("12")) {
                    progress4.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, mbAcc, "Please enter a valid account number").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress4.cancel();
                    bankName = mbAcc;

                    buildDialog(R.style.DialogAnimation);

                }

            }

        }

    }


    public String confirmJSON(String beneacc, String benename, String benetel, String benebank, String benecode, String authToken) {
        return "beneacc=" + beneacc +
                "&benename=" + benename +
                "&benetel=" + benetel +
                "&benebank=" + benebank +
                "&benecode=" + benecode +
                "&authToken=" + authToken;


    }


    public String confirmResponse(String a1, String a2, String a3, String a4, String a5, String a6) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON(a1, a2, a3, a4, a5, a6);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "editExtbeneficiary", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(EditExtBeneficiary.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                System.out.println(beneAccNum + "," + beneName + "," + beneTel + "," + beneBank + "," + token);
                System.out.println("Confirm ext benef: " + token);
                response = confirmResponse(beneAccNum, beneName, beneTel, beneBank, beneCode, token);

                try {
                    if (!response.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    //					creationTime = jsonObject.getString("creationTime");
                    //					lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_token = jsonObject.getString("tokenId");
                    System.out.println("mb_token: " + token);
                    mb_response = jsonObject.getString("mb_response");
                    mb_userId = jsonObject.getString("mb_userId");
                    mbAcc = jsonObject.getString("mb_accDetails");

                    //					System.out.println(mb_userId);
//					System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("EMPTY RESPONSE");
                    e.printStackTrace();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("21")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("20")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("22")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Beneficiary creation unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("23")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Beneficiary creation unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("00")) {
//					progress.cancel();
                    //					System.out.println(mbAcc);
//					new ResponseDialog(CreateExtBeneficiary.this, "Success", mbAcc).showDialog();

                    System.out.println("GET BENEFs");
                    new getBeneficiariesFireMan().execute();

                }

            }

        }

    }


    public String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getExtBeneficiaries", json);

        System.out.println(response);

        return response;
    }


    public class getBeneficiariesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//			progress = new ProgressDialog(CreateExtBeneficiary.this);
//			progress.setCancelable(false);
//			progress.setTitle("Please wait");
//			progress.setMessage("Retrieving beneficiaries...");
//			progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                new ResponseDialog(EditExtBeneficiary.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "No Data Found For This User", "Please login again").showDialog();
                    finish();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(EditExtBeneficiary.this, "No Beneficiary Found", "").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);
                    writeExternalToFile(mbAcc, EditExtBeneficiary.this);
                    new ResponseDialog(EditExtBeneficiary.this, "Success", "You have successfully edited your beneficiary's details").showDialog();


                }

            }

//			new searchBanksFireMan().execute();

        }

    }


    private void writeInternalToFile(String intBene, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("intB.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(intBene);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private void writeExternalToFile(String extBene, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("extB.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(extBene);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is
        // present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                EditExtBeneficiary.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //	   moveTaskToBack(true);
        EditExtBeneficiary.this.finish();
    }


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();
//					Intent confirmTransIntent = new Intent(ConfirmTrans.this,MainMenu.class);
//					startActivity(confirmTransIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


}





package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class ConfirmMobileMoneyToAcc extends AppCompatActivity {

    private EditText pinEditText;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String responseMessage = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;

    ProgressDialog progress;
    int pos;
    String phone = null;
    String transAmt = null;
    String crAccName = null;
	String vodaVoucher = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String accnumText;

    String response = null;

    String from = null;
    String to = null;
    String amt = null;
//    String curr = null;
    String pin = null;
    String narration = null;
    String network = null;
    String voucher = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String questions = null;
    String codeFlag = null;

//    Handler handler;
//    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_mobilemoney_to_acc);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        accountNum = getIntent().getExtras().getString("crAcc").trim();
        phone = getIntent().getExtras().getString("phone").trim();
        transAmt = getIntent().getExtras().getString("amt").trim();
        network = getIntent().getExtras().getString("network").trim();
        narration = getIntent().getExtras().getString("narration").trim();
//        curr = getIntent().getExtras().getString("curr").trim();
        voucher = getIntent().getExtras().getString("voucher").trim();

        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();
        System.out.println(accountNum);
        System.out.println(phone);


        token = getIntent().getExtras().getString("token").trim();

        TextView titleTextview = (TextView) findViewById(R.id.confirm);
        Typeface regularText = Typeface.createFromAsset(getAssets(), "HelveticaRegular.otf");
        titleTextview.setTypeface(regularText);

        Button backButton = (Button) findViewById(R.id.back_button);
        Button confirmButton = (Button) findViewById(R.id.confirm_button);

        final TextView fromTextview = (TextView) findViewById(R.id.from);
        final TextView toTextview = (TextView) findViewById(R.id.to);
        final TextView amtTextview = (TextView) findViewById(R.id.amt);
        final TextView narrationTextview = (TextView) findViewById(R.id.narration);
        final TextView networkTextview = (TextView) findViewById(R.id.network);
        final TextView voucherTextview = (TextView) findViewById(R.id.voucher);
        pinEditText = (EditText) findViewById(R.id.pinedittext);


        fromTextview.setText(phone);
        toTextview.setText(accountNum);
        networkTextview.setText(network);
        narrationTextview.setText(narration);
        voucherTextview.setText(voucher);
        amtTextview.setText("GHS" + GlobalCodes.FormatToMoney(transAmt));

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                finish();

            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                from = fromTextview.getText().toString();
                System.out.println("amount is " + transAmt);
                pin = pinEditText.getText().toString();

                if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4-digit secret PIN");
                } else {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(ConfirmMobileMoneyToAcc.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                }

            }
        });

        //call in onCreate
       // setAppIdleTimeout();
    }




    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }





    public String getJSON(String accNum, String msisdn, String amount, String network, String narration, String voucher, String authToken, String sec) throws UnsupportedEncodingException {
        return "accountNum=" + accNum +
                "&msisdn=" + msisdn +
                "&amount=" + amount +
                "&network=" + network +
                "&narration=" + narration +
                "&voucher=" + voucher +
                "&authToken=" + authToken +
                "&sec=" + sec +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");
    }


    public String myResponse(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getJSON(a1, a2, a3, a4, a5, a6, a7, a8);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "mobileMoneyToAcc", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(ConfirmMobileMoneyToAcc.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(accountNum, phone, transAmt, network, narration, voucher,token, pin );

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
//                    id = jsonObject.getString("id");
//                    creationTime = jsonObject.getString("creationTime");
//                    lastModificationDate = jsonObject.getString("lastModificationDate");
//                    mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("responseCode");
                        responseMessage = jsonObject.getString("response");

                        System.out.println(mb_response);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                showAlertDialog(ConfirmMobileMoneyToAcc.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(ConfirmMobileMoneyToAcc.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(ConfirmMobileMoneyToAcc.this, "Sorry...", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(ConfirmMobileMoneyToAcc.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("55")) {
                    progress.cancel();
                    new UnsuccessfulResponseDialog(ConfirmMobileMoneyToAcc.this, "", responseMessage).showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    new ResponseDialog(ConfirmMobileMoneyToAcc.this, "", responseMessage).showDialog();
//                    populateQuestions();

                }

            }


        }

    }










        private void writeBalancesToFile(String data, Context context) {
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bal.txt", Context.MODE_PRIVATE));
                outputStreamWriter.write(data);
                outputStreamWriter.close();
            } catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }


        public String acctsJSON(String authToken) {
            return "authToken=" + authToken;
        }


        public String myacctsResponse(String a1) throws IOException {
            SendRequest sendReqObj = new SendRequest();

            String json = acctsJSON(a1);
            String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

            System.out.println(response);

            return response;
        }


        public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

            private Exception e = null;

            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    response = myacctsResponse(token);
//				System.out.println(response);
                    try {
                        if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        balances = jsonObject.getString("mb_accDetails");

                        System.out.println(balances);
                        } else {
                            mb_response = "66";
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                // TODO Auto-generated method stub

                if (e != null) {
                    progress.cancel();
                } else {
                    super.onPostExecute(result);

                    if (mb_response.trim().equals("12")) {
                        progress.cancel();
                        new ResponseDialog(ConfirmMobileMoneyToAcc.this, "Success", mbAcc).showDialog();
                    } else if (mb_response.trim().equals("11")) {
                        progress.cancel();
                        new ResponseDialog(ConfirmMobileMoneyToAcc.this, "Success", mbAcc).showDialog();
                    } else if (mb_response.trim().equals("66")) {
                        progress.cancel();
                        new ResponseDialog(ConfirmMobileMoneyToAcc.this, "No response", "Please try again").showDialog();
                    } else if (mb_response.trim().equals("00")) {
                        progress.cancel();
                        System.out.println(balances);
                        writeBalancesToFile(balances, ConfirmMobileMoneyToAcc.this);
                        new ResponseDialog(ConfirmMobileMoneyToAcc.this, "Success", mbAcc).showDialog();

                    }

                }

            }

        }


        public class ResponseDialog extends Builder {

            // Store the passed context
            private Context context;

            // Can be used as a regular builder
            public ResponseDialog(Context context) {
                super(context);
            }

            // Or as a custom builder, which we want
            public ResponseDialog(Context context, String title, String message) {
                super(context);
                // Store context
                this.context = context;
                // Set up everything
                setMessage(message);
                setTitle(title);
                setCancelable(false);
                //			setPadding(0, 5, 0, 5);
                setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        Intent intent = new Intent(ConfirmMobileMoneyToAcc.this, MainMenu.class);
                        intent.putExtra("token", token);
                        intent.putExtra("userName", usrName);
                        intent.putExtra("userId", usrId);
                        intent.putExtra("mb_acc_details", custType);
                        intent.putExtra("balances", balances);
                        intent.putExtra("creationTime", creationTime);
                        intent.putExtra("beneInternal", beneInternal);
                        intent.putExtra("beneExternal", beneExternal);
                        intent.putExtra("questions", questions);
                        intent.putExtra("codeFlag", codeFlag);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
//					Intent confirmTransIntent = new Intent(ConfirmTrans.this,MainMenu.class);
//					startActivity(confirmTransIntent);
                    }
                });
            }

            public void showDialog() {
                // Create and show
                AlertDialog alert = create();
                alert.show();
                // Center align everything
                ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
                ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
            }


        }



    public class UnsuccessfulResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public UnsuccessfulResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public UnsuccessfulResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }




        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                // Respond to the action bar's Up/Home button
                case android.R.id.home:
                    ConfirmMobileMoneyToAcc.this.finish();
                    return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public void onBackPressed() {
            //	   moveTaskToBack(true);
            ConfirmMobileMoneyToAcc.this.finish();
        }

    }

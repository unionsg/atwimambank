package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class SameBankTransferFragment extends Fragment {

    private Context context;

    public SameBankTransferFragment() {
        // Required empty public constructor
    }


    private Spinner spinner1, spinner2;
    private EditText amtEditText;
    private Button confirmButton;
    String mbAccDetails = null;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;
    String curr = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;
    SimpleAdapter adapter;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;

    String response = null;
    String balFromFile = null;
    String beneFromFile = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        token = getActivity().getIntent().getExtras().getString("token").trim();


//		externalBeneficiary = getActivity().getIntent().getExtras().getString("externalBeneficiary").trim();

//		mbAcc = getActivity().getIntent().getExtras().getString("mbAcc").trim();
        System.out.println("token: " + token);
        System.out.println("1111111111");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.funds_transfer, container, false);

//		final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.funds_transfer, null);
//		initView(inflater, root);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

        System.out.println("Same Bank Transfer Fragment");

        spinner1 = (Spinner) view.findViewById(R.id.spinner1);
        spinner2 = (Spinner) view.findViewById(R.id.spinner2);
        amtEditText = (EditText) view.findViewById(R.id.amtedittext);

        confirmButton = (Button) view.findViewById(R.id.confirm_button);

        balFromFile = readBalancesFromFile(getActivity());
        beneFromFile = readBenefFromFile(getActivity());

        if (!balFromFile.equals("")) {
            System.out.println("balances not empty");
            mbAcc = balFromFile;
            System.out.println("MBACC balances: " + mbAcc);
            populateAccounts();
            if (!beneFromFile.equals("")) {
                System.out.println("beneficiaries not empty");
                mbAcc = beneFromFile;
                System.out.println("MBACC benefs: " + mbAcc);
                populateBeneficiaries();
            } else {
                ConnectionDetector cd = new ConnectionDetector(getActivity());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new fireMan().execute();
                } else {
                    showAlertDialog(getActivity(), "No Internet Connection",
                            "You don't have internet connection.", false);
                }
                //new fireMan().execute();
            }
        } else {
            ConnectionDetector cd = new ConnectionDetector(getActivity());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myAcctsFireMan().execute();
            } else {
                showAlertDialog(getActivity(), "No Internet Connection",
                        "You don't have internet connection.", false);
            }
            new myAcctsFireMan().execute();
        }


        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
//				String account = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
                System.out.println("SELECTED: " + parent.getItemAtPosition(position).toString());
//				accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
//				accountNum = ((TextView) view.findViewById(R.id.avail_bal)).getItemAtPosition(position).toString();
                accAvailBal = ((TextView) getActivity().findViewById(R.id.avail_bal)).getText().toString().trim();

                if (accAvailBal.contains("Le")) {
                    curr = "Le";
                } else if (accAvailBal.contains("¢")) {
                    curr = "¢";
                } else if (accAvailBal.contains("$")) {
                    curr = "$";
                } else if (accAvailBal.contains("£")) {
                    curr = "£";
                } else if (accAvailBal.contains("€")) {
                    curr = "€";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });

//		spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//									   int position, long id) {
//
//				//				accountNum = parent.getItemAtPosition(pos).toString();
//				merchantAccNum = ((TextView) view.findViewById(R.id.alias_acc_num)).getText().toString().trim();
//				aliasName = ((TextView) view.findViewById(R.id.alias_name)).getText().toString().trim();
//			}
//
//			public void onNothingSelected(AdapterView<?> parent) {
//				// Do nothing
//
//
//			}
//		});

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //				accountNum = spinner1.getSelectedItem().toString();
                //				accountNum = ((TextView) v.findViewById(R.id.acc_num)).getText().toString().trim();

                //				accountNum = spinner1.getChildAt(1).toString();
                transAmt = amtEditText.getText().toString();
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
                if (transAmt.trim().isEmpty()) {
                    amtEditText.setError("Enter transfer amount");
                } else {
                    amtEditText.setText("");
//					editte
                    Intent confirmTransIntent = new Intent(getActivity(), ConfirmTrans.class);
                    System.out.println(accountNum);
                    System.out.println(aliasAccNum);
                    System.out.println(curr);
                    confirmTransIntent.putExtra("curr", curr);
                    confirmTransIntent.putExtra("dbAcc", accountNum);
                    confirmTransIntent.putExtra("crAcc", aliasAccNum);
                    confirmTransIntent.putExtra("crAccName", aliasName);
                    confirmTransIntent.putExtra("amt", transAmt);
                    confirmTransIntent.putExtra("token", token);
                    startActivity(confirmTransIntent);
                }
                //				accountNum, aliasName, transAmt, token
                //				new confimFireMan().execute();
            }
        });

        return view;
    }


    private String readBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    private String readBenefFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("intB.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public void populateAccounts() {

        System.out.println(mbAcc);

        aliasAccDetails = mbAcc.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);

            System.out.println(accNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

//							hm.put("accname", accName);
            if (accName.length() > 20) {
//							accNameDisp = accName.substring(0, 18) + "...";
                hm.put("accname", accName.substring(0, 17) + "...");
            } else {
                hm.put("accname", accName);
            }
            hm.put("accnum", accNum);

            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            theList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        adapter = new SimpleAdapter(getActivity(), theList, R.layout.list_bills_from, from, to);

        spinner1.setAdapter(adapter);

    }


    public void populateBeneficiaries() {

        System.out.println(mbAcc);

        //					StringTokenizer accDetToken = new StringTokenizer(string)
        aliasAccDetails = mbAcc.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length - 1; a++) {

            StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

//			while(accDetToken.hasMoreTokens()) {

            System.out.println(accDetToken.countTokens());


            alias = accDetToken.nextToken().trim();
            aliasAcc = accDetToken.nextToken().trim();

            System.out.println(alias);
            System.out.println(aliasAcc);

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("aliasname", alias);
            hm.put("aliasaccnum", aliasAcc /*"**********"+*/ /*.lastIndexOf(0, -3) */);

            theList.add(hm);
//			}

        }

        String[] from = {"aliasname", "aliasaccnum"};

        // Ids of views in listview_layout
        int[] to = {R.id.alias_name, R.id.alias_acc_num};

        SimpleAdapter adapter2 = new SimpleAdapter(getActivity(), theList, R.layout.list_to, from, to);

        spinner2.setAdapter(adapter2);

    }


    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("intB.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


//	public String fundsTransJSON(String authToken){
//		return "authToken="+authToken;
//	}
//
//
//	public String myResponse (String a1) throws IOException {
//		SendRequest sendReqObj = new SendRequest();
//
//		String json = fundsTransJSON(a1);
//		String response = sendReqObj.postReq(GlobalCodes.myUrl+"viewbeneficiaries", json);
//
//		System.out.println(response);
//
//		return response;
//	}
//
//
//
//	public class fireMan extends AsyncTask<Void , Void, Void>{
//
//		private Exception e = null;
//
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//			progress = new ProgressDialog(getActivity());
//			progress.setCancelable(false);
//			progress.setTitle("Please wait");
//			progress.setMessage("Retrieving beneficiaries...");
//			progress.show();
//		}
//
//		@Override
//		protected Void doInBackground(Void ... params) {
//
//
//
//			try {
//				response = myResponse(token);
//
//				try {
//					JSONObject jsonObject = new JSONObject(response);
//					id = jsonObject.getString("id");
//					creationTime = jsonObject.getString("creationTime");
//					lastModificationDate = jsonObject.getString("lastModificationDate");
//					mb_token = jsonObject.getString("tokenId");
//					mb_response = jsonObject.getString("mb_response");
//					mbAcc = jsonObject.getString("mb_accDetails");
//					mbAcc2 = jsonObject.getString("mb_transDetails");
//
//
//					System.out.println(mbAcc);
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			return null;
//		}
//		@Override
//		protected void onPostExecute(Void result) {
//			// TODO Auto-generated method stub
//
//			if(e != null){
//				progress.cancel();
//				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
//			} else {
//				super.onPostExecute(result);
//
//				if (mb_response.trim().equals("12")){
//					System.out.println("Received response code > "+mb_response);
//
//					progress.cancel();
//					new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again");
////					finish();
//				}
//				else if(mb_response.trim().equals("19") ){
//					progress.cancel();
//					new ResponseDialog(getActivity(), "No Beneficiary Found", "Please create a beneficiary to transfer funds");
//
//				}
//				else if(mb_response.trim().equals("00") ){
//					progress.cancel();
//
//					System.out.println(mbAcc);
//
//					populateList1();
//
//
//				}
//
//			}
//
//
//		}
//
//	}


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(getActivity());
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
                    mbAcc = jsonObject.getString("mb_accDetails");

                    System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    getActivity().finish();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error !", "Unknown Error");
                    confirmButton.setEnabled(false);
                }else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);

                        System.out.println(accNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

//							hm.put("accname", accName);
                        if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
                            hm.put("accname", accName.substring(0, 17) + "...");
                        } else {
                            hm.put("accname", accName);
                        }
                        hm.put("accnum", accNum);

                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(getActivity(), theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);


                    //				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
                    //				loginIntent.putExtra("token", mb_token);
                    //				loginIntent.putExtra("userName", mb_userName);
                    //				loginIntent.putExtra("mb_acc_details", mb_acc_details);
                    //				startActivity(transDetailsIntent);
                }

            }
            ConnectionDetector cd = new ConnectionDetector(getActivity());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new fireMan().execute();
            } else {
                showAlertDialog(getActivity(), "No Internet Connection",
                        "You don't have internet connection.", false);
            }
         //   new fireMan().execute();

        }

    }


    public String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getalias", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving beneficiaries...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    getActivity().finish();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "Please create a beneficiary to transfer funds");
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);

                    //					StringTokenizer accDetToken = new StringTokenizer(string)
                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

                        System.out.println(accDetToken.countTokens());

                        alias = accDetToken.nextToken().trim();
                        aliasAcc = accDetToken.nextToken().trim();

                        System.out.println(alias);
                        System.out.println(aliasAcc);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("aliasname", alias);
                        hm.put("aliasaccnum", aliasAcc /*"**********"+*/ /*.lastIndexOf(0, -3) */);

                        theList.add(hm);

                    }

                    String[] from = {"aliasname", "aliasaccnum"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.alias_name, R.id.alias_acc_num};

                    SimpleAdapter adapter2 = new SimpleAdapter(getActivity(), theList, R.layout.list_to, from, to);

                    spinner2.setAdapter(adapter2);


                }

            }


        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public void onBackPressed() {
        //		moveTaskToBack(true);
        getActivity().finish();
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d("start", "The onStart() event");
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }

        //INSERT CUSTOM CODE HERE
    }

    /**
     * Called when another activity is taking focus.
     */
    @Override
    public void onPause() {
        super.onPause();
        Log.d("pause", "The onPause() event");

        getFragmentManager().saveFragmentInstanceState(this);

//		FragmentManager manager = getActivity().getSupportFragmentManager();
//		FragmentTransaction ft = manager.beginTransaction();
//		Fragment newFragment = this;
//		this.onDestroy();
//		ft.remove(this);
//		ft.replace(newFragment.getId(),newFragment);
//		//container is the ViewGroup of current fragment
//		ft.addToBackStack(null);
//		ft.commit();
    }

    /**
     * Called when the activity is no longer visible.
     */
    @Override
    public void onStop() {
        super.onStop();
        Log.d("stop", "The onStop() event");
    }

    /**
     * Called just before the activity is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("destroy", "The onDestroy() event");
    }


//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//		final View view = getView();
//		if (view != null) {
//			initView(getActivity().getLayoutInflater(), (ViewGroup) view.findViewById(R.id.container));
//		}
//	}
//
//	private void initView(final LayoutInflater inflater, final ViewGroup parent) {
//		parent.removeAllViews();
//		final View subRoot = inflater.inflate(R.layout.funds_transfer, null);
//		parent.addView(subRoot);
//		//do all the stuff
//
//
//
////		spinner1 = (Spinner) view.findViewById(R.id.spinner1);
////		spinner2 = (Spinner) view.findViewById(R.id.spinner2);
////		amtEditText = (EditText) view.findViewById(R.id.amtedittext);
////
////		confirmButton = (Button)view.findViewById(R.id.confirm_button);
////
////		balFromFile = readBalancesFromFile(getActivity());
////		beneFromFile = readBenefFromFile(getActivity());
////
////		if (!balFromFile.equals("")) {
////			System.out.println("balances not empty");
////			mbAcc = balFromFile;
////			System.out.println("MBACC balances: " + mbAcc);
////			populateAccounts();
////			if (!beneFromFile.equals("")){
////				System.out.println("beneficiaries not empty");
////				mbAcc = beneFromFile;
////				System.out.println("MBACC benefs: " + mbAcc);
////				populateBeneficiaries();
////			} else {
////				new fireMan().execute();
////			}
////		} else {
////			new myAcctsFireMan().execute();
////		}
////
////
////
////		spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
////			@Override
////			public void onItemSelected(AdapterView<?> parent, View view,
////									   int position, long id) {
////
////				//				accountNum = parent.getItemAtPosition(pos).toString();
////				accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
////				accAvailBal = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();
////
////				if (accAvailBal.contains("Le")){
////					curr = "Le";
////				}else if (accAvailBal.contains("¢")){
////					curr = "¢";
////				}else if (accAvailBal.contains("$")){
////					curr = "$";
////				}else if (accAvailBal.contains("£")){
////					curr = "£";
////				}else if (accAvailBal.contains("€")){
////					curr = "€";
////				}else if (accAvailBal.contains("¥")){
////					curr = "¥";
////				}else if (accAvailBal.contains("¥")){
////					curr = "¥";
////				}
////
////			}
////
////			public void onNothingSelected(AdapterView<?> parent) {
////				// Do nothing
////
////
////			}
////		});
////
//	}


}

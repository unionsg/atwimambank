package com.unionsg.blink;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Help extends AppCompatActivity {
	
	int[] arrow = new int[]{	R.drawable.more };
	int[] alias_pic = new int[]{	R.drawable.ic_action_alias };
	int[] next_pic = new int[] {R.drawable.more};
	String token = null;
	
	String oldPassword = null;
	String newPassword1 = null;
	String newPassword2 = null;
	String newNickname1 = null;
	String newNickname2 = null;
	String chPassword = "Change Password";
	String alcreate = "Change nickname";
	String findBranch = "Find branch";
	String newItem = "New item";
	List<HashMap<String,String>> aList  = new ArrayList<HashMap<String,String>>();
	ListView list;
	
	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_transDetails = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	//	String accDetails = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String alias = null;
	String aliasAcc = null;
	
	ProgressDialog progress;
	String response = null;

//	 handler;
//	HandlerRunnable r;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.help);
		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);


		//call in onCreate
	 //	setAppIdleTimeout();
	}


//	private Handler handler;
//	private Runnable runnable;
//
//
//
//	private void setAppIdleTimeout() {
//
//		handler = new Handler();
//		runnable = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						// Navigate to main activity
//						Toast.makeText(Help.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//						Intent loginIntent = new Intent(Help.this, LoginActivity.class);
//						loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//						startActivity(loginIntent);
//						finish();
//					}
//				});
//			}
//		};
//		handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//	}
//
//
//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//		//Log.i(TAG, "transenq interacted");
//		MainMenu.resetAppIdleTimeout();
//		super.onUserInteraction();
//	}
//
//	@Override
//	public void onDestroy() {
//		// TODO Auto-generated method stub
//		handler.removeCallbacks(runnable);
//		super.onDestroy();
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//		MainMenu.resetAppIdleTimeout();
//	}
//

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		//		menu.findItem(R.id.refresh).setVisible(false);
		return true;
	}  
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			//	         NavUtils.navigateUpFromSameTask(this);S
			Help.this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		//		moveTaskToBack(true); 
		Help.this.finish();
	}

}

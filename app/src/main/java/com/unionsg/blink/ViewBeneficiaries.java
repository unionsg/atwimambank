package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ViewBeneficiaries extends AppCompatActivity {

	ProgressDialog progress;
	Intent transDetailsIntent;

	Double amtDouble = null;
	String formattedAmt = null;

	String id = null,idRefresh = null;
	String creationTime = null,creationTimeRefresh = null;
	String mb_userId = null,mb_userIdRefresh = null;
	String lastModificationDate = null,lastModificationDateRefresh = null;
	String mb_response = null,mb_responseRefresh = null;
	String mb_accDetails = null,mb_accDetailsRefresh = null,mb_accDetailsFirstLoad= null;
	String mb_token = null,mb_tokenRefresh = null;
	String mb_userName = null;
	String mbAcc = null;
	String mbAcc2 = null;

	String[] accDetail = null,accDetailRefresh = null;

	String response = null,responseRefresh= null;

	// Array of strings storing country names
	String accNum = null,accNumRefresh = null;

	String accLedgerBal = null,accLedgerBalRefresh=null;

	String accAvailBal = null,accAvailBalRefresh = null;

	// Array of integers points to images stored in /res/drawable/
	int[] next = new int[]{	R.drawable.more };

	// Array of strings to store currencies
	String accName = null,accNameRefresh = null;
	String accNameDisp  = null;

	View view;
	RelativeLayout relLayoutParent;
	TextView accNameTextview;

	String accountName = null;
	String accountNum = null;
	String availableBal = null;
	String ledgerBal = null;
	String currAvail = null;
	String amtAvail = null;
	String decimalAvail = null;
	String currLedger = null;
	String amtLedger = null;
	String decimalLedger = null;

	String mbAccDetails = null;
	String token = null;
	ListView list ;
	SwipeRefreshLayout swipeRefLayout;
	List<HashMap<String,String>> aList,aListRefresh;
	String allAlphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;
	private TabLayout tabLayout;
	private ViewPager viewPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_beneficiaries);
		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
//		getSupportActionBar().setTitle("First Aid");
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		token = getIntent().getExtras().getString("token").trim();
		System.out.println("ViewBeneficiaries: " + token);


		viewPager = (ViewPager) findViewById(R.id.container);
		setupViewPager(viewPager);

		tabLayout = (TabLayout) findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//						.setAction("Action", null).show();


				System.out.println(viewPager.getCurrentItem());

				if (viewPager.getCurrentItem() == 0){
					Intent addBeneficiaryIntent = new Intent(ViewBeneficiaries.this, CreateBeneficiary.class);
					addBeneficiaryIntent.putExtra("token", token);
					startActivity(addBeneficiaryIntent);
				} /*else if (viewPager.getCurrentItem() == 1){
					Intent addBeneficiaryIntent = new Intent(ViewBeneficiaries.this, CreateExtBeneficiary.class);
					addBeneficiaryIntent.putExtra("token", token);
					startActivity(addBeneficiaryIntent);
				}*/


			}
		});


		//call in onCreate
//		setAppIdleTimeout();
	}


//	private Handler handler;
//	private Runnable runnable;



//	private void setAppIdleTimeout() {
//
//		MainMenu.handler = new Handler();
//		MainMenu.runnable = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						// Navigate to main activity
//						Toast.makeText(ViewBeneficiaries.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//						Intent loginIntent = new Intent(ViewBeneficiaries.this, LoginActivity.class);
//						loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//						startActivity(loginIntent);
//						finish();
//					}
//				});
//			}
//		};
//		MainMenu.handler.postDelayed(MainMenu.runnable, MainMenu.timeOutMinutes);
//	}
//
//
//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//		// Log.i(TAG, "transenq interacted");
//		MainMenu.resetAppIdleTimeout();
//		super.onUserInteraction();
//	}
//
//	@Override
//	public void onDestroy() {
//		// TODO Auto-generated method stub
//		MainMenu.handler.removeCallbacks(MainMenu.runnable);
//		super.onDestroy();
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//		MainMenu.resetAppIdleTimeout();
//	}
//
//



	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.addFrag(new SameBankBeneficiaryFragment(), "IN TSF");
//		adapter.addFrag(new OtherBanksBeneficiaryFragment(), "OTHER BANKS");
//		adapter.addFrag(new ThreeFragment(), "THREE");
		viewPager.setAdapter(adapter);
	}

//	private void setupViewPager(ViewPager viewPager) {
//		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//		adapter.addFragment(new SameBankBeneficiaryFragment(), "Within SLCB");
//		//  adapter.addFragment(new OtherBankBeneficiaryFragment(), "Other Banks");
//		//adapter.addFragment(new SaloneLinkFragment(), "Salone Link");
//		viewPager.setAdapter(adapter);
//	}

	class ViewPagerAdapter extends FragmentPagerAdapter {
		private final List<Fragment> mFragmentList = new ArrayList<>();
		private final List<String> mFragmentTitleList = new ArrayList<>();

		public ViewPagerAdapter(FragmentManager manager) {
			super(manager);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragmentList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentList.size();
		}

		public void addFrag(Fragment fragment, String title) {
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mFragmentTitleList.get(position);
		}
	}








	public String fundsTransJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myResponse (String a1) throws IOException {
		SendRequest sendReqObj = new SendRequest();

		String json = fundsTransJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"viewbeneficiaries", json);

		System.out.println(response);

		return response;
	}



	public class fireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(ViewBeneficiaries.this);
			progress.setCancelable(false);
			progress.setTitle("Please wait");
			progress.setMessage("Retrieving beneficiaries...");
			progress.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myResponse(token);

				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");
					mbAcc2 = jsonObject.getString("mb_transDetails");

					System.out.println(mbAcc);
					System.out.println(mbAcc2);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

//				Bundle bundle = new Bundle();
//				bundle.putString("key", "From Activity");
//// set Fragmentclass Arguments
//				SameBankBeneficiaryFragment fragobj = new SameBankBeneficiaryFragment();
//				fragobj.setArguments(bundle);



			}


		}

	}




	public class ResponseDialog extends AlertDialog.Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
					//					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
					//					startActivity(loginIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}









	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		//		menu.findItem(R.id.refresh).setVisible(false);
		return true;
	}  */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			//	         NavUtils.navigateUpFromSameTask(this)
			ViewBeneficiaries.this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}








	@Override
	public void onBackPressed() {
		//		moveTaskToBack(true); 
		ViewBeneficiaries.this.finish();
	}


}
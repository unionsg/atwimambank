package com.unionsg.blink;

/**
 * Created by UNION on 8/3/2017.
 */

public interface FragmentLifecycle {

    public void onResumeFragment();

}

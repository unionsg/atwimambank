package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.liststuff.TransDetailsAdapter;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class TransEnquiry extends AppCompatActivity {

    private static final String TAG = null;
    String valueDate = null;
    TextView lastTransactionsText;
    ProgressDialog progress;

    TextView accNameTextView;
    TextView availBalTextView;
    TextView ledgerBalTextView;
    TextView accNumTextView;
    //    TextView emptyList;
    ImageButton searchButton;
    Button loadMoreButton;

    String availBal = null;
    String ledgerBal = null;
    String chosenAccNum = null;
    String authToken = null;
    String accNum = null;
    String trnsBatchNo = null;
    String trnsNum = null;
    String trnsSummary = null;
    String trnsDate = null;
    String trnsAmt = null;
    String trnsTime = null;
    String transValueDate = null;

    Double amtDouble = null;
    String formattedAmt = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_token = null;
    String mb_response = null;
    String batchNumber = null;
    String mb_userName = null;
    String accName = null;
    String token = null;

    String[] transDetail = null;
    String transSummary = null;
    String terminalID = null;
    String transNum = null;
    String transBy = null;
    String approvedBy = null;
    String postingSysDate = null;
    String postingSysTime = null;
    String approvalSysDate = null;
    String approvalSysTime = null;
    String dbAmount = null;
    String crAmount = null;
    String trDetails = null;
    String branch = null;
    String contra = null;
    String doc_reff = null;

    String transdoc_reff = null;
    String transcontra = null;

    String response = null;
    int transactionNum = 10;

    String from = null;
    String to = null;

    private EditText fromDateEtxt;
    private EditText toDateEtxt;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;

    String transDetails = null;


    ListView list;
    int[] next = new int[]{R.drawable.more};
    int[] pic = new int[]{R.drawable.in};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trans_details);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Typeface regularText = Typeface.createFromAsset(getAssets(), "HelveticaRegular.otf");

        availBal = getIntent().getExtras().getString("availBalKey").trim();
        ledgerBal = getIntent().getExtras().getString("ledgerBalKey").trim();
        chosenAccNum = getIntent().getExtras().getString("accNumKey").trim();
        authToken = getIntent().getExtras().getString("tokenKey").trim();
        trDetails = getIntent().getExtras().getString("transDetailsKey").trim();
        accName = getIntent().getExtras().getString("accNameKey").trim();
        accNum = getIntent().getExtras().getString("accNumKey").trim();
//		imageBytes = getIntent().getExtras().getByteArray("imageBytes");

        accNameTextView = (TextView) findViewById(R.id.acc_name);
        availBalTextView = (TextView) findViewById(R.id.availbal);
        ledgerBalTextView = (TextView) findViewById(R.id.ledgerbal);
        accNumTextView = (TextView) findViewById(R.id.acc_num);
        searchButton = (ImageButton) findViewById(R.id.search_button);
        fromDateEtxt = (EditText) findViewById(R.id.etxt_fromdate);
        toDateEtxt = (EditText) findViewById(R.id.etxt_todate);
        lastTransactionsText = (TextView) findViewById(R.id.last_trans_text);
        loadMoreButton = (Button) findViewById(R.id.load_more_button);

        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

        toDateEtxt.setInputType(InputType.TYPE_NULL);

        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                from = dateFormatter.format(newDate.getTime());
                System.out.println(from);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
                to = dateFormatter.format(newDate.getTime());
                System.out.println(to);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDateEtxt.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fromDatePickerDialog.show();
                return false;
            }
        });


        toDateEtxt.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                toDatePickerDialog.show();
                return false;
            }
        });

		/*if(availBal.contains("-")){
            availBalTextView.setTextColor(Color.RED);
		}else availBalTextView.setTextColor(getResources().getColor(R.color.positive_bal));

		if(ledgerBal.contains("-")){
			ledgerBalTextView.setTextColor(Color.RED);   if (from.isEmpty()) {
					fromDateEtxt.setError("Tap to select a date");
				} else if (to.isEmpty()) {
					toDateEtxt.setError("Tap to select a date");
				} else {
		}else ledgerBalTextView.setTextColor(getResources().getColor(R.color.positive_bal));*/

        accNameTextView.setTypeface(regularText);
        availBalTextView.setTypeface(regularText);
        ledgerBalTextView.setTypeface(regularText);
        accNumTextView.setTypeface(regularText);

        accNameTextView.setText(accName);
        availBalTextView.setText(availBal);
        ledgerBalTextView.setText(ledgerBal);
        accNumTextView.setText(accNum);

        populateTransactions();

        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                System.out.println("from: " + from);
                System.out.println("to: " + to);

                if (fromDateEtxt.getText().length() == 0) {
                    fromDateEtxt.setError("Tap to select start date");
                } else if (fromDateEtxt.getText().length() == 0) {
                    toDateEtxt.setError("Tap to select end date");
                } else {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(TransEnquiry.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                    // new fireMan().execute();

                }

            }
        });

        loadMoreButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new loadMoreFireMan().execute();
                } else {
                    showAlertDialog(TransEnquiry.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }

                //  new loadMoreFireMan().execute();

            }
        });

        //call in onCreate
        //    setAppIdleTimeout();
    }

//
//private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        MainMenu.handler = new Handler();
//        MainMenu.runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(TransEnquiry.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(TransEnquiry.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        MainMenu.handler.postDelayed(MainMenu.runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//        Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        MainMenu.handler.removeCallbacks(MainMenu.runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }
//


    public void populateTransactions() {

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

//		if (mb_response.trim().equals("00")){
        transDetail = trDetails.split("`");

        System.out.println(transDetail.length);

        for (int a = 0; a < transDetail.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(transDetail[a], "~");

            transSummary = myAccToken.nextToken().trim();
            terminalID = myAccToken.nextToken().trim();
            transNum = myAccToken.nextToken().trim();
            approvedBy = myAccToken.nextToken().trim();
            postingSysDate = myAccToken.nextToken().trim();
            postingSysTime = myAccToken.nextToken().trim();
            approvalSysDate = myAccToken.nextToken().trim();
            approvalSysTime = myAccToken.nextToken().trim();
            dbAmount = myAccToken.nextToken().trim();
            branch = myAccToken.nextToken().trim();
            batchNumber = myAccToken.nextToken().trim();
            valueDate = myAccToken.nextToken().trim();
            doc_reff = myAccToken.nextToken().trim();
            contra = myAccToken.nextToken().trim();


            System.out.println(transSummary);
            System.out.println(terminalID);
            System.out.println(transNum);
            System.out.println(approvedBy);
            System.out.println(postingSysDate);
            System.out.println(postingSysTime);
            System.out.println(approvalSysDate);
            System.out.println(approvalSysTime);
            System.out.println(dbAmount);
            System.out.println(branch);
            System.out.println(batchNumber);
            System.out.println(valueDate);
            System.out.println(doc_reff);
            System.out.println(contra);

            HashMap<String, String> hm = new HashMap<String, String>();

				/*if (transSummary.length() >= 25){
                    transSummary = transSummary.substring(0,23) + "...";
				}*/


//				hm.put("summary", transSummary);
//			if (transSummary.length() > 20) {
////							accNameDisp = loanProdCode.substring(0, 18) + "...";
//				hm.put("summary", transSummary.substring(0, 17) + "...");
//			} else {
//				hm.put("summary", transSummary);
//			}
            hm.put("summary", transSummary);
            hm.put("date", postingSysDate);
            hm.put("amount", GlobalCodes.FormatToMoney(dbAmount)
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("time", postingSysTime);
            hm.put("batchno", batchNumber);
            hm.put("transnum", transNum);
            hm.put("valuedate", valueDate);
            hm.put("doc_reff", doc_reff);
            hm.put("contra", contra);
            hm.put("next", Integer.toString(next[0]));
            hm.put("pic", Integer.toString(pic[0]));
            aList.add(hm);

        }
//		}

        String[] from = {"pic", "next", "summary", "date", "amount", "time", "batchno", "transnum", "valuedate","doc_reff","contra"};
        //
        //				// Ids of views in listview_layout
        int[] to = {R.id.pic, R.id.flag, R.id.summary, R.id.trans_date, R.id.amt, R.id.transtime, R.id.batchno, R.id.transnum, R.id.valuedate,R.id.doc_ref,R.id.contra};

        TransDetailsAdapter adapter = new TransDetailsAdapter(TransEnquiry.this, aList, R.layout.list_transdetails, from, to);
        list = (ListView) findViewById(R.id.trans_list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                trnsSummary = ((TextView) v.findViewById(R.id.summary)).getText().toString().trim();
                trnsDate = ((TextView) v.findViewById(R.id.trans_date)).getText().toString().trim();
                trnsAmt = ((TextView) v.findViewById(R.id.amt)).getText().toString().trim();
                trnsTime = ((TextView) v.findViewById(R.id.transtime)).getText().toString().trim();
                trnsBatchNo = ((TextView) v.findViewById(R.id.batchno)).getText().toString().trim();
                transNum = ((TextView) v.findViewById(R.id.transnum)).getText().toString().trim();
                transValueDate = ((TextView) v.findViewById(R.id.valuedate)).getText().toString().trim();
                transdoc_reff = ((TextView) v.findViewById(R.id.doc_ref)).getText().toString().trim();
                transcontra = ((TextView) v.findViewById(R.id.contra)).getText().toString().trim();



//				Toast.makeText(this, trnsSummary, Toast.LENGTH_SHORT).show();
//				System.out.println("byte array is " + MyAccounts.imageBytes);

                Intent transEnqIntent = new Intent(TransEnquiry.this, TransChoice.class);
                transEnqIntent.putExtra("transSumKey", trnsSummary);
                transEnqIntent.putExtra("amtKey", trnsAmt);
                transEnqIntent.putExtra("timeKey", trnsTime);
                transEnqIntent.putExtra("dateKey", trnsDate);
                transEnqIntent.putExtra("branch", branch);
                transEnqIntent.putExtra("transNum", transNum);
                transEnqIntent.putExtra("token", authToken);
                transEnqIntent.putExtra("batchno", trnsBatchNo);
                transEnqIntent.putExtra("valuedate", transValueDate);
                transEnqIntent.putExtra("doc_reff", transdoc_reff);
                transEnqIntent.putExtra("contra", transcontra);
                startActivity(transEnqIntent);
            }
        });

    }


    public String transEnqJSON(String authToken, String accNum, String fromDate, String toDate) {
        return "authToken=" + authToken +
                "&accNum=" + accNum +
                "&fromDate=" + fromDate +
                "&toDate=" + toDate;
    }

    public String myResponse(String a1, String a2, String a3, String a4) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = transEnqJSON(a1, a2, a3, a4);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "transenqbydate", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(TransEnquiry.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(authToken, accNum, from, to);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);

//					mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        trDetails = jsonObject.getString("mb_transDetails");
//					imageBytes = jsonObject.getString("mb_image1").getBytes();
//					imageBytes = mb_accDetails;

                        //					System.out.println(mb_userId);
                        System.out.println(trDetails);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("13")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Account is Locked.Please Retry", Toast.LENGTH_LONG).show();


                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "There are no transactions within this date range", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("11")) {

                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Wrong UserName or Password Entered. Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(TransEnquiry.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    System.out.println("TRANS DETAILS: " + trDetails);
                    if (trDetails.equals("No transactions were made within this date range")) {
                        Toast.makeText(TransEnquiry.this, trDetails, Toast.LENGTH_LONG).show();
                    } else if (trDetails.length() == 4) {
                        Toast.makeText(TransEnquiry.this, "There are no transactions within this date range", Toast.LENGTH_LONG).show();
                    } else {

                        populateTransactions();
                    }

                } else {
                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Network is down. Please Try Again Later", Toast.LENGTH_LONG).show();
                }


            }

        }


    }


    public String transEnqJSON(String authToken, String accNum, String transNum) {
        return "authToken=" + authToken +
                "&accNum=" + accNum +
                "&number=" + transNum;
    }


    public String loadMoreResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = transEnqJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "transenqbynumber", json);

        System.out.println(response);

        return response;
    }


    public class loadMoreFireMan extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(TransEnquiry.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                transactionNum = transactionNum + 20;
                response = loadMoreResponse(token, chosenAccNum, String.valueOf(transactionNum));

                /*if (transactionNum == 10) {
                    transactionNum = transactionNum + 10;
                    response = loadMoreResponse(token, chosenAccNum, String.valueOf(transactionNum));
                } else if (transactionNum == 20) {
                    transactionNum = transactionNum + 10;
                    response = loadMoreResponse(token, chosenAccNum, String.valueOf(transactionNum));
                } else if (transactionNum == 30) {
                    transactionNum = transactionNum + 10;
                    response = loadMoreResponse(token, chosenAccNum, String.valueOf(transactionNum));
                } else if (transactionNum == 40) {
                    transactionNum = transactionNum + 10;
                    response = loadMoreResponse(token, chosenAccNum, String.valueOf(transactionNum));
                } else if (transactionNum == 50) {
                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Please use the date range to view more transactions", Toast.LENGTH_LONG).show();
                }*/

                try {
                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);

                        mb_response = jsonObject.getString("mb_response");
                        transDetails = jsonObject.getString("mb_transDetails");

                        /*id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");

                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
//					mb_accDetails = jsonObject.getString("mb_accDetails");
                        transDetails = jsonObject.getString("mb_transDetails");*/
//					imageBytes = jsonObject.getString("mb_image1").getBytes();
//					imageBytes = mb_accDetails;

                        //					System.out.println(mb_userId);
                        System.out.println(trDetails);

                    } else {
                        mb_response = "66";
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("13")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Account is Locked.Please Retry", Toast.LENGTH_LONG).show();


                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "No Transactions Are Available On This Account", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(TransEnquiry.this, "No response", "Please try again").showDialog();

                } else if (mb_response.trim().equals("11")) {

                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Wrong UserName or Password Entered. Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    System.out.println("TRANS DETAILS: " + transDetails);
                    if (transDetails.equals("No Transactions Are Available On This Account")) {
                        Toast.makeText(TransEnquiry.this, transDetails, Toast.LENGTH_LONG).show();
                    } else {
                        trDetails = transDetails;
                        lastTransactionsText.setText("Last " + transactionNum + " transactions");
                        populateTransactions();
                    }

                } else {
                    progress.cancel();
                    Toast.makeText(TransEnquiry.this, "Network is down. Please Try Again Later", Toast.LENGTH_LONG).show();
                }


            }

        }


    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}

    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is
//		// present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findItem(R.id.refresh).setVisible(false);
//		return true;
//	}
//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                TransEnquiry.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//	   moveTaskToBack(true); 
        TransEnquiry.this.finish();
    }


}
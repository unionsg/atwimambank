package com.unionsg.blink;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLEncoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

//@SuppressLint("NewApi") 197.251.247.6 apps.unionsysglobal.com
@SuppressWarnings("deprecation")
public class LoginActivity extends AppCompatActivity {

    Intent loginIntent;
    EditText username;
    EditText password;

    static String id = null;
    static String creationDate = null;
    static String mb_userId = null;
    //	static String lastModificationDate = null;
    static String mb_response = null;
    static String mb_userName = null;
    static String mb_token = null;
    static String mb_acc_details = null;
    static String mbCreationTime = null;
    static String balances = null;
    String mbAcc = null;
    String creationTime = null;
    ProgressDialog progress;
    static String IPaddress = "";
    String questions = null;
    String codeFlag = null;
    String photo = null;
    static String countryCodeValue = null;

    String usrName = null;
    String passWrd = null;
    String idFromFile = null;
    String response = null;
    String forgotPwdResponse = null;
    String fastBalResponse = null;
    Button post;
    Button fastBalanceButton = null;
    String[] aliasAccDetails = null;
    TextView forgotPassword;
    ImageView mPasswordVisibilityView;

    String mbAccDetails = null;
    String[] accDetail = null;
    String userIdFastBal = null;

    String from = null;
    String to = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;
    List<HashMap<String, String>> aList;
    ListView list;

    TextView touchTextView;

    String beneInternal = null;
    String beneExternal = null;

    ProgressDialog progress2;

//    Handler handler;
//    Runnable r;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;


    Dialog dialog;
    int[] next = new int[]{R.drawable.more};

    // Storage Permissions variables
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_PHONE_STATE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_CONTACTS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.CAMERA,
//            android.Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_WIFI_STATE


    };


//	Get unique ID for installation
//	public synchronized static String id(Context context) {
//		if (uniqueID == null) {
//			SharedPreferences sharedPrefs = context.getSharedPreferences(
//					PREF_UNIQUE_ID, Context.MODE_PRIVATE);
//			uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);
//
//			if (uniqueID == null) {
//				uniqueID = UUID.randomUUID().toString();
//				SharedPreferences.Editor editor = sharedPrefs.edit();
//				editor.putString(PREF_UNIQUE_ID, uniqueID);
//				editor.apply();
//			}
//		}
//
//		return uniqueID;
//	}

    String IMEI, IMSI = null;

    @TargetApi(Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		setContentView(R.layout.main);

//        FirebaseCrash.log("Activity created");


        SharedPreferences preferences = getSharedPreferences("myprefs", MODE_PRIVATE);
        if (preferences.getBoolean("firstLogin", true)) {
//			setContentView(R.layout.main);
//			System.out.println("firstLogin " + firstLogin);
//			SharedPreferences.Editor editor1 = preferences.edit();
//			editor1.putBoolean("firstLogin", false);
//			editor1.apply();
            Intent termsIntent = new Intent(LoginActivity.this, TermsAndConditions.class);
            finish();
            startActivity(termsIntent);


        } else {
            setContentView(R.layout.main);


            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            post = (Button) findViewById(R.id.postlogin);

            username = (EditText) findViewById(R.id.user);
            password = (EditText) findViewById(R.id.password);
            touchTextView = (TextView) findViewById(R.id.touchTextView);
            fastBalanceButton = (Button) findViewById(R.id.fast_bal_button);
            forgotPassword = (TextView) findViewById(R.id.forgot_password);
            mPasswordVisibilityView = (ImageView) findViewById(R.id.show_password);

//			new CountDownTimer(10000, 1000) {
//
//				public void onTick(long millisUntilFinished) {
////					mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
//				}
//
//				public void onFinish() {
//					Toast.makeText(LoginActivity.this, "done", Toast.LENGTH_LONG).show();
//				}
//			}.start();


            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            countryCodeValue = tm.getNetworkCountryIso();

//			WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//			String iPAddress = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());

//            String IMEI_Number_Holder = tm.getDeviceId();

            networkIP();

            if (IPaddress.isEmpty()) {
                IPaddress = "Unavailable";
            }

//            String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
//            System.out.println("androidId: " + androidId);

            String code = id(LoginActivity.this);

            System.out.println("ID: " + code + "\n" +
                    "IP ADDRESS: " + IPaddress + "\n" +
                    "MODEL: " + Build.MODEL + "\n" +
                    "Manufacturer: " + Build.MANUFACTURER.toUpperCase() + "\n" +
                    "Brand: " + Build.BRAND.toUpperCase() + "\n" +
                    "COUNTRY: " + countryCodeValue.toUpperCase());


            //comment out this line if it works
//			Toast.makeText(LoginActivity.this, countryCodeValue.toUpperCase(), Toast.LENGTH_LONG).show();


//			Toast.makeText(LoginActivity.this, "SERIAL: " + Build.SERIAL + "\n" +
//					"MODEL: " + Build.MODEL + "\n" +
//					"ID: " + Build.ID + "\n" +
//					"Manufacture: " + Build.MANUFACTURER + "\n" +
//					"Brand: " + Build.BRAND + "\n" +
//					"Type: " + Build.TYPE + "\n" +
//					"User: " + Build.USER + "\n" +
//					"BASE: " + Build.VERSION_CODES.BASE + "\n" +
//					"INCREMENTAL: " + Build.VERSION.INCREMENTAL + "\n" +
//					"SDK:  " + Build.VERSION.SDK + "\n" +
//					"BOARD: " + Build.BOARD + "\n" +
//					"BRAND: " + Build.BRAND + "\n" +
//					"HOST: " + Build.HOST + "\n" +
//					"FINGERPRINT: "+Build.FINGERPRINT + "\n" +
//					"Version Code: " + Build.VERSION.RELEASE, Toast.LENGTH_LONG).show();


            username.clearFocus();

            final ScrollView scroll = (ScrollView) findViewById(R.id.scroll);

            Button helpButton = (Button) findViewById(R.id.help);
            Button aboutUs = (Button) findViewById(R.id.about_us);
            Button registerButton = (Button) findViewById(R.id.register);
            Button findBranchButton = (Button) findViewById(R.id.find_branch);

            mPasswordVisibilityView.setOnTouchListener(mPasswordVisibleTouchListener);

            touchTextView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    usrName = username.getText().toString();
                    if (usrName.trim().isEmpty()) {
                        username.setError("Please enter user ID");
                    } else {
                        buildTouchDialog(R.style.DialogAnimation);
                    }

                }
            });

            /*contactUsButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent contactUsIntent = new Intent(LoginActivity.this, ContactUs.class);
                    startActivity(contactUsIntent);

                }
            });*/

            registerButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(LoginActivity.this, CustomerOnboarding.class);
                    startActivity(intent);

                }
            });

            helpButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent helpIntent = new Intent(LoginActivity.this, Help.class);
                    startActivity(helpIntent);

                }
            });

            aboutUs.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Intent aboutUsIntent = new Intent(LoginActivity.this, AboutUs.class);
                    startActivity(aboutUsIntent);

                }
            });

            findBranchButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    usrName = username.getText().toString();
                    Intent intent = new Intent(LoginActivity.this, PlotData.class);
                    intent.putExtra("userId", usrName);
                    startActivity(intent);

                }
            });

            username.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    scroll.post(new Runnable() {
                        public void run() {
                            scroll.smoothScrollTo(0, 140);
                        }
                    });
                    return false;
                }
            });

            password.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View arg0, MotionEvent arg1) {
                    scroll.post(new Runnable() {
                        public void run() {
                            scroll.smoothScrollTo(0, 140);
                        }
                    });
                    return false;
                }
            });

//            handler = new Handler();
//            r = new Runnable() {
//
//                @Override
//                public void run() {
//                    // TODO Auto-generated method stub
//
//                }
//            };
//            startHandler();


            verifyStoragePermissions(this);

            idFromFile = readFromFile(LoginActivity.this);
            if (!idFromFile.equals("")) {
                username.setText(idFromFile);


//			username.setEnabled(true);

            } else {
                fastBalanceButton.setEnabled(true);
            }


		/*password.addTextChangedListener(new TextWatcher() {


			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				int passwordCharNumber = password.getText().toString().trim().length();
				if(passwordCharNumber >= 6)
				{ post.setEnabled(true);
				post.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.login_btn_pressed, 0, 0);
				}

				else { post.setEnabled(false);
				post.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.login_btn, 0, 0);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});*/


            post.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    usrName = username.getText().toString();
                    passWrd = password.getText().toString();
                    System.out.println("username =" + usrName);

                    if (usrName.trim().isEmpty()) {
                        username.setError("Please enter user ID");
                    } else if (passWrd.trim().isEmpty()) {
                        password.setError("Please enter password");
                    } else {

                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new fireMan().execute();
                        } else {
                            showAlertDialog(LoginActivity.this, "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                    }
                }
            });


            fastBalanceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    userIdFastBal = username.getText().toString();
                    if (userIdFastBal.trim().isEmpty()) {
                        username.setError("Please enter your user ID");
                    } else {
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new myAcctsFireMan().execute();
                        } else {
                            showAlertDialog(LoginActivity.this, "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                    }


                }
            });


            forgotPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    userIdFastBal = username.getText().toString();
                    if (userIdFastBal.trim().isEmpty()) {
                        username.setError("Please enter your user ID");
                    } else {
//                        new forgotPasswordFireMan().execute();
                        Intent forgotPwdIntent = new Intent(LoginActivity.this, ForgotPassword.class);
                        forgotPwdIntent.putExtra("userId", userIdFastBal);
                        forgotPwdIntent.putExtra("token", mb_token);
                        startActivity(forgotPwdIntent);
                    }


                }
            });

        }
    }




    private View.OnTouchListener mPasswordVisibleTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final boolean isOutsideView = event.getX() < 0 ||
                    event.getX() > v.getWidth() ||
                    event.getY() < 0 ||
                    event.getY() > v.getHeight();

            // change input type will reset cursor position, so we want to save it
            final int cursor = password.getSelectionStart();

            if (isOutsideView || MotionEvent.ACTION_UP == event.getAction())
                password.setInputType( InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_PASSWORD);
            else
                password.setInputType( InputType.TYPE_CLASS_TEXT |
                        InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

            password.setSelection(cursor);
            return true;
        }
    };

    //persmission method.
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have read or write permission
        int writePermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
//        int phoneStatePermission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.READ_PHONE_STATE);

        if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED
                || cameraPermission != PackageManager.PERMISSION_GRANTED
                /*|| phoneStatePermission != PackageManager.PERMISSION_GRANTED*/) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE

            );
        }
    }


    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

    public synchronized static String id(Context context) {
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.apply();
            }
        }


        return uniqueID.toUpperCase();
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void buildTouchDialog(int animationSource) {

        if (android.os.Build.VERSION.SDK_INT >= 23) {

            // Initializing both Android Keyguard Manager and Fingerprint Manager
            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);


            // Check whether the device has a Fingerprint sensor.
            if (!fingerprintManager.isHardwareDetected()) {
                /**
                 * An error message will be displayed if the device does not contain the fingerprint hardware.
                 * However if you plan to implement a default authentication method,
                 * you can redirect the user to a default authentication activity from here.
                 * Example:
                 * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                 * startActivity(intent);
                 */
//						textView.setText("Your Device does not have a Fingerprint Sensor");
                Toast.makeText(LoginActivity.this, "Your Device does not have a Fingerprint Sensor", Toast.LENGTH_LONG).show();
            } else {

                dialog = new Dialog(LoginActivity.this);
                dialog.setContentView(R.layout.activity_fingerprint);
//        dialog.setTitle("Change Password");

                Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;
                dialog.getWindow().setAttributes(lp);

                // Checks whether fingerprint permission is set on manifest
                if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//							textView.setText("Fingerprint authentication permission not enabled");
                    Toast.makeText(LoginActivity.this, "Fingerprint authentication permission not enabled", Toast.LENGTH_LONG).show();
                } else {
                    // Check whether at least one fingerprint is registered
                    if (!fingerprintManager.hasEnrolledFingerprints()) {
//								textView.setText("Register at least one fingerprint in Settings");
                        dialog.dismiss();
                        Toast.makeText(LoginActivity.this, "Register at least one fingerprint in Settings", Toast.LENGTH_LONG).show();
                    } else {
                        // Checks whether lock screen security is enabled or not
                        if (!keyguardManager.isKeyguardSecure()) {
//									textView.setText("Lock screen security not enabled in Settings");
                            Toast.makeText(LoginActivity.this, "Lock screen security not enabled in Settings", Toast.LENGTH_LONG).show();
                        } else {
                            generateKey();


                            if (cipherInit()) {
                                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                FingerprintHandler helper = new FingerprintHandler(LoginActivity.this);
                                helper.startAuth(fingerprintManager, cryptoObject);
                                System.out.println("1");

//                                if (successResponse.equals("1")) {
//                                    new bioFireMan().execute();
//                                    successResponse = "0";
//                                    dialog.dismiss();
//                                } else {
//                                    dialog.dismiss();
//
//                                }
                            }
                        }
                    }
                }
                cancelButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });
                dialog.getWindow().getAttributes().windowAnimations = animationSource;
                dialog.show();
            }

        } else {
            Toast.makeText(LoginActivity.this, "Sorry, this device does not support fingerprint", Toast.LENGTH_LONG).show();
        }


    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }


        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }


        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }


        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


//	public String getLocalIpAddress() {
//		try {
//			for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
//				NetworkInterface intf = en.nextElement();
//				for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
//					InetAddress inetAddress = enumIpAddr.nextElement();
//					System.out.println(Formatter.formatIpAddress(inetAddress.hashCode()));
//					if (!inetAddress.isLoopbackAddress()) {
//						IPaddress = Formatter.formatIpAddress(inetAddress.hashCode());
////						Log.i(TAG, "***** IP="+ ip);
//						return IPaddress;
//					}
//				}
//			}
//		} catch (SocketException ex) {
//			System.out.println(ex.toString());
//		}
//		return null;
//	}



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//        super.onUserInteraction();
//        stopHandler();//stop first and then start
//        startHandler();
//    }
//
//    public void stopHandler() {
//        handler.removeCallbacks(r);
//    }
//
//    public void startHandler() {
//        handler.postDelayed(r, 20000 * 60 * 1000); //for 5 minutes
//    }


//	public String GetDeviceipWiFiData()
//	{
//
//		WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//
//		@SuppressWarnings("deprecation")
//
//		String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
//
//		return Formatter.formatIpAddress(ip.hashCode());
//
//	}

    public static String GetDeviceipWiFiData() {
//        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);


        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            DeviceAdminReceiver admin = new DeviceAdminReceiver();
            DevicePolicyManager devicepolicymanager = admin.getManager(getApplicationContext());
            ComponentName name1 = admin.getWho(getApplicationContext());
            if (devicepolicymanager.isAdminActive(name1)){
                String mac_address = devicepolicymanager.getWifiMacAddress(name1);
                System.out.println("MACADDRESS: " + mac_address);
                Log.e("macAddress",""+mac_address);
            }

        }*/


        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = addr instanceof Inet4Address;
                        if (isIPv4 && intf.getDisplayName().startsWith("wlan")) {
                            return sAddr;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return null;
        }



        return null;
    }

    public static String GetDeviceipMobileData() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface networkinterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("Current IP", ex.toString());
        }
        return null;
    }


    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("savedoc.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }

//    System.out.println("ID: " + code + "\n" +
//            "IP ADDRESS: " + IPaddress + "\n" +
//            "MODEL: " + Build.MODEL + "\n" +
//            "Manufacturer: " + Build.MANUFACTURER.toUpperCase() + "\n" +
//            "Brand: " + Build.BRAND.toUpperCase() + "\n" +
//            "COUNTRY: " + countryCodeValue.toUpperCase());

    public String loginJSON(String phoneNumber, String password) throws UnsupportedEncodingException {
        return "phoneNumber=" + URLEncoder.encode(phoneNumber, "UTF-8") +
                "&password=" + URLEncoder.encode(password, "UTF-8") +
                "&device_id=" + URLEncoder.encode(id(LoginActivity.this), "UTF-8") +
                "&device_ip=" + URLEncoder.encode(IPaddress, "UTF-8") +
                "&model=" + URLEncoder.encode(Build.MODEL, "UTF-8") +
                "&manufacturer=" + URLEncoder.encode(Build.MANUFACTURER.toUpperCase(), "UTF-8") +
                "&brand=" + URLEncoder.encode(Build.BRAND.toUpperCase(), "UTF-8") +
                "&country=" + URLEncoder.encode(countryCodeValue.toUpperCase(), "UTF-8");
    }


    public String myResponse(String a1, String a2) throws UnsupportedEncodingException {
        SendRequest sendReqObj = new SendRequest();

        String json = loginJSON(a1, a2);
//		response = null;
        try {
            response = sendReqObj.postReq(GlobalCodes.myUrl + "signin", json);
            System.out.println("login response: " + response);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(LoginActivity.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Authenticating");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(usrName, passWrd);
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            System.out.println("RESPONSE: " + response);
            //				System.out.println("ENTERED USER NAME = " +usrName);
            //				System.out.println("RAW RESPONSE VALUE 1 = "+ response);


            try {
                if (!response.isEmpty()) {

                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationDate = jsonObject.getString("creationTime");
                    mb_userId = jsonObject.getString("mb_userId");
//                lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_response = jsonObject.getString("mb_response");
                    mb_userName = jsonObject.getString("mb_userName").trim();
                    mb_token = jsonObject.getString("tokenId");
                    mb_acc_details = jsonObject.getString(("mb_accDetails"));
                    balances = jsonObject.getString(("mb_transDetails"));
                    mbCreationTime = jsonObject.getString(("creationTime"));
                    beneInternal = jsonObject.getString(("mb_internal"));
                    beneExternal = jsonObject.getString(("mb_external"));
                    questions = jsonObject.getString("questions");
                    codeFlag = jsonObject.getString("codeFlag");
//                    photo = jsonObject.getString("photo");
                    System.out.println("questions: " + questions);

                } else {
                    mb_response = "66";
                }


                //	mn.getTx().setText(getLmd());
                //tx.setText(raw);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("111111111111111111");
                System.out.println(e.getMessage());
                //					Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
            }


            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                showAlertDialog(LoginActivity.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress.cancel();
                    showAlertDialog(LoginActivity.this, "Error!", "No response from server. Please check your Internet connection and try again", false);

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("13")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "Account is Locked!", "Please contact bank for help").showDialog();

                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "Wrong userID!", "Please try again").showDialog();

                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "Wrong Password!", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    loginIntent = new Intent(LoginActivity.this, MainMenu.class);
                    loginIntent.putExtra("token", mb_token);
                    loginIntent.putExtra("userName", mb_userName);
                    loginIntent.putExtra("userId", usrName);
                    loginIntent.putExtra("mb_acc_details", mb_acc_details);
                    loginIntent.putExtra("balances", balances);
                    loginIntent.putExtra("creationTime", mbCreationTime);
                    loginIntent.putExtra("beneInternal", beneInternal);
                    loginIntent.putExtra("beneExternal", beneExternal);
                    loginIntent.putExtra("questions", questions);
                    loginIntent.putExtra("codeFlag", codeFlag);
//					loginIntent.putExtra("photo", photo);
//                    System.out.println("photo: " + photo);
                    finish();
                    startActivity(loginIntent);
                }

            }

        }


    }


    public String bioLoginJSON(String phoneNumber, String id) throws UnsupportedEncodingException {
        return "phoneNumber=" + phoneNumber +
                "&uniqueId=" + id +
                "&device_ip=" + URLEncoder.encode(IPaddress, "UTF-8") +
                "&model=" + URLEncoder.encode(Build.MODEL, "UTF-8") +
                "&manufacturer=" + URLEncoder.encode(Build.MANUFACTURER.toUpperCase(), "UTF-8") +
                "&brand=" + URLEncoder.encode(Build.BRAND.toUpperCase(), "UTF-8") +
                "&country=" + URLEncoder.encode(countryCodeValue.toUpperCase(), "UTF-8");

    }


    public String myBioResponse(String a1, String a2) throws UnsupportedEncodingException {
        SendRequest sendReqObj = new SendRequest();

        String json = bioLoginJSON(a1, a2);
//		response = null;
        try {
            response = sendReqObj.postReq(GlobalCodes.myUrl + "signinBio", json);
            System.out.println("login response: " + response);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return response;
    }


    public class bioFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(LoginActivity.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Authenticating");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myBioResponse(usrName, uniqueID);
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            System.out.println("RESPONSE: " + response);
            //				System.out.println("ENTERED USER NAME = " +usrName);
            //				System.out.println("RAW RESPONSE VALUE 1 = "+ response);


            try {
                if (!response.isEmpty()) {

                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationDate = jsonObject.getString("creationTime");
                    mb_userId = jsonObject.getString("mb_userId");
//                lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_response = jsonObject.getString("mb_response");
                    mb_userName = jsonObject.getString("mb_userName").trim();
                    mb_token = jsonObject.getString("tokenId");
                    mb_acc_details = jsonObject.getString(("mb_accDetails"));
                    balances = jsonObject.getString(("mb_transDetails"));
                    mbCreationTime = jsonObject.getString(("creationTime"));
                    beneInternal = jsonObject.getString(("mb_internal"));
                    beneExternal = jsonObject.getString(("mb_external"));
                    questions = jsonObject.getString("questions");
                    codeFlag = jsonObject.getString("codeFlag");
//                    photo = jsonObject.getString("photo");
//                    System.out.println();
                    System.out.println("questions: " + questions);

                } else {
                    mb_response = "66";
                }


                //	mn.getTx().setText(getLmd());
                //tx.setText(raw);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("111111111111111111");
                System.out.println(e.getMessage());
                //					Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
            }


            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                showAlertDialog(LoginActivity.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress.cancel();
                    showAlertDialog(LoginActivity.this, "Error!", "No response from server. Please check your Internet connection and try again", false);

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("13")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "Account is Locked!", "Please contact bank for help").showDialog();

                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "Wrong userID!", "Please try again").showDialog();

                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(LoginActivity.this, "Wrong Password!", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    loginIntent = new Intent(LoginActivity.this, MainMenu.class);
                    loginIntent.putExtra("token", mb_token);
                    loginIntent.putExtra("userName", mb_userName);
                    loginIntent.putExtra("userId", usrName);
                    loginIntent.putExtra("mb_acc_details", mb_acc_details);
                    loginIntent.putExtra("balances", balances);
                    loginIntent.putExtra("creationTime", mbCreationTime);
                    loginIntent.putExtra("beneInternal", beneInternal);
                    loginIntent.putExtra("beneExternal", beneExternal);
                    loginIntent.putExtra("questions", questions);
                    loginIntent.putExtra("codeFlag", codeFlag);
//					loginIntent.putExtra("photo", photo);
                    System.out.println("photo: " + photo);
                    finish();
                    startActivity(loginIntent);
                }

            }

        }


    }


    public String acctsJSON(String userId) throws UnsupportedEncodingException {
        return "userId=" + URLEncoder.encode(userId, "UTF-8");
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "fastbal", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(LoginActivity.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                fastBalResponse = myacctsResponse(userIdFastBal);
                //				System.out.println(response);
                try {
                    if (!fastBalResponse.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(fastBalResponse);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
//					lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                showAlertDialog(LoginActivity.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    showAlertDialog(LoginActivity.this, "Error!", "Connection timed out. Please check your Internet connection and try again", false);

                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(LoginActivity.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(LoginActivity.this, "No Data Found For This User", "Please login again").showDialog();
                    //					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(LoginActivity.this, "Error !", "Unknown Error").showDialog();
//					confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("19")) {
                    progress2.cancel();
                    new ResponseDialog(LoginActivity.this, "Sorry", mbAcc).showDialog();
//					confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);
                    buildDialog(R.style.DialogAnimation);

                }

            }

        }

    }


    private void buildDialog(int animationSource) {

        dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.fast_balance);
        dialog.setCancelable(false);
//        dialog.setTitle("Fast balance");

        TextView titleTextView = (TextView) dialog.findViewById(R.id.titleText);
        titleTextView.setText(R.string.fastbal);


        accDetail = mbAcc.split("`");
        aList = new ArrayList<HashMap<String, String>>();

        for (int a = 0; a < accDetail.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(myAccToken.nextToken()).trim();
            accLedgerBal = GlobalCodes.FormatToMoney(myAccToken.nextToken()).trim();
            //			String d = myAccToken.nextToken().trim();
            System.out.println(accNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

//			hm.put("accname", loanProdCode);
//			if (loanProdCode.length() > 20) {
////							accNameDisp = loanProdCode.substring(0, 18) + "...";
//				hm.put("accname", loanProdCode.substring(0, 17) + "...");
//			} else {
//				hm.put("accname", loanProdCode);
//			}
            hm.put("accname", accName);
            hm.put("accnum", accNum);
            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            aList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        SimpleAdapter adapter = new SimpleAdapter(LoginActivity.this, aList, R.layout.list_bills_from, from, to);

        list = (ListView) dialog.findViewById(R.id.fast_bal_list);
        list.setAdapter(adapter);


        Button okButton = (Button) dialog.findViewById(R.id.ok_button);


        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });


        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    /**
     * Extremely simple dialog builder, just so I don't have to mess about when creating
     * dialogs for the user.
     *
     * @author Isaac Whitfield
     * @version 09/03/2014
     */
    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    String successResponse = "0";

    @TargetApi(Build.VERSION_CODES.M)
    public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {


        private Context context;


        // Constructor
        public FingerprintHandler(Context mContext) {
            context = mContext;
        }


        @TargetApi(Build.VERSION_CODES.M)
        public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
            CancellationSignal cancellationSignal = new CancellationSignal();
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }


        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            this.update("Fingerprint Authentication error\n" + errString, false);
        }


        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            this.update("Fingerprint Authentication help\n" + helpString, false);
        }


        @Override
        public void onAuthenticationFailed() {
            this.update("Fingerprint Authentication failed.", false);
        }


        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
            this.update("Fingerprint Authentication succeeded.", true);
            successResponse = "1";
            System.out.println("success response: " + successResponse);
        }


        public void update(String e, Boolean success) {
//        TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
//        textView.setText(e);
            Toast.makeText(context, e, Toast.LENGTH_LONG).show();
            if (success) {
                dialog.dismiss();
                successResponse = "0";
                new bioFireMan().execute();
//            textView.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
            }
        }
    }


}

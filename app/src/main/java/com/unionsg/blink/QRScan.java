package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

//import com.unionsg.blink.barcode.BarcodeCaptureActivity;

public class QRScan extends AppCompatActivity {

	private Spinner spinner1; /*spinner2*/;
	private EditText amtEditText;
	private Button confirmButton;
//	private Button addAliasButton;
	String mbAccDetails = null;
	String[] accDetail = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	//	String accDetails = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String alias = null;
	String aliasAcc = null;

	String accNum = null;
	String accLedgerBal = null;
	String accAvailBal = null;
	String accName = null;

	String accountNum = null;
	String accountName = null;

	ProgressDialog progress;
	ProgressDialog progress2;
	int pos;
	String aliasAccNum = null;
	String aliasName = null;
	String transAmt = null;

	int[] next = new int[]{	R.drawable.more };
	List<HashMap<String,String>> aList;
	View view;

	String token = null;

	String accnumText;

	String response = null;

	private static final int PHOTO_REQUEST = 10;
	private TextView scanResults;
	private BarcodeDetector detector;
	private Uri imageUri;
	private static final int REQUEST_WRITE_PERMISSION = 20;
	private static final String SAVED_INSTANCE_URI = "uri";
	private static final String SAVED_INSTANCE_RESULT = "result";

	static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";



	private static final int BARCODE_READER_REQUEST_CODE = 1;

	private TextView mResultTextView;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.qr_scan);

		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
		token = getIntent().getExtras().getString("token").trim();

//		new fireMan().execute();
		new myAcctsFireMan().execute();

		spinner1 = (Spinner) findViewById(R.id.spinner1);
		amtEditText = (EditText) findViewById(R.id.amtedittext);

		confirmButton = (Button)findViewById(R.id.confirm_button);


		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				//				accountNum = parent.getItemAtPosition(pos).toString();
				accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();

			}

			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing



			}
		});



		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//				accountNum = spinner1.getSelectedItem().toString();
				//				accountNum = ((TextView) v.findViewById(R.id.acc_num)).getText().toString().trim();

				//				accountNum = spinner1.getChildAt(1).toString();
				transAmt = amtEditText.getText().toString();
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
				if (transAmt.trim().isEmpty()){
					amtEditText.setError("Enter transfer amount");
				}
				else{
				Intent confirmTransIntent = new Intent(QRScan.this,ConfirmTrans.class);
				System.out.println(accountNum);
				System.out.println(aliasAccNum);
				confirmTransIntent.putExtra("dbAcc", accountNum);
				confirmTransIntent.putExtra("crAcc", aliasAccNum); 
				confirmTransIntent.putExtra("crAccName", aliasName);
				confirmTransIntent.putExtra("amt", transAmt);
				confirmTransIntent.putExtra("token", token);
				startActivity(confirmTransIntent);
				}
				//				accountNum, aliasName, transAmt, token
				//				new confimFireMan().execute();
			}
		});


		mResultTextView = (TextView) findViewById(R.id.result_textview);

		Button scanBarcodeButton = (Button) findViewById(R.id.scan_barcode_button);
		scanBarcodeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
//				startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
			}
		});





		/*Button button = (Button) findViewById(R.id.button);
		scanResults = (TextView) findViewById(R.id.scan_results);
		if (savedInstanceState != null) {
			imageUri = Uri.parse(savedInstanceState.getString(SAVED_INSTANCE_URI));
			scanResults.setText(savedInstanceState.getString(SAVED_INSTANCE_RESULT));
		}
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				*//*ActivityCompat.requestPermissions(QRScan.this, new
						String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);*//*




			}
		});*/

		/*detector = new BarcodeDetector.Builder(getApplicationContext())
				.setBarcodeFormats(Barcode.DATA_MATRIX | Barcode.QR_CODE)
				.build();
		if (!detector.isOperational()) {
			scanResults.setText("Could not set up the detector!");
			return;
		}*/

		

	
	}






	/*@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BARCODE_READER_REQUEST_CODE) {
			if (resultCode == CommonStatusCodes.SUCCESS) {
				if (data != null) {
					Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
					Point[] p = barcode.cornerPoints;
					mResultTextView.setText(barcode.displayValue);
				} else mResultTextView.setText(R.string.no_barcode_captured);
			} else Log.e(LOG_TAG, String.format(getString(R.string.barcode_error_format),
					CommonStatusCodes.getStatusCodeString(resultCode)));
		} else super.onActivityResult(requestCode, resultCode, data);
	}*/









	/*//product barcode mode
	public void scanBar(View v) {
		try {
			//start the scanning activity from the com.google.zxing.client.android.SCAN intent
			Intent intent = new Intent(ACTION_SCAN);
			intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
			startActivityForResult(intent, 0);
		} catch (ActivityNotFoundException anfe) {
			//on catch, show the download dialog
			showDialog(QRScan.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
		}
	}

	//product qr code mode
	public void scanQR(View v) {
		try {
			//start the scanning activity from the com.google.zxing.client.android.SCAN intent
			Intent intent = new Intent(ACTION_SCAN);
			intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
			startActivityForResult(intent, 0);
		} catch (ActivityNotFoundException anfe) {
			//on catch, show the download dialog
			showDialog(QRScan.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
		}
	}

	//alert dialog for downloadDialog
	private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
		AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
		downloadDialog.setTitle(title);
		downloadDialog.setMessage(message);
		downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
				Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				try {
					act.startActivity(intent);
				} catch (ActivityNotFoundException anfe) {

				}
			}
		});
		downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialogInterface, int i) {
			}
		});
		return downloadDialog.show();
	}

	//on ActivityResult method
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				//get the extras that are returned from the intent
				String contents = intent.getStringExtra("SCAN_RESULT");
				String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				Toast toast = Toast.makeText(this, "Content:" + contents + " Format:" + format, Toast.LENGTH_LONG);
				toast.show();
			}
		}
	}*/

	
	
	public String acctsJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myacctsResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = acctsJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getacc", json);

		System.out.println(response);

		return response;
	}



	public class myAcctsFireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress2 = new ProgressDialog(QRScan.this);
			progress2.setCancelable(true);
			progress2.setTitle("Please wait");
			progress2.setMessage("Retrieving accounts...");
			progress2.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myacctsResponse(token);
//				System.out.println(response);
				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId"); 
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress2.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress2.cancel();
					new ResponseDialog(QRScan.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				}
				else if(mb_response.trim().equals("11") ){
					progress2.cancel();
					new ResponseDialog(QRScan.this, "Error !", "Unknown Error");
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress2.cancel();
					
					System.out.println(mbAcc);
					
					aliasAccDetails = mbAcc.split("`");

					List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

					System.out.println(aliasAccDetails.length);
						
						for(int a=0;a<aliasAccDetails.length-1;a++){
							StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 , 
							accNum = myAccToken.nextToken().trim();
							accName = myAccToken.nextToken().trim();
							accAvailBal = myAccToken.nextToken().trim();
							accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
							accLedgerBal = myAccToken.nextToken().trim();
							accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
							
							System.out.println(accNum);
							System.out.println(accName);
							
							HashMap<String, String> hm = new HashMap<String,String>();

							hm.put("accname", accName);
							hm.put("accnum",  accNum  );
							hm.put("bal", accAvailBal
									.replace("SLL","Le ")
									.replace("GHS","¢")
									.replace("USD","$")
									.replace("GBP","£")
									.replace("EUR","€")
									.replace("CNY","¥")
									.replace("JPY","¥"));
							hm.put("ledgerbal", "   " + accLedgerBal
									.replace("SLL","Le ")
									.replace("GHS","¢")
									.replace("USD","$")
									.replace("GBP","£")
									.replace("EUR","€")
									.replace("CNY","¥")
									.replace("JPY","¥"));
							
							theList.add(hm);

						}		
						
						String[] from = { "next","accname", "bal", "accnum", "ledgerbal" };
						int[] to = { R.id.flag,R.id.acc_name,R.id.avail_bal,R.id.acc_num, R.id.ledger_bal};

						SimpleAdapter adapter = new SimpleAdapter(QRScan.this, theList, R.layout.list_bills_from , from , to);

						spinner1.setAdapter(adapter);
					

					//				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
					//				loginIntent.putExtra("token", mb_token);
					//				loginIntent.putExtra("userName", mb_userName);
					//				loginIntent.putExtra("mb_acc_details", mb_acc_details); 
					//				startActivity(transDetailsIntent);
				}

			}
//			new fireMan().execute();

		}

	}



	public String fundsTransJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = fundsTransJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getalias", json);

		System.out.println(response);

		return response;
	}



	/*public class fireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(QRScan.this);
			progress.setCancelable(false);
			progress.setTitle("Please wait");
			progress.setMessage("Retrieving beneficiaries...");
			progress.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myResponse(token);

				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress.cancel();
					new ResponseDialog(QRScan.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				}
				else if(mb_response.trim().equals("19") ){
					progress.cancel();
					new ResponseDialog(QRScan.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds");
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress.cancel();

					System.out.println(mbAcc);

					//					StringTokenizer accDetToken = new StringTokenizer(string)
					aliasAccDetails = mbAcc.split(",");

					List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

					System.out.println(aliasAccDetails.length);

					for(int a=0;a<aliasAccDetails.length-1;a++){
						StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

						System.out.println(accDetToken.countTokens());

						beneName = accDetToken.nextToken().trim();
						beneAcc = accDetToken.nextToken().trim();

						System.out.println(beneName);
						System.out.println(beneAcc);

						HashMap<String, String> hm = new HashMap<String,String>();

						hm.put("aliasname", beneName);
						hm.put("aliasaccnum", beneAcc *//*"**********"+*//* *//*.lastIndexOf(0, -3) *//* );

						theList.add(hm);

					}

					String[] from = { "aliasname", "aliasaccnum" };

					// Ids of views in listview_layout
					int[] to = { R.id.alias_name,R.id.alias_acc_num};

					SimpleAdapter adapter2 = new SimpleAdapter(QRScan.this, theList, R.layout.list_to , from , to);

					spinner2.setAdapter(adapter2);


				}

			}


		}

	}*/











	// add items into spinner dynamically
	/*public void addItemsOnSpinner1() {

		aList = new ArrayList<HashMap<String,String>>();

		for(int a=0;a<accDetail.length;a++){
			StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

			if (a==0){
				loanProdCode = "Transfer From";
				loanProdCode = "";
				accAvailBal = "";
				accLedgerBal = "";
			}else{

			accNum = myAccToken.nextToken().trim();
			loanProdCode = myAccToken.nextToken().trim();
			accAvailBal = myAccToken.nextToken().trim();
			accLedgerBal = myAccToken.nextToken().trim();
			//			String d = myAccToken.nextToken().trim();
			System.out.println(accNum);
			System.out.println(loanProdCode);
			//			System.out.println(accAvailBal);
			//			System.out.println(accLedgerBal);

			HashMap<String, String> hm = new HashMap<String,String>();

			hm.put("accname", loanProdCode);
			hm.put("accnum",  accNum  );
			hm.put("bal", accAvailBal);
			hm.put("ledgerbal", "   " + accLedgerBal);
			//			hm.put("next", Integer.toString(next[0]) );

			aList.add(hm);


		}		


	}*/



	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
					//					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
					//					startActivity(loginIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}











	/*@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case REQUEST_WRITE_PERMISSION:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					takePicture();
				} else {
					Toast.makeText(QRScan.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
				}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PHOTO_REQUEST && resultCode == RESULT_OK) {
			launchMediaScanIntent();
			try {
				Bitmap bitmap = decodeBitmapUri(this, imageUri);
				if (detector.isOperational() && bitmap != null) {
					Frame frame = new Frame.Builder().setBitmap(bitmap).build();
					SparseArray<Barcode> barcodes = detector.detect(frame);
					for (int index = 0; index < barcodes.size(); index++) {
						Barcode code = barcodes.valueAt(index);
						scanResults.setText(scanResults.getText() + code.displayValue + "\n");

						//Required only if you need to extract the type of barcode
						int type = barcodes.valueAt(index).valueFormat;
						switch (type) {
							case Barcode.CONTACT_INFO:
								Log.i(LOG_TAG, code.contactInfo.title);
								break;
							case Barcode.EMAIL:
								Log.i(LOG_TAG, code.email.address);
								break;
							case Barcode.ISBN:
								Log.i(LOG_TAG, code.rawValue);
								break;
							case Barcode.PHONE:
								Log.i(LOG_TAG, code.phone.number);
								break;
							case Barcode.PRODUCT:
								Log.i(LOG_TAG, code.rawValue);
								break;
							case Barcode.SMS:
								Log.i(LOG_TAG, code.sms.message);
								break;
							case Barcode.TEXT:
								Log.i(LOG_TAG, code.rawValue);
								break;
							case Barcode.URL:
								Log.i(LOG_TAG, "url: " + code.url.url);
								break;
							case Barcode.WIFI:
								Log.i(LOG_TAG, code.wifi.ssid);
								break;
							case Barcode.GEO:
								Log.i(LOG_TAG, code.geoPoint.lat + ":" + code.geoPoint.lng);
								break;
							case Barcode.CALENDAR_EVENT:
								Log.i(LOG_TAG, code.calendarEvent.description);
								break;
							case Barcode.DRIVER_LICENSE:
								Log.i(LOG_TAG, code.driverLicense.licenseNumber);
								break;
							default:
								Log.i(LOG_TAG, code.rawValue);
								break;
						}
					}
					if (barcodes.size() == 0) {
						scanResults.setText("Scan Failed: Found nothing to scan");
					}
				} else {
					scanResults.setText("Could not set up the detector!");
				}
			} catch (Exception e) {
				Toast.makeText(this, "Failed to load Image", Toast.LENGTH_SHORT)
						.show();
				Log.e(LOG_TAG, e.toString());
			}
		}
	}

	private void takePicture() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File photo = new File(Environment.getExternalStorageDirectory(), "picture.jpg");
		imageUri = Uri.fromFile(photo);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(intent, PHOTO_REQUEST);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (imageUri != null) {
			outState.putString(SAVED_INSTANCE_URI, imageUri.toString());
			outState.putString(SAVED_INSTANCE_RESULT, scanResults.getText().toString());
		}
		super.onSaveInstanceState(outState);
	}

	private void launchMediaScanIntent() {
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(imageUri);
		this.sendBroadcast(mediaScanIntent);
	}

	private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
		int targetW = 600;
		int targetH = 600;
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;

		return BitmapFactory.decodeStream(ctx.getContentResolver()
				.openInputStream(uri), null, bmOptions);
	}*/







//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findItem(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		switch (item.getItemId()) {
//		// Respond to the action bar's Up/Home button
//		case android.R.id.home:
//			//	         NavUtils.navigateUpFromSameTask(this);S
//			QRScan.this.finish();
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
	@Override
	public void onBackPressed() {
		//		moveTaskToBack(true); 
		QRScan.this.finish();
	}



}

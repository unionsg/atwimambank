package com.unionsg.blink;

/**
 * Created by pato7755 on 4/4/16.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import cropdemo.MainActivity;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;


public class ChequeDeposit extends AppCompatActivity {

//    public static byte[] picByte;

    private static final int CAMERA_REQUEST_FRONT = 1888;
    private static final int CAMERA_REQUEST_BACK = 1889;

    private static Bitmap bitmap_frontChq = null;
    private static Bitmap bitmap_backChq = null;

    private static String chqNo = null;

    ImageView imageview_frontPreview;
    ImageView imageview_backPreview;
    Spinner bankspinner;
    TextView titleTextView;

    String mbAccDetails = null;
    String[] accDetail = null;
    ProgressDialog progress2;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String[] bankDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;
    String bankCode = null;
    String dispAmt = null;
    String bankName = null;
    String brCode = null;
    EditText pinEditText;

    ProgressDialog progress1;

    //    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;
    Dialog dialog = null;

    String accountNum = null;
    String accountName = null;

    ProgressDialog progress;
    int pos;
    String phoneNum = null;
    String amt = null;
    String pin = null;
    Double amtToBuy = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;

    String response = null;

    EditText chqNoEditText;
    EditText amtTextEdit;
    Button searchButton;
    Spinner spinner1;
    Button btn_completeChqdep;
    ProgressDialog progress3;

    static int cameraFlag;
    byte[] frontByte;
    public static byte[] picByte;
    Bitmap photo1;
    public static byte[] picByte2;
    Bitmap photo2;

    String base64Front = null;
    String base64Back = null;

    static String staticChqNum = "";
    static String staticBank = null;
    static String staticAmount = null;
    static String staticBankCode = null;
    static String staticAccDetails = null;
    static String staticBankDetails = null;
    static int staticAccNumPosition = 10000;
    static int staticBankPosition = 10000;

//    int currentapiVersion = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //inflate my view(make my UI visible)
        setContentView(R.layout.cheque_dep);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras().getString("token").trim();
        cameraFlag = getIntent().getExtras().getInt("cameraFlag");
        System.out.println("camera flag is now " + cameraFlag);

//        currentapiVersion = android.os.Build.VERSION.SDK_INT;


        chqNoEditText = (EditText) findViewById(R.id.chequeTextbox);
        amtTextEdit = (EditText) findViewById(R.id.amtTextBox);
        bankspinner = (Spinner) findViewById(R.id.bankspinner);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        imageview_frontPreview = (ImageView) findViewById(R.id.imageButton_front);
        imageview_backPreview = (ImageView) findViewById(R.id.imageButton_back);

        chqNoEditText.setText(staticChqNum);
        amtTextEdit.setText(staticAmount);

        if (staticChqNum.length()==0) {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myAcctsFireMan().execute();
            } else {
                showAlertDialog(ChequeDeposit.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        } else {

            populateBalances(staticAccDetails);
            populateBanks(staticBankDetails);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }


        /*if (staticBankPosition == 10000) {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new searchFireMan().execute();
            } else {
                showAlertDialog(ChequeDeposit.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        } else {

//            populateBanks(staticBankDetails);
        }*/


        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
                System.out.println("spinner position: " + position);
                staticAccNumPosition = position;

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


        bankspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v,
                                       int position, long id) {

//                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
//                System.out.println("spinner position: " + position);
//                staticAccNumPosition = position;

                bankName = ((TextView) v.findViewById(R.id.branchname)).getText().toString().trim();
                brCode = ((TextView) v.findViewById(R.id.bankcode)).getText().toString().trim();
                staticBankCode = brCode;
                staticBank = bankName;
                staticBankPosition = position;

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


        if (cameraFlag == 0) {
            imageview_frontPreview.setImageDrawable(getResources().getDrawable(R.mipmap.ic_camera));
            imageview_frontPreview.setTag(R.mipmap.ic_camera);
            imageview_backPreview.setImageDrawable(getResources().getDrawable(R.mipmap.ic_camera));
            imageview_backPreview.setTag(R.mipmap.ic_camera);
        } else if (cameraFlag == 1) {

            photo1 = BitmapFactory.decodeByteArray(picByte, 0, picByte.length);
            imageview_frontPreview.setImageBitmap(photo1);
            imageview_backPreview.setImageDrawable(getResources().getDrawable(R.drawable.img));
//            imageview_frontPreview.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    savePic(imageBytes1, "1");
//                    File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + summary + "1.jpg");  // -> filename = maven.pdf
//                    Uri path = Uri.fromFile(pdfFile);
//                    Intent intent = new Intent();
//                    intent.setAction(Intent.ACTION_VIEW);
//                    intent.setDataAndType(path, "image/*");
////					intent.setDataAndType(Uri.parse("filepath"), "application/pdf");
//                    startActivity(intent);
//
//                }
//            });
        } else if (cameraFlag == 2) {

            photo1 = BitmapFactory.decodeByteArray(picByte, 0, picByte.length);
            imageview_frontPreview.setImageBitmap(photo1);
            photo2 = BitmapFactory.decodeByteArray(picByte2, 0, picByte2.length);
            imageview_backPreview.setImageBitmap(photo2);
        }

        imageview_frontPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("id for front pic is " + imageview_frontPreview.getTag());
                System.out.println("id for back pic is " + imageview_backPreview.getTag());

                if (chqNoEditText.getText().toString().isEmpty()) {
                    chqNoEditText.setError("Enter cheque number");
                } else if (amtTextEdit.getText().toString().isEmpty()) {
                    amtTextEdit.setError("Enter amount on cheque");
                } else {

                    staticChqNum = chqNoEditText.getText().toString();
//                    staticBank = searchEditText.getText().toString();
                    staticAmount = amtTextEdit.getText().toString();

                    Intent intent = new Intent(ChequeDeposit.this, MainActivity.class);
                    intent.putExtra("token", token);
                    intent.putExtra("cameraFlag", 0);
                    startActivityForResult(intent, CAMERA_REQUEST_FRONT);
                    finish();
                }
            }
        });


        //now get my back preview button by referencing its id in the layout resources
//        Button btn_backPreview = (Button)findViewById(R.id.button_backPreview);

        imageview_backPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (amtTextEdit.getText().toString().isEmpty()) {
                    amtTextEdit.setError("Enter amount on cheque");
                } else if (chqNoEditText.getText().toString().isEmpty()) {
                    chqNoEditText.setError("Enter cheque number");
                } else {

                    staticChqNum = chqNoEditText.getText().toString();
//                    staticBank = searchEditText.getText().toString();
                    staticAmount = amtTextEdit.getText().toString();

                    if (checkCameraHardware(getApplicationContext())) {
                        /*Intent cameraIntent = new Intent(ChequeDeposit.this, CameraActivity.class);
                        cameraIntent.putExtra("token", token);
                        cameraIntent.putExtra("cameraFlag", 1);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST_BACK);
                        ChequeDeposit.this.finish();*/

                        Intent intent = new Intent(ChequeDeposit.this, MainActivity.class);
                        intent.putExtra("token", token);
                        intent.putExtra("cameraFlag", 1);
                        startActivityForResult(intent, CAMERA_REQUEST_BACK);
                        finish();
                    }
                }
            }
        });

        //now get my complete button by referencing its id in the layout resources
        btn_completeChqdep = (Button) findViewById(R.id.button_completeChqDep);


        btn_completeChqdep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                chqNo = chqNoEditText.getText().toString();
                amt = amtTextEdit.getText().toString();
                brCode = staticBankCode;

                buildDialog(R.style.DialogAnimation);

                /*ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new confirmFireMan().execute();
                } else {
                    showAlertDialog(ChequeDeposit.this, "No Internet Connection",
                            "You don't have internet connection", false);
                }*/

            }
        });


    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_FRONT && resultCode == RESULT_OK) {
            System.out.println("i am here");
        } else if (requestCode == CAMERA_REQUEST_BACK && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bitmap_backChq = photo;
            imageview_backPreview = (ImageView) findViewById(R.id.imageButton_back);
            imageview_backPreview.setImageBitmap(photo);
        }
    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }



    public void populateBalances(String balances) {

        aliasAccDetails = balances.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            accountNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = accAvailBal;
            //			String d = myAccToken.nextToken().trim();
            System.out.println(accountNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

            if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
                hm.put("accname", accName.substring(0, 17) + "...");
            } else {
                hm.put("accname", accName);
            }
            hm.put("accnum", accountNum);
            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            theList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        SimpleAdapter adapter = new SimpleAdapter(ChequeDeposit.this, theList, R.layout.list_bills_from, from, to);

        spinner1.setAdapter(adapter);

        spinner1.setSelection(staticAccNumPosition);

    }


    public void populateBanks(String banks) {

        bankDetails = banks.split(",");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(bankDetails.length);

        for (int a = 0; a <= bankDetails.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(bankDetails[a], "~");
            bankCode = myAccToken.nextToken().trim();
            bankName = myAccToken.nextToken().trim();

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("branchname", bankName);
            hm.put("bankcode", bankCode);

            theList.add(hm);

        }

        String[] from = {"branchname", "bankcode"};
        int[] to = {R.id.branchname, R.id.bankcode};

        SimpleAdapter adapter2 = new SimpleAdapter(ChequeDeposit.this, theList, R.layout.list_ach, from, to);

        bankspinner.setAdapter(adapter2);

        bankspinner.setSelection(staticBankPosition);

    }


    public String confirmJSON(String bnkCode, String accr, String chqNum, String amount, String frontByte, String backByte, String authToken, String myPin) throws UnsupportedEncodingException {


//                        "bankCode="+bnkCode+
//                                "&accNum="+accr+
//                                "&chqNum="+chqNum+
//                                "&amt="+amount+
//                                "&image1="+ URLEncoder.encode(frontByte, "UTF-8") +
//                                "&image2=" + URLEncoder.encode(backByte, "UTF-8") +
//                                "&authToken=" + authToken
//                        ;
        // Sending side
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        photo1.compress(Bitmap.CompressFormat.JPEG, 40, baos);
        byte[] frontPicByte = baos.toByteArray();
        base64Front = Base64.encodeToString(frontPicByte, Base64.DEFAULT);


        ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
        photo2.compress(Bitmap.CompressFormat.JPEG, 40, baos2);
        byte[] backPicByte = baos2.toByteArray();
        base64Back = Base64.encodeToString(backPicByte, Base64.DEFAULT);


        return "bankCode=" + bnkCode +
                "&accNum=" + accr +
                "&chqNum=" + chqNum +
                "&amt=" + amount +
                "&image1=" + URLEncoder.encode(base64Front, "UTF-8") +
                "&image2=" + URLEncoder.encode(base64Back, "UTF-8") +
                "&authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8") +
                "&pin=" + myPin;
    }


    private void buildDialog(int animationSource) {

        dialog = new Dialog(ChequeDeposit.this);
        dialog.setContentView(R.layout.confirm_cheque_dep);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
        TextView senderBankTV = (TextView) dialog.findViewById(R.id.sender_bank);
        TextView chequeNumTV = (TextView) dialog.findViewById(R.id.cheque_num);
        TextView amtTV = (TextView) dialog.findViewById(R.id.amt);


//          device ip
        pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);


        accountNumTV.setText(accountNum);
        senderBankTV.setText(staticBank);
        chequeNumTV.setText(chqNo);
        amtTV.setText(amt);
//        toTV.setText(to);


        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pin = pinEditText.getText().toString();
                if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new confirmFireMan().execute();
                    } else {
                        showAlertDialog(ChequeDeposit.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                    //  new fireMan().execute();
                    dialog.dismiss();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    public String confirmResponse(String a1, String a2, String a3, String a4, String a5, String a6, String a7, String a8) throws IOException {
        SendRequest sendReqObj = new SendRequest();


        String json = confirmJSON(a1, a2, a3, a4, a5, a6, a7, a8);
        String response = sendReqObj.postReqCheque(GlobalCodes.myUrl + "chqdep", json, 100);

//        System.out.println(response);

        return response;
    }


    public class confirmFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress3 = new ProgressDialog(ChequeDeposit.this);
            progress3.setCancelable(false);
            progress3.setTitle("Please wait");
            progress3.setMessage("Sending request...");
            progress3.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                response = confirmResponse(staticBankCode, accountNum, chqNo, amt, base64Front, base64Back, token, pin);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        //					System.out.println(mb_userId);
//					System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.println("json error");
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("io error ");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            System.out.println("RESPONSE: " + response);
            System.out.println(mb_token);
            System.out.println(mb_response);
            System.out.println(mb_userId);
            System.out.println(mbAcc);

            if (e != null) {
                progress3.cancel();
                new ResponseDialog(ChequeDeposit.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress3.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);
                    progress3.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Transaction unsuccessful", "Sorry, you do not have enough funds").showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress3.cancel();
                    new ResponseDialog(ChequeDeposit.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("21")) {
                    progress3.cancel();
                    new ResponseDialog(ChequeDeposit.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    progress3.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Cheque deposit unsuccessful", "Sorry a technical problem occured").showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress3.cancel();
                    System.out.println("mbAcc is " + mbAcc);
                    new ResponseDialog(ChequeDeposit.this, "Successful", "Request has been sent for approval").showDialog();


                }

            }

        }

    }


    public String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "bankCodesChq", json);

        System.out.println(response);

        return response;

    }


    public class searchFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress1 = new ProgressDialog(ChequeDeposit.this);
            progress1.setCancelable(false);
            progress1.setTitle("Please wait");
            progress1.setMessage("Retrieving banks...");
            progress1.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress1.cancel();
                new ResponseDialog(ChequeDeposit.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress1.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);

                    progress1.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Not Found", mbAcc);
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress1.cancel();
                    new ResponseDialog(ChequeDeposit.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress1.cancel();
                    /*if(mbAcc.isEmpty()){
                        new ResponseDialog(TopUp.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds");
					}
					else{*/
                    System.out.println(mbAcc);

//                    dialog = new Dialog(ChequeDeposit.this);
//                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
////                    dialog.setTitle("Long press to select recipient's bank");
//                    dialog.setContentView(R.layout.selectbank_dialog);
//                    dialog.setCancelable(true);
//                    titleTextView = (TextView) dialog.findViewById(R.id.titleText);
//                    titleTextView.setText("Select bank");
//                    ListView list = (ListView) dialog.findViewById(R.id.ach_banklist);

                    staticBankDetails = mbAcc;

                    aliasAccDetails = mbAcc.split(",");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a <= aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
                        bankCode = myAccToken.nextToken().trim();
                        bankName = myAccToken.nextToken().trim();


                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("branchname", bankName);
                        hm.put("bankcode", bankCode);

                        theList.add(hm);

                    }

                    String[] from = {"branchname", "bankcode"};
                    int[] to = {R.id.branchname, R.id.bankcode};


                    SimpleAdapter adapter2 = new SimpleAdapter(ChequeDeposit.this, theList, R.layout.list_ach, from, to);

                    bankspinner.setAdapter(adapter2);

//                    bankspinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//                        @Override
//                        public void onItemClick(AdapterView<?> arg0, View v, int arg2,
//                                                long arg3) {
//                            // TODO Auto-generated method stub
//                            bankName = ((TextView) v.findViewById(R.id.branchname)).getText().toString().trim();
//                            brCode = ((TextView) v.findViewById(R.id.bankcode)).getText().toString().trim();
//                            searchEditText.setText(bankName);
//                            staticBankCode = brCode;
//
//
//
//                        }
//                    });


                }

            }

        }

    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "accounts", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(ChequeDeposit.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                new ResponseDialog(ChequeDeposit.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(ChequeDeposit.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(ChequeDeposit.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    btn_completeChqdep.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(ChequeDeposit.this, "Error !", "Unknown Error").showDialog();
                    btn_completeChqdep.setEnabled(false);
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    staticAccDetails = mbAcc;

                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        accountNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = accAvailBal;
                        //			String d = myAccToken.nextToken().trim();
                        System.out.println(accountNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
                            hm.put("accname", accName.substring(0, 17) + "...");
                        } else {
                            hm.put("accname", accName);
                        }
                        hm.put("accnum", accountNum);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(ChequeDeposit.this, theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);


                    //				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
                    //				loginIntent.putExtra("token", mb_token);
                    //				loginIntent.putExtra("userName", mb_userName);
                    //				loginIntent.putExtra("mb_acc_details", mb_acc_details);
                    //				startActivity(transDetailsIntent);
                }

                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new searchFireMan().execute();
                } else {
                    showAlertDialog(ChequeDeposit.this, "No Internet Connection",
                            "You don't have internet connection", false);
                }

            }

        }

    }


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    staticAmount = "";
                    staticBankCode = "";
                    staticBank = "";
                    staticChqNum = "";
                    staticAccDetails = "";
                    staticAccNumPosition = 10000;
                    staticBankPosition = 10000;
                    cameraFlag = 0;
                    ChequeDeposit.this.finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    //    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        menu.findItem(R.id.refresh).setVisible(false);
//        return true;
//    }
//
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                staticAmount = "";
                staticBankCode = "";
                staticBank = "";
                staticChqNum = "";
                staticAccDetails = "";
                staticAccNumPosition = 10000;
                staticBankPosition = 10000;
                cameraFlag = 0;
                ChequeDeposit.this.finish();
                System.out.println("onOptionsItemSelected");
                return true;
        }
//		return super.onOptionsItemSelected(item);
        return false;
    }

    public void onBackPressed() {
        //		moveTaskToBack(true);
        ChequeDeposit.this.finish();
        staticAmount = "";
        staticBankCode = "";
        staticBank = "";
        staticChqNum = "";
        staticAccDetails = "";
        staticAccNumPosition = 10000;
        staticBankPosition = 10000;
        cameraFlag = 0;
        System.out.println("onBackPressed");

    }


    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context) {
        // this device has a camera
// no camera on this device
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }


}

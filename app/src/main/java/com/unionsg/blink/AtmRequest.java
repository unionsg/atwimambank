package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class AtmRequest extends AppCompatActivity {

    private Spinner spinner1;
    //	private Spinner statementTypeSpinner;
    Spinner branchSpinner;
    private Button confirmButton;
    Dialog dialog;
    TextView titleTextView;
    TextView warningTextView;

    String pin = null;

    String mbAccDetails = null;
    String[] accDetail = null;

    String from = null;
    String to = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String email = null;
    String[] aliasAccDetails = null;
    String telcos = null;
    String aliasAcc = null;
    String statementTypes = "Select statement type~ORDINARY~VISA~ELECTRONIC";
    String stmtType = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;

    ProgressDialog progress1;
    ProgressDialog progress2;
    int pos;
    String branchCode = null;
    String branchName = null;
    String brName = null;
    String brCode = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;
    TextView branchTextView;

    String response = null;
    String balFromFile = null;
    LinearLayout branchLayout = null;

//	Handler handler;
//	Runnable r;


    private SimpleDateFormat dateFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atm_request);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();
        //		accDetail = mbAccDetails.split(",");

        confirmButton = (Button) findViewById(R.id.confirm_button);
        branchTextView = (TextView) findViewById(R.id.selectbranch);
        branchLayout = (LinearLayout) findViewById(R.id.branch_layout);
        branchSpinner = (Spinner) findViewById(R.id.branch_spinner);
        warningTextView = (TextView) findViewById(R.id.warning);
        spinner1 = (Spinner) findViewById(R.id.from_spinner);


//		balFromFile = readBalancesFromFile(AtmRequest.this);
//
//		if (!balFromFile.equals("")) {
//			System.out.println("balances not empty");
//			mbAcc = balFromFile;
//			System.out.println("MBACC balances: " + mbAcc);
//			populateAccounts();
//		} else {
//			new myAcctsFireMan().execute();
//		}


        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new myAcctsFireMan().execute();
        } else {
            showAlertDialog(AtmRequest.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                buildDialog(R.style.DialogAnimation);

            }
        });


//		//timeout code
//		handler = new Handler();
//		r = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				Toast.makeText(AtmRequest.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//				Intent loginIntent = new Intent(AtmRequest.this, LoginActivity.class);
//				loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				startActivity(loginIntent);
//				finish();
//			}
//		};
//		startHandler();
//


    }

//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//		super.onUserInteraction();
//		stopHandler();//stop first and then start
//		startHandler();
//	}
//
//	public void stopHandler() {
//		handler.removeCallbacks(r);
//	}
//
//	public void startHandler() {
//		handler.postDelayed(r, MainMenu.timeOutMinutes);
//	}
//

    private void buildDialog(int animationSource) {

        dialog = new Dialog(AtmRequest.this);
        dialog.setContentView(R.layout.confirm_atm_reqt);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
        TextView branchTV = (TextView) dialog.findViewById(R.id.branch);

        final EditText pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);

        accountNumTV.setText(accountNum);
        branchTV.setText(brName);

//
        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                pin = pinEditText.getText().toString();

                if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(AtmRequest.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                    dialog.dismiss();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    private String readBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public String stmtJSON(String authToken, String accnum, String branch) {
        return "authToken=" + authToken +
                "&accNum=" + accnum +
                "&branchCode=" + branch;
    }


    public String myResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = stmtJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "atmreqt", json);

        System.out.println(response);

        return response;
    }


    public void populateAccounts() {

        System.out.println(mbAcc);

//					staticAccDetails = mbAcc;

        aliasAccDetails = mbAcc.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            accountNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = accAvailBal;
            //			String d = myAccToken.nextToken().trim();
            System.out.println(accountNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

            if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
                hm.put("accname", accName.substring(0, 17) + "...");
            } else {
                hm.put("accname", accName);
            }
            hm.put("accnum", accountNum);
            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            theList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        SimpleAdapter adapter = new SimpleAdapter(AtmRequest.this, theList, R.layout.list_bills_from, from, to);

        spinner1.setAdapter(adapter);

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new branchesFireMan().execute();
        } else {
            showAlertDialog(AtmRequest.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }

    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress1 = new ProgressDialog(AtmRequest.this);
            progress1.setCancelable(false);
            progress1.setTitle("Please wait");
            progress1.setMessage("Sending request...");
            progress1.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = myResponse(token, accountNum, brCode);

                if (response.equals("22")) {
                    mb_response = response;
                } else {

                    try {
                        if (!response.isEmpty()) {
                            JSONObject jsonObject = new JSONObject(response);
                            id = jsonObject.getString("id");
                            creationTime = jsonObject.getString("creationTime");
                            lastModificationDate = jsonObject.getString("lastModificationDate");
                            mb_token = jsonObject.getString("tokenId");
                            mb_response = jsonObject.getString("mb_response");
                            mbAcc = jsonObject.getString("mb_accDetails");

                            System.out.println(mbAcc);

                        } else {
                            mb_response = "66";
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress1.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress1.cancel();
                    new ResponseDialog(AtmRequest.this, "No Data Found For This User", "Please login again").showDialog();
                    //					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("66")) {
                    progress1.cancel();
                    new ResponseDialog(AtmRequest.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("22")) {
                    progress1.cancel();
                    new ResponseDialog(AtmRequest.this, "Sorry", "You have already requested for an ATM card. You will be notified shortly on the progress").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress1.cancel();

                    new ResponseDialog(AtmRequest.this, "Sent successfully", mbAcc).showDialog();
                }

            }

        }

    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "accounts", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(AtmRequest.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                new ResponseDialog(AtmRequest.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "No Data Found For This User", "Please login again");
                    finish();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "Error !", "Unknown Error");
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

//					staticAccDetails = mbAcc;

                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        accountNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = accAvailBal;
                        //			String d = myAccToken.nextToken().trim();
                        System.out.println(accountNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
                            hm.put("accname", accName.substring(0, 17) + "...");
                        } else {
                            hm.put("accname", accName);
                        }
                        hm.put("accnum", accountNum);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(AtmRequest.this, theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);


                    //				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
                    //				loginIntent.putExtra("token", mb_token);
                    //				loginIntent.putExtra("userName", mb_userName);
                    //				loginIntent.putExtra("mb_acc_details", mb_acc_details);
                    //				startActivity(transDetailsIntent);
                }

            }

            new branchesFireMan().execute();
        }

    }


    public String branchesJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String branchesResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = branchesJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getbranches", json);

        System.out.println(response);

        return response;
    }


    public class branchesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(AtmRequest.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving branches...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = branchesResponse(token);
                //				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "No Data Found For This User", "Please login again");
                    //					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(AtmRequest.this, "Error !", "Unknown Error");
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);


//					dialog = new Dialog(StatementReqt.this);
//					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//					dialog.setContentView(R.layout.selectbank_dialog);
//					dialog.setCancelable(true);
//					titleTextView = (TextView) dialog.findViewById(R.id.titleText);
//					titleTextView.setText("Select pickup branch");
                    ListView list = (ListView) findViewById(R.id.ach_banklist);
                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
                        branchCode = myAccToken.nextToken().trim();
                        branchName = myAccToken.nextToken().trim();

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("branchname", branchName);
                        hm.put("bankcode", branchCode);
                        theList.add(hm);

                    }

                    String[] from = {"branchname", "bankcode"};
                    int[] to = {R.id.branchname, R.id.bankcode};


                    SimpleAdapter adapter2 = new SimpleAdapter(AtmRequest.this, theList, R.layout.list_ach, from, to);


                    branchSpinner.setAdapter(adapter2);

                    branchSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            // TODO Auto-generated method stub
                            brName = ((TextView) view.findViewById(R.id.branchname)).getText().toString().trim();
                            brCode = ((TextView) view.findViewById(R.id.bankcode)).getText().toString().trim();
                            System.out.println(brName);
                            System.out.println(brCode);
//							searchEditText.setText(brName);
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            // Do nothing

                        }
                    });


                }

            }

        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();// It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is
        // present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                AtmRequest.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //	   moveTaskToBack(true);
        AtmRequest.this.finish();
    }


}

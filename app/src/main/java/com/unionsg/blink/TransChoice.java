package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;
//import static com.unionsg.blink.R.id.aliasName;

public class TransChoice extends AppCompatActivity {

    String summary = null;
    String date = null;
    String time = null;
    String amount = null;
    String amount2 = null;
    String batchNum = null;
    String branch = null;
    String respCode = null;
    int count = 0;
    ImageView imageView1;
    LinearLayout layout1;
    LinearLayout layout2;
    ImageView imageView2;
    TextView viewDoc = null;
    String token = null;
    //    byte[] imageBytes = null;
    String pic1 = "";
    String pic2 = "";
    ImageButton editNarrationButton = null;
    EditText narrEditText;
    Button viewDocTextView;
    ProgressDialog progress;
    String response = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;
    String transNum = null;
    String valueDate = null;
    String narration = null;
    String doc_reff = null;
    String contra = null;
    Dialog dialog;
    StringTokenizer picToken;

    TextView summaryTextview;

    byte[] imageBytes1 = null;
    byte[] imageBytes2 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaction_choice);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        summary = getIntent().getExtras().getString("transSumKey").trim();
        date = getIntent().getExtras().getString("dateKey").trim();
        time = getIntent().getExtras().getString("timeKey").trim();
        amount = getIntent().getExtras().getString("amtKey").trim();
        branch = getIntent().getExtras().getString("branch").trim();
        transNum = getIntent().getExtras().getString("transNum").trim();
        token = getIntent().getExtras().getString("token").trim();
        batchNum = getIntent().getExtras().getString("batchno").trim();
        valueDate = getIntent().getExtras().getString("valuedate").trim();
        doc_reff = getIntent().getExtras().getString("doc_reff").trim();
        contra = getIntent().getExtras().getString("contra").trim();


        System.out.println("batch number is " + batchNum);
        System.out.println("token is " + token);

        TextView timeTextView = (TextView) findViewById(R.id.time);
        TextView amtTextview = (TextView) findViewById(R.id.amt);
        TextView amt2Textview = (TextView) findViewById(R.id.theAmt);
        TextView titleTextview = (TextView) findViewById(R.id.trans_title);
        TextView branchTextview = (TextView) findViewById(R.id.branch);
        TextView valueDateTextview = (TextView) findViewById(R.id.valuedate);
        TextView dateTextview = (TextView) findViewById(R.id.date);
		TextView docrefTextview = (TextView) findViewById(R.id.doc_ref);
        TextView contraTextview = (TextView) findViewById(R.id.contra);

        summaryTextview = (TextView) findViewById(R.id.summary);
        ImageView pic = (ImageView) findViewById(R.id.pic);
        imageView1 = (ImageView) findViewById(R.id.imageview1);
        imageView2 = (ImageView) findViewById(R.id.imageview2);
        layout1 = (LinearLayout) findViewById(R.id.ll1);
        layout2 = (LinearLayout) findViewById(R.id.ll2);
        editNarrationButton = (ImageButton) findViewById(R.id.change_summary);
        viewDocTextView = (Button) findViewById(R.id.disp_pdf);

        Typeface regularText = Typeface.createFromAsset(getAssets(), "HelveticaRegular.otf");
        Typeface mediumText = Typeface.createFromAsset(getAssets(), "HelveticaMedium.otf");
        timeTextView.setTypeface(regularText);
        amtTextview.setTypeface(mediumText);
        dateTextview.setTypeface(regularText);
        summaryTextview.setTypeface(regularText);
        valueDateTextview.setTypeface(regularText);
        amt2Textview.setTypeface(regularText);
        titleTextview.setTypeface(regularText);
        branchTextview.setTypeface(regularText);
        docrefTextview.setTypeface(regularText);
        contraTextview.setTypeface(regularText);

        timeTextView.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));
        amtTextview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.white_color));
        dateTextview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));
        branchTextview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));
        docrefTextview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));
        contraTextview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));
        //		date2Textview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));
        amt2Textview.setTextColor(amtTextview.getContext().getResources().getColor(R.color.positive_bal));

//        if (summary.length() >= 18) {
//            summary = summary.substring(0, 16) + "...";
//        }
//        if (summary.length() > 18) {
//            summaryTextview.setText(summary.substring(0, 16) + "...");
////            hm.put("summary", transSummary.substring(0, 17) + "...");
//        } else {
//
//        }
        summaryTextview.setText(summary);
        dateTextview.setText(date);
        valueDateTextview.setText(valueDate);
        amt2Textview.setText(amount);
        amtTextview.setText(amount);
        timeTextView.setText(time);
        branchTextview.setText(branch);
        docrefTextview.setText(doc_reff);
        contraTextview.setText(contra);

        amount = (String) amtTextview.getText();
        amount2 = (String) amt2Textview.getText();

        if (amount.contains("-")) {
            amtTextview.setTextColor(Color.RED);
            pic.setImageResource(R.drawable.out);
        } else {
            amtTextview.setTextColor(this.getResources().getColor(R.color.white_color));
            pic.setBackgroundResource(R.drawable.in);
        }

        if (amount2.contains("-")) {
            amt2Textview.setTextColor(Color.RED);
            //			pic.setImageResource(R.drawable.out);
        } else {
            amt2Textview.setTextColor(this.getResources().getColor(R.color.positive_bal));
            //		pic.setBackgroundResource(R.drawable.in);
        }


        editNarrationButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                dialog = new Dialog(TransChoice.this, R.style.CustumDialog);
                //dialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
                dialog.setContentView(R.layout.change_narr_dialog);
                dialog.setTitle("EDIT TRANSACTION NARRATION ");
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(false);
                narrEditText = (EditText) dialog.findViewById(R.id.narr_edittext);
                Button confirmChange = (Button) dialog.findViewById(R.id.confirm_change);
                Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);


                confirmChange.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        narration = narrEditText.getText().toString().trim();

                        if (narration.length() == 0) {
                            narrEditText.setError("Please enter desired narration");
                        } else {
                            dialog.cancel();

                            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                            Boolean isInternetPresent = cd.isConnectingToInternet();
                            if (isInternetPresent) {
                                new fireMan().execute();
                            } else {
                                showAlertDialog(TransChoice.this, "No Internet Connection",
                                        "You don't have internet connection.", false);
                            }

                            // new fireMan().execute();
                        }

                    }
                });

                cancelButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });


                dialog.show();


            }
        });


//                        try {
//                    showPdf();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//

//        viewDocTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    openPdf();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        viewDocTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new retrieveDocFireMan().execute();
                } else {
                    showAlertDialog(TransChoice.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }

               // new retrieveDocFireMan().execute();

                /*try {
                    showPdf();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }*/
            }
        });

        //call in onCreate
        //   setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(TransChoice.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(TransChoice.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//      //  Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String fundsTransJSON(String transno, String newNarr, String authToken) throws UnsupportedEncodingException {
        return "transNum=" + transno +
                "&narration=" + newNarr +
                "&authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");
    }


    public String myResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "changeNarration", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(TransChoice.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(transNum, narration, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);
                    new ResponseDialog(TransChoice.this, "Unsuccessful", mbAcc).showDialog();

                    progress.cancel();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(TransChoice.this, "Unsuccessful", mbAcc).showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(TransChoice.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    new ResponseDialog(TransChoice.this, "Successful", mbAcc).showDialog();
                    System.out.println(mbAcc);
                    summaryTextview.setText(narration);


                }

            }


        }

    }

    Context context = TransChoice.this;

    public void showPdf() throws Exception {

        try {
//			byte[] decodedString = Base64.decode(pic, Base64.DEFAULT);
////								byte[] picByte = pic.getBytes();
//			Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
////			if (bitmap != null) {
//				photoImageView.setImageBitmap(bitmap);
            Toast.makeText(TransChoice.this, "Please scroll down", Toast.LENGTH_LONG).show();

            imageBytes1 = Base64.decode(pic1, Base64.DEFAULT);
            imageBytes2 = Base64.decode(pic2, Base64.DEFAULT);

            layout1.setVisibility(View.VISIBLE);
            layout2.setVisibility(View.VISIBLE);
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.VISIBLE);

            Bitmap bitmap1 = BitmapFactory.decodeByteArray(imageBytes1, 0, imageBytes1.length);
            imageView1.setImageBitmap(bitmap1);
            Bitmap bitmap2 = BitmapFactory.decodeByteArray(imageBytes2, 0, imageBytes2.length);
            imageView2.setImageBitmap(bitmap2);

            imageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePic(imageBytes1, "1");
                    File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + summary + "1.jpg");  // -> filename = maven.pdf
//                    Uri path = Uri.fromFile(pdfFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);

                    Uri apkURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", pdfFile);
                    intent.setDataAndType(apkURI, "image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(intent);

                }
            });
            imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePic(imageBytes2, "2");
                    File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + summary + "2.jpg");  // -> filename = maven.pdf
//                    Uri path = Uri.fromFile(pdfFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);

                    Uri apkURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", pdfFile);
                    intent.setDataAndType(apkURI, "image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(intent);

                }
            });

        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }


    }

    public void showPdf1() throws Exception {

        try {
            Toast.makeText(TransChoice.this, "Please scroll down", Toast.LENGTH_LONG).show();

            imageBytes1 = Base64.decode(pic1, Base64.DEFAULT);

            Bitmap bitmap1 = BitmapFactory.decodeByteArray(imageBytes1, 0, imageBytes1.length);
            layout1.setVisibility(View.VISIBLE);
            imageView1.setVisibility(View.VISIBLE);
            imageView1.setImageBitmap(bitmap1);

            imageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePic(imageBytes1, "1");
                    File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + summary + "1.jpg");  // -> filename = maven.pdf
//                    Uri path = Uri.fromFile(pdfFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);

                    Uri apkURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", pdfFile);
                    intent.setDataAndType(apkURI, "image/*");
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivity(intent);

                }
            });


        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }


    }


    public void showPdf2() throws Exception {

        try {
//			byte[] decodedString = Base64.decode(pic, Base64.DEFAULT);
////								byte[] picByte = pic.getBytes();
//			Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
////			if (bitmap != null) {
//				photoImageView.setImageBitmap(bitmap);
            Toast.makeText(TransChoice.this, "Please scroll down", Toast.LENGTH_LONG).show();

            imageBytes2 = Base64.decode(pic2, Base64.DEFAULT);

            Bitmap bitmap2 = BitmapFactory.decodeByteArray(imageBytes2, 0, imageBytes2.length);
            imageView2.setImageBitmap(bitmap2);

            imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    savePic(imageBytes2, "2");
                    File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + summary + "2.jpg");  // -> filename = maven.pdf
                    Uri path = Uri.fromFile(pdfFile);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(path, "image/*");
//					intent.setDataAndType(Uri.parse("filepath"), "application/pdf");
                    startActivity(intent);

                }
            });

        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }


    }


    public void savePic(byte[] image, String num) {

        File dir = Environment.getExternalStorageDirectory();

        File data = new File(dir, summary + num + ".jpg");
        OutputStream op = null;
        try {
            op = new FileOutputStream(data);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        try {
            op.write(image);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

    }


    public String transEnqJSON(String authToken, String batchNum) {
        return "authToken=" + authToken +
                "&batchno=" + batchNum;
    }

    public String myResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = transEnqJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "retrieveDoc", json);

        System.out.println(response);

        return response;
    }


    public class retrieveDocFireMan extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(TransChoice.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token, batchNum);
                if (!response.isEmpty()) {
                    picToken = new StringTokenizer(response, "`");
                respCode = picToken.nextToken();
                } else {
                    response = "66";
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else if (response.trim().equals("66")) {
                progress.cancel();
                new ResponseDialog(TransChoice.this, "No response", "Please try again").showDialog();
            }else {
                super.onPostExecute(result);


//				pic1 = token.nextToken();
//				pic2 = token.nextToken();




				/*if (respCode.trim().equals("12")){
                    progress.cancel();
					System.out.println("pic2 is " + pic2);
					pic1 = picToken.nextToken();
					pic2 = picToken.nextToken();
					try {
						showPdf2();
					} catch (Exception e) {
						e.printStackTrace();
					}
					Toast.makeText(TransChoice.this, "Sorry, there is only one document for this transaction", Toast.LENGTH_LONG).show();

				}
				else if (respCode.trim().equals("21")){
					progress.cancel();
					System.out.println("pic1 is " + pic1);
					pic1 = picToken.nextToken();
					pic2 = picToken.nextToken();
					try {
						showPdf1();
					} catch (Exception e) {
						e.printStackTrace();
					}
					Toast.makeText(TransChoice.this, "Sorry, there is only one document for this transaction", Toast.LENGTH_LONG).show();

				}*/
                if (respCode.trim().equals("11")) {

                    progress.cancel();
                    Toast.makeText(TransChoice.this, "Sorry, there is no document for this transaction", Toast.LENGTH_LONG).show();

                } else if (respCode.trim().equals("00")) {
                    System.out.println("picTokens: " + picToken.countTokens());
                    progress.cancel();
                    if (picToken.countTokens() < 1) {
                        progress.cancel();
                        pic1 = picToken.nextToken();
                        try {
                            showPdf1();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (picToken.countTokens() == 0) {
                        progress.cancel();
                        Toast.makeText(TransChoice.this, "Sorry, there is no document for this transaction", Toast.LENGTH_LONG).show();
                    } else {
                        System.out.println("there are two tokens");
                        pic1 = picToken.nextToken().trim();
                        pic2 = picToken.nextToken().trim();
                        System.out.println("pic1 length:" + pic1.length());
                        System.out.println("pic2 length: " + pic2.length());
                        if (pic1.equals("null") && pic2.equals("null")) {
                            Toast.makeText(TransChoice.this, "Sorry, there is no document for this transaction", Toast.LENGTH_LONG).show();
                        } else {
                            System.out.println("pic1 is " + pic1);
                            System.out.println("pic2 is " + pic2);

                            try {
                                showPdf();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }

            }

        }

    }





    /*@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void openPdf() throws FileNotFoundException {

        try {


            File myFile = new File(Environment.getExternalStorageDirectory(), "rules.pdf");
            Bitmap bitmap = Bitmap.createBitmap(400, 400, Bitmap.Config.ARGB_4444);
            int currentPage = 0;
            try {
                myFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            PdfRenderer renderer = new PdfRenderer(ParcelFileDescriptor.open(myFile, ParcelFileDescriptor.MODE_READ_ONLY));

            if(currentPage<0){
                currentPage = 0;
            }else if(currentPage > renderer.getPageCount()){
                currentPage = renderer.getPageCount()-1;
            }

            renderer.openPage(currentPage).render(bitmap, new Rect(0, 0, 400, 400), null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            imageView.setImageBitmap(bitmap);
            imageView.invalidate();

        }catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_LONG).show();
        }


    }*/

//    @Override
//    public void onUserInteraction() {
//        super.onUserInteraction();
//        MainMenu menuObject = new MainMenu();
//        menuObject.countDownTimer.cancel();
//        menuObject.countDownTimer.start();
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is
        // present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                TransChoice.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //	   moveTaskToBack(true);
        TransChoice.this.finish();
    }


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


}

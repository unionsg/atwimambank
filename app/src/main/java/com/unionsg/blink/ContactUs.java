package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ContactUs extends AppCompatActivity {

    ProgressDialog progress;
    String msg = null;
    String usrId = null;
    String passWrd = null;
    Button sendButton;
    String currentDate = null;
    String strDate = null;

    TextView num1TextView;
    TextView num2TextView;
    TextView num3TextView;

    TextView emailTextView;

    String num1 = "+233303932153/55";
    String website = "https://www.theseedfunds.com/";
//    String num2 = "+233 79 216 364";
//    String num3 = "+233 50 144 9424";

    String email1 = "info@theseedfunds.com";
//
//    Handler handler;
//    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.contact_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        num1TextView = (TextView) findViewById(R.id.num1);
//        num2TextView = (TextView) findViewById(R.id.num2);
//        num3TextView = (TextView) findViewById(R.id.num3);
        emailTextView = (TextView) findViewById(R.id.email1);

        num1TextView.setText(website);
//        num2TextView.setText(num2);
//        num3TextView.setText(num3);
        emailTextView.setText(email1);



//        num1TextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                call("tel:"+num1);
//            }
//        });

//        num2TextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                call("tel:"+num2);
//            }
//        });
//
//        num3TextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                call("tel:"+num3);
//            }
//        });

        emailTextView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + email1));
                try {
                    startActivity(emailIntent);
                } catch (ActivityNotFoundException e) {
                    //TODO: Handle case where no email app is available
//                    Toast.makeText(getApplicationContext(),"No app found to send email",Toast.LENGTH_LONG).show();
                    new ResponseDialog(ContactUs.this, "Sorry", "No app found to send email. Please go to the Play Store on your device and download one");
                }
            }
        });

        //call in onCreate
      //  setAppIdleTimeout();
    }

//
//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(ContactUs.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(ContactUs.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }




    // to make call
    public void call(String theNumber) {

//        String[] perms = {"android.permission.RECORD_AUDIO", "android.permission.CAMERA"};
//
//        int permsRequestCode = 200;
//
//        requestPermissions(perms, permsRequestCode);

        Intent in=new Intent(Intent.ACTION_CALL, Uri.parse(theNumber));
        try{
            startActivity(in);
        }

        catch (android.content.ActivityNotFoundException ex){
            new ResponseDialog(ContactUs.this, "Sorry", "No app found to make call. Please go to the Play Store on your device and download one");
//            Toast.makeText(getApplicationContext(),"No app found to make call",Toast.LENGTH_LONG).show();
        }


    }



//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog(){
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                ContactUs.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        ContactUs.this.finish();
    }



}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class ChequeBookReqt extends AppCompatActivity {

    private Spinner spinner1;
    private EditText pinEditText;
    private Button confirmButton;
    EditText numEditText;
    Dialog dialog;

    String mbAccDetails = null;
    String[] accDetail = null;
    String pin = null;

    String from = null;
    String to = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String telcos = null;
    String aliasAcc = null;
    String balances = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;

    ProgressDialog progress1;
    ProgressDialog progress2;
    ProgressDialog progress3;
    int pos;

    Spinner branchSpinner;
    //    Spinner numLeavesSpinner = null;
    String accTypes = "Number of leaves~50";

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;

    String accnumText;

    String response = null;
    String acctsResponse = null;
    String noOfLeaves = null;
    String balFromFile = null;

    String branchCode = null;
    String branchName = null;
    String brName = null;
    String brCode = null;

//    Handler handler;
//    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chequebk_reqt);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();
        //		accDetail = mbAccDetails.split(",");

//        numLeavesSpinner = (Spinner) findViewById(R.id.leaves_spinner);
        branchSpinner = (Spinner) findViewById(R.id.branch_spinner);
        confirmButton = (Button) findViewById(R.id.confirm_button);

        spinner1 = (Spinner) findViewById(R.id.from_spinner);

//        balFromFile = readBalancesFromFile(ChequeBookReqt.this);

        /*if (!balFromFile.equals("")) {
            System.out.println("balances not empty");
            mbAcc = balFromFile;
            System.out.println("MBACC balances: " + mbAcc);
            populateAccounts();
        } else {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myAcctsFireMan().execute();
            } else {
                showAlertDialog(ChequeBookReqt.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        }*/

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new myAcctsFireMan().execute();
        } else {
            showAlertDialog(ChequeBookReqt.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }

        noOfLeaves = "50";


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


//        numLeavesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//
//                //				accountNum = parent.getItemAtPosition(pos).toString();
//                noOfLeaves = ((TextView) view.findViewById(R.id.acctype)).getText().toString().trim();
//
//            }
//
//            public void onNothingSelected(AdapterView<?> parent) {
//                // Do nothing
//
//            }
//        });


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                buildDialog(R.style.DialogAnimation);
//                new fireMan().execute();


            }
        });
        //call in onCreate
        //   setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(ChequeBookReqt.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(ChequeBookReqt.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }

    private void buildDialog(int animationSource) {

        dialog = new Dialog(ChequeBookReqt.this);
        dialog.setContentView(R.layout.confirm_chqbk_reqt);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
        TextView numLeavesTV = (TextView) dialog.findViewById(R.id.numleaves);
        TextView branchTV = (TextView) dialog.findViewById(R.id.branch);

        pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);

        accountNumTV.setText(accountNum);
        numLeavesTV.setText(noOfLeaves);
        branchTV.setText(brName);
//
        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                pin = pinEditText.getText().toString();

                if (brName.length() == 0) {
                    Toast.makeText(ChequeBookReqt.this, "Please select a branch to pick up your cheque book from", Toast.LENGTH_LONG).show();
                } else if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(ChequeBookReqt.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                    dialog.dismiss();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }


    private String readBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


//    public void populateSpinner1() {
//
//        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();
//
////						System.out.println(aliasAccDetails.length);
//        StringTokenizer accDetToken = new StringTokenizer(accTypes, "~");
//
//        for (int a = 0; accDetToken.hasMoreTokens(); a++) {
//
//            System.out.println(accDetToken.countTokens());
//
//            telcos = accDetToken.nextToken().trim();
////							beneAcc = accDetToken.nextToken().trim();
//
//            System.out.println(telcos);
////
//            HashMap<String, String> hm = new HashMap<String, String>();
//
//            hm.put("telconame", telcos);
////							hm.put("aliasaccnum", beneAcc "**********"+ .lastIndexOf(0, -3)  );
//
//            theList.add(hm);
//
//        }
//
//        String[] from = {"telconame"};
//        // Ids of views in listview_layout
//        int[] to = {R.id.acctype};
//
//        SimpleAdapter adapter2 = new SimpleAdapter(ChequeBookReqt.this, theList, R.layout.list_acctypes, from, to);
//
//        numLeavesSpinner.setAdapter(adapter2);
//
//    }


    private void writeBalancesToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bal.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


//    public void populateAccounts() {
//
//        System.out.println(mbAcc);
//
//        aliasAccDetails = balances.split(" ` ");
//
//        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();
//
//        System.out.println(aliasAccDetails.length);
//
//        for (int a = 0; a < aliasAccDetails.length; a++) {
//            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
////							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
//            accNum = myAccToken.nextToken().trim();
//            accName = myAccToken.nextToken().trim();
//            accAvailBal = myAccToken.nextToken().trim();
//            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
//            accLedgerBal = myAccToken.nextToken().trim();
//            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
//
//            System.out.println(accNum);
//            System.out.println(accName);
//
//            HashMap<String, String> hm = new HashMap<String, String>();
//
////							hm.put("accname", accName);
////            if (accName.length() > 20) {
//////							accNameDisp = accName.substring(0, 18) + "...";
////                hm.put("accname", accName.substring(0, 17) + "...");
////            } else {
////                hm.put("accname", accName);
////            }
//            hm.put("accnum", accNum);
//            hm.put("accname", accName);
//            hm.put("bal", accAvailBal
//                    .replace("SLL", "Le ")
//                    .replace("GHS", "¢")
//                    .replace("USD", "$")
//                    .replace("GBP", "£")
//                    .replace("EUR", "€")
//                    .replace("CNY", "¥")
//                    .replace("JPY", "¥"));
//            hm.put("ledgerbal", "   " + accLedgerBal
//                    .replace("SLL", "Le ")
//                    .replace("GHS", "¢")
//                    .replace("USD", "$")
//                    .replace("GBP", "£")
//                    .replace("EUR", "€")
//                    .replace("CNY", "¥")
//                    .replace("JPY", "¥"));
//
//            theList.add(hm);
//
//        }
//
//        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
//        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};
//
//        SimpleAdapter adapter = new SimpleAdapter(ChequeBookReqt.this, theList, R.layout.list_bills_from, from, to);
//
//        spinner1.setAdapter(adapter);
//
//        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//        Boolean isInternetPresent = cd.isConnectingToInternet();
//        if (isInternetPresent) {
//            new branchesFireMan().execute();
//        } else {
//            new NoInternetResponseDialog(ChequeBookReqt.this, "Sorry", "You don't have Internet connection" +
//                    " to proceed with this request").showDialog();
//        }
////        new branchesFireMan().execute();
//
//    }


    public String branchesJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String branchesResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = branchesJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getbranches", json);

        System.out.println(response);

        return response;
    }


    public class branchesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(ChequeBookReqt.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving branches...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = branchesResponse(token);
                //				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "Error!", "Connection timed out. Please check your Internet connection and try again").showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "No Data Found For This User", "Please login again").showDialog();

                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "Error !", "Unknown Error").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    ListView list = (ListView) findViewById(R.id.ach_banklist);
                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
                        branchCode = myAccToken.nextToken().trim();
                        branchName = myAccToken.nextToken().trim();

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("branchname", branchName);
                        hm.put("bankcode", branchCode);
                        theList.add(hm);

                    }

                    String[] from = {"branchname", "bankcode"};
                    int[] to = {R.id.branchname, R.id.bankcode};


                    SimpleAdapter adapter2 = new SimpleAdapter(ChequeBookReqt.this, theList, R.layout.list_ach, from, to);


                    branchSpinner.setAdapter(adapter2);

                    branchSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            // TODO Auto-generated method stub
                            brName = ((TextView) view.findViewById(R.id.branchname)).getText().toString().trim();
                            brCode = ((TextView) view.findViewById(R.id.bankcode)).getText().toString().trim();
                            System.out.println(brName);
                            System.out.println(brCode);
//							searchEditText.setText(brName);
                        }

                        public void onNothingSelected(AdapterView<?> parent) {
                            // Do nothing

                        }
                    });


                }

            }

        }

    }




    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }



    public String stmtJSON(String authToken, String noOfLeaves, String br) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&accNum=" + accountNum +
                "&noOfLeaves=" + noOfLeaves +
                "&branch=" + br +
                "&sec=" + pin+
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");
    }


    public String myResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = stmtJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "chqreqt", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress1 = new ProgressDialog(ChequeBookReqt.this);
            progress1.setCancelable(false);
            progress1.setTitle("Please wait");
            progress1.setMessage("Sending request...");
            progress1.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = myResponse(token, noOfLeaves, brName);

                try {

                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress1.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress1.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "No Data Found For This User", "Please login again").showDialog();
                    //					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                }
                else if(mb_response.trim().equals("15") ){
                    progress1.cancel();
					new ResponseDialog(ChequeBookReqt.this, "Unsuccessful", mbAcc).showDialog();
				}
                else if (mb_response.trim().equals("99")) {
                    progress1.cancel();
                    new WrongPinResponseDialog(ChequeBookReqt.this, "Transaction unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress1.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {

                    System.out.println(mbAcc);
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new myUpdateAcctsFireMan().execute();
                    } else {
                        showAlertDialog(ChequeBookReqt.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
//                    new ResponseDialog(ChequeBookReqt.this, "Sent successfully", mbAcc).showDialog();
                }

            }

        }

    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "accounts", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress3 = new ProgressDialog(ChequeBookReqt.this);
            progress3.setCancelable(true);
            progress3.setTitle("Please wait");
            progress3.setMessage("Retrieving accounts...");
            progress3.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
                //				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        balances = jsonObject.getString("mb_accDetails");

                        System.out.println(balances);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress3.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    progress3.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "Success", mbAcc).showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress3.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress3.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "Success", mbAcc).showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress3.cancel();
                    System.out.println(balances);
//                    writeBalancesToFile(balances, ChequeBookReqt.this);
//                    new ResponseDialog(ChequeBookReqt.this, "Success", mbAcc).showDialog();

                    aliasAccDetails = balances.split(" ` ");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);

                        System.out.println(accNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

//							hm.put("accname", accName);
//            if (accName.length() > 20) {
////							accNameDisp = accName.substring(0, 18) + "...";
//                hm.put("accname", accName.substring(0, 17) + "...");
//            } else {
//                hm.put("accname", accName);
//            }
                        hm.put("accnum", accNum);
                        hm.put("accname", accName);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(ChequeBookReqt.this, theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new branchesFireMan().execute();
                    } else {
                        new NoInternetResponseDialog(ChequeBookReqt.this, "Sorry", "You don't have Internet connection" +
                                " to proceed with this request").showDialog();
                    }

                }


            }

        }

    }




    public String updateAcctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myUpdateAcctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = updateAcctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class myUpdateAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myUpdateAcctsResponse(token);
                //				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        balances = jsonObject.getString("mb_accDetails");

                        System.out.println(balances);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress1.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    progress1.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "Success", mbAcc).showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress1.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress1.cancel();
                    new ResponseDialog(ChequeBookReqt.this, "Success", mbAcc).showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress1.cancel();
                    System.out.println(balances);
                    writeBalancesToFile(balances, ChequeBookReqt.this);
                    new ResponseDialog(ChequeBookReqt.this, "Sent successfully", mbAcc).showDialog();

                }


            }

        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();// It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class NoInternetResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public NoInternetResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public NoInternetResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();// It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class WrongPinResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public WrongPinResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public WrongPinResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    pinEditText.setText("");
                    pinEditText.setError("You entered a wrong PIN code");
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is
        // present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ChequeBookReqt.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //	   moveTaskToBack(true);
        ChequeBookReqt.this.finish();
    }


}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class AccountToMobileMoney extends AppCompatActivity {

    private Spinner spinner1, spinner2;
    private EditText amtEditText;
    private EditText phoneEditText;
    private EditText narrationEditText;
    private Button confirmButton;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = "";
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;
    String beneTel = null;
    String beneCode = null;

    String accNum = null;
    String accLedgerBal = null;
    String accType = null;
    String accAvailBal = null;
    String accName = null;
    String phone = null;

    String accountNum = null;
    String accountName = null;
    String curr = null;
    String network = null;
    String chosenNetwork = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String accnumText;

    String response = null;
    String balFromFile = null;
    String beneFromFile = null;
    String narration = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String codeFlag = null;
    String questions = null;

    JSONArray jsonArray;
//    Handler handler;
//    Runnable r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_to_mobilemoney);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        amtEditText = (EditText) findViewById(R.id.amtedittext);
        phoneEditText = (EditText) findViewById(R.id.recipient_phone);
        narrationEditText = (EditText) findViewById(R.id.narrationedittext);

        confirmButton = (Button) findViewById(R.id.confirm_button);

        balFromFile = readBalancesFromFile(AccountToMobileMoney.this);
//        beneFromFile = readBenefFromFile(AccountToMobileMoney.this);

        if (!balFromFile.equals("")) {
            System.out.println("balances not empty");
            mbAcc = balFromFile;
            System.out.println("MBACC balances: " + mbAcc);
            populateAccounts();

        } else {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myAcctsFireMan().execute();
            } else {
                showAlertDialog(AccountToMobileMoney.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        }


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
                accAvailBal = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();

                if (accAvailBal.contains("Le")) {
                    curr = "Le";
                } else if (accAvailBal.contains("¢")) {
                    curr = "¢";
                } else if (accAvailBal.contains("$")) {
                    curr = "$";
                } else if (accAvailBal.contains("£")) {
                    curr = "£";
                } else if (accAvailBal.contains("€")) {
                    curr = "€";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });

        spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                chosenNetwork = ((TextView) view.findViewById(R.id.telco_name)).getText().toString().trim();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                transAmt = amtEditText.getText().toString();
                narration = narrationEditText.getText().toString();
                phone = phoneEditText.getText().toString();

                if (transAmt.trim().isEmpty()) {
                    amtEditText.setError("Enter transfer amount");
                } else if (phone.trim().isEmpty()) {
                    phoneEditText.setError("Enter phone number to send to");
                } else {
                    if (narration.trim().isEmpty()) {
                        narration = "MB TRANSFER TO WALLET";
                    }

                    Intent confirmTransIntent = new Intent(AccountToMobileMoney.this, ConfirmAccToMobileMoney.class);
                    System.out.println(accountNum);
                    System.out.println(curr);
                    confirmTransIntent.putExtra("curr", curr);
                    confirmTransIntent.putExtra("dbAcc", accountNum);
                    confirmTransIntent.putExtra("phone", phone);
                    confirmTransIntent.putExtra("amt", transAmt);
                    confirmTransIntent.putExtra("network", chosenNetwork);
                    confirmTransIntent.putExtra("narration", narration.toUpperCase());
                    confirmTransIntent.putExtra("token", token);
                    confirmTransIntent.putExtra("userName", usrName);
                    confirmTransIntent.putExtra("userId", usrId);
                    confirmTransIntent.putExtra("mb_acc_details", custType);
                    confirmTransIntent.putExtra("balances", balances);
                    confirmTransIntent.putExtra("creationTime", creationTime);
                    confirmTransIntent.putExtra("beneInternal", beneInternal);
                    confirmTransIntent.putExtra("beneExternal", beneExternal);
                    confirmTransIntent.putExtra("questions", questions);
                    confirmTransIntent.putExtra("codeFlag", codeFlag);
                    startActivity(confirmTransIntent);
                }

            }
        });

        //call in onCreate
        //   setAppIdleTimeout();
    }



    private String readBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }



    public void populateAccounts() {

        System.out.println(mbAcc);

        aliasAccDetails = mbAcc.split(" ` ");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
            accType = myAccToken.nextToken().trim();

            System.out.println(accNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("accnum", accNum);
            hm.put("accname", accName);
            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            theList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        SimpleAdapter adapter = new SimpleAdapter(AccountToMobileMoney.this, theList, R.layout.list_bills_from, from, to);

        spinner1.setAdapter(adapter);

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new getNetworksFireMan().execute();
        } else {
            showAlertDialog(AccountToMobileMoney.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }

    }


    public void populateNetworks(List<HashMap<String, String>> arrayOfNetworks) {

//        arrayOfNetworks = new ArrayList<HashMap<String, String>>();

        String[] from = {"name"};
        // Ids of views in listview_layout
        int[] to = {R.id.telco_name};

        SimpleAdapter adapter2 = new SimpleAdapter(AccountToMobileMoney.this, arrayOfNetworks, R.layout.list_telcos, from, to);

        spinner2.setAdapter(adapter2);

    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(AccountToMobileMoney.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(AccountToMobileMoney.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(AccountToMobileMoney.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(AccountToMobileMoney.this, "Error !", "Unknown Error").showDialog();
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    aliasAccDetails = mbAcc.split(" ` ");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 , 
                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
                        accType = myAccToken.nextToken().trim();

                        System.out.println(accNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

//							hm.put("accname", accName);
//							if (accName.length() > 20) {
////							accNameDisp = accName.substring(0, 18) + "...";
//								hm.put("accname", accName.substring(0, 17) + "...");
//							} else {
//								hm.put("accname", accName);
//							}
                        hm.put("accnum", accNum);
                        hm.put("accname", accName);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(AccountToMobileMoney.this, theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);


                }

            }
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new getNetworksFireMan().execute();
            } else {
                showAlertDialog(AccountToMobileMoney.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }

        }

    }


//    @FormParam("amount") String amount,
//    @FormParam("network") String network,
//    @FormParam("narration") String narration,
//    @FormParam("voucher") String voucher,
//    @FormParam("authToken") String authToken


    public String getNetworksJSON( /*String accNum, String msisdn, String amount, String network, String narration*/) {
//        return "accountNum=" + accNum +
//                "&msisdn=" + msisdn +
//                "&amount=" + amount +
//                "&network=" + network +
//                "&narration=" + narration;
        return "";
    }


    public String getNetworksResponse() throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getNetworksJSON();
        String response = sendReqObj.getReq(GlobalCodes.myUrl + "getAllNetworks");

        System.out.println(response);

        return response;
    }

    String[] networkArray = {""};
    List<HashMap<String, String>> networkList = new ArrayList<HashMap<String, String>>();

    public class getNetworksFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(AccountToMobileMoney.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving networks...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = getNetworksResponse();//(accountNum, accountNum, accountNum, accountNum, accountNum);

                try {
                    if (response.trim().equals("88")) {
                        mb_response = "88";
                    } else {
                        if (!response.isEmpty()) {
                            jsonArray = new JSONArray(response);
                            System.out.println("jsonArray length: " + jsonArray.length());
                            mb_response = "00";

                        } else {
                            mb_response = "66";
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            System.out.println("11111111111111111111111");

            if (e != null) {
                progress.cancel();
                showAlertDialog(AccountToMobileMoney.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                System.out.println("222222222222222222222222222222");
                super.onPostExecute(result);

                if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(AccountToMobileMoney.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("88")) {
                    progress.cancel();
                    new ResponseDialog(AccountToMobileMoney.this, "Sorry", "Error in connection").showDialog();
                } else if (mb_response.trim().equals("00")){
                    System.out.println("333333333333333333333333333333333333");
                    progress.cancel();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            System.out.println("network: " + jsonArray.getString(i));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                        System.out.println("i: " + i);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        try {
                            hm.put("name", jsonArray.getString(i));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }


                        networkList.add(hm);


                    }

//                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

//                    populateNetworks(theList);

                    String[] from = {"name"};
                    // Ids of views in listview_layout
                    int[] to = {R.id.telco_name};

                    SimpleAdapter adapter2 = new SimpleAdapter(AccountToMobileMoney.this, networkList, R.layout.list_telcos, from, to);

                    spinner2.setAdapter(adapter2);

                }


            }


        }

    }






    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                AccountToMobileMoney.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        AccountToMobileMoney.this.finish();
    }


}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.liststuff.BeneficiaryAdapter;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class SameBankBeneficiaryFragment extends Fragment {

    private Context context;

    public SameBankBeneficiaryFragment() {
        // Required empty public constructor
    }


    Dialog dialog;
    ListView list;
    TextView firstLetterTV;
    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_beneficiaries = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String beneCode = null;
    String mbAcc = null;
    String mbAcc2 = null;
    String[] aliasAccDetails = null;
    String beneName = null;
    String beneAcc = null;
    String beneTel = null;

    String selectedName = null;
    String selectedAcc = null;
    String selectedTel = null;
    String selectedCode = null;

    String beneFromFile = null;
    ProgressDialog progress;
    String internalBeneficiary = null;
    String externalBeneficiary = null;
    String response = null;

    SwipeRefreshLayout swipeRefLayout;
    RelativeLayout emptyListLayout;
    TextView emptyText;
    TextView editBeneficiary;
    TextView transferToBeneficiary;
    TextView deleteBeneficiary;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String questions = null;
    String codeFlag = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        token = getActivity().getIntent().getExtras().getString("token").trim();
        usrName = getActivity().getIntent().getExtras().getString("userName").trim();
        usrId = getActivity().getIntent().getExtras().getString("userId").trim();
        custType = getActivity().getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getActivity().getIntent().getExtras().getString("balances").trim();
        beneInternal = getActivity().getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getActivity().getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getActivity().getIntent().getExtras().getString("creationTime").trim();
        questions = getActivity().getIntent().getExtras().getString("questions").trim();
        codeFlag = getActivity().getIntent().getExtras().getString("codeFlag").trim();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//		token = getActivity().getIntent().getExtras().getString("token").trim();
//		System.out.println("token: " + token);

//		Bundle bundle = this.getArguments();
//		String myValue = bundle.getString("key");
//		System.out.println(myValue);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.same_bank_beneficiaries, container, false);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

        list = (ListView) view.findViewById(R.id.bene_list);
//		emptyListLayout = (RelativeLayout) view.findViewById(R.id.emptylist_layout);
        emptyText = (TextView) view.findViewById(R.id.empty);
        swipeRefLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);


        beneFromFile = readFromFile(getActivity());
        if (/*!beneFromFile.equals("") ||*/ !beneFromFile.equals("empty")) {
            System.out.println("not empty");
            internalBeneficiary = beneFromFile;
            populateList1();
//			username.setEnabled(true);

        } else {
//			internalBeneficiary = getActivity().getIntent().getExtras().getString("internalBeneficiary").trim();
            System.out.println("empty");
//			list.setEmptyView(emptyText);
//			swipeRefLayout.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), "Sorry you do not have any SLCB beneficiaries. Tap on the plus button to create one", Toast.LENGTH_LONG).show();
//			new ResponseDialog(getActivity(), "Sorry", "You do not have any beneficiaries" ).showDialog();
        }


        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0)
                    swipeRefLayout.setEnabled(true);
                else
                    swipeRefLayout.setEnabled(false);
            }
        });


        swipeRefLayout.setColorSchemeColors(this.getResources().getColor(R.color.negative_bal),
                this.getResources().getColor(R.color.refresh_yellow),
                this.getResources().getColor(R.color.positive_bal),
                this.getResources().getColor(R.color.refresh_orange));


        swipeRefLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
//						getActivity().getSupportActionBar().setSubtitle(R.string.string_pulling);
//						new fireManRefresh().execute();
                        ConnectionDetector cd = new ConnectionDetector(getActivity());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new fireMan().execute();
                        } else {
                            showAlertDialog(getActivity(), "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                        swipeRefLayout.setRefreshing(false);

                    }
                }, 1000);
            }
        });


        return view;
    }


    public void populateList1() {

        System.out.println("internalBeneficiary: " + internalBeneficiary);

        aliasAccDetails = internalBeneficiary.split(" ` ");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length; a++) {
            StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

            System.out.println(accDetToken.countTokens());

//			beneCode = accDetToken.nextToken().trim();
            beneName = accDetToken.nextToken().trim();
            beneAcc = accDetToken.nextToken().trim();
            beneTel = accDetToken.nextToken().trim();
            beneCode = accDetToken.nextToken().trim();

            System.out.println("benename: " + beneName);
            System.out.println("beneacc: " + beneAcc);
            System.out.println("benetel: " + beneTel);
            System.out.println("benecode: " + beneCode);

            HashMap<String, String> hm = new HashMap<String, String>();

//			hm.put("benecode", beneCode);
            hm.put("benename", beneName);
            hm.put("beneacc", beneAcc);
            hm.put("benetel", beneTel);
            hm.put("benecode", beneCode);
            hm.put("firstletter", beneName.substring(0, 1).toUpperCase());

            theList.add(hm);

        }

        String[] from = { /*"benecode",*/ "benename", "beneacc", "benetel", "benecode", "firstletter"};

        // Ids of views in listview_layout
        int[] to = { /*R.id.bene_code,*/ R.id.bene_name, R.id.bene_acc, R.id.bene_tel, R.id.bene_code, R.id.first_letter};

        BeneficiaryAdapter adapter2 = new BeneficiaryAdapter(getActivity(), theList, R.layout.list_beneficiary, from, to);

        list.setAdapter(adapter2);

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                // TODO Auto-generated method stub
                selectedName = ((TextView) v.findViewById(R.id.bene_name)).getText().toString().trim();
                selectedAcc = ((TextView) v.findViewById(R.id.bene_acc)).getText().toString().trim();
                selectedTel = ((TextView) v.findViewById(R.id.bene_tel)).getText().toString().trim();
                selectedCode = ((TextView) v.findViewById(R.id.bene_code)).getText().toString().trim();

                showBeneficiaryOptions();

                return false;
            }
        });

    }


    private int getRandomMaterialColor() {
        int returnColor = Color.GRAY;
        int arrayId = getActivity().getResources().getIdentifier("list_item_colors", "array", getActivity().getPackageName());

        if (arrayId != 0) {
            TypedArray colors = getActivity().getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }


    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("intB.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getalias", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving beneficiaries...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            try {
                if (!response.isEmpty()) {


                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
                    mbAcc = jsonObject.getString("mb_accDetails");

                    System.out.println(mbAcc);
                } else {
                    mb_response = "66";
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("111111111111111111");
                System.out.println(e.getMessage());
                //					Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "Sorry you do not have any SLCB beneficiaries. Tap on the plus button to create one").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);

                    System.out.println("internalBeneficiary: " + mbAcc);

                    if (!mbAcc.equals("empty")) {

                        aliasAccDetails = mbAcc.split(" ` ");

                        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                        System.out.println(aliasAccDetails.length);

                        for (int a = 0; a < aliasAccDetails.length; a++) {
                            StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a].replace("`", ""), "~");

                            System.out.println(accDetToken.countTokens());

                            beneName = accDetToken.nextToken().trim();
                            beneAcc = accDetToken.nextToken().trim();
                            beneTel = accDetToken.nextToken().trim();
                            beneCode = accDetToken.nextToken().trim();

                            System.out.println(beneName);
                            System.out.println(beneAcc);
                            System.out.println(beneTel);
                            System.out.println(beneCode);

                            HashMap<String, String> hm = new HashMap<String, String>();

                            hm.put("benename", beneName);
                            hm.put("beneacc", beneAcc);
                            hm.put("benetel", beneTel);
                            hm.put("benecode", beneCode);
                            hm.put("firstletter", beneName.substring(0, 1).toUpperCase());

                            theList.add(hm);

                        }

                        String[] from = {"benename", "beneacc", "benetel", "benecode", "firstletter"};

                        // Ids of views in listview_layout
                        int[] to = {R.id.bene_name, R.id.bene_acc, R.id.bene_tel, R.id.bene_code, R.id.first_letter};

                        BeneficiaryAdapter adapter2 = new BeneficiaryAdapter(getActivity(), theList, R.layout.list_beneficiary, from, to);

                        list.setAdapter(adapter2);

                        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                            @Override
                            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                                // TODO Auto-generated method stub
                                selectedName = ((TextView) v.findViewById(R.id.bene_name)).getText().toString().trim();
                                selectedAcc = ((TextView) v.findViewById(R.id.bene_acc)).getText().toString().trim();
                                selectedTel = ((TextView) v.findViewById(R.id.bene_tel)).getText().toString().trim();
                                selectedCode = ((TextView) v.findViewById(R.id.bene_code)).getText().toString().trim();

                                showBeneficiaryOptions();

                                return false;
                            }
                        });


                        writeInternalToFile(mbAcc, getActivity());

                    } else {
                        Toast.makeText(getActivity(), "Sorry you do not have any SLCB beneficiaries. Tap on the plus button to create one", Toast.LENGTH_LONG).show();

                    }

                }

            }


        }

    }


    public String deleteJSON(String benefCode, String authToken) {
        return "benecode=" + benefCode +
                "&authToken=" + authToken;
    }


    public String myDeleteResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = deleteJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "deletebeneficiary", json);

        System.out.println(response);

        return response;
    }


    public class deleteFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving beneficiaries...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myDeleteResponse(selectedCode, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "Sorry you do not have any SLCB beneficiaries. Tap on the plus button to create one").showDialog();
                } else if (mb_response.trim().equals("00")) {

                    ConnectionDetector cd = new ConnectionDetector(getActivity());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        cd = new ConnectionDetector(getActivity());
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new beneficiariesUpdateFireMan().execute();
                        } else {
                            showAlertDialog(getActivity(), "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                    } else {
                        showAlertDialog(getActivity(), "No Internet Connection",
                                "You don't have internet connection", false);
                    }
//                    new fireMan().execute();

                }

            }


        }

    }


    public String updateJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myUpdateResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = updateJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getalias", json);

        System.out.println(response);

        return response;
    }


    public class beneficiariesUpdateFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//			progress = new ProgressDialog(CreateBeneficiary.this);
//			progress.setCancelable(false);
//			progress.setTitle("Please wait");
//			progress.setMessage("Retrieving beneficiaries...");
//			progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myUpdateResponse(token);
                System.out.println("response");

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);
//                    writeInternalToFile(mbAcc, getActivity());
                    new ResponseDialog(getActivity(), "Success", "You have successfully deleted this beneficiary").showDialog();

                    if (!mbAcc.equals("empty")) {

                        aliasAccDetails = mbAcc.split(" ` ");

                        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                        System.out.println(aliasAccDetails.length);

                        for (int a = 0; a < aliasAccDetails.length; a++) {
                            StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a].replace("`", ""), "~");

                            System.out.println(accDetToken.countTokens());

                            beneName = accDetToken.nextToken().trim();
                            beneAcc = accDetToken.nextToken().trim();
                            beneTel = accDetToken.nextToken().trim();
                            beneCode = accDetToken.nextToken().trim();

                            System.out.println(beneName);
                            System.out.println(beneAcc);
                            System.out.println(beneTel);
                            System.out.println(beneCode);

                            HashMap<String, String> hm = new HashMap<String, String>();

                            hm.put("benename", beneName);
                            hm.put("beneacc", beneAcc);
                            hm.put("benetel", beneTel);
                            hm.put("benecode", beneCode);
                            hm.put("firstletter", beneName.substring(0, 1).toUpperCase());

                            theList.add(hm);

                        }

                        String[] from = {"benename", "beneacc", "benetel", "benecode", "firstletter"};

                        // Ids of views in listview_layout
                        int[] to = {R.id.bene_name, R.id.bene_acc, R.id.bene_tel, R.id.bene_code, R.id.first_letter};

                        BeneficiaryAdapter adapter2 = new BeneficiaryAdapter(getActivity(), theList, R.layout.list_beneficiary, from, to);

                        list.setAdapter(adapter2);

                        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                            @Override
                            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                                // TODO Auto-generated method stub
                                selectedName = ((TextView) v.findViewById(R.id.bene_name)).getText().toString().trim();
                                selectedAcc = ((TextView) v.findViewById(R.id.bene_acc)).getText().toString().trim();
                                selectedTel = ((TextView) v.findViewById(R.id.bene_tel)).getText().toString().trim();
                                selectedCode = ((TextView) v.findViewById(R.id.bene_code)).getText().toString().trim();

                                showBeneficiaryOptions();

                                return false;
                            }
                        });


                        writeInternalToFile(mbAcc, getActivity());

                    } else {
                        Toast.makeText(getActivity(), "Sorry you do not have any SLCB beneficiaries. Tap on the plus button to create one", Toast.LENGTH_LONG).show();

                    }

                }

            }


        }

    }


    private void writeInternalToFile(String intBene, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("intB.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(intBene);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public void showBeneficiaryOptions() {

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.select_beneficiary_options);
        dialog.setCancelable(true);
        transferToBeneficiary = (TextView) dialog.findViewById(R.id.transfer_funds);
        editBeneficiary = (TextView) dialog.findViewById(R.id.edit_benef);
        deleteBeneficiary = (TextView) dialog.findViewById(R.id.delete_benef);

        transferToBeneficiary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                Intent fundstrnsferIntent = new Intent(getActivity(), FundsTransferFromBeneficiary.class);
                fundstrnsferIntent.putExtra("token", token);
                fundstrnsferIntent.putExtra("benename", selectedName);
                fundstrnsferIntent.putExtra("beneacc", selectedAcc);
                fundstrnsferIntent.putExtra("userName", usrName);
                fundstrnsferIntent.putExtra("userId", usrId);
                fundstrnsferIntent.putExtra("mb_acc_details", custType);
                fundstrnsferIntent.putExtra("balances", balances);
                fundstrnsferIntent.putExtra("creationTime", creationTime);
                fundstrnsferIntent.putExtra("beneInternal", beneInternal);
                fundstrnsferIntent.putExtra("beneExternal", beneExternal);
                fundstrnsferIntent.putExtra("questions", questions);
                fundstrnsferIntent.putExtra("codeFlag", codeFlag);

                startActivity(fundstrnsferIntent);
            }
        });

        editBeneficiary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                Intent fundstrnsferIntent = new Intent(getActivity(), EditBeneficiary.class);
                fundstrnsferIntent.putExtra("token", token);
                fundstrnsferIntent.putExtra("benename", selectedName);
                fundstrnsferIntent.putExtra("beneacc", selectedAcc);
                fundstrnsferIntent.putExtra("benetel", selectedTel);
                fundstrnsferIntent.putExtra("benecode", selectedCode);

                startActivity(fundstrnsferIntent);
            }
        });

        deleteBeneficiary.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                new ResponseDialogDelete(getActivity(), "Are you sure?", "This action will delete the selected beneficiary").showDialog();
            }
        });


        dialog.show();

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
//					getActivity().finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class ResponseDialogDelete extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialogDelete(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialogDelete(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    ConnectionDetector cd = new ConnectionDetector(getActivity());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        cd = new ConnectionDetector(getActivity());
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new deleteFireMan().execute();
                        } else {
                            showAlertDialog(getActivity(), "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                    } else {
                        showAlertDialog(getActivity(), "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                }
            });

            setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });

        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    /*@Override
    public void onResume() {
        super.onResume();
        System.out.println("RESUMING");
        populateList1();
    }*/


}

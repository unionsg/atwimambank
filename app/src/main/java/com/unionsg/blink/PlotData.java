package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

//import myUrl.UrlClass;

/**
 * Created by pato7755 on 6/24/16.
 */

public class PlotData extends AppCompatActivity implements OnMapReadyCallback {

    Dialog dialog;
    String desc = null;
    String address1 = null;
    String address2 = null;
    String tel = null;
    String lngString = null;
    String latString = null;
    Double lng = 0.00;
    Double lat = 0.00;

    String mbAccDetails = null;
    String[] accDetail = null;

    String from = null;
    String to = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
//    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String telcos = null;
    String aliasAcc = null;
    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;

    ProgressDialog progress1;
    ProgressDialog progress2;
    int pos;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String userId = null;


    Bitmap photo1;

    ProgressDialog progress;
    String username = null;

    String[] myStuff = {""};
    String resp = null;

    private GoogleMap map;
    Marker marker;
    CameraPosition googlePlex;

    //    TextView nameTextView;
//    ModelClass model;
//    View v;

    TextView nameTextView;
    TextView zoneTextView;
    TextView purposeTextView;
    TextView memberNameTextView;
    TextView numberTextView;
    TextView dateTextView;
    TextView latTextView;
    TextView lngTextView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mymap);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userId = getIntent().getExtras().getString("userId").trim();

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new branchesFireMan().execute();
        } else {
            showAlertDialog(PlotData.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }
//        name = getIntent().getExtras().getString("name").trim();
//        zone = getIntent().getExtras().getString("zone").trim();
//        visitPurpose = getIntent().getExtras().getString("purpose").trim();
//        memberName = getIntent().getExtras().getString("phoneNumber").trim();
//        phoneNumber = getIntent().getExtras().getString("memberName").trim();
//        imageBytes = getIntent().getExtras().getByteArray("picByte");
//        lat = getIntent().getExtras().getDouble("lat");
//        lng = getIntent().getExtras().getDouble("lng");

        /*LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {
            Toast.makeText(PlotData.this, "An error occured while checking GPS", Toast.LENGTH_SHORT).show();
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {
            Toast.makeText(PlotData.this, "An error occured while checking network", Toast.LENGTH_SHORT).show();
        }

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(PlotData.this);
            dialog.setMessage(PlotData.this.getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(PlotData.this.getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    PlotData.this.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(PlotData.this.getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        }


        if (gps_enabled && network_enabled) {



        }*/


    }


    public void fetchMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(PlotData.this);
//        mapFragment.getMapAsync(this);
//        onMapReady(map);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        System.out.println("ON MAP READY");

        map = googleMap;
//        googleMap = map;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        googleMap.getUiSettings().setMapToolbarEnabled(true);

        googlePlex = CameraPosition.builder()
                .target(new LatLng(lat, lng))
                .zoom(17)
                .bearing(90)
                .tilt(30)
                .build();

        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                new LatLng(8.48, -13.23)).zoom(6).build();

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        myStuff = mbAcc.split(" ` ");
        System.out.println("mystuff length is" + myStuff.length);


        for (int a = 0; a < myStuff.length - 1; a++) {

            StringTokenizer myToken = new StringTokenizer(myStuff[a], "~");

            desc = myToken.nextToken().trim();
            address1 = myToken.nextToken().trim();
            address2 = myToken.nextToken().trim();
            tel = myToken.nextToken().trim();
            latString = myToken.nextToken().trim();
            lngString = myToken.nextToken().trim();

            map.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(latString), Double.parseDouble(lngString)))
                    .title(desc + " BRANCH" + "\n"
                            + "LOCATION: " + address1 + "\n"
                            + address2 + "\n"
                            + "CONTACT NUMBER: " + tel
                    )
                    .flat(true)
                    .snippet(desc)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker2)))/*.showInfoWindow()*/;

//            map.animateCamera( CameraUpdateFactory.zoomTo( 4.0f ) );


            // Setting a custom info window adapter for the google map
            map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                // Use default InfoWindow frame
                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                // Defines the contents of the InfoWindow
                @Override
                public View getInfoContents(Marker arg0) {

                    // Getting view from the layout file info_window_layout
                    View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);

                    nameTextView = (TextView) v.findViewById(R.id.name);
//                    LatLng latLng = arg0.getPosition();

                    nameTextView.setText(arg0.getTitle());

                    ImageView img = (ImageView) v.findViewById(R.id.pic);

//                    img.setImageBitmap(photo1);

//                    getCurrentDate();

// Returning the view containing InfoWindow contents
                    return v;

                }
            });

        }


    }


    BufferedReader in = null;
    String response = null;


    public String branchesJSON(String authToken) {
        return "userId=" + authToken;
    }


    public String branchesResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = branchesJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getbranchlocations", json);

        System.out.println(response);

        return response;
    }


    public class branchesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(PlotData.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving branches...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = branchesResponse(userId);
                //				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
//                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                new ResponseDialog(PlotData.this, "Sorry", "An error occured").showDialog();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(PlotData.this, "No Data Found For This User", "Please login again").showDialog();
                    //					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(PlotData.this, "Error !", "Unknown Error").showDialog();
//                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(PlotData.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    fetchMap();


                }

            }

        }

    }



    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                PlotData.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        PlotData.this.finish();
    }


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();     //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    /*@Override
    protected void onResume() {
        super.onResume();
//        new branchesFireMan().execute();
    }*/
}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;

public class ForgotPin extends AppCompatActivity {

    private EditText pinEditText;
    private EditText confirmEditText;
    private EditText answerEditText;
    private Button confirmButton;
    Spinner spinner1;

    String token = null;
    String pin = null;
    String pin2 = null;
    //	String userId = null;
    String response = null;
    String mb_response = null;
    String mbAcc = null;

    ProgressDialog progress;
    ProgressDialog progress2;

    String usrName = null;
    String usrId = null;
    String custType = null;
    String mb_token = null;
    String mb_acc_details = null;
    String mb_userName = null;
    String creationTime = null;
    String balances = null;
    String beneInternal = null;
    String beneExternal = null;

    String id = null;
    String lastModificationDate = null;
    String mbAcc2 = null;
    String questions = null;
    String qCode = null;
    String qDesc = null;
    String questionCode = null;
    String questionDesc = null;
    String answer = null;
    String codeFlag = null;
    String codeQuestions[] = null;

//    Handler handler;
//    Runnable r;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pin);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        usrName = getIntent().getExtras().getString("userName").trim();
//        usrId = getIntent().getExtras().getString("userId").trim();
        token = getIntent().getExtras().getString("token").trim();
//        custType = getIntent().getExtras().getString("mb_acc_details").trim();
//        creationTime = getIntent().getExtras().getString("creationTime").trim();
//        balances = getIntent().getExtras().getString("balances").trim();
//        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
//        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
//        questions = getIntent().getExtras().getString("questions").trim();
//        codeFlag = getIntent().getExtras().getString("codeFlag").trim();


        spinner1 = (Spinner) findViewById(R.id.questions_spinner);
        answerEditText = (EditText) findViewById(R.id.answer_edittext);
        pinEditText = (EditText) findViewById(R.id.pinedittext);
        confirmEditText = (EditText) findViewById(R.id.confirm_pinedittext);

        confirmButton = (Button) findViewById(R.id.confirm_button);

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new questionsFireMan().execute();
        } else {
            showAlertDialog(ForgotPin.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				userId = userIdEditText.getText().toString();

                answer = answerEditText.getText().toString();
                pin = pinEditText.getText().toString();
                pin2 = confirmEditText.getText().toString();
                if (answer.trim().isEmpty()) {
                    answerEditText.setError("Enter your security answer");
                } else if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your new secret PIN");
                } else if (pin2.trim().isEmpty()) {
                    confirmEditText.setError("Confirm your secret PIN");
                } else if (!pin.trim().equals(pin2.trim())) {
                    confirmEditText.setError("Both PINs do not match");
                } else {

                    System.out.println(usrId);
                    System.out.println(pin);
                    System.out.println(pin2);
                    System.out.println(answer);

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(ForgotPin.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                }
            }
        });


        //call in onCreate
      //  setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(ForgotPin.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(ForgotPin.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }


    public void populateQuestions() {

//        ListView list = (ListView) findViewById(R.id.ach_banklist);
        codeQuestions = questions.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(codeQuestions.length);

        for (int a = 0; a < codeQuestions.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(codeQuestions[a], "~");
            qCode = myAccToken.nextToken().trim();
            qDesc = myAccToken.nextToken().trim();

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("branchname", qDesc);
            hm.put("bankcode", qCode);
            theList.add(hm);

        }

        String[] from = {"branchname", "bankcode"};
        int[] to = {R.id.branchname, R.id.bankcode};


        SimpleAdapter adapter2 = new SimpleAdapter(ForgotPin.this, theList, R.layout.list_ach, from, to);


        spinner1.setAdapter(adapter2);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                questionDesc = ((TextView) view.findViewById(R.id.branchname)).getText().toString().trim();
                questionCode = ((TextView) view.findViewById(R.id.bankcode)).getText().toString().trim();
                System.out.println(questionDesc);
                System.out.println(questionCode);
//							searchEditText.setText(brName);
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });

    }


    public String getQuestionsJSON(String authToken)  {
        return "authToken=" + authToken;
    }


    public String myQuestionsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getQuestionsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getUserQues", json);

        System.out.println(response);

        return response;
    }


    public class questionsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(ForgotPin.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Loading security question...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myQuestionsResponse(token);

                try {
                    if (!response.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(response);
//                    id = jsonObject.getString("id");
//                    creationTime = jsonObject.getString("creationTime");
//                    lastModificationDate = jsonObject.getString("lastModificationDate");
//                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
                    mbAcc = jsonObject.getString("mb_accDetails");

                    System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(ForgotPin.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(ForgotPin.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);

                    questions = mbAcc;

                    populateQuestions();

                }

            }


        }

    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String acctsJSON(String myToken, String myPin, String question, String secAnswer) throws UnsupportedEncodingException {
        return "authToken=" + myToken +
                "&pin=" + myPin +
                "&secQuestion=" + question +
                "&secAnswer=" + secAnswer +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");
    }


    public String myacctsResponse(String a1, String a2, String a3, String a4) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1, a2, a3, a4);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "forgotpin", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(ForgotPin.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Setting up PIN...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token, pin, questionCode, answer);
//				System.out.println(response);
                try {

                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);

                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");
//					internalBeneficiary = jsonObject.getString("mb_internal");
//					externalBeneficiary = jsonObject.getString("mb_external");


                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("10")) {
                    progress2.cancel();
                    new ResponseDialog(ForgotPin.this, "Sorry", mbAcc).showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(ForgotPin.this, "Sorry", mbAcc).showDialog();
                } else if (mb_response.trim().equals("12")) {
                    progress2.cancel();
                    new ResponseDialog(ForgotPin.this, "Sorry", mbAcc).showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(ForgotPin.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    new ResponseDialog(ForgotPin.this, "Setup successful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                }

            }

        }

    }


//	private void writeInternalToFile(String intBene, Context context) {
//		try {
//			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("intB.txt", Context.MODE_PRIVATE));
//			outputStreamWriter.write(intBene);
//			outputStreamWriter.close();
//		} catch (IOException e) {
//			Log.e("Exception", "File write failed: " + e.toString());
//		}
//	}
//
//
//	private void writeExternalToFile(String extBene, Context context) {
//		try {
//			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("extB.txt", Context.MODE_PRIVATE));
//			outputStreamWriter.write(extBene);
//			outputStreamWriter.close();
//		} catch (IOException e) {
//			Log.e("Exception", "File write failed: " + e.toString());
//		}
//	}


    public String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "viewbeneficiaries", json);

        System.out.println(response);

        return response;
    }


    public class beneficiariesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(ForgotPin.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving beneficiaries...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
                    mbAcc = jsonObject.getString("mb_accDetails");
                    mbAcc2 = jsonObject.getString("mb_transDetails");

                    System.out.println(mbAcc);
                    System.out.println(mbAcc2);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

//				Bundle bundle = new Bundle();
//				bundle.putString("key", "From Activity");
//// set Fragmentclass Arguments
//				SameBankBeneficiaryFragment fragobj = new SameBankBeneficiaryFragment();
//				fragobj.setArguments(bundle);


            }


        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does

//                    if (codeFlag.equals("setup")){

                    finish();
//                    } else if (codeFlag.equals("empty")){

//                        Intent loginIntent = new Intent(LoginChangePassword.this, MainMenu.class);
//                        loginIntent.putExtra("token", token);
//                        loginIntent.putExtra("userName", usrName);
//                        loginIntent.putExtra("userId", usrId);
//                        loginIntent.putExtra("mb_acc_details", custType);
//                        loginIntent.putExtra("creationTime", creationTime);
//                        loginIntent.putExtra("balances", balances);
//                        loginIntent.putExtra("beneInternal", beneInternal);
//                        loginIntent.putExtra("beneExternal", beneExternal);
//                        finish();
//                        startActivity(loginIntent);
//                    }
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                ForgotPin.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        ForgotPin.this.finish();
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();//which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }
}

package com.unionsg.blink;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class CustomerOnboarding extends AppCompatActivity {

	String mbAccDetails = null;
	String[] accDetail = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String aliasAcc = null;
	String[] fxDetails = null;
	String fxRates = null;
	String buy = null;
	String sell = null;
	String currPair = null;

	String accNum = null;
	String accName = null;
	String accountNum = null;

	TextView buyTextView;
	TextView sellTextView;
	TextView currTextView;
	ListView list;
	TextView buyLabelTextView;
	TextView sellLabelTextView;
	TextView currLabelTextView;

	ProgressDialog progress1;

	int[] next = new int[]{	R.drawable.ic_usd };
	List<HashMap<String,String>> aList;
	View view;

	String token = null;
	String rateType = null;
	String accnumText;

	String response = null;
	String acctsResponse = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customer_onboarding);

		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		WebView myWebView = (WebView) findViewById(R.id.webview);
		myWebView.loadUrl("http://197.159.129.211/onlinebanking/register");
		myWebView.setWebViewClient(new MyWebViewClient());

		WebSettings webSettings = myWebView.getSettings();

		webSettings.setJavaScriptEnabled(true);


		//timeout code
//		handler = new Handler();
//		r = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				Toast.makeText(LiveChat.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//				Intent loginIntent = new Intent(LiveChat.this, LoginActivity.class);
//				loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				startActivity(loginIntent);
//				finish();
//			}
//		};
//		startHandler();


	}

//	Handler handler;
//	Runnable r;
//
//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//		super.onUserInteraction();
//		stopHandler();//stop first and then start
//		startHandler();
//	}
//
//	public void stopHandler() {
//		handler.removeCallbacks(r);
//	}
//
//	public void startHandler() {
//		handler.postDelayed(r, MainMenu.timeOutMinutes);
//	}



	// Use When the user clicks a link from a web page in your WebView
	private class MyWebViewClient extends WebViewClient {

		@SuppressWarnings("deprecation")
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

				view.loadUrl("http://197.159.129.211/onlinebanking/register");
				return false;

//			if (Uri.parse(url).getHost().equals("http://www.slcb.com/")) {
//
//				return false;
//
//			}


//			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//
//			startActivity(intent);

//			return true;
		}

		@TargetApi(Build.VERSION_CODES.N)
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
			view.loadUrl("http://197.159.129.211/onlinebanking/register");
			return false;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			CustomerOnboarding.this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		//	   moveTaskToBack(true); 
		CustomerOnboarding.this.finish();
	}


}

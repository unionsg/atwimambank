package com.unionsg.blink;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

//import com.google.android.gms.samples.vision.barcodereader.ui.camera.GraphicOverlay;

/**
 * Created by pato7755 on 2/18/17.
 */

public class SendQRCash extends AppCompatActivity {


    private static final int ACTIVITY_RESULT_QR_DRDROID = 0;

    TextView report;
    Button scan;
    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String questions = null;
    String codeFlag = null;
    String creationTime = null;

    SurfaceView cameraView;
    TextView barcodeInfoTextView;

    CameraSource cameraSource;
    BarcodeDetector barcodeDetector;

    GraphicOverlay mGraphicOverlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_qr_cash);

        cameraView = (SurfaceView) findViewById(R.id.camera_view);

        barcodeInfoTextView = (TextView) findViewById(R.id.code_info);
        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);

        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

//        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) findViewById(R.id.graphicOverlay);

        barcodeDetector =
                new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(Barcode.QR_CODE)
                        .build();

        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .setRequestedFps(15.0f)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(SendQRCash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    cameraSource.start(cameraView.getHolder());


                } catch (IOException ie) {
                    Log.e("CAMERA SOURCE", ie.getMessage());
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();


                if (barcodes.size() != 0) {
                    barcodeInfoTextView.post(new Runnable() {    // Use the post method of the TextView
                        public void run() {
                            barcodeInfoTextView.setText(R.string.successful
                                    /*barcodes.valueAt(0).displayValue*/
                            );

                        }
                    });
                    QRPayments.merchantDetails = barcodes.valueAt(0).displayValue;
//                    Toast.makeText(SendQRCash.this, "Merchant account number is " + QRPayments.merchantAccNum, Toast.LENGTH_LONG).show();
//                    finish();
                    if (QRPayments.merchantDetails.length() > 18) {
                        Intent qrIntent = new Intent(SendQRCash.this, QRPayments.class);
                        qrIntent.putExtra("token", token);
                        qrIntent.putExtra("userName", usrName);
                        qrIntent.putExtra("userId", usrId);
                        qrIntent.putExtra("mb_acc_details", custType);
                        qrIntent.putExtra("balances", balances);
                        qrIntent.putExtra("creationTime", creationTime);
                        qrIntent.putExtra("beneInternal", beneInternal);
                        qrIntent.putExtra("beneExternal", beneExternal);
                        qrIntent.putExtra("questions", questions);
                        qrIntent.putExtra("codeFlag", codeFlag);
                        startActivity(qrIntent);
                        SendQRCash.this.finish();
                    }
                } else {
                    Toast.makeText(SendQRCash.this, "Please ensure that you are scanning a valid TSF QR code", Toast.LENGTH_LONG).show();
//                    finish();
                }
            }
        });


    }


//    public Tracker<Barcode> create(Barcode barcode) {
//        BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay);
//        return new GraphicTracker<>(mGraphicOverlay, graphic);
//    }

    private Paint mRectPaint;
    private Paint mTextPaint;
    private volatile Barcode mBarcode;


    /**
     * Draws the barcode annotations for position, size, and raw value on the supplied canvas.
     */

    public void draw(Canvas canvas, Barcode myBarcode) {
//        Barcode barcode = mBarcode;
        if (myBarcode == null) {
            return;
        }

        // Draws the bounding box around the barcode.
        RectF rect = new RectF(myBarcode.getBoundingBox());
        rect.left = translateX(rect.left);
        rect.top = translateY(rect.top);
        rect.right = translateX(rect.right);
        rect.bottom = translateY(rect.bottom);
        canvas.drawRect(rect, mRectPaint);

        // Draws a label at the bottom of the barcode indicate the barcode value that was detected.
//        canvas.drawText(barcode.rawValue, rect.left, rect.bottom, mTextPaint);

        Log.v("On Draw", "called");
    }


    public float scaleX(float horizontal) {
        return horizontal * mGraphicOverlay.mWidthScaleFactor;
    }

    /**
     * Adjusts a vertical value of the supplied value from the preview scale to the view scale.
     */
    public float scaleY(float vertical) {
        return vertical * mGraphicOverlay.mHeightScaleFactor;
    }

    public float translateX(float x) {
        if (mGraphicOverlay.mFacing == CameraSource.CAMERA_FACING_FRONT) {
            return mGraphicOverlay.getWidth() - scaleX(x);
        } else {
            return scaleX(x);
        }
    }

    /**
     * Adjusts the y coordinate from the preview's coordinate system to the view coordinate
     * system.
     */
    public float translateY(float y) {
        return scaleY(y);
    }


}
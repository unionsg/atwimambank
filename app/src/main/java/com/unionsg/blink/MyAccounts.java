package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

//import com.github.mikephil.charting.charts.PieChart;
//import com.github.mikephil.charting.data.PieData;
//import com.github.mikephil.charting.data.PieDataSet;
//import com.github.mikephil.charting.data.PieEntry;
import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.liststuff.MyAccountsAdapter;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;


public class MyAccounts extends AppCompatActivity {

    public String respFromDB = null;//yet to change
    TextView disp;
    ProgressDialog progress;
    Intent transDetailsIntent;
//    PieChart pieChart;
//    List<PieEntry> entries;
//    ArrayList<String> PieEntryLabels;
//    PieDataSet pieDataSet;
//    PieData pieData;

    Double amtDouble = null;
    String formattedAmt = null;

    String id = null, idRefresh = null;
    String creationTime = null, creationTimeRefresh = null;
    String mb_userId = null, mb_userIdRefresh = null;
    String lastModificationDate = null, lastModificationDateRefresh = null;
    String mb_response = null, mb_responseRefresh = null;
    String mb_accDetails = null, mb_accDetailsRefresh = null, mb_accDetailsFirstLoad = null;
    String mb_token = null, mb_tokenRefresh = null;
    String mb_userName = null;
    String transDetails = null;
    static byte[] imageBytes = null;

    String[] accDetail = null, accDetailRefresh = null;

    String response = null, responseRefresh = null;

    // Array of strings storing country names
    String accNum = null, accNumRefresh = null;

    String accLedgerBal = null, accLedgerBalRefresh = null;

    String accAvailBal = null, accAvailBalRefresh = null;
    String accType = null;

    // Array of integers points to images stored in /res/drawable/
    int[] next = new int[]{R.drawable.more};

    // Array of strings to store currencies
    String accName = null, accNameRefresh = null;
    String accNameDisp = null;

    View view;
    RelativeLayout relLayoutParent;
    TextView accNameTextview;

    String accountName = null;
    String accountNum = null;
    String availableBal = null;
    String ledgerBal = null;

    String mbAccDetails = null;
    String token = null;
    ListView list;
    SwipeRefreshLayout swipeRefLayout;
    List<HashMap<String, String>> aList, aListRefresh;
    ArrayList<Integer> colors;
    int[] colorList = new int[]{R.color.slcb_red, R.color.slcb_blue, R.color.refresh_yellow, R.color.positive_bal, R.color.light_blue,
            R.color.refresh_orange, R.color.cream, R.color.light_blue, R.color.status_bar, R.color.pink, R.color.c4, R.color.refresh_orange,
            R.color.cream, R.color.light_blue, R.color.status_bar};


    String balFromFile = null;

    String balances = null;

    //	@SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_accounts);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
//		getSupportActionBar().setTitle("First Aid");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras().getString("token").trim();

        swipeRefLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);


//        pieChart = (PieChart) findViewById(R.id.piechart);
//        entries = new ArrayList<>();
//        PieEntryLabels = new ArrayList<String>();

//        AddValuesToPIEENTRY();
//        AddValuesToPieEntryLabels();

        colors = new ArrayList<>();
        colors.add(ResourcesCompat.getColor(getResources(), R.color.negative_bal, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.slcb_blue, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.slcb_red, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.light_blue, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.cream, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.negative_bal, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.slcb_blue, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.slcb_red, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.light_blue, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.cream, null));
        colors.add(ResourcesCompat.getColor(getResources(), R.color.cream, null));


//        pieDataSet = new PieDataSet(entries, "");
//        pieDataSet.setColors(colors);
//        pieData = new PieData(pieDataSet);
//        pieData.setDrawValues(false);
////        pieDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
//        pieChart.setData(pieData);
//        pieChart.setDrawEntryLabels(false);
//        pieChart.setHoleRadius(30f);
//        pieChart.getLegend().setEnabled(false);
//        pieChart.getDescription().setEnabled(false);
//        pieChart.animateXY(1000, 1000);
//        pieChart.isUsePercentValuesEnabled();
//        pieChart.

        list = (ListView) swipeRefLayout.findViewById(R.id.myacclist);
        balFromFile = readFromFile(MyAccounts.this);
        System.out.println("balFromFile: " + balFromFile);
        if (!balFromFile.equals("")) {
            System.out.println("not empty");
            mb_accDetailsFirstLoad = balFromFile;
            populateBalances();
        } else {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new fireManFirstLoad().execute();
            } else {
                showAlertDialog(MyAccounts.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        }
//		new fireManFirstLoad().execute();
        getSupportActionBar().setSubtitle(R.string.string_pull);

        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0)
                    swipeRefLayout.setEnabled(true);
                else
                    swipeRefLayout.setEnabled(false);
            }
        });


        swipeRefLayout.setColorSchemeColors(this.getResources().getColor(R.color.negative_bal),
                this.getResources().getColor(R.color.refresh_yellow),
                this.getResources().getColor(R.color.positive_bal),
                this.getResources().getColor(R.color.refresh_orange));


        swipeRefLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getSupportActionBar().setSubtitle(R.string.string_pulling);
//						new fireManRefresh().execute();
                        new fireManFirstLoad().execute();
                        swipeRefLayout.setRefreshing(false);

                    }
                }, 1000);
            }
        });


        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                accountName = ((TextView) v.findViewById(R.id.acc_name)).getText().toString().trim();
                accountNum = ((TextView) v.findViewById(R.id.acc_num)).getText().toString().trim();
                availableBal = ((TextView) v.findViewById(R.id.curr)).getText().toString().trim() +
                        ((TextView) v.findViewById(R.id.avail_bal)).getText().toString().trim()/*+
                        ((TextView) v.findViewById(R.id.decimal)).getText().toString().trim()*/;
                ledgerBal = ((TextView) v.findViewById(R.id.curr2)).getText().toString().trim() +
                        ((TextView) v.findViewById(R.id.ledger_bal)).getText().toString().trim()/*+
                        ((TextView) v.findViewById(R.id.decimal2)).getText().toString().trim()*/;


                new fireMan().execute();
            }
        });


        //accDetail = GlobalCodes.getMbAccDetails().split(",");

        //		accDetail = mb_accDetails.split(",");

        //System.out.println(accDetail.length);

        //call in onCreate
        //   setAppIdleTimeout();
    }

//
//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(MyAccounts.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(MyAccounts.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }


//    public void AddValuesToPIEENTRY(){
//
//        entries.add(new PieEntry(2f, 0));
//        entries.add(new PieEntry(4f, 1));
//        entries.add(new PieEntry(6f, 2));
//        entries.add(new PieEntry(8f, 3));
//        entries.add(new PieEntry(7f, 4));
//        entries.add(new PieEntry(3f, 5));
//
//    }
//
////    for (int a = 0; a )
//
//    public void AddValuesToPieEntryLabels(){
//
//        PieEntryLabels.add("January");
//        PieEntryLabels.add("February");
//        PieEntryLabels.add("March");
//        PieEntryLabels.add("April");
//        PieEntryLabels.add("May");
//        PieEntryLabels.add("June");
//
//    }






	/*	public void onRefresh() {
	    new Handler().postDelayed(new Runnable() {
	        @Override 
	        public void run() {

	        	getSupportActionBar().setSubtitle(R.string.string_pulling);     
new fireManRefresh().execute();
swipeRefLayout.setRefreshing(false);


	        }
	    }, 5000);
	}
	 */

    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public void populateBalances() {

        GlobalCodes.setMbAccDetails(mb_accDetailsFirstLoad);
        accDetail = GlobalCodes.getMbAccDetails().split(" ` ");
        System.out.println("length of accdetail array: " + accDetail.length);
        aList = new ArrayList<HashMap<String, String>>();


        for (int a = 0; a < accDetail.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
           // entries.add(new PieEntry(Float.parseFloat(GlobalCodes.numberValue), a));
            System.out.println("entry " + a + ": " + Float.parseFloat(GlobalCodes.numberValue));
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
            accType = myAccToken.nextToken().trim();

            GlobalCodes.breakUpAmt(accAvailBal);
            GlobalCodes.breakUpLedger(accLedgerBal);


            HashMap<String, String> hm = new HashMap<String, String>();


            hm.put("accname", accName);
            hm.put("accnum", accNum);
            hm.put("curr", GlobalCodes.currAvail);
            hm.put("curr2", GlobalCodes.currLedger);
            hm.put("bal", GlobalCodes.amtAvail + "." + GlobalCodes.decimalAvail);
//						hm.put("dec", );
//						hm.put("dec2", "." + GlobalCodes.decimalLedger);
            hm.put("ledgerbal", GlobalCodes.amtLedger + "." + GlobalCodes.decimalLedger);
            hm.put("acctype", accType);
            hm.put("next", Integer.toString(next[0]));
            hm.put("colors", Integer.toString(colorList[a]));

//            colorImage.setBackgroundColor(Color.BLUE);

            aList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal", "curr", /*"dec",*/ "curr2", "acctype", "colors"};

        // Ids of views in listview_layout
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal, R.id.curr,  /*R.id.decimal,*/ R.id.curr2, R.id.acc_type, R.id.my_color};

        MyAccountsAdapter adapter = new MyAccountsAdapter(MyAccounts.this, aList, R.layout.list_myaccounts, from, to);

        list.setAdapter(adapter);

    }


    public String refreshJSON(String authToken) {
        return "authToken=" + authToken;

    }

    public String myResponseRefresh(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = refreshJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class fireManFirstLoad extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(MyAccounts.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Getting Balances....");
            progress.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                responseRefresh = myResponseRefresh(token);

                try {
                    JSONObject jsonObject = new JSONObject(responseRefresh);
                    idRefresh = jsonObject.getString("id");
                    creationTimeRefresh = jsonObject.getString("creationTime");

                    lastModificationDateRefresh = jsonObject.getString("lastModificationDate");
                    mb_tokenRefresh = jsonObject.getString("tokenId");
                    mb_responseRefresh = jsonObject.getString("mb_response");
                    mb_userIdRefresh = jsonObject.getString("mb_userId");
                    mb_accDetailsFirstLoad = jsonObject.getString("mb_accDetails");


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                getSupportActionBar().setSubtitle(R.string.string_pull);
                progress.cancel();
                Toast.makeText(MyAccounts.this, "Error.Please Try Again", Toast.LENGTH_LONG).show();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_responseRefresh.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_responseRefresh);
                    getSupportActionBar().setSubtitle(R.string.string_pull);
                    progress.cancel();
                    Toast.makeText(MyAccounts.this, "Error.Please Try Again", Toast.LENGTH_LONG).show();

                } else if (mb_responseRefresh.trim().equals("00")) {
                    getSupportActionBar().setSubtitle(R.string.string_pull);
                    progress.cancel();
                    GlobalCodes.setMbAccDetails(mb_accDetailsFirstLoad);
                    accDetail = GlobalCodes.getMbAccDetails().split(" ` ");
                    aList = new ArrayList<HashMap<String, String>>();

                    for (int a = 0; a < accDetail.length; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
//						if (accName.length() > 21) {
//							accNameDisp = accName.substring(0, 18) + "...";
//						}
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
//                        entries.add(new PieEntry(Float.parseFloat(GlobalCodes.numberValue), a));
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
                        accType = myAccToken.nextToken().trim();

                        GlobalCodes.breakUpAmt(accAvailBal);
                        GlobalCodes.breakUpLedger(accLedgerBal);


                        HashMap<String, String> hm = new HashMap<String, String>();

						/*if (accName.length() > 21) {
//							accNameDisp = accName.substring(0, 18) + "...";
							hm.put("accname", accName.substring(0, 18) + "...");
						} else {
							hm.put("accname", accName);
						}*/

                        hm.put("accname", accName);
                        hm.put("accnum", accNum);
                        hm.put("curr", GlobalCodes.currAvail);
                        hm.put("curr2", GlobalCodes.currLedger);
                        hm.put("bal", GlobalCodes.amtAvail + "." + GlobalCodes.decimalAvail);
//						hm.put("dec", );
//						hm.put("dec2", "." + GlobalCodes.decimalLedger);
                        hm.put("ledgerbal", GlobalCodes.amtLedger + "." + GlobalCodes.decimalLedger);
                        hm.put("acctype", accType);
                        hm.put("next", Integer.toString(next[0]));
                        hm.put("colors", Integer.toString(colorList[a]));

                        aList.add(hm);

                    }


                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal", "curr", /*"dec",*/ "curr2", "acctype", "colors"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal, R.id.curr,  /*R.id.decimal,*/ R.id.curr2, R.id.acc_type, R.id.my_color};

                    MyAccountsAdapter adapter = new MyAccountsAdapter(MyAccounts.this, aList, R.layout.list_myaccounts, from, to);


                    writeBalancesToFile(mb_accDetailsFirstLoad, MyAccounts.this);
                    list.setAdapter(adapter);


                } else {
                    getSupportActionBar().setSubtitle(R.string.string_pull);
                    progress.cancel();
                    Toast.makeText(MyAccounts.this, "Network is down. Please Try Again Later", Toast.LENGTH_LONG).show();
                }

            }

        }

    }


    private void writeBalancesToFile(String bal, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bal.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(bal);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public String transEnqJSON(String authToken, String accNum) {
        return "authToken=" + authToken +
                "&accNum=" + accNum;
    }


    public String myResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = transEnqJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "transenq", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(MyAccounts.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token, accountNum);

                try {
                    if (!response.isEmpty()) {


                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");

                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
//					mb_accDetails = jsonObject.getString("mb_accDetails");
                        transDetails = jsonObject.getString("mb_transDetails");
//					imageBytes = jsonObject.getString("mb_image1").getBytes();
//					imageBytes = mb_accDetails;

                        //					System.out.println(mb_userId);
                        System.out.println(transDetails);

                    } else {
                        mb_response = "66";
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("13")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(MyAccounts.this, "Account is Locked.Please Retry", Toast.LENGTH_LONG).show();


                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(MyAccounts.this, "No Transactions Are Available On This Account", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(MyAccounts.this, "No response", "Please try again").showDialog();

                } else if (mb_response.trim().equals("11")) {

                    progress.cancel();
                    Toast.makeText(MyAccounts.this, "Wrong UserName or Password Entered. Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    System.out.println("TRANS DETAILS: " + transDetails);
                    if (transDetails.equals("No Transactions Are Available On This Account")) {
                        Toast.makeText(MyAccounts.this, transDetails, Toast.LENGTH_LONG).show();
                    } else {
                        Intent transEnqIntent = new Intent(MyAccounts.this, TransEnquiry.class);
                        transEnqIntent.putExtra("accNameKey", accountName);
                        transEnqIntent.putExtra("accNumKey", accountNum);
                        transEnqIntent.putExtra("availBalKey", availableBal);

                        transEnqIntent.putExtra("ledgerBalKey", ledgerBal);
                        transEnqIntent.putExtra("tokenKey", mb_token);
                        transEnqIntent.putExtra("transDetailsKey", transDetails);
                        transEnqIntent.putExtra("imageBytes", imageBytes);
                        startActivity(transEnqIntent);
                    }

                } else {
                    progress.cancel();
                    Toast.makeText(MyAccounts.this, "Network is down. Please Try Again Later", Toast.LENGTH_LONG).show();
                }


            }

        }


    }


	/*public String formatAmounts(String theNumber){

		amtDouble = Double.parseDouble(theNumber);
		DecimalFormat formatter = new DecimalFormat("#,###.00");

		formattedAmt = formatter.format(amtDouble);
		return formattedAmt;
	}*/


//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //		menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this)
                MyAccounts.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onBackPressed() {
//        //		moveTaskToBack(true);
//        MyAccounts.this.finish();
//    }
//
//    Handler handler;
//    Runnable r;
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//        super.onUserInteraction();
//        stopHandler();//stop first and then start
//        startHandler();
//    }
//
//    public void stopHandler() {
//        handler.removeCallbacks(r);
//    }
//
//    public void startHandler() {
//        handler.postDelayed(r, MainMenu.timeOutMinutes);
//    }


}
package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;


public class Settings extends AppCompatActivity {

    int[] arrow = new int[]{R.drawable.more};
    int[] alias_pic = new int[]{R.drawable.ic_action_alias};
    int[] next_pic = new int[]{R.drawable.more};
    String token = null;

    String oldPassword = null;
    String newPassword1 = null;
    String newPassword2 = null;
    String newNickname1 = null;
    String newNickname2 = null;
    String chPassword = "Change password";
    String alcreate = "Change nickname";
    String forgotPin = "Forgot PIN code";
    String changePin = "Change PIN code";
    String addBeneficiary = "Add Beneficiary for funds transfer";
    String fastBalActivation = "Fast Balance Activation";
    String fastBalDeactivation = "Fast Balance Deactivation";
    String biometricActivation = "Biometric Login Settings";
    String newItem = "New item";
    List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
    ListView list;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_transDetails = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    ProgressDialog progress;
    String response = null;
    String oldpin = null;
    String newpin1 = null;
    String newpin2 = null;

    Dialog dialog;
    TextView titleTextView;
    TextView transferRate;
    TextView noteRate;

    TextView sameBank;
    TextView otherBank;

    int[] pics = new int[]{
            R.drawable.ic_change_password,
            R.drawable.ic_action_edit_icon,
            R.drawable.ic_change_pin,
            R.drawable.ic_action_fast_bal,
            R.drawable.ic_stop_fast_bal,
            R.drawable.ic_forgot_pin,
            R.drawable.ic_fingerpint_blue


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.home_inflater);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        token = getIntent().getExtras().getString("token").trim();

        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("menuitem", chPassword);
        hm.put("pix", Integer.toString(arrow[0]));
        hm.put("icon", Integer.toString(pics[0]));

        HashMap<String, String> hm2 = new HashMap<String, String>();
        hm2.put("menuitem", alcreate);
        hm2.put("pix", Integer.toString(arrow[0]));
        hm2.put("icon", Integer.toString(pics[1]));

//		HashMap<String,String> hm3 = new HashMap<String, String>();
//		hm3.put("menuitem",findBranch);
//		hm3.put("pix", Integer.toString(arrow[0]));

        HashMap<String, String> hm4 = new HashMap<String, String>();
        hm4.put("menuitem", changePin);
        hm4.put("pix", Integer.toString(arrow[0]));
        hm4.put("icon", Integer.toString(pics[2]));

//		HashMap<String,String> hm5 = new HashMap<String, String>();
//		hm5.put("menuitem",addBeneficiary);
//		hm5.put("pix", Integer.toString(arrow[0]));

        HashMap<String, String> hm6 = new HashMap<String, String>();
        hm6.put("menuitem", fastBalActivation);
        hm6.put("pix", Integer.toString(arrow[0]));
        hm6.put("icon", Integer.toString(pics[3]));

        HashMap<String, String> hm7 = new HashMap<String, String>();
        hm7.put("menuitem", fastBalDeactivation);
        hm7.put("pix", Integer.toString(arrow[0]));
        hm7.put("icon", Integer.toString(pics[4]));

        HashMap<String, String> hm8 = new HashMap<String, String>();
        hm8.put("menuitem", forgotPin);
        hm8.put("pix", Integer.toString(arrow[0]));
        hm8.put("icon", Integer.toString(pics[5]));

        HashMap<String, String> hm9 = new HashMap<String, String>();
        hm9.put("menuitem", biometricActivation);
        hm9.put("pix", Integer.toString(arrow[0]));
        hm9.put("icon", Integer.toString(pics[6]));

        aList.add(hm);
        aList.add(hm2);
//		aList.add(hm3);
        aList.add(hm4);
//		aList.add(hm5);
        aList.add(hm6);
        aList.add(hm7);
        aList.add(hm8);
        aList.add(hm9);


        String[] from = {"menuitem", "pix", "icon"};
        int[] to = {R.id.menu_item, R.id.flag, R.id.icon};

        SimpleAdapter adpt = new SimpleAdapter(this, aList, R.layout.home_inflater_list, from, to);
        list = (ListView) findViewById(R.id.function_list);
        list.setAdapter(adpt);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //accountName = ((TextView) v.findViewById(R.id.acc_name)).getText().toString().trim();

                String dataToCompare = ((TextView) v.findViewById(R.id.menu_item)).getText().toString().trim();

                switch (dataToCompare) {
                    case ("Change nickname"): {
                        final Dialog dialog = new Dialog(Settings.this, R.style.CustumDialog);
                        dialog.setContentView(R.layout.chnge_nicknamedialog);
                        dialog.setTitle("CHANGE NICK NAME");
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.setCancelable(true);
                        dialog.setCanceledOnTouchOutside(false);

//						final EditText oldPinEdittext = (EditText) dialog.findViewById(R.id.old_password);
                        final EditText newNicknameEdittext1 = (EditText) dialog.findViewById(R.id.new_nickname1);
                        final EditText newNicknameEdittext2 = (EditText) dialog.findViewById(R.id.new_nickname2);

                        Button okButton = (Button) dialog.findViewById(R.id.ok_button);
                        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

                        okButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
//								oldPin = oldPinEdittext.getText().toString().trim();
                                newNickname1 = newNicknameEdittext1.getText().toString().trim();
                                newNickname2 = newNicknameEdittext2.getText().toString().trim();

                                if (newNickname1.length() == 0) {
                                    newNicknameEdittext1.setError("Please enter new nickname");
                                } else if (newNickname2.length() == 0) {
                                    newNicknameEdittext2.setError("Please re-enter new nickname");
                                } else if (!newNickname2.equals(newNickname1)) {
                                    newNicknameEdittext2.setError("Does not match what you typed in the previous field");
                                } else {
                                    dialog.cancel();

//									System.out.println(oldPin);
                                    System.out.println(newNicknameEdittext1);
                                    System.out.println(newNicknameEdittext2);
                                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                                    Boolean isInternetPresent = cd.isConnectingToInternet();
                                    if (isInternetPresent) {
                                        new nicknameFireMan().execute();
                                    } else {
                                        showAlertDialog(Settings.this, "No Internet Connection",
                                                "You don't have internet connection.", false);
                                    }

                                    //  new nicknameFireMan().execute();
                                }

                            }
                        });

                        cancelButton.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();

                            }
                        });

                        dialog.show();

                        break;
                    }
                    case ("Change password"): {

                        Intent intent = new Intent(Settings.this, ChangePassword.class);
                        intent.putExtra("token", token);
//                        intent.putExtra("userId", usrName);
                        startActivity(intent);

                        break;
                    }
                    case ("Change PIN code"): {
                        Intent intent = new Intent(Settings.this, ChangePin.class);
                        intent.putExtra("token", token);
//                        intent.putExtra("userId", usrName);
                        startActivity(intent);

                        break;
                    }
                    case ("Add Beneficiary for funds transfer"): {

                        dialog = new Dialog(Settings.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.beneficiary_dialog);
                        dialog.setCancelable(true);

                        sameBank = (TextView) dialog.findViewById(R.id.same_bank);
                        otherBank = (TextView) dialog.findViewById(R.id.other_bank);

                        sameBank.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                Intent beneficiaryIntent = new Intent(Settings.this, CreateBeneficiary.class);
                                beneficiaryIntent.putExtra("token", token);
//                                    stmtIntent.putExtra("rateType", chosenRateType);

                                startActivity(beneficiaryIntent);
                            }
                        });

                        otherBank.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                // TODO Auto-generated method stub
                                dialog.dismiss();
                                Intent beneficiaryIntent = new Intent(Settings.this, CreateExtBeneficiary.class);
                                beneficiaryIntent.putExtra("token", token);
//                                    stmtIntent.putExtra("rateType", chosenRateType);

                                startActivity(beneficiaryIntent);
                            }
                        });


                        dialog.show();


                        break;

                    }
                    case ("Fast Balance Activation"): {
                        new ResponseDialogFastBal(Settings.this, "Fast Balance Activation", "This will allow you to view your account balances without logging in. " +
                                "Would you like to request for this feature?").showDialog();
                        break;
                    }
                    case ("Fast Balance Deactivation"): {
                        new DialogFastBalDeactivate(Settings.this, "Fast Balance Deactivation", "This will deactivate the fast balance feature. " +
                                "Would you like to continue?").showDialog();
                        break;
                    }
                    case ("Forgot PIN code"): {

                        Intent forgotPinIntent = new Intent(Settings.this, ForgotPin.class);
                        forgotPinIntent.putExtra("token", token);
                        startActivity(forgotPinIntent);
                        break;
                    }

                    case ("Biometric Login Settings"): {

                        Intent intent = new Intent(Settings.this, BiometricActivation.class);
                        intent.putExtra("token", token);
//                        intent.putExtra("userId", usrName);
                        startActivity(intent);
                        break;
                    }

                }
            }


        });

        //call in onCreate
        //  setAppIdleTimeout();
    }

//
//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(Settings.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(Settings.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }

    public String confirmJSON(String authToken, String pwd1, String pwd2) {
        return "authToken=" + authToken +
                "&pwd1=" + pwd1 +
                "&pwd2=" + pwd2;


    }


    public String pinConfirmJSON(String authToken, String pin1, String pin2) {
        return "authToken=" + authToken +
                "&oldpin=" + pin1 +
                "&newpin=" + pin2;


    }


    public String pinConfirmResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = pinConfirmJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "pinchange", json);

        System.out.println("response: " + response);

        return response;
    }

    public class pinChangeFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(Settings.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = pinConfirmResponse(token, oldpin, newpin1);
                System.out.println("response: " + response);
                System.out.println("token: " + token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        //	       				lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        //					System.out.println(mb_userId);
//					System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry...", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry...", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(Settings.this, "Request successful", mbAcc).showDialog();

                }

            }

        }
    }


    public String nicknameJSON(String authToken, String nn1, String nn2) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&nickname1=" + nn1 +
                "&nickname2=" + nn2 +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");


    }


    public String nicknameResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = nicknameJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "changenickname", json);

        System.out.println("response: " + response);

        return response;
    }

    public class nicknameFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(Settings.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = nicknameResponse(token, newNickname1, newNickname2);
                System.out.println("response: " + response);
                System.out.println("token: " + token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        //					lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
                        mb_accDetails = jsonObject.getString("mb_accDetails");

                        //					System.out.println(mb_userId);
//					System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry...", mb_accDetails).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry...", mb_accDetails).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(Settings.this, "Request successful", mb_accDetails).showDialog();

                }

            }

        }
    }


    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String confirmJSON(String authToken) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");

    }

    public String confirmResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "fastbalreqt", json);

        System.out.println("response: " + response);

        return response;
    }

    public class fastBalFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(Settings.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = confirmResponse(token);
                System.out.println("response: " + response);
                System.out.println("token: " + token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mbAcc);
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry...", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(Settings.this, "Successful", mbAcc).showDialog();

                }

            }

        }
    }


    public String confirmJSON2(String authToken) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");

    }


    public String confirmDeactivate(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON2(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "fastbaldeactivate", json);

        System.out.println("response: " + response);

        return response;
    }

    public class fastBalDeactivateFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(Settings.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = confirmDeactivate(token);
                System.out.println("response: " + response);
                System.out.println("token: " + token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mbAcc);
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry...", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "Sorry", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(Settings.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(Settings.this, "Successful", mbAcc).showDialog();

                }

            }

        }
    }


    public class ResponseDialogFastBal extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialogFastBal(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialogFastBal(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fastBalFireMan().execute();
                    } else {
                        showAlertDialog(Settings.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                  //  new fastBalFireMan().execute();
                }
            });

            setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });

        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class DialogFastBalDeactivate extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public DialogFastBalDeactivate(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public DialogFastBalDeactivate(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fastBalDeactivateFireMan().execute();
                    } else {
                        showAlertDialog(Settings.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                 //   new fastBalDeactivateFireMan().execute();
                }
            });

            setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });

        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();
//					Intent confirmTransIntent = new Intent(ConfirmTrans.this,MainMenu.class);
//					startActivity(confirmTransIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //		menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                Settings.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        Settings.this.finish();
    }

}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RequestList extends AppCompatActivity {

    int[] arrow = new int[]{R.drawable.more};
    int[] alias_pic = new int[]{R.drawable.ic_action_alias};
    int[] next_pic = new int[]{R.drawable.more};
    String token = null;

    Dialog dialog;
    TextView titleTextView;
    TextView transferRate;
    TextView noteRate;

    TextView sameBank;
    TextView otherBank;

    String oldPassword = null;
    String newPassword1 = null;
    String newPassword2 = null;
    String[] menus = {"Statement Request", "Cheque Book Request",  /*"View Forex Rate",*/ "Stop Cheque"/*, "ATM Card Request", "Block ATM card"*/};
    String[] rates = {"Transfer rate", "Note rate"/*, "Cross rate"*/};
    String[] beneficiaries = {"Same bank", "Other bank"};
    String rateType = null;
    String beneficiaryType = null;
    String chosenRateType = null;
    String chosenBeneficiaryType = null;
    List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();
    ListView list;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_transDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    //	String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    ProgressDialog progress;
    String response = null;
    String title = null;

    int[] pics = new int[]{
            R.drawable.ic_statement,
            R.drawable.ic_chequebk_reqt,
            R.drawable.ic_forex_rate,
            R.drawable.ic_chequebk_reqt,
            R.drawable.ic_atm_reqt,
            R.drawable.ic_statement

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.requests);
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        token = getIntent().getExtras().getString("token").trim();

        System.out.println(menus.length);

        for (int a = 0; a < menus.length; a++) {

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("menuitem", menus[a]);
            hm.put("pix", Integer.toString(arrow[0]));
            hm.put("icons", Integer.toString(pics[a]));

            aList.add(hm);

        }

        String[] from = {"menuitem", "pix", "icons"};
        int[] to = {R.id.menu_item, R.id.flag, R.id.icon};

        SimpleAdapter adpt = new SimpleAdapter(this, aList, R.layout.list_requests, from, to);
        list = (ListView) findViewById(R.id.request_list);
        list.setAdapter(adpt);
        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //accountName = ((TextView) v.findViewById(R.id.acc_name)).getText().toString().trim();

                String dataToCompare = ((TextView) v.findViewById(R.id.menu_item)).getText().toString().trim();

                switch (dataToCompare) {
                    case ("Statement Request"): {
                        Intent stmtIntent = new Intent(RequestList.this, StatementReqt.class);
                        stmtIntent.putExtra("token", token);
                        startActivity(stmtIntent);
                        //					Toast.makeText(RequestList.this, dataToCompare, Toast.LENGTH_LONG).show();
                        break;
                    }
                    case ("Cheque Book Request"): {
                        Intent stmtIntent = new Intent(RequestList.this, ChequeBookReqt.class);
                        stmtIntent.putExtra("token", token);
                        startActivity(stmtIntent);
                        break;
                    }

                    /*case ("View Forex Rate"): {
                        Intent fxIntent = new Intent(RequestList.this, FxRatesReqt.class);
                        fxIntent.putExtra("token", token);

                        startActivity(fxIntent);


                        break;
                    }*/
                    case ("ATM Card Request"): {
                        Intent atmIntent = new Intent(RequestList.this, AtmRequest.class);
                        atmIntent.putExtra("token", token);
                        startActivity(atmIntent);
                        break;
                    }
                    case ("Stop Cheque"): {
                        Intent stopChequeIntent = new Intent(RequestList.this, StopCheque.class);
                        stopChequeIntent.putExtra("token", token);
                        startActivity(stopChequeIntent);
                        break;
                    }
                    case ("Block ATM card"): {
                        Intent atmIntent = new Intent(RequestList.this, AtmBlock.class);
                        atmIntent.putExtra("token", token);
                        startActivity(atmIntent);
                        break;
                    }


                }
            }


        });

        //call in onCreate
      //  setAppIdleTimeout();
    }

//
//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(RequestList.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(RequestList.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }

    public String confirmJSON(String authToken) {
        return "authToken=" + authToken;

    }


    public String confirmResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "fastbalreqt", json);

        System.out.println("response: " + response);

        return response;
    }

    public class fastBalFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(RequestList.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = confirmResponse(token);
                System.out.println("response: " + response);
                System.out.println("token: " + token);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
                    mbAcc = jsonObject.getString("mb_accDetails");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mbAcc);
                    progress.cancel();
                    new ResponseDialog(RequestList.this, "Sorry...", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(RequestList.this, "Sorry", mbAcc).showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(RequestList.this, "Successful", mbAcc).showDialog();

                }

            }

        }
    }


    public String confirmJSON2(String authToken) {
        return "authToken=" + authToken;

    }


    public String confirmDeactivate(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON2(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "fastbaldeactivate", json);

        System.out.println("response: " + response);

        return response;
    }

    public class fastBalDeactivateFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(RequestList.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = confirmDeactivate(token);
                System.out.println("response: " + response);
                System.out.println("token: " + token);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
                    mbAcc = jsonObject.getString("mb_accDetails");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mbAcc);
                    progress.cancel();
                    new ResponseDialog(RequestList.this, "Sorry...", mbAcc).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(RequestList.this, "Sorry", mbAcc).showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(RequestList.this, "Successful", mbAcc).showDialog();

                }

            }

        }
    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    finish();
                    //					Intent confirmTransIntent = new Intent(ConfirmTrans.this,MainMenu.class);
                    //					startActivity(confirmTransIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class ResponseDialogFastBal extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialogFastBal(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialogFastBal(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    new fastBalFireMan().execute();
                }
            });

            setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });

        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class DialogFastBalDeactivate extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public DialogFastBalDeactivate(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public DialogFastBalDeactivate(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    new fastBalDeactivateFireMan().execute();
                }
            });

            setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });

        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //		menu.findItem(R.id.refresh).setVisible(false);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                RequestList.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        RequestList.this.finish();
    }

}

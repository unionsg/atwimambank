package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;
import com.unionsg.otherservices.PasswordValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;

public class ChangePassword extends AppCompatActivity {


    private EditText answerEditText;
    private Button confirmButton;
    Spinner spinner1;

    String token = null;
    String pin = null;
    String pin2 = null;
    //	String userId = null;
    String response = null;
    String mbAcc = null;
    String response2 = null;
    String mb_response2 = null;

    ProgressDialog progress;
    ProgressDialog progress2;

    String usrName = null;
    //    String usrId = null;
    String custType = null;
    String mb_token = null;
    String mb_acc_details = null;
    String mb_userName = null;
    String creationTime = null;
    String balances = null;
    String beneInternal = null;
    String beneExternal = null;
    String mb_userId = null;
    String mb_response = null;
    String mb_transDetails = null;

    String id = null;
    String lastModificationDate = null;
    String mbAcc2 = null;
    String questions = null;
    String qCode = null;
    String qDesc = null;
    String questionCode = null;
    String questionDesc = null;
    String answer = null;
    String codeFlag = null;
    String codeQuestions[] = null;
    String userId = null;


    EditText oldPasswordEdittext;
    EditText newPasswordEdittext1;
    EditText newPasswordEdittext2;

    String oldPassword = null;
    String newPassword1 = null;
    String newPassword2 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        usrName = getIntent().getExtras().getString("userName").trim();
//        userId = getIntent().getExtras().getString("userId").trim();
        token = getIntent().getExtras().getString("token").trim();


        spinner1 = (Spinner) findViewById(R.id.questions_spinner);
        answerEditText = (EditText) findViewById(R.id.answer_edittext);
        oldPasswordEdittext = (EditText) findViewById(R.id.old_password);
        newPasswordEdittext1 = (EditText) findViewById(R.id.new_password1);
        newPasswordEdittext2 = (EditText) findViewById(R.id.new_password2);

        confirmButton = (Button) findViewById(R.id.confirm_button);

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new questionsFireMan().execute();
        } else {
            showAlertDialog(ChangePassword.this, "No Internet Connection",
                    "You don't have internet connection", false);
        }

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				userId = userIdEditText.getText().toString();

                answer = answerEditText.getText().toString();
                oldPassword = oldPasswordEdittext.getText().toString().trim();
                newPassword1 = newPasswordEdittext1.getText().toString().trim();
                newPassword2 = newPasswordEdittext2.getText().toString().trim();

                if (answer.trim().isEmpty()) {
                    answerEditText.setError("Enter your security answer");
                } else if (oldPassword.length() == 0) {
                    oldPasswordEdittext.setError("Please enter current password");
                } else if (newPassword1.length() == 0) {
                    newPasswordEdittext1.setError("Please enter new password");
                } else if (newPassword2.length() == 0) {
                    newPasswordEdittext2.setError("Please re-enter new password");
                } else if (!newPassword2.equals(newPassword1)) {
                    newPasswordEdittext2.setError("Does not match what you typed in the previous field");
                } else {

                    System.out.println(oldPassword);
                    System.out.println(newPassword1);
                    System.out.println(newPassword2);

                    PasswordValidator pwdValidate = new PasswordValidator();
                    if (!pwdValidate.validateLength(newPassword1)) {
                        new PasswordResponseDialog(ChangePassword.this, "Weak password", "Your password must be at least 8 characters " +
                                "and at most 19 characters").showDialog();

                    } else if (!pwdValidate.validateNumber(newPassword1)) {
                        new PasswordResponseDialog(ChangePassword.this, "Weak password", "Your password must contain a number(eg. 5)").showDialog();

                    } else if (!pwdValidate.validateLowercase(newPassword1)) {
                        new PasswordResponseDialog(ChangePassword.this, "Weak password", "Your password must contain a lowercase character(eg. d)").showDialog();

                    } else if (!pwdValidate.validateUppercase(newPassword1)) {
                        new PasswordResponseDialog(ChangePassword.this, "Weak password", "Your password must contain an uppercase character(eg. D)").showDialog();

                    } else if (!pwdValidate.validateSymbol(newPassword1)) {
                        new PasswordResponseDialog(ChangePassword.this, "Weak password", "Your password must contain a special character(`,*,',~,!,@,#,$,%,^,&,,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?,\\\\").showDialog();

                    } else {
//                        final String[] specialChars = {"~","!","@","#","$","%","^","&" ,",","(",")","-","_","=","+","[","{","]","}","|",";",":","<",">","/","?"};
//                        newPassword1 = newPassword1.replaceAll("[,~,!,@,#,$,%,^,&,,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?]","\\");
//                        newPassword1 = newPassword1.replaceAll("[,~,!,@,#,$,%,^,&,,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?]","\\");
//                        newPassword1 = escapeSpecialChars(newPassword1);
                        System.out.println("newPassword1 is now: " + newPassword1);
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new confimFireMan().execute();
                        } else {
                            showAlertDialog(ChangePassword.this, "No Internet Connection",
                                    "You don't have internet connection", false);
                        }
                    }
                }
            }
        });


        //call in onCreate
        //  setAppIdleTimeout();
    }


    public String escapeSpecialChars(String inputString){
        final String[] metaCharacters = {"'", "`", "\\", "*", "~", "!" ,"@" , "#", "$", "%", "^", "&" , "," , "(" , ")" , "-", "_", "=", "+", "[", "{", "]", "}", "|", ";", ":", "<", ">", "/","?"};
        String outputString="";
        for (int i = 0 ; i < metaCharacters.length ; i++){
            if(inputString.contains(metaCharacters[i])){
                outputString = inputString.replace(metaCharacters[i],"\\"+metaCharacters[i]);
                inputString = outputString;
            }
        }
        return outputString;
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(ForgotPassword.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(ForgotPassword.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//      //  Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }


    public void populateQuestions() {

//        ListView list = (ListView) findViewById(R.id.ach_banklist);
        codeQuestions = questions.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(codeQuestions.length);

        for (int a = 0; a < codeQuestions.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(codeQuestions[a], "~");
            qCode = myAccToken.nextToken().trim();
            qDesc = myAccToken.nextToken().trim();

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("branchname", qDesc);
            hm.put("bankcode", qCode);
            theList.add(hm);

        }

        String[] from = {"branchname", "bankcode"};
        int[] to = {R.id.branchname, R.id.bankcode};


        SimpleAdapter adapter2 = new SimpleAdapter(ChangePassword.this, theList, R.layout.list_ach, from, to);


        spinner1.setAdapter(adapter2);

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                questionDesc = ((TextView) view.findViewById(R.id.branchname)).getText().toString().trim();
                questionCode = ((TextView) view.findViewById(R.id.bankcode)).getText().toString().trim();
                System.out.println(questionDesc);
                System.out.println(questionCode);
//							searchEditText.setText(brName);
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });

    }


    public String getQuestionsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myQuestionsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getQuestionsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getUserQues", json);

        System.out.println(response);

        return response;
    }


    public class questionsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(ChangePassword.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Loading security question...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myQuestionsResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
//                    id = jsonObject.getString("id");
//                    creationTime = jsonObject.getString("creationTime");
//                    lastModificationDate = jsonObject.getString("lastModificationDate");
//                    mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                showAlertDialog(ChangePassword.this, "Error!", "No response from server. Please check your Internet connection and try again", false);
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(ChangePassword.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(ChangePassword.this, "Sorry...", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(ChangePassword.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    System.out.println(mbAcc);
                    questions = mbAcc;
                    populateQuestions();

                }

            }


        }

    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String confirmJSON(String authToken, String pwd1, String pwd2, String secAnswer) throws UnsupportedEncodingException {
        return "authToken=" + URLEncoder.encode(authToken, "UTF-8") +
                "&pwd1=" + URLEncoder.encode(pwd1, "UTF-8") +
                "&pwd2=" + URLEncoder.encode(pwd2, "UTF-8") +
                "&secAnswer=" + URLEncoder.encode(secAnswer, "UTF-8") +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");


    }

    public String confirmResponse(String a1, String a2, String a3, String a4) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = confirmJSON(a1, a2, a3, a4);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "pwdchange", json);

        System.out.println("response: " + response);

        return response;
    }

    public class confimFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(ChangePassword.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response2 = confirmResponse(token, oldPassword, newPassword1, answer);
                System.out.println("response: " + response2);
                System.out.println("token: " + token);

                try {
                    if (!response2.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response2);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response2 = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
                        mb_transDetails = jsonObject.getString("mb_transDetails");

                        //					System.out.println(mb_userId);
//					System.out.println(mbAcc);

                    } else {
                        mb_response2 = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //			Toast.makeText(Settings.this, mb_response.trim(), Toast.LENGTH_LONG).show();

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response2.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response2);
                    progress.cancel();
                    new ResponseDialog(ChangePassword.this, "Sorry...", mb_transDetails).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response2.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(ChangePassword.this, "No response", "Please try again").showDialog();
                } else if (mb_response2.trim().equals("12")) {
                    progress.cancel();
                    new ResponseDialog(ChangePassword.this, "Sorry...", mb_transDetails).showDialog();

                } else if (mb_response2.trim().equals("00")) {
                    progress.cancel();
                    //					System.out.println(mbAcc);
                    new ResponseDialog(ChangePassword.this, "Request successful", "Password has been changed successfully. You can use your new password the next time you login").showDialog();

                }

            }

        }
    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does

//                    if (codeFlag.equals("setup")){

                    finish();
//                    } else if (codeFlag.equals("empty")){

//                        Intent loginIntent = new Intent(LoginChangePassword.this, MainMenu.class);
//                        loginIntent.putExtra("token", token);
//                        loginIntent.putExtra("userName", usrName);
//                        loginIntent.putExtra("userId", usrId);
//                        loginIntent.putExtra("mb_acc_details", custType);
//                        loginIntent.putExtra("creationTime", creationTime);
//                        loginIntent.putExtra("balances", balances);
//                        loginIntent.putExtra("beneInternal", beneInternal);
//                        loginIntent.putExtra("beneExternal", beneExternal);
//                        finish();
//                        startActivity(loginIntent);
//                    }
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                ChangePassword.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        ChangePassword.this.finish();
    }


    public class PasswordResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public PasswordResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public PasswordResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does

                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


}

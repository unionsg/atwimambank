package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;

public class LoanBalances extends Fragment {

    ListView listView;

    String facilityNumber = null;
    String currentBalance = null;
    String arrearsAmount = null;
    String loanType = null;
    String name = null;
    String email = null;
    String email2 = null;
    String amount = null;
    String phone = null;

    private TextView feesTextView;
    String mbAccDetails = null;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String successMsg = null;
    String[] loanBalDetails = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String facilityNum = null;
    String loanBal = null;
    String curr = null;
    String description = null;
    String amtInArrears = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    ProgressDialog progress4;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String token = null;
    String accnumText;
    String usrId = null;
    String response = null;
    Dialog dialog;
    String balFromFile = null;

    static int flag = 0;
    int colorFlag = 0;

    int[] colorList = new int[]{R.color.slcb_red, R.color.slcb_blue};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        token = getActivity().getIntent().getExtras().getString("token").trim();

//        pattern = Pattern.compile(regex);

        System.out.println("token: " + token);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//		token = getActivity().getIntent().getExtras().getString("token").trim();
//		System.out.println("token: " + token);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.loan_balances, container, false);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");

        listView = (ListView) view.findViewById(R.id.loan_bal_list);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            new loanBalancesFireMan().execute();
        } else {

            showAlertDialog(getActivity(), "No Internet Connection",
                    "You don't have internet connection", false);

        }

//        balFromFile = readLoanBalancesFromFile(getActivity());

//        if (flag == 1) {
//
//            System.out.println("not empty");
//
//        } else if (flag == 0){
//            System.out.println("empty");
//
//        }


//        populateSpinner1();


        return view;
    }


    public String acctsJSON(String authToken) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");

    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "loanbal", json);

        System.out.println(response);

        return response;
    }


    public class loanBalancesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(getActivity());
            progress2.setCancelable(false);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);

                try {

                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_transDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                new ResponseDialog(getActivity(), "Error!", "No response from server. Please check your Internet connection and try again").showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error!", "Connection timed out. Please check your Internet connection and try again").showDialog();

                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
                    getActivity().finish();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error !", "Unknown Error").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

//                    writeLoanBalancesToFile(mbAcc, getActivity());

                    loanBalDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println("loanBalDetails.length: " + loanBalDetails.length);

                    for (int a = 0; a < loanBalDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(loanBalDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        facilityNum = myAccToken.nextToken().trim();
                        loanBal = myAccToken.nextToken().trim();
                        description = myAccToken.nextToken().trim();
                        amtInArrears = myAccToken.nextToken().trim();
                        loanBal = GlobalCodes.FormatToMoney(loanBal);
                        amtInArrears = GlobalCodes.FormatToMoney(amtInArrears);

                        GlobalCodes.breakUpAmt(loanBal);
                        GlobalCodes.breakUpLedger(amtInArrears);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("facilityNum", facilityNum);
                        hm.put("description", description);
                        hm.put("curr", GlobalCodes.currAvail);
                        hm.put("curr2", GlobalCodes.currLedger);
                        hm.put("loanBal", GlobalCodes.amtAvail + "." + GlobalCodes.decimalAvail);
                        hm.put("amtInArrears", GlobalCodes.amtLedger + "." + GlobalCodes.decimalLedger);
                        if (colorFlag == 0) {
                            hm.put("colors", Integer.toString(colorList[colorFlag]));
                            colorFlag = 1;
                        } else if (colorFlag == 1) {
                            hm.put("colors", Integer.toString(colorList[colorFlag]));
                            colorFlag = 0;
                        }
                        /*hm.put("loanBal", loanBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("amtInArrears", "   " + amtInArrears
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));*/


                        theList.add(hm);

                    }

                    String[] from = {"next", "facilityNum", "description", "loanBal", "amtInArrears", "colors"};
                    int[] to = {R.id.flag, R.id.facility_num, R.id.description, R.id.current_bal, R.id.amt_arrears, R.id.my_color};

                    SimpleAdapter adapter = new SimpleAdapter(getActivity(), theList, R.layout.list_loanaccounts, from, to);

                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                                long arg3) {
                            // TODO Auto-generated method stub
                            facilityNumber = ((TextView) v.findViewById(R.id.facility_num)).getText().toString().trim();
                            loanType = ((TextView) v.findViewById(R.id.description)).getText().toString().trim();
                            currentBalance = ((TextView) v.findViewById(R.id.current_bal)).getText().toString().trim();
                            arrearsAmount = ((TextView) v.findViewById(R.id.amt_arrears)).getText().toString().trim();
                            System.out.println("facility no: " + facilityNumber);

                            ConnectionDetector cd = new ConnectionDetector(getActivity());
                            Boolean isInternetPresent = cd.isConnectingToInternet();
                            if (isInternetPresent) {
                                System.out.println("list view fireman");
                                new fireMan().execute();
                            } else {

                                showAlertDialog(getActivity(), "No Internet Connection",
                                        "You don't have internet connection", false);

                            }

                        }
                    });

                }

            }

        }

    }


    private void writeLoanBalancesToFile(String bal, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("loanbal.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(bal);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readLoanBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("loanbal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }

    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String blinkPayJSON(String facilityNo, String authToken) {

        return "facility_no=" + facilityNo +
                "&authToken=" + authToken;

    }


    public String myResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = blinkPayJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "loantrans", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending details...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(facilityNumber, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
//                        id = jsonObject.getString("id");
                        mbAcc = jsonObject.getString("mb_transDetails");
//                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");

                        System.out.println(mb_response);
                        System.out.println(successMsg);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub


            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);
                System.out.println(mb_response);
                System.out.println(successMsg);

                if (mb_response.trim().equals("21")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "Transaction unsuccessful", successMsg).showDialog();
                    //					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("20")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "Insufficient funds", successMsg).showDialog();

                } else if (mb_response.trim().equals("18")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "Transaction unsuccessful", successMsg).showDialog();

                } else if (mb_response.trim().equals("99")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "Transaction unsuccessful", successMsg).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    Intent intent = new Intent(getActivity(), LoanSchedule.class);
                    intent.putExtra("facilityNum", facilityNumber);
                    intent.putExtra("schedule", mbAcc);
                    intent.putExtra("arrears", arrearsAmount);
                    intent.putExtra("currentBal", currentBalance);
                    intent.putExtra("loanType", loanType);
//                    intent.putExtra("flag", 1);
                    startActivity(intent);


                }

            }


        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    getActivity().finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
//                getActivity().finish();  //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
//                flag = 0;
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onBackPressed() {
//        flag = 0;
        getActivity().finish();
    }


}

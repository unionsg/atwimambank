package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class TopUp extends AppCompatActivity {

	private Spinner spinner1, spinner2;
	private EditText amtEditText;
	private EditText numEditText;
	private Button confirmButton;
	
	String mbAccDetails = null;
	String[] accDetail = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	//	String accDetails = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String telcos = null;
	String aliasAcc = null;

	String accNum = null;
	String accLedgerBal = null;
	String accAvailBal = null;
	String accName = null;

	String accountNum = null;
	String accountName = null;

	ProgressDialog progress1;
	ProgressDialog progress2;
	int pos;
	String network = null;
	String phoneNum = null;
	String topupAmt = null;
	String topAmt = null;

	int[] next = new int[]{	R.drawable.more };
	List<HashMap<String,String>> aList;
	View view;

	String token = null;

	String accnumText;

	String response = null;
	String acctsResponse = null;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.topup);

		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
		token = getIntent().getExtras().getString("token").trim();
//		accDetail = mbAccDetails.split(",");


		new myAcctsFireMan().execute();


//		System.out.println(accDetail.length);

		spinner2 = (Spinner) findViewById(R.id.spinner2);
		numEditText = (EditText) findViewById(R.id.numberedittext);
		amtEditText = (EditText) findViewById(R.id.amtedittext);
		confirmButton = (Button)findViewById(R.id.confirm_button);
		spinner1 = (Spinner) findViewById(R.id.spinner1);

		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				//				accountNum = parent.getItemAtPosition(pos).toString();
				accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();

			}

			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing

			}
		});

		spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				network = ((TextView) view.findViewById(R.id.telco_name)).getText().toString().trim(); 
//				aliasName = ((TextView) view.findViewById(R.id.alias_name)).getText().toString().trim(); 
			}

			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing

			}
		});

		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				topupAmt = amtEditText.getText().toString();
				topAmt = topupAmt;
				phoneNum = numEditText.getText().toString();
				
				if (phoneNum.trim().isEmpty()){numEditText.setError("Enter Phone Number");}
				else if (topupAmt.trim().isEmpty()){amtEditText.setError("Enter Topup Amount");}
				else{
//				Intent confirmTopupIntent = new Intent(TopUp.this,ConfirmTopup.class);
//				confirmTopupIntent.putExtra("dbAcc", accountNum);
//				confirmTopupIntent.putExtra("network", network);
//				confirmTopupIntent.putExtra("num", phoneNum);
//				confirmTopupIntent.putExtra("amtDisplay", GlobalCodes.FormatToMoney(topAmt));
//					confirmTopupIntent.putExtra("amt", topupAmt);
//				confirmTopupIntent.putExtra("token", token);
//				startActivity(confirmTopupIntent);

				}
			}
		});
		
		
		

	
	}



	public String fundsTransJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = fundsTransJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"telcos", json);

		System.out.println(response);

		return response;
	}



	public class fireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress1 = new ProgressDialog(TopUp.this);
			progress1.setCancelable(false);
			progress1.setTitle("Please wait");
			progress1.setMessage("Retrieving networks...");
			progress1.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myResponse(token);

				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId"); 
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress1.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress1.cancel();
					new ResponseDialog(TopUp.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				}
				else if(mb_response.trim().equals("11") ){
					progress1.cancel();
					new ResponseDialog(TopUp.this, "Error !", "Unknown Error");					
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress1.cancel();
					/*if(mbAcc.isEmpty()){
						new ResponseDialog(TopUp.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds");
					}
					else{*/
						System.out.println(mbAcc);

						//					StringTokenizer accDetToken = new StringTokenizer(string)
//						aliasAccDetails = mbAcc.split("");

						List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

//						System.out.println(aliasAccDetails.length);
						StringTokenizer accDetToken = new StringTokenizer(mbAcc, "~");

						for(int a=0;accDetToken.hasMoreTokens();a++){
							
							System.out.println(accDetToken.countTokens());

							telcos = accDetToken.nextToken().trim();
//							beneAcc = accDetToken.nextToken().trim();

							System.out.println(telcos);
//							
							HashMap<String, String> hm = new HashMap<String,String>();

							hm.put("telconame", telcos);
//							hm.put("aliasaccnum", beneAcc "**********"+ .lastIndexOf(0, -3)  );

							theList.add( hm);

						}

						String[] from = { "telconame" };
						// Ids of views in listview_layout
						int[] to = { R.id.telco_name};

						SimpleAdapter adapter2 = new SimpleAdapter(TopUp.this, theList, R.layout.list_telcos , from , to);

						spinner2.setAdapter(adapter2);
					

					//				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
					//				loginIntent.putExtra("token", mb_token);
					//				loginIntent.putExtra("userName", mb_userName);
					//				loginIntent.putExtra("mb_acc_details", mb_acc_details); 
					//				startActivity(transDetailsIntent);
				}

			}

		}

	}





	public String acctsJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myacctsResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = acctsJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"accounts", json);

		System.out.println(response);

		return response;
	}



	public class myAcctsFireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress2 = new ProgressDialog(TopUp.this);
			progress2.setCancelable(true);
			progress2.setTitle("Please wait");
			progress2.setMessage("Retrieving accounts...");
			progress2.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myacctsResponse(token);
//				System.out.println(response);
				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress2.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress2.cancel();
					new ResponseDialog(TopUp.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				}
				else if(mb_response.trim().equals("11") ){
					progress2.cancel();
					new ResponseDialog(TopUp.this, "Error !", "Unknown Error");
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress2.cancel();

					System.out.println(mbAcc);

					aliasAccDetails = mbAcc.split("`");

					List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

					System.out.println(aliasAccDetails.length);

					for(int a=0;a<aliasAccDetails.length-1;a++){
						StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
						accNum = myAccToken.nextToken().trim();
						accName = myAccToken.nextToken().trim();
						accAvailBal = myAccToken.nextToken().trim();
						accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
						accLedgerBal = myAccToken.nextToken().trim();
						accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);

						System.out.println(accNum);
						System.out.println(accName);

						HashMap<String, String> hm = new HashMap<String,String>();

						hm.put("accname", accName);
						hm.put("accnum",  accNum  );
						hm.put("bal", accAvailBal);
						hm.put("ledgerbal", "   " + accLedgerBal);

						theList.add(hm);

					}

					String[] from = { "next","accname", "bal", "accnum", "ledgerbal" };
					int[] to = { R.id.flag,R.id.acc_name,R.id.avail_bal,R.id.acc_num, R.id.ledger_bal};

					SimpleAdapter adapter = new SimpleAdapter(TopUp.this, theList, R.layout.list_from , from , to);

					spinner1.setAdapter(adapter);


					//				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
					//				loginIntent.putExtra("token", mb_token);
					//				loginIntent.putExtra("userName", mb_userName);
					//				loginIntent.putExtra("mb_acc_details", mb_acc_details);
					//				startActivity(transDetailsIntent);
				}

			}

			new fireMan().execute();

		}

	}








	// add items into spinner dynamically
	/*public void addItemsOnSpinner1() {

		aList = new ArrayList<HashMap<String,String>>();

		for(int a=0;a<accDetail.length;a++){
			StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

			if (a==0){
				loanProdCode = "Transfer From";
				loanProdCode = "";
				accAvailBal = "";
				accLedgerBal = "";
			}else{

			accNum = myAccToken.nextToken().trim();
			loanProdCode = myAccToken.nextToken().trim();
			accAvailBal = myAccToken.nextToken().trim();
			accLedgerBal = myAccToken.nextToken().trim();
			//			String d = myAccToken.nextToken().trim();
			System.out.println(accNum);
			System.out.println(loanProdCode);
			//			System.out.println(accAvailBal);
			//			System.out.println(accLedgerBal);

			HashMap<String, String> hm = new HashMap<String,String>();

			hm.put("accname", loanProdCode);
			hm.put("accnum",  accNum  );
			hm.put("bal", accAvailBal);
			hm.put("ledgerbal", "   " + accLedgerBal);
			//			hm.put("next", Integer.toString(next[0]) );

			aList.add(hm);


		}		


	}*/



	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
					//					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
					//					startActivity(loginIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}


//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findItem(R.id.refresh).setVisible(false);
//		return true;
//	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			//	         NavUtils.navigateUpFromSameTask(this);
			TopUp.this.finish();
			return true;
		}
//		return super.onOptionsItemSelected(item);
		return false;
	}
	public void onBackPressed() {
		//		moveTaskToBack(true); 
		TopUp.this.finish();
	}
	



}

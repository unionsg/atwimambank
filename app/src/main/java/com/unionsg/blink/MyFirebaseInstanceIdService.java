package com.unionsg.fbcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    public MyFirebaseInstanceIdService() {
    }


    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d("Token", "Refreshed token: " + token);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(token);

        setToken(token);


    }


    public String setToken(String deviceToken){
//        String deviceToken;

        return deviceToken;
    }


    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }

}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class BiometricActivation extends AppCompatActivity {

    private EditText pinEditText;
    Dialog dialog;
    private Button declineButton;
    private Button confirmButton;
    String[] accDetail = null;
    Spinner spinner1;
    String codeQuestions[] = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;
    String beneTel = null;
    String beneCode = null;
    String pin = null;
    String answer = null;

    String accNum = null;
    String accLedgerBal = null;
    String accType = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;
    String curr = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String accnumText;

    String response = null;
    String balFromFile = null;
    String beneFromFile = null;
    String narration = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String codeFlag = null;
    String questions = null;

    String qCode = null;
    String qDesc = null;
    String questionCode = null;
    String questionDesc = null;
    String choice = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biometric_activation);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		mbAccDetails = getIntent().getExtras().getString("accdetails").trim();
        token = getIntent().getExtras().getString("token").trim();

        declineButton = (Button) findViewById(R.id.decline_button);
        confirmButton = (Button) findViewById(R.id.confirm_button);


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                choice = "accept";
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new questionsFireMan().execute();
                } else {
                    showAlertDialog(BiometricActivation.this, "No Internet Connection",
                            "You don't have internet connection", false);
                }

            }
        });

        declineButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                choice = "decline";
                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new questionsFireMan().execute();
                } else {
                    showAlertDialog(BiometricActivation.this, "No Internet Connection",
                            "You don't have internet connection", false);
                }

            }
        });


    }


    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }




    private void buildDialog(int animationSource) {

        dialog = new Dialog(BiometricActivation.this);
        dialog.setContentView(R.layout.confirm_activate_biometric);
        TextView titleTextView = (TextView) dialog.findViewById(R.id.title_tv);
        if (choice.equals("activate")) {
            titleTextView.setText(R.string.bio_activation);
        } else if (choice.equals("deactivate")){
            titleTextView.setText(R.string.bio_deactivation);
        }

        final TextView secAnswerTV = (TextView) dialog.findViewById(R.id.answer_edittext);

        pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);
        spinner1 = (Spinner) dialog.findViewById(R.id.questions_spinner);

        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        spinner1.setAdapter(populateQuestions());

        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                questionDesc = ((TextView) view.findViewById(R.id.branchname)).getText().toString().trim();
                questionCode = ((TextView) view.findViewById(R.id.bankcode)).getText().toString().trim();
                System.out.println(questionDesc);
                System.out.println(questionCode);
//							searchEditText.setText(brName);
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pin = pinEditText.getText().toString();
                answer = secAnswerTV.getText().toString();
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
                if (answer.trim().isEmpty()) {
                    secAnswerTV.setError("Enter your security answer");
                } else if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        if (choice.equals("activate")) {
                            new activateFireMan().execute();
                        } else if (choice.equals("deactivate")){
                            new deactivateFireMan().execute();
                        }
                    } else {
                        showAlertDialog(BiometricActivation.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                    //  new fireMan().execute();
                    dialog.dismiss();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();


    }




    public String getQuestionsJSON(String authToken)  {
        return "authToken=" + authToken;
    }


    public String myQuestionsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getQuestionsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getUserQues", json);

        System.out.println(response);

        return response;
    }


    public class questionsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(BiometricActivation.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Loading security question...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myQuestionsResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
//                    id = jsonObject.getString("id");
//                    creationTime = jsonObject.getString("creationTime");
//                    lastModificationDate = jsonObject.getString("lastModificationDate");
//                    mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);

                    questions = mbAcc;

                    buildDialog(R.style.DialogAnimation);

                }

            }


        }

    }



    public SimpleAdapter populateQuestions() {

//        ListView list = (ListView) findViewById(R.id.ach_banklist);
        codeQuestions = questions.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(codeQuestions.length);

        for (int a = 0; a < codeQuestions.length - 1; a++) {
            StringTokenizer myAccToken = new StringTokenizer(codeQuestions[a], "~");
            qCode = myAccToken.nextToken().trim();
            qDesc = myAccToken.nextToken().trim();

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("branchname", qDesc);
            hm.put("bankcode", qCode);
            theList.add(hm);

        }

        String[] from = {"branchname", "bankcode"};
        int[] to = {R.id.branchname, R.id.bankcode};


        SimpleAdapter adapter2 = new SimpleAdapter(BiometricActivation.this, theList, R.layout.list_ach, from, to);


        return adapter2;

    }





    public String getJSON(String authToken, String myPin, String secAnswer) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&pin=" + myPin +
                "&uniqueId=" + URLEncoder.encode(LoginActivity.id(BiometricActivation.this), "UTF-8") +
                "&secAns=" + secAnswer +
                "&device_ip=" + networkIP();
    }


    public String myResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "biosetup", json);

        System.out.println(response);

        return response;
    }


    public class activateFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(BiometricActivation.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Activating fingerprint login...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token, pin, answer);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
//                    id = jsonObject.getString("id");
//                    creationTime = jsonObject.getString("creationTime");
//                    lastModificationDate = jsonObject.getString("lastModificationDate");
//                    mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_message");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Error", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Successful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                } else if (mb_response.trim().equals("11")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Unsuccessful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                } else if (mb_response.trim().equals("99")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Unsuccessful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                }

            }


        }

    }




    public String getDeactivateJSON(String authToken, String myPin, String secAnswer) throws UnsupportedEncodingException {
        return "authToken=" + authToken +
                "&pin=" + myPin +
                "&secAns=" + secAnswer +
                "&device_ip=" + networkIP();
    }


    public String myDeactivateResponse(String a1, String a2, String a3) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getDeactivateJSON(a1, a2, a3);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "biodeactivate", json);


        System.out.println(response);

        return response;
    }


    public class deactivateFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(BiometricActivation.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Deactivating fingerprint login...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myDeactivateResponse(token, pin, answer);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
//                    id = jsonObject.getString("id");
//                    creationTime = jsonObject.getString("creationTime");
//                    lastModificationDate = jsonObject.getString("lastModificationDate");
//                    mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_message");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Error", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Successful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                } else if (mb_response.trim().equals("11")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Unsuccessful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                } else if (mb_response.trim().equals("99")) {
                    progress.cancel();
                    new ResponseDialog(BiometricActivation.this, "Unsuccessful", mbAcc).showDialog();
                    System.out.println(mbAcc);

                }

            }


        }

    }




    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                BiometricActivation.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        BiometricActivation.this.finish();
    }


}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.liststuff.FxRatesAdapter;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class FxNoteRate extends Fragment {

	private Context context;

	public FxNoteRate() {
		// Required empty public constructor
	}


	String mbAccDetails = null;
	String[] accDetail = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String aliasAcc = null;
	String[] fxDetails = null;
	String fxRates = null;
	String buy = null;
	String sell = null;
	String currPair = null;

	String accNum = null;
	String accName = null;
	String accountNum = null;

	TextView buyTextView;
	TextView sellTextView;
	TextView currTextView;
	ListView list;
	TextView buyLabelTextView;
	TextView sellLabelTextView;
	TextView currLabelTextView;

	ProgressDialog progress1;

	int[] next = new int[]{	R.drawable.ic_usd };
	List<HashMap<String,String>> aList;
	View view;

	String token = null;
	String rateType = null;
	String accnumText;

	String response = null;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		token = getActivity().getIntent().getExtras().getString("token").trim();
		rateType = "Note rate";


	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fx_note_rate, container, false);

		currTextView = (TextView) view.findViewById(R.id.cur);
		buyTextView = (TextView) view.findViewById(R.id.buy);
		sellTextView = (TextView) view.findViewById(R.id.sell);
		currLabelTextView = (TextView) view.findViewById(R.id.curLabel);
		buyLabelTextView = (TextView) view.findViewById(R.id.buyLabel);
		sellLabelTextView = (TextView) view.findViewById(R.id.sellLabel);
		list = (ListView) view.findViewById(R.id.fx_note_list);

		Typeface regularText = Typeface.createFromAsset(getActivity().getAssets(), "HelveticaRegular.otf");
//		Typeface mediumText = Typeface.createFromAsset(getAssets(), "HelveticaMedium.otf");

		currLabelTextView.setTypeface(regularText);
		buyLabelTextView.setTypeface(regularText);
		sellLabelTextView.setTypeface(regularText);

		ConnectionDetector cd = new ConnectionDetector(getActivity());
		Boolean isInternetPresent = cd.isConnectingToInternet();
		if (isInternetPresent) {
			new fireMan().execute();
		} else {
			new NoInternetResponseDialog(getActivity(), "Sorry", "You don't have Internet connection" +
					" to proceed with this request").showDialog();
		}

		return view;
	}



	public String fxJSON(String authToken){
		return "rateType="+rateType+
				"&authToken="+authToken;
	}


	public String myResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = fxJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"fxrates", json);

		System.out.println(response);

		return response;
	}



	public class fireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress1 = new ProgressDialog(getActivity());
			progress1.setCancelable(false);
			progress1.setTitle("Please wait");
			progress1.setMessage("Sending request...");
			progress1.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {

			try {
				response = myResponse(token);

				try {
					if (!response.isEmpty()) {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);
					} else {
						mb_response = "66";
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress1.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress1.cancel();
					new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again");
					//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
					getActivity().finish();
				} else if (mb_response.trim().equals("66")) {
					progress1.cancel();
					new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
				}
				else if(mb_response.trim().equals("00") ){
					progress1.cancel();
					System.out.println(mbAcc);

//					new ResponseDialog(FxRatesReqt.this, "Sent successfully", mbAcc + ". Please pick up your statement from the nearest branch").showDialog();

					List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();

					fxDetails = mbAcc.split(",");

					System.out.println(fxDetails.length);

					for(int a=0;a< fxDetails.length -1;a++){
						StringTokenizer myAccToken = new StringTokenizer(fxDetails[a], "~");

						currPair = myAccToken.nextToken().trim();
						sell = myAccToken.nextToken().trim();
						buy = myAccToken.nextToken().trim();


						//					String d = myAccToken.nextToken().trim();
						System.out.println(currPair);
						System.out.println(sell);
						System.out.println(buy);

						HashMap<String, String> hm = new HashMap<String,String>();


						hm.put("pair", currPair);
						hm.put("sell", sell );
						hm.put("buy", buy);
						hm.put("flag", Integer.toString(next[0]) );
						aList.add(hm);

					}

					String[] from = { "flag","pair","buy", "sell" };

					//				// Ids of views in listview_layout
					int[] to = { R.id.flag, R.id.cur, R.id.buy, R.id.sell};

					FxRatesAdapter adapter = new FxRatesAdapter(getActivity(),aList,R.layout.list_note_fx,from,to);
					list.setAdapter(adapter);



				}

			}

		}

	}









	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
//					getActivity().finish();
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}



	public class NoInternetResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public NoInternetResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public NoInternetResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					getActivity().finish();// It's just for info so we don't really care what this does
					//					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
					//					startActivity(loginIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}




}

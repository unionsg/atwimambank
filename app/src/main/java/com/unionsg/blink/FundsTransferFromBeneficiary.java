package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class FundsTransferFromBeneficiary extends AppCompatActivity {

	private Spinner spinner1;
	private EditText transferToEditText;
	private EditText amtEditText;
	private EditText narrationEditText;
	private Button confirmButton;
	String[] accDetail = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String alias = null;
	String aliasAcc = null;
	String beneTel = null;
	String beneCode = null;

	String accNum = null;
	String accLedgerBal = null;
	String accType = null;
	String accAvailBal = null;
	String accName = null;

	String accountNum = null;
	String accountName = null;
	String curr = null;

	ProgressDialog progress;
	ProgressDialog progress2;
	int pos;
	String aliasAccNum = null;
	String aliasName = null;
	String transAmt = null;

	int[] next = new int[]{	R.drawable.more };
	List<HashMap<String,String>> aList;
	View view;

	String accnumText;

	String response = null;
	String balFromFile = null;
	String beneFromFile = null;
	String narration = null;

	String usrName = null;
	String mbAccDetails = null;
	String token = null;
	String usrId = null;
	String beneInternal = null;
	String beneExternal = null;
	String custType = null;
	String balances = null;
	String questions = null;
	String codeFlag = null;
//	Handler handler;
//	Runnable r;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.funds_transfer_from_benef);

		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		aliasAccNum = getIntent().getExtras().getString("beneacc").trim();
		aliasName = getIntent().getExtras().getString("benename").trim();
		token = getIntent().getExtras().getString("token").trim();
		usrName = getIntent().getExtras().getString("userName").trim();
		usrId = getIntent().getExtras().getString("userId").trim();
		custType = getIntent().getExtras().getString("mb_acc_details").trim();
		balances = getIntent().getExtras().getString("balances").trim();
		beneInternal = getIntent().getExtras().getString("beneInternal").trim();
		beneExternal = getIntent().getExtras().getString("beneExternal").trim();
		creationTime = getIntent().getExtras().getString("creationTime").trim();
		questions = getIntent().getExtras().getString("questions").trim();
		codeFlag = getIntent().getExtras().getString("codeFlag").trim();

		spinner1 = (Spinner) findViewById(R.id.spinner1);
		transferToEditText = (EditText) findViewById(R.id.transferto_edittext);
		amtEditText = (EditText) findViewById(R.id.amtedittext);
		narrationEditText = (EditText) findViewById(R.id.narrationedittext);

		transferToEditText.setText(aliasAccNum);
		transferToEditText.setEnabled(false);

		confirmButton = (Button)findViewById(R.id.confirm_button);

		balFromFile = readBalancesFromFile(FundsTransferFromBeneficiary.this);
		beneFromFile = readBenefFromFile(FundsTransferFromBeneficiary.this);

		if (!balFromFile.equals("")) {
			System.out.println("balances not empty");
			mbAcc = balFromFile;
			System.out.println("MBACC balances: " + mbAcc);
			populateAccounts();
			/*if (!beneFromFile.equals("empty")){
				System.out.println("beneficiaries not empty");
				mbAcc = beneFromFile;
				System.out.println("MBACC benefs: " + mbAcc);
				populateBeneficiaries();
			} else {
//				new fireMan().execute();
				new ResponseDialog(FundsTransferFromBeneficiary.this, "Sorry", "You cannot proceed with this transfer because you have not created any SLCB beneficiary. " +
						"Please create a beneficiary to transfer funds").showDialog();

			}*/
		} else {
			ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
			Boolean isInternetPresent = cd.isConnectingToInternet();
			if (isInternetPresent) {
				new myAcctsFireMan().execute();
			} else {
				showAlertDialog(FundsTransferFromBeneficiary.this, "No Internet Connection",
						"You don't have internet connection", false);
			}
		}



		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

				//				accountNum = parent.getItemAtPosition(pos).toString();
				accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
				accAvailBal = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();

				if (accAvailBal.contains("Le")){
					curr = "Le";
				}else if (accAvailBal.contains("¢")){
					curr = "¢";
				}else if (accAvailBal.contains("$")){
					curr = "$";
				}else if (accAvailBal.contains("£")){
					curr = "£";
				}else if (accAvailBal.contains("€")){
					curr = "€";
				}else if (accAvailBal.contains("¥")){
					curr = "¥";
				}else if (accAvailBal.contains("¥")){
					curr = "¥";
				}

			}

			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing


			}
		});



		confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				transAmt = amtEditText.getText().toString();
				narration = narrationEditText.getText().toString();

				if (transAmt.trim().isEmpty()){
					amtEditText.setError("Enter transfer amount");
				} else{
					if (narration.trim().isEmpty()){
						narration = "MB FUNDS TRANSFER";
					}
//					amtEditText.setText("");

					Intent confirmTransIntent = new Intent(FundsTransferFromBeneficiary.this,ConfirmTrans.class);
					System.out.println(accountNum);
					System.out.println(aliasAccNum);
					System.out.println(curr);
					confirmTransIntent.putExtra("curr", curr);
					confirmTransIntent.putExtra("dbAcc", accountNum);
					confirmTransIntent.putExtra("crAcc", aliasAccNum);
					confirmTransIntent.putExtra("crAccName", aliasName);
					confirmTransIntent.putExtra("amt", transAmt);
					confirmTransIntent.putExtra("narration", narration.toUpperCase());
					confirmTransIntent.putExtra("token", token);
					confirmTransIntent.putExtra("userName", usrName);
					confirmTransIntent.putExtra("userId", usrId);
					confirmTransIntent.putExtra("mb_acc_details", custType);
					confirmTransIntent.putExtra("balances", balances);
					confirmTransIntent.putExtra("creationTime", creationTime);
					confirmTransIntent.putExtra("beneInternal", beneInternal);
					confirmTransIntent.putExtra("beneExternal", beneExternal);
					confirmTransIntent.putExtra("questions", questions);
					confirmTransIntent.putExtra("codeFlag", codeFlag);
					startActivity(confirmTransIntent);
				}

			}
		});

		//call in onCreate
		// setAppIdleTimeout();
	}


//	private Handler handler;
//	private Runnable runnable;
//
//
//
//	private void setAppIdleTimeout() {
//
//		handler = new Handler();
//		runnable = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						// Navigate to main activity
//						Toast.makeText(FundsTransferFromBeneficiary.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//						Intent loginIntent = new Intent(FundsTransferFromBeneficiary.this, LoginActivity.class);
//						loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//						startActivity(loginIntent);
//						finish();
//					}
//				});
//			}
//		};
//		handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//	}
//
//
//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//	//	Log.i(TAG, "transenq interacted");
//		MainMenu.resetAppIdleTimeout();
//		super.onUserInteraction();
//	}
//
//	@Override
//	public void onDestroy() {
//		// TODO Auto-generated method stub
//		handler.removeCallbacks(runnable);
//		super.onDestroy();
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//		MainMenu.resetAppIdleTimeout();
//	}


	private String readBalancesFromFile(Context context) {

		String ret = "";

		try {
			InputStream inputStream = context.openFileInput("bal.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("Login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("Login activity", "Cannot read file: " + e.toString());
		}

		return ret;
	}



	private String readBenefFromFile(Context context) {

		String ret = "";

		try {
			InputStream inputStream = context.openFileInput("intB.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("Login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("Login activity", "Cannot read file: " + e.toString());
		}

		return ret;
	}


	public void populateAccounts(){

		System.out.println(mbAcc);

		aliasAccDetails = mbAcc.split(" ` ");

		List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

		System.out.println(aliasAccDetails.length);

		for(int a=0;a<aliasAccDetails.length;a++){
			StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
			accNum = myAccToken.nextToken().trim();
			accName = myAccToken.nextToken().trim();
			accAvailBal = myAccToken.nextToken().trim();
			accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
			accLedgerBal = myAccToken.nextToken().trim();
			accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
			accType = myAccToken.nextToken().trim();

			System.out.println(accNum);
			System.out.println(accName);

			HashMap<String, String> hm = new HashMap<String,String>();

//							hm.put("accname", loanProdCode);
			if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
				hm.put("accname", accName.substring(0, 17) + "...");
			} else {
				hm.put("accname", accName);
			}
			hm.put("accnum",  accNum  );

			hm.put("bal", accAvailBal
					.replace("SLL","Le ")
					.replace("GHS","¢")
					.replace("USD","$")
					.replace("GBP","£")
					.replace("EUR","€")
					.replace("CNY","¥")
					.replace("JPY","¥"));
			hm.put("ledgerbal", "   " + accLedgerBal
					.replace("SLL","Le ")
					.replace("GHS","¢")
					.replace("USD","$")
					.replace("GBP","£")
					.replace("EUR","€")
					.replace("CNY","¥")
					.replace("JPY","¥"));

			theList.add(hm);

		}

		String[] from = { "next","accname", "bal", "accnum", "ledgerbal" };
		int[] to = { R.id.flag,R.id.acc_name,R.id.avail_bal,R.id.acc_num, R.id.ledger_bal};

		SimpleAdapter adapter = new SimpleAdapter(FundsTransferFromBeneficiary.this, theList, R.layout.list_bills_from , from , to);

		spinner1.setAdapter(adapter);

	}




	public String acctsJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myacctsResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = acctsJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getacc", json);

		System.out.println(response);

		return response;
	}



	public class myAcctsFireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress2 = new ProgressDialog(FundsTransferFromBeneficiary.this);
			progress2.setCancelable(true);
			progress2.setTitle("Please wait");
			progress2.setMessage("Retrieving accounts...");
			progress2.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myacctsResponse(token);
//				System.out.println(response);
				try {
					if (!response.isEmpty()) {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId"); 
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);
					} else {
						mb_response = "66";
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress2.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);

					progress2.cancel();
					new ResponseDialog(FundsTransferFromBeneficiary.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
					confirmButton.setEnabled(false);
					finish();
				} else if (mb_response.trim().equals("66")) {
					progress2.cancel();
					new ResponseDialog(FundsTransferFromBeneficiary.this, "No response", "Please try again").showDialog();
				}
				else if(mb_response.trim().equals("11") ){
					progress2.cancel();
					new ResponseDialog(FundsTransferFromBeneficiary.this, "Error !", "Unknown Error");
					confirmButton.setEnabled(false);
				}
				else if(mb_response.trim().equals("00") ){
					progress2.cancel();
					
					System.out.println(mbAcc);
					
					aliasAccDetails = mbAcc.split(" ` ");

					List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

					System.out.println(aliasAccDetails.length);
						
						for(int a=0;a<aliasAccDetails.length-1;a++){
							StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 , 
							accNum = myAccToken.nextToken().trim();
							accName = myAccToken.nextToken().trim();
							accAvailBal = myAccToken.nextToken().trim();
							accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
							accLedgerBal = myAccToken.nextToken().trim();
							accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
							accType = myAccToken.nextToken().trim();
							
							System.out.println(accNum);
							System.out.println(accName);
							
							HashMap<String, String> hm = new HashMap<String,String>();

//							hm.put("accname", loanProdCode);
							if (accName.length() > 20) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
								hm.put("accname", accName.substring(0, 17) + "...");
							} else {
								hm.put("accname", accName);
							}
							hm.put("accnum",  accNum  );

							hm.put("bal", accAvailBal
									.replace("SLL","Le ")
									.replace("GHS","¢")
									.replace("USD","$")
									.replace("GBP","£")
									.replace("EUR","€")
									.replace("CNY","¥")
									.replace("JPY","¥"));
							hm.put("ledgerbal", "   " + accLedgerBal
									.replace("SLL","Le ")
									.replace("GHS","¢")
									.replace("USD","$")
									.replace("GBP","£")
									.replace("EUR","€")
									.replace("CNY","¥")
									.replace("JPY","¥"));
							
							theList.add(hm);

						}		
						
						String[] from = { "next","accname", "bal", "accnum", "ledgerbal" };
						int[] to = { R.id.flag,R.id.acc_name,R.id.avail_bal,R.id.acc_num, R.id.ledger_bal};

						SimpleAdapter adapter = new SimpleAdapter(FundsTransferFromBeneficiary.this, theList, R.layout.list_bills_from , from , to);

						spinner1.setAdapter(adapter);
					

					//				transDetailsIntent = new Intent(MyAccounts.this, TransEnquiry.class);
					//				loginIntent.putExtra("token", mb_token);
					//				loginIntent.putExtra("userName", mb_userName);
					//				loginIntent.putExtra("mb_acc_details", mb_acc_details); 
					//				startActivity(transDetailsIntent);
				}

			}
//			new fireMan().execute();

		}

	}




	// add items into spinner dynamically
	/*public void addItemsOnSpinner1() {

		aList = new ArrayList<HashMap<String,String>>();

		for(int a=0;a<accDetail.length;a++){
			StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

			if (a==0){
				loanProdCode = "Transfer From";
				loanProdCode = "";
				accAvailBal = "";
				accLedgerBal = "";
			}else{

			accNum = myAccToken.nextToken().trim();
			loanProdCode = myAccToken.nextToken().trim();
			accAvailBal = myAccToken.nextToken().trim();
			accLedgerBal = myAccToken.nextToken().trim();
			//			String d = myAccToken.nextToken().trim();
			System.out.println(accNum);
			System.out.println(loanProdCode);
			//			System.out.println(accAvailBal);
			//			System.out.println(accLedgerBal);

			HashMap<String, String> hm = new HashMap<String,String>();

			hm.put("accname", loanProdCode);
			hm.put("accnum",  accNum  );
			hm.put("bal", accAvailBal);
			hm.put("ledgerbal", "   " + accLedgerBal);
			//			hm.put("next", Integer.toString(next[0]) );

			aList.add(hm);


		}		


	}*/



	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
					finish();
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			//	         NavUtils.navigateUpFromSameTask(this);S
			FundsTransferFromBeneficiary.this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onBackPressed() {
		//		moveTaskToBack(true); 
		FundsTransferFromBeneficiary.this.finish();
	}



}

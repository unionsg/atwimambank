package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class SaloneLinkExtra extends AppCompatActivity {

    private Spinner spinner1, spinner2;
    private EditText amtEditText;
    private Button confirmButton;
    private Button getFeesButton;
    private EditText pinEditText;
    EditText senderNameEditText;
    EditText senderAddressEditText;
    EditText senderPhoneNumberEditText;
    EditText receiverNameEditText;
    Button addQuestionButton;
    EditText receiverAddressEditText;
    EditText receiverPhoneEditText;
    EditText answerEditText;
    EditText addQuestionEditText;
    TextInputLayout questionLayout;
    TextInputLayout answerLayout;


    String senderName = null;
    String senderAddress = null;
    String senderPhoneNumber = null;
    String receiverName = null;
    String receiverAddress = null;
    String receiverPhone = null;
    String feesOnAmount = null;
    String answer = null;
    String pin = null;
    Double totalAmount = 0.00;

    private TextView feesTextView;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String alias = null;
    String aliasAcc = null;

    String accNum = null;
    String accLedgerBal = null;
    String accAvailBal = null;
    String accName = null;

    String accountNum = null;
    String accountName = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    ProgressDialog progress4;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    String remittanceAmt = null;
    String respCode = null;
    String resp = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;

    String accnumText;

    String response = null;

    String question = null;
    String chosenQuestion = null;
    String testQuestions = "Select a test question~What is the name of your first boss?~What is the name of your favourite car?~What is the name of your pet?~Add a question";

    Dialog dialog;
    String balances = null;
    boolean isTypingQuestion = false;
    String questions = null;
    String codeFlag = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.salone_link_extra);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        remittanceAmt = getIntent().getExtras().getString("remittanceAmt").trim();
        accountNum = getIntent().getExtras().getString("accountNum").trim();
        accountName = getIntent().getExtras().getString("accountName").trim();
        feesOnAmount = getIntent().getExtras().getString("feesOnAmount").trim();
        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

        senderNameEditText = (EditText) findViewById(R.id.sender_name);
        senderAddressEditText = (EditText) findViewById(R.id.sender_address);
        senderPhoneNumberEditText = (EditText) findViewById(R.id.sender_phone);
        receiverNameEditText = (EditText) findViewById(R.id.receiver_name);
        receiverAddressEditText = (EditText) findViewById(R.id.receiver_address);
        receiverPhoneEditText = (EditText) findViewById(R.id.receiver_phone);
        answerEditText = (EditText) findViewById(R.id.answer);
        addQuestionEditText = (EditText) findViewById(R.id.add_question_edittext);
        questionLayout = (TextInputLayout) findViewById(R.id.question_layout);
        answerLayout = (TextInputLayout) findViewById(R.id.answer_layout);

        senderNameEditText.setEnabled(false);
        senderNameEditText.setText(accountName);

        spinner1 = (Spinner) findViewById(R.id.questions_spinner);

        confirmButton = (Button) findViewById(R.id.proceed_button);


        populateSpinner1();


//        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//
//                //				accountNum = parent.getItemAtPosition(pos).toString();
//                question = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
//
//            }
//
//            public void onNothingSelected(AdapterView<?> parent) {
//                // Do nothing
//
//
//            }
//        });


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                chosenQuestion = ((TextView) view.findViewById(R.id.branchname)).getText().toString().trim();

                if (chosenQuestion.trim().equals("Add a question")) {
                    questionLayout.setVisibility(View.VISIBLE);
                    chosenQuestion = "";
                    isTypingQuestion = true;
                } else {
                    isTypingQuestion = false;
                    questionLayout.setVisibility(View.INVISIBLE);
                }
//                addQuestionEditText.setVisibility(View.VISIBLE);
//                chosenQuestion = "";

//                if (isTypingQuestion){
//                    chosenQuestion = addQuestionEditText.getText().toString();
//                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });

        /*addQuestionButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                spinner1.setVisibility(View.INVISIBLE);
                addQuestionEditText.setVisibility(View.VISIBLE);
                chosenQuestion = "";
                isTypingQuestion = true;
                System.out.println("istypingquestion: " + isTypingQuestion);
            }
        });*/



        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                senderName = senderNameEditText.getText().toString();
                senderAddress = senderAddressEditText.getText().toString();
                senderPhoneNumber = senderPhoneNumberEditText.getText().toString();
                receiverName = receiverNameEditText.getText().toString();
                receiverAddress = receiverAddressEditText.getText().toString();
                receiverPhone = receiverPhoneEditText.getText().toString();
                answer = answerEditText.getText().toString();
                if (isTypingQuestion){
                    chosenQuestion = addQuestionEditText.getText().toString();
                }
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
                if (senderName.trim().isEmpty()) {
                    senderNameEditText.setError("Enter your name");
                } else if (senderAddress.trim().isEmpty()) {
                    senderAddressEditText.setError("Enter your address");
                } else if (senderPhoneNumber.trim().isEmpty()) {
                    senderPhoneNumberEditText.setError("Enter your phone number");
                } else if (receiverName.trim().isEmpty()) {
                    receiverNameEditText.setError("Enter receiver's name");
                } else if (receiverAddress.trim().isEmpty()) {
                    receiverAddressEditText.setError("Enter receiver's address");
                } else if (receiverPhone.trim().isEmpty()) {
                    receiverPhoneEditText.setError("Enter receiver's phone number");
                } else if (answer.trim().isEmpty()) {
                    answerEditText.setError("Enter your test answer");
                } else if (chosenQuestion.trim().equals("Select a test question")) {
                    Toast.makeText(SaloneLinkExtra.this, "Please select a test question", Toast.LENGTH_LONG).show();
                } else if (chosenQuestion.trim().isEmpty()) {
                    Toast.makeText(SaloneLinkExtra.this, "Please enter a test question", Toast.LENGTH_LONG).show();
                } else if (chosenQuestion.trim().equals("Add your question")) {
                    addQuestionEditText.setError("Please enter a test question");
                } /*if (isTypingQuestion){
                    chosenQuestion = addQuestionEditText.getText().toString();
                }*/ else {
//                    Intent confirmTransIntent = new Intent(SaloneLinkExtra.this, ConfirmSaloneLink.class);
                    System.out.println(accountNum);
                    System.out.println(remittanceAmt);
                    System.out.println(feesOnAmount);
                    System.out.println(senderName);
                    System.out.println(senderAddress);
                    System.out.println(senderPhoneNumber);
                    System.out.println(receiverName);
                    System.out.println(receiverAddress);
                    System.out.println(receiverPhone);
                    System.out.println(chosenQuestion);
                    System.out.println(answer);
                    System.out.println(token);


                    buildDialog(R.style.DialogAnimation);


                }
            }
        });

        //call in onCreate
     //   setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(SaloneLinkExtra.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(SaloneLinkExtra.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//      //  Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }




    private void buildDialog(int animationSource) {

        dialog = new Dialog(SaloneLinkExtra.this);
        dialog.setContentView(R.layout.confirm_salone_link);
//        dialog.setTitle("Change Password");

        TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
        TextView remittanceAmtTV = (TextView) dialog.findViewById(R.id.remittance_amount);
        TextView feeAmountTV = (TextView) dialog.findViewById(R.id.fee_amount);
        TextView totalAmtTV = (TextView) dialog.findViewById(R.id.totalamount);

        TextView senderNameTV = (TextView) dialog.findViewById(R.id.sender_name);
        TextView senderAddressTV = (TextView) dialog.findViewById(R.id.sender_address);
        TextView senderPhoneTV = (TextView) dialog.findViewById(R.id.sender_phone);


        TextView receiverNameTV = (TextView) dialog.findViewById(R.id.receiver_name);
        TextView receiverAddressTV = (TextView) dialog.findViewById(R.id.receiver_address);
        TextView receiverPhoneTV = (TextView) dialog.findViewById(R.id.receiver_phone);

        TextView questionTV = (TextView) dialog.findViewById(R.id.test_question);
        TextView answerTV = (TextView) dialog.findViewById(R.id.test_answer);

        pinEditText = (EditText) dialog.findViewById(R.id.pinedittext);


        totalAmount = Double.parseDouble(remittanceAmt) + Double.parseDouble(feesOnAmount);

        accountNumTV.setText(accountNum);
        remittanceAmtTV.setText(remittanceAmt);
        feeAmountTV.setText(feesOnAmount);
        totalAmtTV.setText("Le" + GlobalCodes.FormatToMoney(totalAmount.toString()));

        senderNameTV.setText(senderName);
        senderAddressTV.setText(senderAddress);
        senderPhoneTV.setText(senderPhoneNumber);

        receiverNameTV.setText(receiverName);
        receiverAddressTV.setText(receiverAddress);
        receiverPhoneTV.setText(receiverPhone);

        questionTV.setText(chosenQuestion);
        answerTV.setText(answer);

        Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                pin = pinEditText.getText().toString();
//				Toast.makeText(FundsTransfer.this, accountNum + " " + merchantAccNum, Toast.LENGTH_SHORT).show();
                if (pin.trim().isEmpty()) {
                    pinEditText.setError("Enter your 4 digit PIN");
                } else {

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireMan().execute();
                    } else {
                        showAlertDialog(SaloneLinkExtra.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                    dialog.dismiss();
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();



    }




    public void populateSpinner1(){

        List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();

//						System.out.println(aliasAccDetails.length);
        StringTokenizer accDetToken = new StringTokenizer(testQuestions, "~");

        for(int a=0;accDetToken.hasMoreTokens();a++){

            System.out.println(accDetToken.countTokens());

            question = accDetToken.nextToken().trim();
//							beneAcc = accDetToken.nextToken().trim();

            System.out.println(question);
//
            HashMap<String, String> hm = new HashMap<String,String>();

            hm.put("question", question);
//							hm.put("aliasaccnum", beneAcc "**********"+ .lastIndexOf(0, -3)  );

            theList.add(hm);

        }

        String[] from = { "question" };
        // Ids of views in listview_layout
        int[] to = { R.id.branchname};

        SimpleAdapter adapter2 = new SimpleAdapter(SaloneLinkExtra.this, theList, R.layout.list_ach , from , to);

        spinner1.setAdapter(adapter2);

    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }

    public String saloneLinkJSON(String accNum, String amt, String amountFee, String sendName, String sendAddress, String sendPhone ,
                                 String receiveName, String receiveAddress, String receivePhone, String testQuestion, String testAnswer, String myPin, String authToken) throws UnsupportedEncodingException {
        return "accountNum=" + accNum+
                "&amount="+amt+
                "&fee="+amountFee+
                "&senderName="+sendName+
                "&senderAddress="+sendAddress+
                "&senderPhone="+sendPhone+
                "&receiverName="+receiveName+
                "&receiverAddress="+receiveAddress+
                "&receiverPhone="+receivePhone+
                "&question="+testQuestion+
                "&answer="+testAnswer+
                "&sec="+myPin+
                "&authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");
    }


    public String myResponse(String a1,String a2,String a3,String a4,String a5,String a6,String a7,String a8,String a9,String a10,String a11,String a12,String a13) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = saloneLinkJSON(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "sendSalone", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(SaloneLinkExtra.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending details...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(accountNum, remittanceAmt, feesOnAmount, senderName, senderAddress, senderPhoneNumber, receiverName,
                        receiverAddress, receiverPhone, chosenQuestion, answer, pin, token);

                if (response.isEmpty()){
                    response = "66#No response";
                }


            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub



            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                try {
                    StringTokenizer token = new StringTokenizer(response, "#");
                    respCode = token.nextToken();
                    resp = token.nextToken();

                    System.out.println(respCode);
                    System.out.println(resp);

                    if (respCode.trim().equals("12")) {
                        System.out.println("Received response code > " + resp);

                        progress.cancel();
                        new ResponseDialog(SaloneLinkExtra.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//                    confirmButton.setEnabled(false);
                    } else if (respCode.trim().equals("11")) {
                        progress.cancel();
                        new ResponseDialog(SaloneLinkExtra.this, "Sorry", resp).showDialog();
                    } else if (respCode.trim().equals("22")) {
                        progress.cancel();
                        new ResponseDialog(SaloneLinkExtra.this, "Sorry", resp).showDialog();
                    } else if(respCode.trim().equals("66") ){
                        progress.cancel();
                        new ResponseDialog(SaloneLinkExtra.this, "No response", "Please try again").showDialog();
                    } else if (respCode.trim().equals("00")) {
//                        progress.cancel();
//                        new ResponseDialog(SaloneLinkExtra.this, "Successful", resp).showDialog();
                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new myAcctsFireMan().execute();
                        } else {
                            showAlertDialog(SaloneLinkExtra.this, "No Internet Connection",
                                    "You don't have internet connection", false);
                        }

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }



        }

    }



    private void writeBalancesToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bal.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }



    public String acctsJSON(String authToken){
        return "authToken="+authToken;
    }


    public String myacctsResponse (String a1) throws IOException{
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl+"getacc", json);

        System.out.println(response);

        return response;
    }



    public class myAcctsFireMan extends AsyncTask<Void , Void, Void>{

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void ... params) {

            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
                    balances = jsonObject.getString("mb_accDetails");

                    System.out.println(balances);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if(e != null){
                progress.cancel();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")){
                    progress.cancel();
                    new ResponseDialog(SaloneLinkExtra.this, "Successful", resp).showDialog();
                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(SaloneLinkExtra.this, "No response", "Please try again").showDialog();
                }
                else if(mb_response.trim().equals("11") ){
                    progress.cancel();
                    new ResponseDialog(SaloneLinkExtra.this, "Successful", resp).showDialog();
                }
                else if(mb_response.trim().equals("00") ){
                    progress.cancel();
                    System.out.println(balances);
                    writeBalancesToFile(balances, SaloneLinkExtra.this);
                    new ResponseDialog(SaloneLinkExtra.this, "Successful", resp).showDialog();

                }

            }

        }

    }





    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    Intent intent = new Intent(SaloneLinkExtra.this, MainMenu.class);
                    intent.putExtra("token", token);
                    intent.putExtra("userName", usrName);
                    intent.putExtra("userId", usrId);
                    intent.putExtra("mb_acc_details", custType);
                    intent.putExtra("balances", balances);
                    intent.putExtra("creationTime", creationTime);
                    intent.putExtra("beneInternal", beneInternal);
                    intent.putExtra("beneExternal", beneExternal);
                    intent.putExtra("questions", questions);
                    intent.putExtra("codeFlag", codeFlag);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    public class WrongPinResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public WrongPinResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public WrongPinResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    pinEditText.setText("");
                    pinEditText.setError("You entered a wrong PIN code");
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        menu.findItem(R.id.refresh).setVisible(false);
//        return true;
//    }
//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                SaloneLinkExtra.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //		moveTaskToBack(true);
        SaloneLinkExtra.this.finish();
    }


}

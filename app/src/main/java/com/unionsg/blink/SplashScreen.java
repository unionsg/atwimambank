package com.unionsg.blink;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

public class SplashScreen extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
//		getSupportActionBar().hide();

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(3000);
				} catch(InterruptedException e){
					e.printStackTrace();
					
				}finally{
					Intent openMainActivity = new Intent(SplashScreen.this, LoginActivity.class);
					startActivity(openMainActivity);
				}
			}
		};
		timer.start();
	
//		 R28G30B2B4F
		//timeout code
//		handler = new Handler();
//		r = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				Toast.makeText(SplashScreen.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//				Intent loginIntent = new Intent(SplashScreen.this, LoginActivity.class);
//				loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				startActivity(loginIntent);
//				finish();
//			}
//		};
//		startHandler();


	}

//	Handler handler;
//	Runnable r;
//
//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//		super.onUserInteraction();
//		stopHandler();//stop first and then start
//		startHandler();
//	}
//
//	public void stopHandler() {
//		handler.removeCallbacks(r);
//	}
//
//	public void startHandler() {
//		handler.postDelayed(r, MainMenu.timeOutMinutes);
//	}

@Override
public void onStop() {
	// TODO Auto-generated method stub
	super.onStop();

	finish();
}

/*@Override
public void onBackPressed() {
	// TODO Auto-generated method stub
	super.onBackPressed();
	
	finishFromChild(SplashScreen.this);
}*/
	

}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class CreateBeneficiary extends AppCompatActivity {

	String beneAccNum = null,beneName= null,beneTel= null;
	String response = null;
	String token = null;
	String accountNum = null;
	String accountName = null;
	String internalBeneficiary = null;
	String externalBeneficiary = null;
	String mbAcc2 = null;
	String bnkCode = null;

	String id = null;
	String creationTime = null;
	String lastModificationDate = null;
	String mb_token = null;
	String mb_userId = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;

	EditText beneAccEditText,beneNameEditText,beneTelEditText;
	EditText edtUserAccName;
	TextView aliasNameTextView;
	ProgressDialog progress;
	Spinner spinBankData;
	List<HashMap<String,String>> myList;
	Button addButton;
	String aloo = null;
	Dialog dialog;
	String beneficiaryAccName = null;
//
//	Handler handler;
//	Runnable r;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_beneficiary);

		Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mActionBarToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		token = getIntent().getExtras().getString("token").trim();
		System.out.println("CreateBeneficiary: " + token);

		beneAccEditText = (EditText) findViewById (R.id.bene_acc);
		beneNameEditText = (EditText) findViewById (R.id.bene_name);
		beneTelEditText = (EditText) findViewById (R.id.bene_tel);

		addButton = (Button) findViewById (R.id.add_button);

		addButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				beneAccNum = beneAccEditText.getText().toString();
				beneName = beneNameEditText.getText().toString();
				beneTel = beneTelEditText.getText().toString();

				System.out.println("beneAccNum: " + beneAccNum);

				if (beneAccNum.trim().isEmpty()){
					beneAccEditText.setError("Enter beneficiary's account number");
				}else if (beneAccNum.length() != 13) {
					beneAccEditText.setError("Enter valid account number");
				}else if (beneName.trim().isEmpty()){
					beneNameEditText.setError("Enter beneficiary's name");
				}else if (beneTel.trim().isEmpty()){
					beneTelEditText.setError("Enter beneficiary's mobile number");
				}
				else{
					ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
					Boolean isInternetPresent = cd.isConnectingToInternet();
					if (isInternetPresent) {
						new getNamefireMan().execute();
					} else {
						showAlertDialog(CreateBeneficiary.this, "No Internet Connection",
								"You don't have internet connection", false);
					}

			}}
		});

		//call in onCreate
		//setAppIdleTimeout();
	}


//	private Handler handler;
//	private Runnable runnable;
//
//
//
//	private void setAppIdleTimeout() {
//
//		handler = new Handler();
//		runnable = new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						// Navigate to main activity
//						Toast.makeText(CreateBeneficiary.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//						Intent loginIntent = new Intent(CreateBeneficiary.this, LoginActivity.class);
//						loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//						startActivity(loginIntent);
//						finish();
//					}
//				});
//			}
//		};
//		handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//	}
//
//
//	@Override
//	public void onUserInteraction() {
//		// TODO Auto-generated method stub
//		//Log.i(TAG, "transenq interacted");
//		MainMenu.resetAppIdleTimeout();
//		super.onUserInteraction();
//	}
//
//	@Override
//	public void onDestroy() {
//		// TODO Auto-generated method stub
//		handler.removeCallbacks(runnable);
//		super.onDestroy();
//	}
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//		MainMenu.resetAppIdleTimeout();
//	}



	private void buildDialog(int animationSource) {

		dialog = new Dialog(CreateBeneficiary.this);
		dialog.setContentView(R.layout.confirm_beneficiary_creation);
//        dialog.setTitle("Change Password");

		TextView accountNumTV = (TextView) dialog.findViewById(R.id.accountnum);
//        TextView senderNameTV = (TextView) dialog.findViewById(R.id.name);
		TextView beneficiaryAccNameTV = (TextView) dialog.findViewById(R.id.benename);
		TextView aliasNameTV = (TextView) dialog.findViewById(R.id.aliasname);
		TextView phoneNumberTV = (TextView) dialog.findViewById(R.id.benephone);

		accountNumTV.setText(beneAccNum);
		beneficiaryAccNameTV.setText(beneficiaryAccName);
		aliasNameTV.setText(beneName);
		phoneNumberTV.setText(beneTel);



//        questionTV.setText(question);
//        answerTV.setText(answer);
//
		Button okButton = (Button) dialog.findViewById(R.id.confirm_button);
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		lp.gravity = Gravity.CENTER;
		dialog.getWindow().setAttributes(lp);

		okButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
				Boolean isInternetPresent = cd.isConnectingToInternet();
				if (isInternetPresent) {
					new fireMan().execute();
				} else {
					showAlertDialog(CreateBeneficiary.this, "No Internet Connection",
							"You don't have internet connection", false);
				}
				dialog.dismiss();

			}
		});

		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();

			}
		});
		dialog.getWindow().getAttributes().windowAnimations = animationSource;
		dialog.show();



	}





	public String confirmsendJSON(String accnumber){
		return "accnum="+accnumber;



	}


	public String confirmAcctResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = confirmsendJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getAcctName", json);

		System.out.println(response);

		return response;
	}



	public class getNamefireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(CreateBeneficiary.this);
			progress.setCancelable(false);
			progress.setTitle("Please wait");
			progress.setMessage("Sending request...");
			progress.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {

			try {
				System.out.println(beneAccNum);
				response = confirmAcctResponse(beneAccNum);
				if (response.isEmpty()) {
					response = "66";
				}

//				try {
//					JSONObject jsonObject = new JSONObject(response);
//					id = jsonObject.getString("id");
//					//					creationTime = jsonObject.getString("creationTime");
//					//					lastModificationDate = jsonObject.getString("lastModificationDate");
//					mb_token = jsonObject.getString("tokenId");
//					mb_response = jsonObject.getString("mb_response");
//					mb_userId = jsonObject.getString("mb_userId");
//					mbAcc = jsonObject.getString("accDetails");
//
//					//					System.out.println(mb_userId);
////					System.out.println(mbAcc);
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (response.trim().equals("11")){

					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "Sorry", "The beneficiary account number you have entered is wrong").showDialog();
					//					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

				} else if (response.trim().equals("66")) {
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "No response", "Please try again").showDialog();
				}

				else {
					progress.cancel();
					beneficiaryAccName = response;
					System.out.println("beneficiaryName: " + beneficiaryAccName);
					buildDialog(R.style.DialogAnimation);


				}

			}

		}

	}





	public String confirmJSON(String beneacc,String benename, String benetel,String authToken){
		return "beneacc="+beneacc+
				"&benename="+benename+
				"&benetel="+benetel+
				"&authToken="+authToken;



	}


	public String confirmResponse (String a1,String a2,String a3,String a4) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = confirmJSON(a1,a2,a3,a4);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"beneficiaryCreate", json);

		System.out.println(response);

		return response;
	}



	public class fireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress = new ProgressDialog(CreateBeneficiary.this);
			progress.setCancelable(false);
			progress.setTitle("Please wait");
			progress.setMessage("Sending request...");
			progress.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {

			try {
				System.out.println(beneAccNum+","+beneName+","+beneTel+","+token);
				response = confirmResponse(beneAccNum, beneName, beneTel, token);

				try {
					if (!response.isEmpty()) {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					//					creationTime = jsonObject.getString("creationTime");
					//					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mb_userId = jsonObject.getString("mb_userId");
					mbAcc = jsonObject.getString("mb_accDetails");
//					internalBeneficiary = jsonObject.getString("mb_accDetails");
//					externalBeneficiary = jsonObject.getString("mb_transDetails");

					//					System.out.println(mb_userId);
//					System.out.println(mbAcc);
					} else {
						mb_response = "66";
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("21")){
					System.out.println("Received response code > "+mb_response);

					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();
					//					Toast.makeText(ConfirmTrans.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

				}
				else if (mb_response.trim().equals("66")) {
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "No response", "Please try again").showDialog();
				}
				else if(mb_response.trim().equals("20") ){
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();

				}
				else if(mb_response.trim().equals("12") ){
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();

				}else if(mb_response.trim().equals("18") ){
                    progress.cancel();
                    new ResponseDialog(CreateBeneficiary.this, "Transaction unsuccessful", mbAcc).showDialog();

                }else if(mb_response.trim().equals("11") ){
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "Beneficiary Account Status Not Valid", "").showDialog();
				}
				else if(mb_response.trim().equals("00") ){
//					progress.cancel();
//					writeInternalToFile(internalBeneficiary, CreateBeneficiary.this);
//					new ResponseDialog(CreateBeneficiary.this, "Success", "You have successfully created a beneficiary").showDialog();

					ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
					Boolean isInternetPresent = cd.isConnectingToInternet();
					if (isInternetPresent) {
						new getBeneficiariesFireMan().execute();
					} else {
						showAlertDialog(CreateBeneficiary.this, "No Internet Connection",
								"You don't have internet connection", false);
					}

				}


			}

		}

	}






	private void writeInternalToFile(String intBene, Context context) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("intB.txt", Context.MODE_PRIVATE));
			outputStreamWriter.write(intBene);
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}


	private void writeExternalToFile(String extBene, Context context) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("extB.txt", Context.MODE_PRIVATE));
			outputStreamWriter.write(extBene);
			outputStreamWriter.close();
		} catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}




	public String fundsTransJSON(String authToken){
		return "authToken="+authToken;
	}


	public String myResponse (String a1) throws IOException{
		SendRequest sendReqObj = new SendRequest();

		String json = fundsTransJSON(a1);
		String response = sendReqObj.postReq(GlobalCodes.myUrl+"getalias", json);

		System.out.println(response);

		return response;
	}



	public class getBeneficiariesFireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			progress = new ProgressDialog(CreateBeneficiary.this);
//			progress.setCancelable(false);
//			progress.setTitle("Please wait");
//			progress.setMessage("Retrieving beneficiaries...");
//			progress.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myResponse(token);
				System.out.println("response");

				try {
					if (!response.isEmpty()) {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);
					} else {
						mb_response = "66";
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress.cancel();
				//				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response.trim().equals("12")){
					System.out.println("Received response code > "+mb_response);
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "No Data Found For This User", "Please login again").showDialog();
				} else if (mb_response.trim().equals("66")) {
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "No response", "Please try again").showDialog();
				}
				else if(mb_response.trim().equals("19") ){
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "No Beneficiary Found", "").showDialog();
				}else if(mb_response.trim().equals("11") ){
					progress.cancel();
					new ResponseDialog(CreateBeneficiary.this, "Beneficiary Account Status Not Valid", "").showDialog();
				}
				else if(mb_response.trim().equals("00") ){
					progress.cancel();

					System.out.println(mbAcc);
					writeInternalToFile(mbAcc, CreateBeneficiary.this);
					new ResponseDialog(CreateBeneficiary.this, "Success", "You have successfully created a beneficiary").showDialog();


				}

			}


		}

	}









	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is 
		// present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}*/

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			CreateBeneficiary.this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		//	   moveTaskToBack(true); 
		CreateBeneficiary.this.finish();
	}



	public class ResponseDialog extends AlertDialog.Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
					finish();
//					Intent confirmTransIntent = new Intent(ConfirmTrans.this,MainMenu.class);
//					startActivity(confirmTransIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}





}





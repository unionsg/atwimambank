package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;


//import static com.unionsg.blink.R.id.banksSpinner;

public class Ach extends AppCompatActivity {

    private Spinner spinner1;
    Spinner spinner2;
    //	EditText banksEditText;
//	private EditText searchEditText;
    private EditText amtEditText;
    private EditText acNumEditText;
    private EditText transDetEditText;
    private Button confirmButton;
    private EditText accNameEditText;
    TextView titleTextView;
    ImageButton fab;
    //	Button searchButton;
    Dialog dialog;
    ProgressDialog progress4;
    String bankcode = null;

    String[] accDetail = null;

    String aliasAccNum = null;
    String aliasName = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    //	String accDetails = null;
    String mbAcc = null;
    String[] aliasAccDetails = null;
    String telcos = null;
    String aliasAcc = null;
    String beneCode = null;
    String beneTel = null;

    String accNum = null;
    String accLedgerBal = null;
    String accType = null;
    String accAvailBal = null;
    String accName = null;
    String crAcc = null;
    String transDet = null;

    String accountNum = null;
    String accountName = null;
    String searchString = null;
    String selectedBank = null;

    ProgressDialog progress1;
    ProgressDialog progress2;
    ProgressDialog progress;
    int pos;
    String network = null;
    String phoneNum = null;
    String amount = null;
    String topAmt = null;

    String curr = null;

    String alias = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;

    String accnumText;

    String response = null;
    String branch = null;
    String beneFromFile = null;
    String balFromFile = null;
    String externalBeneficiary = null;
    String questions = null;
    String codeFlag = null;

//    Handler handler;
//    Runnable r;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ach);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        token = getIntent().getExtras().getString("token").trim();
        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();

        spinner2 = (Spinner) findViewById(R.id.spinner2);
        acNumEditText = (EditText) findViewById(R.id.accnumedittext);
        amtEditText = (EditText) findViewById(R.id.amtedittext);
        transDetEditText = (EditText) findViewById(R.id.trans_details);
        confirmButton = (Button) findViewById(R.id.confirm_button);
        accNameEditText = (EditText) findViewById(R.id.accname);
        spinner1 = (Spinner) findViewById(R.id.spinner1);

        balFromFile = readBalancesFromFile(Ach.this);
        beneFromFile = readFromFile(Ach.this);

        if (!balFromFile.equals("")) {
            System.out.println("balances not empty");
            mbAcc = balFromFile;
            System.out.println("MBACC balances: " + mbAcc);
            populateAccounts();
            if (!beneFromFile.equals("empty")) {
                System.out.println("beneficiaries not empty");
                mbAcc = beneFromFile;
                System.out.println("mbAcc: " + mbAcc);
                populateBeneficiaries();
            } else {
//				new fireMan().execute();
                new ResponseDialog(Ach.this, "Sorry", "You cannot proceed with this transfer because you have not created any Other Bank beneficiary. " +
                        "Please create a beneficiary to transfer funds").showDialog();
            }
        } else {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myAcctsFireMan().execute();
            } else {
                showAlertDialog(Ach.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }
        }


        spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //				accountNum = parent.getItemAtPosition(pos).toString();
                accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
                accAvailBal = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();

                if (accAvailBal.contains("Le")) {
                    curr = "Le";
                } else if (accAvailBal.contains("¢")) {
                    curr = "¢";
                } else if (accAvailBal.contains("$")) {
                    curr = "$";
                } else if (accAvailBal.contains("£")) {
                    curr = "£";
                } else if (accAvailBal.contains("€")) {
                    curr = "€";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                } else if (accAvailBal.contains("¥")) {
                    curr = "¥";
                }


            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });

        spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                aliasAccNum = ((TextView) view.findViewById(R.id.alias_acc_num)).getText().toString().trim();
                aliasName = ((TextView) view.findViewById(R.id.alias_name)).getText().toString().trim();

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                amount = amtEditText.getText().toString();
                transDet = transDetEditText.getText().toString();
//				myBankName = banksEditText.getText().toString();
                if (amount.isEmpty()) {
                    amtEditText.setError("Please enter amount");
                } /*else if (brCode.isEmpty()){
                    searchEditText.setError("Please enter bank name and tap the search button");
				}*/ else if (accountNum.isEmpty()) {
                    Toast.makeText(Ach.this, "Please select an account to send from", Toast.LENGTH_LONG).show();
                }
                /*else if (myBankName.isEmpty()){
					banksEditText.setError("Please enter recipient's account number");
				}*/
                else if (transDet.isEmpty()) {
                    transDetEditText.setError("Please enter a summary of the transaction");
                } else {

                    bankcode = aliasAccNum.substring(0, 3);
                    System.out.println("bankcode: " + bankcode);

                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new searchBanksFireMan().execute();
                    } else {
                        showAlertDialog(Ach.this, "No Internet Connection",
                                "You don't have internet connection", false);
                    }
                }

            }
        });

        //call in onCreate
        // setAppIdleTimeout();
    }


//    private Handler handler;
//    private Runnable runnable;
//
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(Ach.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(Ach.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//       // Log.i(TAG, "transenq interacted");
//        MainMenu.resetAppIdleTimeout();
//        super.onUserInteraction();
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        MainMenu.resetAppIdleTimeout();
//    }


    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("extB.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    private String readBalancesFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public void populateBeneficiaries() {

        System.out.println(mbAcc);

        aliasAccDetails = mbAcc.split(" ` ");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length; a++) {
            StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

            System.out.println(accDetToken.countTokens());

            alias = accDetToken.nextToken().trim();
            aliasAcc = accDetToken.nextToken().trim();
            beneTel = accDetToken.nextToken().trim();
            beneCode = accDetToken.nextToken().trim();

            System.out.println(alias);
            System.out.println(aliasAcc);

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("aliasname", alias);
            hm.put("aliasaccnum", aliasAcc /*"**********"+*/ /*.lastIndexOf(0, -3) */);

            theList.add(hm);

        }

        String[] from = {"aliasname", "aliasaccnum"};

        // Ids of views in listview_layout
        int[] to = {R.id.alias_name, R.id.alias_acc_num};

        SimpleAdapter adapter2 = new SimpleAdapter(Ach.this, theList, R.layout.list_to, from, to);

        spinner2.setAdapter(adapter2);


    }


    public void populateAccounts() {

        System.out.println("MBACC: \n\n\n" + mbAcc);

        aliasAccDetails = mbAcc.split(" ` ");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        System.out.println(aliasAccDetails.length);

        for (int a = 0; a < aliasAccDetails.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
            accType = myAccToken.nextToken().trim();

            System.out.println(accNum);
            System.out.println(accName);

            HashMap<String, String> hm = new HashMap<String, String>();

//			if (accName.length() > 20) {
////							accNameDisp = accName.substring(0, 18) + "...";
//				hm.put("accname", accName.substring(0, 17) + "...");
//			} else {
//				hm.put("accname", accName);
//			}
            hm.put("accname", accName);
            hm.put("accnum", accNum);
            hm.put("bal", accAvailBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));
            hm.put("ledgerbal", "   " + accLedgerBal
                    .replace("SLL", "Le ")
                    .replace("GHS", "¢")
                    .replace("USD", "$")
                    .replace("GBP", "£")
                    .replace("EUR", "€")
                    .replace("CNY", "¥")
                    .replace("JPY", "¥"));

            theList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

        SimpleAdapter adapter = new SimpleAdapter(Ach.this, theList, R.layout.list_bills_from, from, to);

        spinner1.setAdapter(adapter);


    }


    public String bankListJSON(String bankCode, String authToken) {
        return "bankCode=" + bankCode +
                "&authToken=" + authToken;
    }


    public String myBanksListResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = bankListJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getBankName", json);

        System.out.println(response);

        return response;

    }


    public class searchBanksFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress4 = new ProgressDialog(Ach.this);
            progress4.setCancelable(false);
            progress4.setTitle("Please wait");
            progress4.setMessage("Retrieving bank name...");
            progress4.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                response = myBanksListResponse(bankcode, token);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    id = jsonObject.getString("id");
                    creationTime = jsonObject.getString("creationTime");
                    lastModificationDate = jsonObject.getString("lastModificationDate");
                    mb_token = jsonObject.getString("tokenId");
                    mb_response = jsonObject.getString("mb_response");
                    mbAcc = jsonObject.getString("mb_accDetails");

                    System.out.println(mbAcc);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress4.cancel();
                new ResponseDialog(Ach.this, "Error!", "No response from server. Please check your Internet connection and try again").showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress4.cancel();
                    new ResponseDialog(Ach.this, "Error!", "Connection timed out. Please check your Internet connection and try again").showDialog();

                } else if (mb_response.trim().equals("11")) {
                    System.out.println("Received response code > " + mb_response);

                    progress4.cancel();
                    new ResponseDialog(Ach.this, "Not Found", mbAcc).showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
//					confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("00")) {
                    progress4.cancel();
                    System.out.println(mbAcc);

                    amtEditText.setText("");
                    transDetEditText.setText("");

                    Intent confirmTopupIntent = new Intent(Ach.this, ConfirmAch.class);
                    confirmTopupIntent.putExtra("benename", alias);
                    confirmTopupIntent.putExtra("beneacc", aliasAcc);
                    confirmTopupIntent.putExtra("dbAcc", accountNum);
                    confirmTopupIntent.putExtra("amt", amount);
                    confirmTopupIntent.putExtra("aliasName", aliasName);
                    confirmTopupIntent.putExtra("crAcc", aliasAccNum);
                    confirmTopupIntent.putExtra("bankName", mbAcc);
                    confirmTopupIntent.putExtra("curr", curr);
                    confirmTopupIntent.putExtra("transDet", transDet);
                    confirmTopupIntent.putExtra("token", token);
                    confirmTopupIntent.putExtra("userName", usrName);
                    confirmTopupIntent.putExtra("userId", usrId);
                    confirmTopupIntent.putExtra("mb_acc_details", custType);
                    confirmTopupIntent.putExtra("balances", balances);
                    confirmTopupIntent.putExtra("creationTime", creationTime);
                    confirmTopupIntent.putExtra("beneInternal", beneInternal);
                    confirmTopupIntent.putExtra("beneExternal", beneExternal);
                    confirmTopupIntent.putExtra("questions", questions);
                    confirmTopupIntent.putExtra("codeFlag", codeFlag);
                    startActivity(confirmTopupIntent);

                }

            }

        }

    }


    public String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getExtAlias", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(Ach.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving beneficiaries...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);

                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                new ResponseDialog(Ach.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress.cancel();
                    new ResponseDialog(Ach.this, "Error!", "Connection timed out. Please check your Internet connection and try again").showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(Ach.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(Ach.this, "No Data Found For This User", "Please login again").showDialog();
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(Ach.this, "No Beneficiary Found", "Please create a beneficiary to transfer funds").showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    System.out.println(mbAcc);

                    aliasAccDetails = mbAcc.split(" ` ");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length; a++) {
                        StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");

                        System.out.println(accDetToken.countTokens());

                        alias = accDetToken.nextToken().trim();
                        aliasAcc = accDetToken.nextToken().trim();
//						beneTel = accDetToken.nextToken().trim();
//						beneCode = accDetToken.nextToken().trim();

                        System.out.println(alias);
                        System.out.println(aliasAcc);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("aliasname", alias);
                        hm.put("aliasaccnum", aliasAcc /*"**********"+*/ /*.lastIndexOf(0, -3) */);

                        theList.add(hm);

                    }

                    String[] from = {"aliasname", "aliasaccnum"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.alias_name, R.id.alias_acc_num};

                    SimpleAdapter adapter2 = new SimpleAdapter(Ach.this, theList, R.layout.list_to, from, to);

                    spinner2.setAdapter(adapter2);


                }

            }

//			new searchBanksFireMan().execute();

        }

    }


    public String acctsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myacctsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = acctsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class myAcctsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(Ach.this);
            progress2.setCancelable(true);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving accounts...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myacctsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");
                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress2.cancel();
                    new ResponseDialog(Ach.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(Ach.this, "No Data Found For This User", "Please login again");
//					Toast.makeText(FundsTransfer.this, "No Data Found For This User.Please Retry", Toast.LENGTH_LONG).show();
                    confirmButton.setEnabled(false);
                    finish();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(Ach.this, "Error !", "Unknown Error");
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(Ach.this, "No response", "Please check internet connection").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println("MBACC: \n\n\n" + mbAcc);

                    aliasAccDetails = mbAcc.split(" ` ");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
//							0122000113700~SAVINGS ACCOUNT~GHS9900~GHS9900 ,
                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);

                        System.out.println(accNum);
                        System.out.println(accName);

                        HashMap<String, String> hm = new HashMap<String, String>();

//						if (loanProdCode.length() > 20) {
////							accNameDisp = loanProdCode.substring(0, 18) + "...";
//							hm.put("accname", loanProdCode.substring(0, 17) + "...");
//						} else {
//							hm.put("accname", loanProdCode);
//						}
                        hm.put("accname", accName);
                        hm.put("accnum", accNum);
                        hm.put("bal", accAvailBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));
                        hm.put("ledgerbal", "   " + accLedgerBal
                                .replace("SLL", "Le ")
                                .replace("GHS", "¢")
                                .replace("USD", "$")
                                .replace("GBP", "£")
                                .replace("EUR", "€")
                                .replace("CNY", "¥")
                                .replace("JPY", "¥"));

                        theList.add(hm);

                    }

                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal"};
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal};

                    SimpleAdapter adapter = new SimpleAdapter(Ach.this, theList, R.layout.list_bills_from, from, to);

                    spinner1.setAdapter(adapter);

                }


            }

            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new fireMan().execute();
            } else {
                showAlertDialog(Ach.this, "No Internet Connection",
                        "You don't have internet connection", false);
            }

        }

    }


    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


    //	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findItem(R.id.refresh).setVisible(false);
//		return true;
//	}
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);
                Ach.this.finish();
                return true;
        }
//		return super.onOptionsItemSelected(item);
        return false;
    }

    public void onBackPressed() {
        //		moveTaskToBack(true);
        Ach.this.finish();
    }


//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


}

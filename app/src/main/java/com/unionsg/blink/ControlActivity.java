package com.unionsg.blink;

import android.app.Activity;
import android.util.Log;

/**
 * Created by UNION on 11/14/2017.
 */

public class ControlActivity extends Activity {
    private static final String TAG=ControlActivity.class.getName();

    /**
     * Gets reference to global Application
     * @return must always be type of ControlApplication! See AndroidManifest.xml
     */
    public ControlApplication getApp()
    {
        return (ControlApplication )this.getApplication();
    }

    @Override
    public void onUserInteraction()
    {
        super.onUserInteraction();
        getApp().touch();
        Log.d(TAG, "User interaction to "+this.toString());
    }

}

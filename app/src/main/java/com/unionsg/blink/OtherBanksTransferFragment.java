package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.liststuff.BeneficiaryExtAdapter;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class OtherBanksTransferFragment extends Fragment {

	public OtherBanksTransferFragment() {
		// Required empty public constructor
	}


	private Spinner spinner1;
	Spinner spinner2;
	//	EditText banksEditText;
//	private EditText searchEditText;
	private EditText amtEditText;
	private EditText acNumEditText;
	private EditText transDetEditText;
	private Button confirmButton;
	private EditText accNameEditText;
	TextView titleTextView;
	ImageButton fab;
	//	Button searchButton;
	Dialog dialog;
	ProgressDialog progress4;
	String bankcode = null;

	String mbAccDetails = null;
	String[] accDetail = null;

	String aliasAccNum = null;
	String aliasName = null;

	String id = null;
	String creationTime = null;
	String mb_userId = null;
	String lastModificationDate = null;
	String mb_response = null;
	String mb_accDetails = null;
	String mb_token = null;
	String mb_userName = null;
	//	String accDetails = null;
	String mbAcc = null;
	String[] aliasAccDetails = null;
	String telcos = null;
	String aliasAcc = null;

	String accNum = null;
	String accLedgerBal = null;
	String accAvailBal = null;
	String accName = null;
	String crAcc = null;
	String transDet = null;

	String accountNum = null;
	String accountName = null;
	String searchString = null;
	String selectedBank = null;

	ProgressDialog progress1;
	ProgressDialog progress2;
	ProgressDialog progress;
	int pos;
	String network = null;
	String phoneNum = null;
	String amount = null;
	String topAmt = null;

	String curr = null;

	String alias = null;

	int[] next = new int[]{	R.drawable.more };
	List<HashMap<String,String>> aList;
	View view;

	String token = null;

	String accnumText;

	String response = null;
	String branch = null;
	String externalBeneficiary = null;
	String beneFromFile = null;
	String balFromFile = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		token = getActivity().getIntent().getExtras().getString("token").trim();

		System.out.println("token: " + token);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {

//		token = getActivity().getIntent().getExtras().getString("token").trim();
//		System.out.println("token: " + token);
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.ach, container, false);

		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular.ttf");


		spinner2 = (Spinner) view.findViewById(R.id.spinner2);
		acNumEditText = (EditText) view.findViewById(R.id.accnumedittext);
//		searchEditText = (EditText) findViewById(R.id.bankcodeEditText);
		amtEditText = (EditText) view.findViewById(R.id.amtedittext);
		transDetEditText = (EditText) view.findViewById(R.id.trans_details);
		confirmButton = (Button)view.findViewById(R.id.confirm_button);
		accNameEditText = (EditText) view.findViewById(R.id.accname);
		spinner1 = (Spinner) view.findViewById(R.id.spinner1);



		balFromFile = readBalancesFromFile(getActivity());
		beneFromFile = readFromFile(getActivity());
		if (!beneFromFile.equals("")) {
			System.out.println("not empty");
			externalBeneficiary = beneFromFile;
			System.out.println("externalBeneficiary: " + externalBeneficiary);
//			populateList2();
		} else {
			System.out.println("empty");
			Toast.makeText(getActivity(), "Sorry you do not have any external beneficiaries. Tap on the plus button to create one", Toast.LENGTH_LONG).show();
		}




		/*spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {

				//				accountNum = parent.getItemAtPosition(pos).toString();
				accountNum = ((TextView) view.findViewById(R.id.acc_num)).getText().toString().trim();
				accAvailBal = ((TextView) view.findViewById(R.id.avail_bal)).getText().toString().trim();

				if (accAvailBal.contains("Le")){
					curr = "Le";
				}else if (accAvailBal.contains("¢")){
					curr = "¢";
				}else if (accAvailBal.contains("$")){
					curr = "$";
				}else if (accAvailBal.contains("£")){
					curr = "£";
				}else if (accAvailBal.contains("€")){
					curr = "€";
				}else if (accAvailBal.contains("¥")){
					curr = "¥";
				}else if (accAvailBal.contains("¥")){
					curr = "¥";
				}



			}

			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing

			}
		});

		spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
									   int position, long id) {

				merchantAccNum = ((TextView) view.findViewById(R.id.alias_acc_num)).getText().toString().trim();
				aliasName = ((TextView) view.findViewById(R.id.alias_name)).getText().toString().trim();

			}

			public void onNothingSelected(AdapterView<?> parent) {
				// Do nothing

			}
		});*/



		/*confirmButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				amount = amtEditText.getText().toString();
				transDet = transDetEditText.getText().toString();
				if (amount.isEmpty()){
					amtEditText.setError("Please enter amount");
				}  else if (accountNum.isEmpty()){
					Toast.makeText(getActivity(), "Please select an account to send from", Toast.LENGTH_LONG).show();
				}else if (transDet.isEmpty()){
					transDetEditText.setError("Please enter a summary of the transaction");
				} else {

					bankcode = merchantAccNum.substring(0,3);
					System.out.println("bankcode: " + bankcode);

					new getBankNameFireMan().execute();

				}

			}
		});*/





		return view;
	}








	private String readFromFile(Context context) {

		String ret = "";

		try {
			InputStream inputStream = context.openFileInput("extB.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("Login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("Login activity", "Cannot read file: " + e.toString());
		}

		return ret;
	}


	private String readBalancesFromFile(Context context) {

		String ret = "";

		try {
			InputStream inputStream = context.openFileInput("bal.txt");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("Login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("Login activity", "Cannot read file: " + e.toString());
		}

		return ret;
	}




//	public void populateList2(){
//		aliasAccDetails = externalBeneficiary.split("`");
//
//		List<HashMap<String,String>> theList = new ArrayList<HashMap<String,String>>();
//
//		System.out.println(aliasAccDetails.length);
//
//		for(int a=0;a<aliasAccDetails.length-1;a++){
//			StringTokenizer accDetToken = new StringTokenizer(aliasAccDetails[a], "~");
//
//			System.out.println(accDetToken.countTokens());
//
////			beneCode = accDetToken.nextToken().trim();
//			beneName = accDetToken.nextToken().trim();
//			beneAcc = accDetToken.nextToken().trim();
//			beneBank = accDetToken.nextToken().trim();
//
//			System.out.println(beneName);
//			System.out.println(beneAcc);
//			System.out.println(beneBank);
////			System.out.println(beneCode);
//
//			HashMap<String, String> hm = new HashMap<String,String>();
//
////			hm.put("benecode", beneCode);
//			hm.put("benename", beneName);
//			hm.put("beneacc", beneAcc );
//			hm.put("benebank", beneBank );
//			hm.put("firstletter", beneName.substring(0,1).toUpperCase());
//
//			theList.add(hm);
//
//		}
//
//		String[] from = { /*"benecode",*/ "benename", "beneacc", "benebank", "firstletter" };
//
//		// Ids of views in listview_layout
//		int[] to = { /*R.id.bene_code,*/ R.id.bene_name, R.id.bene_acc, R.id.bene_bank, R.id.first_letter};
//
//		BeneficiaryExtAdapter adapter2 = new BeneficiaryExtAdapter(getActivity(), theList, R.layout.list_ext_beneficiary , from , to);
//
//		list.setAdapter(adapter2);
//	}







	public String bankListJSON(String bankCode, String authToken){
		return "bankCode="+bankCode+
				"&authToken="+authToken;
	}


	public String myBanksListResponse (String a1, String a2) throws IOException {
		SendRequest sendReqObj = new SendRequest();

		String json = bankListJSON(a1, a2);
		String response = sendReqObj.postReq(GlobalCodes.myUrl + "getBankName", json);

		System.out.println(response);

		return response;

	}


	public class getBankNameFireMan extends AsyncTask<Void , Void, Void>{

		private Exception e = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progress4 = new ProgressDialog(getActivity());
			progress4.setCancelable(false);
			progress4.setTitle("Please wait");
			progress4.setMessage("Retrieving bank name...");
			progress4.show();
		}

		@Override
		protected Void doInBackground(Void ... params) {



			try {
				response = myBanksListResponse(bankcode, token);

				try {
					JSONObject jsonObject = new JSONObject(response);
					id = jsonObject.getString("id");
					creationTime = jsonObject.getString("creationTime");
					lastModificationDate = jsonObject.getString("lastModificationDate");
					mb_token = jsonObject.getString("tokenId");
					mb_response = jsonObject.getString("mb_response");
					mbAcc = jsonObject.getString("mb_accDetails");

					System.out.println(mbAcc);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if(e != null){
				progress4.cancel();
				new ResponseDialog(getActivity(), "Error!","No response from server. Please check your Internet connection and try again").showDialog();
			} else {
				super.onPostExecute(result);

				if (mb_response == null){
					progress4.cancel();
					new ResponseDialog(getActivity(), "Error!","Connection timed out. Please check your Internet connection and try again").showDialog();

				}
				else if (mb_response.trim().equals("11")){
					System.out.println("Received response code > "+mb_response);

					progress4.cancel();
					new ResponseDialog(getActivity(), "Not Found", mbAcc).showDialog();
				}
				else if(mb_response.trim().equals("00") ){
					progress4.cancel();
					System.out.println(mbAcc);

					amtEditText.setText("");
					transDetEditText.setText("");

					Intent confirmTopupIntent = new Intent(getActivity(),ConfirmAch.class);
					confirmTopupIntent.putExtra("dbAcc", accountNum);
					confirmTopupIntent.putExtra("amt", amount);
					confirmTopupIntent.putExtra("aliasName", aliasName);
					confirmTopupIntent.putExtra("crAcc", aliasAccNum);
					confirmTopupIntent.putExtra("bankName", mbAcc);
					confirmTopupIntent.putExtra("curr", curr);
					confirmTopupIntent.putExtra("transDet", transDet);
					confirmTopupIntent.putExtra("token", token);
					startActivity(confirmTopupIntent);

				}

			}

		}

	}






	public class ResponseDialog extends Builder {

		// Store the passed context
		private Context context;

		// Can be used as a regular builder
		public ResponseDialog(Context context) {
			super(context);
		}

		// Or as a custom builder, which we want
		public ResponseDialog(Context context, String title, String message) {
			super(context);
			// Store context
			this.context = context;
			// Set up everything
			setMessage(message);
			setTitle(title);
			setCancelable(false);
			//			setPadding(0, 5, 0, 5);
			setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss(); // It's just for info so we don't really care what this does
					//					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
					//					startActivity(loginIntent);
				}
			});
		}

		public void showDialog(){
			// Create and show
			AlertDialog alert = create();
			alert.show();
			// Center align everything
			((TextView)alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
			((TextView)alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
		}


	}






}

package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.liststuff.MyAccountsAdapter;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

//import com.github.mikephil.charting.charts.PieChart;
//import com.github.mikephil.charting.data.PieData;
//import com.github.mikephil.charting.data.PieDataSet;
//import com.github.mikephil.charting.data.PieEntry;

public class MainMenu extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = null;
    DrawerLayout drawerLayout;

    String usrName = null;
    ProgressDialog progress3;
    String usrId = null;
    static int a = 1;
    String mbAcc = null;

    TextView bankIn;
    TextView bankOut;
    TextView mobileMoney;
    CircleImageView photoImageView;
    TextView transBetweenAccounts;
    TextView greetingTextView;

    Dialog dialog;
    static byte[] picByte;
    String custType = null;
    boolean justLoggedIn = true;
    private CoordinatorLayout coordinatorLayout;
    String beneInternal = null;
    String beneExternal = null;
    String greeting = null;
    byte[] imageBytes1 = null;
    byte[] decodedPicByte = null;
    Bitmap decodedBitmap = null;
    TextView bottomsheetTitle;
    String base64Photo = null;


    // Array of strings storing country names
    String[] countries = new String[]{
            /*"QR Payment",*/
            "Transfer Funds",
            /*"Blink Pay",*/
            "Requests",
            "Beneficiaries",
            /*"Pay Bills",
            "Top Up Airtime",*/
//            "Blink Payment",
            "Add Account",
            "Customer Complaint",
            "Live Chat",
            "Settings",
            /*"Cheque Deposit",*/
            "Loans",
//            "Mobile Money"
    };

    // Array of integers points to images stored in /res/drawable-ldpi/
    int[] flags = new int[]{
            /*R.drawable.ic_qrscan,*/
            R.drawable.transferfunds_btn,
            /*R.drawable.ic_blinkpay,*/
            R.drawable.request_btn,
            R.drawable.ic_beneficiaries,
            /*R.drawable.topup_btn,*/
//            R.drawable.chqdeposit_btn,
            R.drawable.ic_add_acc,
            R.drawable.ic_customer_complain,
            R.drawable.ic_live_chat,
            R.drawable.ic_settings,
            /*R.drawable.chqdeposit_btn,*/
            R.drawable.ic_loan,
//            R.drawable.ic_pizza_icon

    };


    CountDownTimer countDownTimer;
    String mb_acc_details = null;
    String questions = null;
    String codeFlag = null;

    ProgressDialog progress;
    Intent transDetailsIntent;
//    PieChart pieChart;
//    List<PieEntry> entries;
//    ArrayList<String> PieEntryLabels;
//    PieDataSet pieDataSet;
//    PieData pieData;

    Double amtDouble = null;
    String formattedAmt = null;

    String id = null, idRefresh = null;
    String creationTime = null, creationTimeRefresh = null;
    String mb_userId = null, mb_userIdRefresh = null;
    String lastModificationDate = null, lastModificationDateRefresh = null;
    String mb_response = null, mb_responseRefresh = null;
    String mb_accDetails = null, mb_accDetailsRefresh = null, mb_accDetailsFirstLoad = null;
    String mb_token = null, mb_tokenRefresh = null;
    String mb_userName = null;
    String transDetails = null;
    static byte[] imageBytes = null;

    String[] accDetail = null, accDetailRefresh = null;

    String response = null, responseRefresh = null;

    // Array of strings storing country names
    String accNum = null, accNumRefresh = null;

    String accLedgerBal = null, accLedgerBalRefresh = null;

    String accAvailBal = null, accAvailBalRefresh = null;
    String accType = null;

    // Array of integers points to images stored in /res/drawable/
    int[] next = new int[]{R.drawable.more};

    // Array of strings to store currencies
    String accName = null, accNameRefresh = null;
    String accNameDisp = null;

    View view;

    String accountName = null;
    String accountNum = null;
    String availableBal = null;
    String ledgerBal = null;

    String mbAccDetails = null;
    String token = null;
    String photo = null;
    ListView list;
    SwipeRefreshLayout swipeRefLayout;
    List<HashMap<String, String>> aList, aListRefresh;

    //        Handler handler;
//   Runnable r;
    static long timeOutMinutes = 360 * 1000; // 2 minutes

    int[] colorList = new int[]{R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red,
            R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue,
            R.color.slcb_red, R.color.slcb_blue, R.color.slcb_red, R.color.slcb_blue};

//    int[] colorList = new int[]{R.color.slcb_red, R.color.slcb_blue, R.color.refresh_yellow, R.color.positive_bal, R.color.light_blue,
//            R.color.refresh_orange, R.color.cream, R.color.light_blue, R.color.status_bar, R.color.pink, R.color.c4, R.color.refresh_orange,
//            R.color.cream, R.color.light_blue, R.color.status_bar};


    String balFromFile = null;
    String photoData = null;
    String balances = null;

    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        usrName = getIntent().getExtras().getString("userName").trim();
        usrId = getIntent().getExtras().getString("userId").trim();
        token = getIntent().getExtras().getString("token").trim();
        custType = getIntent().getExtras().getString("mb_acc_details").trim();
        balances = getIntent().getExtras().getString("balances").trim();
        beneInternal = getIntent().getExtras().getString("beneInternal").trim();
        beneExternal = getIntent().getExtras().getString("beneExternal").trim();
        creationTime = getIntent().getExtras().getString("creationTime").trim();
        questions = getIntent().getExtras().getString("questions").trim();
        codeFlag = getIntent().getExtras().getString("codeFlag").trim();
//        if (!getIntent().getExtras().getString("photo").trim().equals("null")) {
//            photo = getIntent().getExtras().getString("photo").trim();
//        }


        SharedPreferences preferences = getSharedPreferences("myprefs", MODE_PRIVATE);

        if (!questions.equals("empty")) {

            Intent pinIntent = new Intent(MainMenu.this, LoginChangePassword.class);
            pinIntent.putExtra("token", token);
            pinIntent.putExtra("userName", usrName);
            pinIntent.putExtra("userId", usrId);
            pinIntent.putExtra("mb_acc_details", custType);
            pinIntent.putExtra("balances", balances);
            pinIntent.putExtra("creationTime", creationTime);
            pinIntent.putExtra("beneInternal", beneInternal);
            pinIntent.putExtra("beneExternal", beneExternal);
            pinIntent.putExtra("questions", questions);
            pinIntent.putExtra("codeFlag", codeFlag);
            finish();
            startActivity(pinIntent);

        } else if (questions.equals("empty")) {

            System.out.println("questions is empty");
            if (!codeFlag.equals("setup")) {
                System.out.println("codeflag is empty");
                Intent pinIntent = new Intent(MainMenu.this, PinSetup.class);
                pinIntent.putExtra("token", token);
                pinIntent.putExtra("userName", usrName);
                pinIntent.putExtra("userId", usrId);
                pinIntent.putExtra("mb_acc_details", custType);
                pinIntent.putExtra("balances", balances);
                pinIntent.putExtra("creationTime", creationTime);
                pinIntent.putExtra("beneInternal", beneInternal);
                pinIntent.putExtra("beneExternal", beneExternal);
                pinIntent.putExtra("questions", questions);
                pinIntent.putExtra("codeFlag", codeFlag);
                finish();
                startActivity(pinIntent);
            } else {

                setContentView(R.layout.gridmenu);

                Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(mActionBarToolbar);
//		setupNavigationView();

                usrName = getIntent().getExtras().getString("userName").trim();
                usrId = getIntent().getExtras().getString("userId").trim();
                token = getIntent().getExtras().getString("token").trim();
                custType = getIntent().getExtras().getString("mb_acc_details").trim();
                balances = getIntent().getExtras().getString("balances").trim();
                creationTime = getIntent().getExtras().getString("creationTime").trim();
                beneInternal = getIntent().getExtras().getString("beneInternal").trim();
                beneExternal = getIntent().getExtras().getString("beneExternal").trim();

                coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
                photoImageView = (CircleImageView) findViewById(R.id.text_drawable);
                photoData = readPicFromFile(MainMenu.this);

                ChequeDeposit.staticAmount = "";
                ChequeDeposit.staticBankCode = "";
                ChequeDeposit.staticBank = "";
                ChequeDeposit.staticChqNum = "";
                ChequeDeposit.staticAccDetails = "";
                ChequeDeposit.staticAccNumPosition = 10000;
                ChequeDeposit.staticBankPosition = 10000;
                ChequeDeposit.cameraFlag = 0;
                LoanQuotation._areLecturesLoaded = false;

                if (!photoData.equals("")) {
                    decodedPicByte = Base64.decode(photoData, Base64.DEFAULT);
                    decodedBitmap = BitmapFactory.decodeByteArray(decodedPicByte, 0, decodedPicByte.length);
                    photoImageView.setImageBitmap(decodedBitmap);
                    System.out.println("it worked!!!");
                }

                //         setAppIdleTimeout();
//                handler = new Handler();
//                r = new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // TODO Auto-generated method stub
//                        Toast.makeText(MainMenu.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(MainMenu.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        stopHandler();
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                };
//                startHandler();


                if (justLoggedIn) {
                    Calendar c = Calendar.getInstance();
                    int hours = c.get(Calendar.HOUR_OF_DAY);

                    if (hours >= 00 && hours < 12) {
                        greeting = "GOOD MORNING";
                    } else if (hours >= 12 && hours < 16) {
                        greeting = "GOOD AFTERNOON";
                    } else if (hours >= 16 && hours <= 23) {
                        greeting = "GOOD EVENING";
                    } /*else if(hours>=21 && hours<=24){
                greeting = "Good Night";
            }*/
                    System.out.println("time is " + hours);
//                    Snackbar snackbar = Snackbar
//                            .make(coordinatorLayout, greeting + " " + usrName, Snackbar.LENGTH_LONG);

//                    snackbar.show();
                    justLoggedIn = false;
                }

                writeToFile(usrId, MainMenu.this);
//                hashUserId(usrId);
                writeBalancesToFile(balances, MainMenu.this);
                writeInternalToFile(beneInternal, MainMenu.this);
                writeExternalToFile(beneExternal, MainMenu.this);


                TextView userName = (TextView) findViewById(R.id.user_name);
                TextView menuLastlogin = (TextView) findViewById(R.id.menu_lastlogin);
                TextView lastlogin = (TextView) findViewById(R.id.lastlogintime);
                greetingTextView = (TextView) findViewById(R.id.greeting);

                Typeface thinText = Typeface.createFromAsset(getAssets(), "HelveticaThin.otf");
                Typeface lightText = Typeface.createFromAsset(getAssets(), "HelveticaLight.otf");
                Typeface regularText = Typeface.createFromAsset(getAssets(), "HelveticaRegular.otf");
//		Typeface mediumText = Typeface.createFromAsset(getAssets(), "HelveticaMedium.otf");

                userName.setTypeface(lightText);
                greetingTextView.setTypeface(lightText);
                greetingTextView.setText(greeting);
                userName.setText(usrName);
                userName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                menuLastlogin.setTypeface(thinText);
                lastlogin.setTypeface(regularText);
                lastlogin.setText(creationTime);


                swipeRefLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);

//                Display display = getWindowManager().getDefaultDisplay();
//                Point size = new Point();
//                try {
//                    display.getRealSize(size);
////                    MainMenu.this.getResources().getDisplayMetrics();
//                } catch (NoSuchMethodError err) {
//                    display.getSize(size);
//                }
//                int width = size.x;
//                int height = size.y;

                photoImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkCameraHardware(getApplicationContext())) {
                            dispatchTakePictureIntent();
                        }

                    }
                });


                list = (ListView) swipeRefLayout.findViewById(R.id.myacclist);
                balFromFile = readFromFile(MainMenu.this);
                System.out.println("balFromFile: " + balFromFile);
                if (!balFromFile.equals("")) {
                    System.out.println("not empty");
                    mb_accDetailsFirstLoad = balFromFile;
                    populateBalances();
                } else {
                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    Boolean isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new fireManFirstLoad().execute();
                    } else {
                        showAlertDialog(MainMenu.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }
                    // new fireManFirstLoad().execute();
                }
//		new fireManFirstLoad().execute();
//                getSupportActionBar().setSubtitle(R.string.string_pull);

                list.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int i) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        if (firstVisibleItem == 0)
                            swipeRefLayout.setEnabled(true);
                        else
                            swipeRefLayout.setEnabled(false);
                    }
                });


                swipeRefLayout.setColorSchemeColors(this.getResources().getColor(R.color.negative_bal),
                        this.getResources().getColor(R.color.refresh_yellow),
                        this.getResources().getColor(R.color.positive_bal),
                        this.getResources().getColor(R.color.refresh_orange));


                swipeRefLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipeRefLayout.setRefreshing(true);
                        (new Handler()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
//                                getSupportActionBar().setSubtitle(R.string.string_pulling);
//						new fireManRefresh().execute();
                                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                                Boolean isInternetPresent = cd.isConnectingToInternet();
                                if (isInternetPresent) {
                                    new fireManFirstLoad().execute();
                                } else {
                                    showAlertDialog(MainMenu.this, "No Internet Connection",
                                            "You don't have internet connection.", false);
                                }
                                //  new fireManFirstLoad().execute();
                                swipeRefLayout.setRefreshing(false);

                            }
                        }, 1000);
                    }
                });


                list.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                            long arg3) {
                        // TODO Auto-generated method stub
                        accountName = ((TextView) v.findViewById(R.id.acc_name)).getText().toString().trim();
                        accountNum = ((TextView) v.findViewById(R.id.acc_num)).getText().toString().trim();
                        availableBal = ((TextView) v.findViewById(R.id.curr)).getText().toString().trim() +
                                ((TextView) v.findViewById(R.id.avail_bal)).getText().toString().trim()/*+
                        ((TextView) v.findViewById(R.id.decimal)).getText().toString().trim()*/;
                        ledgerBal = ((TextView) v.findViewById(R.id.curr2)).getText().toString().trim() +
                                ((TextView) v.findViewById(R.id.ledger_bal)).getText().toString().trim()/*+
                        ((TextView) v.findViewById(R.id.decimal2)).getText().toString().trim()*/;

                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        Boolean isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            new fireMan().execute();
                        } else {
                            showAlertDialog(MainMenu.this, "No Internet Connection",
                                    "You don't have internet connection.", false);
                        }
                        //   new fireMan().execute();
                    }
                });


                View bottomSheet = findViewById(R.id.design_bottom_sheet);
                final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);

                bottomsheetTitle = (TextView) findViewById(R.id.bottomsheet_text);
                bottomsheetTitle.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        } else {
                            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

//					behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED);
                        }
                    }
                });

                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        switch (newState) {
                            case BottomSheetBehavior.STATE_DRAGGING:
                                Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                                break;
                            case BottomSheetBehavior.STATE_SETTLING:
                                Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                                break;
                            case BottomSheetBehavior.STATE_EXPANDED:
                                bottomsheetTitle.setText("Menu");
                                Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                                break;
                            case BottomSheetBehavior.STATE_COLLAPSED:
                                bottomsheetTitle.setText(R.string.bottomsheet_title);
                                Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                                break;
                            case BottomSheetBehavior.STATE_HIDDEN:
                                Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                                break;
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
                    }
                });


                List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

                for (int i = 0; i < countries.length; i++) {
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm.put("txt", countries[i]);
                    hm.put("flag", Integer.toString(flags[i]));
                    aList.add(hm);
                }

                // Keys used in Hashmap
                String[] from = {"flag", "txt"};

                // Ids of views in listview_layout
                int[] to = {R.id.flag, R.id.txt};

                // Instantiating an adapter to store each items
                // R.layout.listview_layout defines the layout of each item
                SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), aList, R.layout.gridview_layout, from, to);

                // Getting a reference to gridview of MainActivity
                final GridView gridView = (GridView) findViewById(R.id.gridview);

                // Setting an adapter containing images to the gridview
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                        String text = (String) ((TextView) v.findViewById(R.id.txt)).getText();

                        switch (text) {
                            case "QR Payment":
                                if (custType.equals("C")) {
                                    Intent addAccIntent = new Intent(MainMenu.this, QRPayments.class);
                                    addAccIntent.putExtra("token", token);
                                    addAccIntent.putExtra("userName", usrName);
                                    addAccIntent.putExtra("userId", usrId);
                                    addAccIntent.putExtra("mb_acc_details", custType);
                                    addAccIntent.putExtra("balances", balances);
                                    addAccIntent.putExtra("creationTime", creationTime);
                                    addAccIntent.putExtra("beneInternal", beneInternal);
                                    addAccIntent.putExtra("beneExternal", beneExternal);
                                    addAccIntent.putExtra("questions", questions);
                                    addAccIntent.putExtra("codeFlag", codeFlag);
                                    addAccIntent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                                    startActivity(addAccIntent);
                                } else if (custType.equals("I")) {
                                    Intent addAccIntent = new Intent(MainMenu.this, QRPayments.class);
                                    addAccIntent.putExtra("token", token);
                                    addAccIntent.putExtra("userName", usrName);
                                    addAccIntent.putExtra("userId", usrId);
                                    addAccIntent.putExtra("mb_acc_details", custType);
                                    addAccIntent.putExtra("balances", balances);
                                    addAccIntent.putExtra("creationTime", creationTime);
                                    addAccIntent.putExtra("beneInternal", beneInternal);
                                    addAccIntent.putExtra("beneExternal", beneExternal);
                                    addAccIntent.putExtra("questions", questions);
                                    addAccIntent.putExtra("codeFlag", codeFlag);
                                    addAccIntent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                                    startActivity(addAccIntent);
                                }
                                break;
                            case "Loans":
                                if (custType.equals("C")) {
                                    Intent addAccIntent = new Intent(MainMenu.this, Loans.class);
                                    addAccIntent.putExtra("token", token);
//                                    addAccIntent.putExtra("userName", usrName);
//                                    addAccIntent.putExtra("userId", usrId);
//                                    addAccIntent.putExtra("mb_acc_details", custType);
//                                    addAccIntent.putExtra("balances", balances);
//                                    addAccIntent.putExtra("creationTime", creationTime);
//                                    addAccIntent.putExtra("beneInternal", beneInternal);
//                                    addAccIntent.putExtra("beneExternal", beneExternal);
//                                    addAccIntent.putExtra("questions", questions);
//                                    addAccIntent.putExtra("codeFlag", codeFlag);
//                                    addAccIntent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                                    startActivity(addAccIntent);
                                } else if (custType.equals("I")) {
                                    Intent addAccIntent = new Intent(MainMenu.this, Loans.class);
                                    addAccIntent.putExtra("token", token);
//                                    addAccIntent.putExtra("userName", usrName);
//                                    addAccIntent.putExtra("userId", usrId);
//                                    addAccIntent.putExtra("mb_acc_details", custType);
//                                    addAccIntent.putExtra("balances", balances);
//                                    addAccIntent.putExtra("creationTime", creationTime);
//                                    addAccIntent.putExtra("beneInternal", beneInternal);
//                                    addAccIntent.putExtra("beneExternal", beneExternal);
//                                    addAccIntent.putExtra("questions", questions);
//                                    addAccIntent.putExtra("codeFlag", codeFlag);
//                                    addAccIntent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                                    startActivity(addAccIntent);
                                }
                                break;
                            case "Transfer Funds":
                                if (custType.equals("C")) {
                                    showDialogMenu();
                                } else if (custType.equals("I")) {
                                    showDialogMenu();
                                }
                                break;
                            case "Beneficiaries":
                                if (custType.equals("C")) {
                                    Intent beneficiaryIntent = new Intent(MainMenu.this, ViewBeneficiaries.class);
                                    beneficiaryIntent.putExtra("token", token);
                                    beneficiaryIntent.putExtra("userName", usrName);
                                    beneficiaryIntent.putExtra("userId", usrId);
                                    beneficiaryIntent.putExtra("mb_acc_details", custType);
                                    beneficiaryIntent.putExtra("balances", balances);
                                    beneficiaryIntent.putExtra("creationTime", creationTime);
                                    beneficiaryIntent.putExtra("beneInternal", beneInternal);
                                    beneficiaryIntent.putExtra("beneExternal", beneExternal);
                                    beneficiaryIntent.putExtra("questions", questions);
                                    beneficiaryIntent.putExtra("codeFlag", codeFlag);
                                    startActivity(beneficiaryIntent);
                                } else {
                                    Intent beneficiaryIntent = new Intent(MainMenu.this, ViewBeneficiaries.class);
                                    beneficiaryIntent.putExtra("token", token);
                                    beneficiaryIntent.putExtra("userName", usrName);
                                    beneficiaryIntent.putExtra("userId", usrId);
                                    beneficiaryIntent.putExtra("mb_acc_details", custType);
                                    beneficiaryIntent.putExtra("balances", balances);
                                    beneficiaryIntent.putExtra("creationTime", creationTime);
                                    beneficiaryIntent.putExtra("beneInternal", beneInternal);
                                    beneficiaryIntent.putExtra("beneExternal", beneExternal);
                                    beneficiaryIntent.putExtra("questions", questions);
                                    beneficiaryIntent.putExtra("codeFlag", codeFlag);
                                    startActivity(beneficiaryIntent);
                                }
                                break;
                            case "Customer Complaint":
                                Intent intent = new Intent(MainMenu.this, CustomerComplaint.class);
                                intent.putExtra("token", token);
                                intent.putExtra("userName", usrName);
                                intent.putExtra("userId", usrId);
                                intent.putExtra("mb_acc_details", custType);
                                intent.putExtra("balances", balances);
                                intent.putExtra("creationTime", creationTime);
                                intent.putExtra("beneInternal", beneInternal);
                                intent.putExtra("beneExternal", beneExternal);
                                intent.putExtra("questions", questions);
                                intent.putExtra("codeFlag", codeFlag);
                                startActivity(intent);
                                break;
                            case "Cheque Deposit":
                                if (custType.equals("C")) {
//                            Toast.makeText(MainMenu.this,"Sorry, corporate customers cannot use this functionality", Toast.LENGTH_LONG).show();
                                    Intent entIntent = new Intent(MainMenu.this, ChequeDeposit.class);
                                    entIntent.putExtra("token", token);
                                    entIntent.putExtra("cameraFlag", 0);
                                    String pic = "djfkd";
                                    picByte = pic.getBytes();
                                    entIntent.putExtra("data", picByte);
                                    startActivity(entIntent);
                                } else if (custType.equals("I")) {
                                    Intent entIntent = new Intent(MainMenu.this, ChequeDeposit.class);
                                    entIntent.putExtra("token", token);
                                    entIntent.putExtra("cameraFlag", 0);
                                    String pic = "djfkd";
                                    picByte = pic.getBytes();
                                    entIntent.putExtra("data", picByte);
                                    startActivity(entIntent);
                                }
                                break;
                    /*Intent payBills = new Intent(MainMenu.this,PayBillsList.class);
                    payBills.putExtra("token", token);
					startActivity(payBills);*//*

                        break;
                    /*case "Top Up Airtime":
					*//*Intent topupIntent = new Intent(MainMenu.this,TopUp.class);
                    topupIntent.putExtra("token", token);
					//					topupIntent.putExtra("accdetails", mbAccDetails);
					startActivity(topupIntent);*//*
                        Toast.makeText(MainMenu.this, "Under development", Toast.LENGTH_LONG).show();
                        break;
                    */

                            case "Requests":
                                if (custType.equals("C")) {
//                                    Toast.makeText(MainMenu.this, "Sorry, corporate customers cannot use this functionality", Toast.LENGTH_LONG).show();
                                    Intent reqtIntent = new Intent(MainMenu.this, RequestList.class);
                                    reqtIntent.putExtra("token", token);
                                    startActivity(reqtIntent);
                                } else if (custType.equals("I")) {
                                    Intent reqtIntent = new Intent(MainMenu.this, RequestList.class);
                                    reqtIntent.putExtra("token", token);
                                    startActivity(reqtIntent);
                                }
                                break;
                            case "Add Account":
                                if (custType.equals("C")) {
//                                    Toast.makeText(MainMenu.this, "Sorry, corporate customers cannot use this functionality", Toast.LENGTH_LONG).show();
                                    Intent addAccIntent = new Intent(MainMenu.this, AcctCreation.class);
                                    addAccIntent.putExtra("token", token);
                                    addAccIntent.putExtra("userName", usrName);
                                    startActivity(addAccIntent);
                                } else if (custType.equals("I")) {
                                    Intent addAccIntent = new Intent(MainMenu.this, AcctCreation.class);
                                    addAccIntent.putExtra("token", token);
                                    addAccIntent.putExtra("userName", usrName);
                                    startActivity(addAccIntent);
                                }
                                break;
                            case "Blink Pay":
                                if (custType.equals("C")) {
//                                    Toast.makeText(MainMenu.this, "Sorry, corporate customers cannot use this functionality", Toast.LENGTH_LONG).show();
                                    Intent blinkPayIntent = new Intent(MainMenu.this, BlinkPay.class);
                                    blinkPayIntent.putExtra("token", token);
                                    blinkPayIntent.putExtra("usrId", usrId);
                                    startActivity(blinkPayIntent);
                                } else if (custType.equals("I")) {
                                    Intent blinkPayIntent = new Intent(MainMenu.this, BlinkPay.class);
                                    blinkPayIntent.putExtra("token", token);
                                    blinkPayIntent.putExtra("usrId", usrId);
                                    startActivity(blinkPayIntent);
                                }
                                break;
                            case "Live Chat":
                                if (custType.equals("C")) {
//                            Toast.makeText(MainMenu.this,"Sorry, corporate customers cannot use this functionality", Toast.LENGTH_LONG).show();
                                    Intent liveChatIntent = new Intent(MainMenu.this, LiveChat.class);
                                    startActivity(liveChatIntent);
                                } else if (custType.equals("I")) {
                                    Intent liveChatIntent = new Intent(MainMenu.this, LiveChat.class);
                                    startActivity(liveChatIntent);
                                }
                                break;
                            case "Settings":
                                Intent hoverList = new Intent(MainMenu.this, Settings.class);
                                hoverList.putExtra("token", token);
                                startActivity(hoverList);
                            default:
                                break;

                        }
                    }

                });

            }


        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            photoImageView.setImageBitmap(imageBitmap);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            byte[] picByte = baos.toByteArray();
            base64Photo = Base64.encodeToString(picByte, Base64.DEFAULT);


            writePicToFile(base64Photo, MainMenu.this);

            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new confirmFireMan().execute();
            } else {
                showAlertDialog(MainMenu.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
            //new confirmFireMan().execute();

//            BitmapFactory.decodeByteArray(picByte, 0, picByte.length);
        }
    }


    public void showDialogMenu() {

        dialog = new Dialog(MainMenu.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.transfer_dialog);
        dialog.setCancelable(true);
        bankIn = (TextView) dialog.findViewById(R.id.bank_in);
        bankOut = (TextView) dialog.findViewById(R.id.bank_out);
        transBetweenAccounts = (TextView) dialog.findViewById(R.id.between_accounts);
        mobileMoney = (TextView) dialog.findViewById(R.id.mobile_money);
//        blinkPayment = (TextView) dialog.findViewById(R.id.blink_payment);
        //				bankIn.setClickable(true);
        bankIn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                Intent fundstrnsferIntent = new Intent(MainMenu.this, FundsTransfer.class);
                fundstrnsferIntent.putExtra("token", token);
                fundstrnsferIntent.putExtra("userName", usrName);
                fundstrnsferIntent.putExtra("userId", usrId);
                fundstrnsferIntent.putExtra("mb_acc_details", custType);
                fundstrnsferIntent.putExtra("balances", balances);
                fundstrnsferIntent.putExtra("creationTime", creationTime);
                fundstrnsferIntent.putExtra("beneInternal", beneInternal);
                fundstrnsferIntent.putExtra("beneExternal", beneExternal);
                fundstrnsferIntent.putExtra("questions", questions);
                fundstrnsferIntent.putExtra("codeFlag", codeFlag);
                fundstrnsferIntent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                startActivity(fundstrnsferIntent);
            }
        });


        transBetweenAccounts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                Intent fundstrnsferIntent = new Intent(MainMenu.this, TransferBetweenAccounts.class);
                fundstrnsferIntent.putExtra("token", token);
                fundstrnsferIntent.putExtra("userName", usrName);
                fundstrnsferIntent.putExtra("userId", usrId);
                fundstrnsferIntent.putExtra("mb_acc_details", custType);
                fundstrnsferIntent.putExtra("balances", balances);
                fundstrnsferIntent.putExtra("creationTime", creationTime);
                fundstrnsferIntent.putExtra("beneInternal", beneInternal);
                fundstrnsferIntent.putExtra("beneExternal", beneExternal);
                fundstrnsferIntent.putExtra("questions", questions);
                fundstrnsferIntent.putExtra("codeFlag", codeFlag);
                fundstrnsferIntent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                startActivity(fundstrnsferIntent);
            }
        });

        mobileMoney.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                Intent intent = new Intent(MainMenu.this, MobileMoneyTransfers.class);
                intent.putExtra("token", token);
                intent.putExtra("userName", usrName);
                intent.putExtra("userId", usrId);
                intent.putExtra("mb_acc_details", custType);
                intent.putExtra("balances", balances);
                intent.putExtra("creationTime", creationTime);
                intent.putExtra("beneInternal", beneInternal);
                intent.putExtra("beneExternal", beneExternal);
                intent.putExtra("questions", questions);
                intent.putExtra("codeFlag", codeFlag);
                intent.putExtra("accdetails", GlobalCodes.getMbAccDetails());
                startActivity(intent);
            }
        });

        dialog.show();

    }


    private boolean checkCameraHardware(Context context) {
        // this device has a camera
// no camera on this device
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private String readPicFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("pic.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//        super.onUserInteraction();
//        stopHandler();//stop first and then start
//        startHandler();
//    }
//    public void stopHandler() {
//        handler.removeCallbacks(r);
//    }
//    public void startHandler() {
//        handler.postDelayed(r, 5*60*1000); //for 5 minutes
//    }


//    public static Handler handler;
//    public static Runnable runnable;
//
//
//    private void setAppIdleTimeout() {
//
//        handler = new Handler();
//        runnable = new Runnable() {
//
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//                runOnUiThread(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        // Navigate to main activity
//                        Toast.makeText(MainMenu.this, "You have been timed out", Toast.LENGTH_SHORT).show();
//                        Intent loginIntent = new Intent(MainMenu.this, LoginActivity.class);
//                        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(loginIntent);
//                        finish();
//                    }
//                });
//            }
//        };
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//    //reset timer on user interaction and in onResume
//    public static void resetAppIdleTimeout() {
//        System.out.println("time reset");
//        handler.removeCallbacks(runnable);
//        handler.postDelayed(runnable, MainMenu.timeOutMinutes);
//    }
//
//    @Override
//    protected void onResume() {
//        // TODO Auto-generated method stub
//        super.onResume();
//        resetAppIdleTimeout();
//    }
//
//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//
//        super.onUserInteraction();
//        resetAppIdleTimeout();
//        System.out.println("mainmenu interacted");
//        Log.i(TAG, "interacted");
//    }
//
//    @Override
//    public void onDestroy() {
//        // TODO Auto-generated method stub
//        handler.removeCallbacks(runnable);
//        super.onDestroy();
//    }


    public static void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("savedoc.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private void writeInternalToFile(String intBene, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("intB.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(intBene);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private void writeExternalToFile(String extBene, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("extB.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(extBene);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private void writePicToFile(String pic, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("pic.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(pic);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.action_settings:
////                Intent hoverList = new Intent(MainMenu.this, Settings.class);
////                hoverList.putExtra("token", token);
////                startActivity(hoverList);
//                break;
            case R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.action_exit:
                exitApp();
                break;

            default:
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void exitApp() {
        try {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainMenu.this);
                    alertDialog.setTitle("Confirm exit");
                    alertDialog.setMessage("Do you want to logout?");

                    alertDialog.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    MainMenu.super.onBackPressed();
                                }
                            });
                    // Setting Negative "NO" Btn
                    alertDialog.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog

                                    dialog.cancel();

                                }
                            });
                    alertDialog.show();

                }
            });

        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    public void timedOut() {

        Toast.makeText(MainMenu.this, "You have been timed out", Toast.LENGTH_SHORT).show();
        Intent loginIntent = new Intent(MainMenu.this, LoginActivity.class);
        loginIntent.setFlags(loginIntent.FLAG_ACTIVITY_NEW_TASK | loginIntent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();

    }


    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("bal.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("Login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Login activity", "Cannot read file: " + e.toString());
        }

        return ret;
    }


    public void populateBalances() {

        GlobalCodes.setMbAccDetails(mb_accDetailsFirstLoad);
        accDetail = GlobalCodes.getMbAccDetails().split(" ` ");
        System.out.println("length of accdetail array: " + accDetail.length);
        aList = new ArrayList<HashMap<String, String>>();


        for (int a = 0; a < accDetail.length; a++) {
            StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

            accNum = myAccToken.nextToken().trim();
            accName = myAccToken.nextToken().trim();
            accAvailBal = myAccToken.nextToken().trim();
            accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
//            entries.add(new PieEntry(Float.parseFloat(GlobalCodes.numberValue), a));
            System.out.println("entry " + a + ": " + Float.parseFloat(GlobalCodes.numberValue));
            accLedgerBal = myAccToken.nextToken().trim();
            accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
            accType = myAccToken.nextToken().trim();

            GlobalCodes.breakUpAmt(accAvailBal);
            GlobalCodes.breakUpLedger(accLedgerBal);


            HashMap<String, String> hm = new HashMap<String, String>();


            hm.put("accname", accName);
            hm.put("accnum", accNum);
            hm.put("curr", GlobalCodes.currAvail);
            hm.put("curr2", GlobalCodes.currLedger);
            hm.put("bal", GlobalCodes.amtAvail + "." + GlobalCodes.decimalAvail);
//						hm.put("dec", );
//						hm.put("dec2", "." + GlobalCodes.decimalLedger);
            hm.put("ledgerbal", GlobalCodes.amtLedger + "." + GlobalCodes.decimalLedger);
            hm.put("acctype", accType);
            hm.put("next", Integer.toString(next[0]));
            hm.put("colors", Integer.toString(colorList[a]));

//            colorImage.setBackgroundColor(Color.BLUE);

            aList.add(hm);

        }

        String[] from = {"next", "accname", "bal", "accnum", "ledgerbal", "curr", /*"dec",*/ "curr2", "acctype", "colors"};

        // Ids of views in listview_layout
        int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal, R.id.curr,  /*R.id.decimal,*/ R.id.curr2, R.id.acc_type, R.id.my_color};

        MyAccountsAdapter adapter = new MyAccountsAdapter(MainMenu.this, aList, R.layout.list_myaccounts, from, to);

        list.setAdapter(adapter);

    }


    public String confirmJSON(String photo, String authToken) throws UnsupportedEncodingException {

        return photo + "~" + authToken;

    }


    public String confirmResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();


        String json = confirmJSON(a1, a2);
        String response = sendReqObj.postReqCheque(GlobalCodes.myUrl + "savePhoto", json, 120);

//        System.out.println(response);

        return response;
    }


    public class confirmFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress3 = new ProgressDialog(MainMenu.this);
            progress3.setCancelable(false);
            progress3.setTitle("Please wait");
            progress3.setMessage("Sending request...");
            progress3.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {

                response = confirmResponse(base64Photo, token);

                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        //					System.out.println(mb_userId);
//					System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.println("json error");
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.out.println("io error ");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            System.out.println("RESPONSE: " + response);
            System.out.println(mb_token);
            System.out.println(mb_response);
            System.out.println(mb_userId);
            System.out.println(mbAcc);

            if (e != null) {
                progress3.cancel();
                new ResponseDialog(MainMenu.this, "Error!", "No response from server. Please check your Internet connection and try again");
            } else {
                super.onPostExecute(result);

                if (mb_response == null) {
                    progress3.cancel();
                    new ResponseDialog(MainMenu.this, "Error!", "Connection timed out. Please check your Internet connection and try again");

                } else if (mb_response.trim().equals("21")) {
                    progress3.cancel();
                    new ResponseDialog(MainMenu.this, "Unsuccessful", mbAcc).showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress3.cancel();
                    new ResponseDialog(MainMenu.this, "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("00")) {
                    progress3.cancel();
                    System.out.println("mbAcc is " + mbAcc);
                    new ResponseDialog(MainMenu.this, "Successful", mbAcc).showDialog();


                }

            }

        }

    }


    public String transEnqJSON(String authToken, String accNum) {
        return "authToken=" + authToken +
                "&accNum=" + accNum;
    }

    public String refreshJSON(String authToken) {
        return "authToken=" + authToken;

    }

    public String myResponseRefresh(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = refreshJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getacc", json);

        System.out.println(response);

        return response;
    }


    public class fireManFirstLoad extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(MainMenu.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Getting Balances....");
            progress.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                responseRefresh = myResponseRefresh(token);

                try {
                    if (!responseRefresh.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(responseRefresh);
                        idRefresh = jsonObject.getString("id");
                        creationTimeRefresh = jsonObject.getString("creationTime");

                        lastModificationDateRefresh = jsonObject.getString("lastModificationDate");
                        mb_tokenRefresh = jsonObject.getString("tokenId");
                        mb_responseRefresh = jsonObject.getString("mb_response");
                        mb_userIdRefresh = jsonObject.getString("mb_userId");
                        mb_accDetailsFirstLoad = jsonObject.getString("mb_accDetails");

                    } else {
                        mb_responseRefresh = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
//                getSupportActionBar().setSubtitle(R.string.string_pull);
                progress.cancel();
                Toast.makeText(MainMenu.this, "Error.Please Try Again", Toast.LENGTH_LONG).show();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_responseRefresh.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_responseRefresh);
//                    getSupportActionBar().setSubtitle(R.string.string_pull);
                    progress.cancel();
                    Toast.makeText(MainMenu.this, "Error.Please Try Again", Toast.LENGTH_LONG).show();

                } else if (mb_responseRefresh.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(MainMenu.this, "No response", "Please try again").showDialog();
                } else if (mb_responseRefresh.trim().equals("00")) {
//                    getSupportActionBar().setSubtitle(R.string.string_pull);
                    progress.cancel();
                    GlobalCodes.setMbAccDetails(mb_accDetailsFirstLoad);
                    accDetail = GlobalCodes.getMbAccDetails().split(" ` ");
                    aList = new ArrayList<HashMap<String, String>>();

                    for (int a = 0; a < accDetail.length; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(accDetail[a], "~");

                        accNum = myAccToken.nextToken().trim();
                        accName = myAccToken.nextToken().trim();
//						if (loanProdCode.length() > 21) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
//						}
                        accAvailBal = myAccToken.nextToken().trim();
                        accAvailBal = GlobalCodes.FormatToMoney(accAvailBal);
//                        entries.add(new PieEntry(Float.parseFloat(GlobalCodes.numberValue), a));
                        accLedgerBal = myAccToken.nextToken().trim();
                        accLedgerBal = GlobalCodes.FormatToMoney(accLedgerBal);
                        accType = myAccToken.nextToken().trim();

                        GlobalCodes.breakUpAmt(accAvailBal);
                        GlobalCodes.breakUpLedger(accLedgerBal);


                        HashMap<String, String> hm = new HashMap<String, String>();

						/*if (loanProdCode.length() > 21) {
//							accNameDisp = loanProdCode.substring(0, 18) + "...";
							hm.put("accname", loanProdCode.substring(0, 18) + "...");
						} else {
							hm.put("accname", loanProdCode);
						}*/

                        hm.put("accname", accName);
                        hm.put("accnum", accNum);
                        hm.put("curr", GlobalCodes.currAvail);
                        hm.put("curr2", GlobalCodes.currLedger);
                        hm.put("bal", GlobalCodes.amtAvail + "." + GlobalCodes.decimalAvail);
//						hm.put("dec", );
//						hm.put("dec2", "." + GlobalCodes.decimalLedger);
                        hm.put("ledgerbal", GlobalCodes.amtLedger + "." + GlobalCodes.decimalLedger);
                        hm.put("acctype", accType);
                        hm.put("next", Integer.toString(next[0]));
                        hm.put("colors", Integer.toString(colorList[a]));

                        aList.add(hm);

                    }


                    String[] from = {"next", "accname", "bal", "accnum", "ledgerbal", "curr", /*"dec",*/ "curr2", "acctype", "colors"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.flag, R.id.acc_name, R.id.avail_bal, R.id.acc_num, R.id.ledger_bal, R.id.curr,  /*R.id.decimal,*/ R.id.curr2, R.id.acc_type, R.id.my_color};

                    MyAccountsAdapter adapter = new MyAccountsAdapter(MainMenu.this, aList, R.layout.list_myaccounts, from, to);


                    writeBalancesToFile(mb_accDetailsFirstLoad, MainMenu.this);
                    list.setAdapter(adapter);


                } else {
//                    getSupportActionBar().setSubtitle(R.string.string_pull);
                    progress.cancel();
                    Toast.makeText(MainMenu.this, "Network is down. Please Try Again Later", Toast.LENGTH_LONG).show();
                }

            }

        }

    }


//    public String hashUserId(String userId){
//
////        String length = String.valueOf(userId.length());
//        System.out.println("length of userId:  " + userId.length());
//        String firstThreeChars = userId.substring(0, 3);
//        for (int i = 0; i <= userId.length()-4; i++){
//            firstThreeChars = firstThreeChars + "*";
//        }
//        userId = firstThreeChars;
//        System.out.println("hashed userId: " + userId);
//        System.out.println("length of userId:  " + userId.length());
//        return userId;
//
//    }


    private void writeBalancesToFile(String bal, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("bal.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(bal);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    public String myResponse(String a1, String a2) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = transEnqJSON(a1, a2);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "transenq", json);

        System.out.println(response);

        return response;
    }


    public class fireMan extends AsyncTask<Void, Void, Void>/*<Void, Void, Void>*/ {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(MainMenu.this);
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myResponse(token, accountNum);

                try {
                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        id = jsonObject.getString("id");
                        creationTime = jsonObject.getString("creationTime");

                        lastModificationDate = jsonObject.getString("lastModificationDate");
                        mb_token = jsonObject.getString("tokenId");
                        mb_response = jsonObject.getString("mb_response");
                        mb_userId = jsonObject.getString("mb_userId");
//					mb_accDetails = jsonObject.getString("mb_accDetails");
                        transDetails = jsonObject.getString("mb_transDetails");
//					imageBytes = jsonObject.getString("mb_image1").getBytes();
//					imageBytes = mb_accDetails;

                        //					System.out.println(mb_userId);
                        System.out.println(transDetails);

                    } else {
                        mb_response = "66";
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("13")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(MainMenu.this, "Account is Locked.Please Retry", Toast.LENGTH_LONG).show();


                } else if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    Toast.makeText(MainMenu.this, "No Transactions Are Available On This Account", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(MainMenu.this, "No response", "Please try again").showDialog();

                } else if (mb_response.trim().equals("11")) {

                    progress.cancel();
                    Toast.makeText(MainMenu.this, "Wrong UserName or Password Entered. Please Retry", Toast.LENGTH_LONG).show();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();
                    System.out.println("TRANS DETAILS: " + transDetails);
                    if (transDetails.equals("No Transactions Are Available On This Account")) {
                        Toast.makeText(MainMenu.this, transDetails, Toast.LENGTH_LONG).show();
                    } else {
                        Intent transEnqIntent = new Intent(MainMenu.this, TransEnquiry.class);
                        transEnqIntent.putExtra("accNameKey", accountName);
                        transEnqIntent.putExtra("accNumKey", accountNum);
                        transEnqIntent.putExtra("availBalKey", availableBal);

                        transEnqIntent.putExtra("ledgerBalKey", ledgerBal);
                        transEnqIntent.putExtra("tokenKey", mb_token);
                        transEnqIntent.putExtra("transDetailsKey", transDetails);
                        transEnqIntent.putExtra("imageBytes", imageBytes);
                        startActivity(transEnqIntent);
                    }

                } else {
                    progress.cancel();
                    Toast.makeText(MainMenu.this, "Network is down. Please Try Again Later", Toast.LENGTH_LONG).show();
                }


            }

        }


    }


    public class ResponseDialog extends AlertDialog.Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// TODO Auto-generated method stub
//		switch(item.getItemId()){
//
//		case R.id.action_settings:
//			Intent hoverList = new Intent(MainMenu.this, Settings.class);
//			hoverList.putExtra("token", token);
//			startActivity(hoverList);
//			break;
//		case R.id.action_exit:
//			exitApp();
//			break;
//
//		default:
//		}
//		return super.onOptionsItemSelected(item);
//	}


    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        exitApp();
    }

    @Override
    public void onClick(View view) {

    }


//    @Override
//    public void onUserInteraction() {
//        // TODO Auto-generated method stub
//        super.onUserInteraction();
//        stopHandler();//stop first and then start
//        startHandler();
//    }
//
//    public void stopHandler() {
//        handler.removeCallbacks(r);
//    }
//
//    public void startHandler() {
//        handler.postDelayed(r, timeOutMinutes); //for 5 minutes
//    }


//    @Override
//    public void onUserInteraction() {
//        super.onUserInteraction();
//        countDownTimer.cancel();
//        countDownTimer.start();
//    }




    /*@Override
    public boolean onTouchEvent(MotionEvent event) {

        int eventAction = event.getAction();

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                Toast.makeText(MainMenu.this, "DOWN", Toast.LENGTH_SHORT).show();
                countDownTimer.cancel();
                countDownTimer.start();
                break;
            case MotionEvent.ACTION_UP:
                Toast.makeText(MainMenu.this, "UP", Toast.LENGTH_SHORT).show();
                countDownTimer.cancel();
                countDownTimer.start();
                break;
            case MotionEvent.ACTION_SCROLL:
                Toast.makeText(MainMenu.this, "SCROLL", Toast.LENGTH_SHORT).show();
                countDownTimer.cancel();
                countDownTimer.start();
                break;
            case MotionEvent.ACTION_MOVE:
                Toast.makeText(MainMenu.this, "MOVE", Toast.LENGTH_SHORT).show();
                countDownTimer.cancel();
                countDownTimer.start();
                break;
            case MotionEvent.EDGE_LEFT:
                Toast.makeText(MainMenu.this, "LEFT", Toast.LENGTH_SHORT).show();
                countDownTimer.cancel();
                countDownTimer.start();
                break;

        }

//        if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_SCROLL ||
//                event.getAction() == MotionEvent.ACTION_MOVE || event.getAction() == MotionEvent.ACTION_DOWN) {
//            countDownTimer.cancel();
//            countDownTimer.start();
//
////            Toast.makeText(MainMenu.this, "touch", Toast.LENGTH_LONG).show();
//            return true;
//         {
//            return false;
//        }
        return false;
    }*/


//    public boolean onTouchEvent(MotionEvent event) {
//        super.onTouchEvent(event);
//
//        new CountDownTimer(15000, 1000) {
//
//            public void onTick(long millisUntilFinished) {
////					mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
//                System.out.println("seconds remaining: " + millisUntilFinished / 1000);
//            }
//
//            public void onFinish() {
//                timedOut();
//            }
//        }.start();
//
//        return false;
//    }


//	@Override
//	public boolean dispatchTouchEvent(MotionEvent ev) {
//		lastActivity = new Date().getTime();
//		System.out.println("last touch event: " + lastActivity);
//		return super.dispatchTouchEvent(ev);
//	}

//	@Override
//	public void onResume() {
//		long now = new Date().getTime();
//		System.out.println("current time minus last time is " + (now - lastActivity) );
////		if (now - lastActivity > xxxx) {
////			// startActivity and force logon
////		}
//	}

	/*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_HOME)) {
        	System.out.println("KEYCODE_HOME");
        	Toast.makeText(MainMenu.this, "home", Toast.LENGTH_LONG).show();
        	System.out.println("home");
        	finish();
            return true;
        }
		return false;
	}*/

	/*@Override
    public void onPause() {
	    if (isApplicationSentToBackground(this)){
	    	finish();
	    }
	    super.onPause();
	}
	
	public boolean isApplicationSentToBackground(final Context context) {
	    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    List<RunningTaskInfo> tasks = am.getRunningTasks(1);
	    if (!tasks.isEmpty()) {
	        ComponentName topActivity = tasks.get(0).topActivity;
	        if (!topActivity.getPackageName().equals(context.getPackageName())) {
	            return true;
	        }
	    }
	    return false;
	}
*/


}

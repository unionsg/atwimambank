package com.unionsg.blink;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.unionsg.connection.ConnectionDetector;
import com.unionsg.jsonstuff.SendRequest;
import com.unionsg.otherservices.GlobalCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.unionsg.blink.LoginActivity.GetDeviceipMobileData;
import static com.unionsg.blink.LoginActivity.GetDeviceipWiFiData;
import static com.unionsg.otherservices.GlobalCodes.showAlertDialog;

public class LoanQuotation extends Fragment {

    private Spinner loanProdSpinner, interestTypeSpinner, principalRepayFreqSpinner, interestRepayFreqSpinner;
    private EditText amtEditText;
    private EditText tenureInMonthEditText;
    private Button confirmButton;
    String[] accDetail = null;

    String id = null;
    String creationTime = null;
    String mb_userId = null;
    String lastModificationDate = null;
    String mb_response = null;
    String mb_accDetails = null;
    String mb_token = null;
    String mb_userName = null;
    String mbAcc = null;
    String repayFrequencies = null;
    String interestTypes = null;
    String[] aliasAccDetails = null;
    String[] interestTypesArray = null;
    String[] freqArray = null;
    String[] interestFreqArray = null;
    String interestType = null;
    String interestCode = null;
    String intDesc = null;
    String intCode = null;

    String loanProduct = null;
    String principalFreqDesc = null;
    String principalFreqCode = null;
    String loanProdCode = null;

    String loanSchedule = null;
    String interest = null;

    String loanProd = null;
    String loanCode = null;
    String freqDesc = null;
    String freqCode = null;
    String intFreqDesc = null;
    String intFreqCode = null;

    ProgressDialog progress;
    ProgressDialog progress2;
    int pos;
    String aliasAccNum = null;
    String aliasName = null;
    String transAmt = null;

    int[] next = new int[]{R.drawable.more};
    List<HashMap<String, String>> aList;
    View view;

    String accnumText;

    String response = null;
    String balFromFile = null;
    String beneFromFile = null;
    String tenureInMonths = null;

    String usrName = null;
    String mbAccDetails = null;
    String token = null;
    String usrId = null;
    String beneInternal = null;
    String beneExternal = null;
    String custType = null;
    String balances = null;
    String codeFlag = null;
    String questions = null;

    static boolean _areLecturesLoaded = false;
//    Handler handler;
//    Runnable r;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        token = getActivity().getIntent().getExtras().getString("token").trim();

//        pattern = Pattern.compile(regex);

        System.out.println("token: " + token);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.loan_quotation, container, false);


//        getloanproducts
//        getinteresttype
//        getfreq

        loanProdSpinner = (Spinner) view.findViewById(R.id.spinner1);
        interestTypeSpinner = (Spinner) view.findViewById(R.id.spinner2);
        principalRepayFreqSpinner = (Spinner) view.findViewById(R.id.prin_repay_freq_spinner);
        interestRepayFreqSpinner = (Spinner) view.findViewById(R.id.int_repay_freq_spinner);
        amtEditText = (EditText) view.findViewById(R.id.amtedittext);
        tenureInMonthEditText = (EditText) view.findViewById(R.id.tenure_edittext);
        confirmButton = (Button) view.findViewById(R.id.confirm_button);

//        ConnectionDetector cd = new ConnectionDetector(getActivity());
//        Boolean isInternetPresent = cd.isConnectingToInternet();
//        if (isInternetPresent) {
//            new myProductsFireMan().execute();
//        } else {
//
//            showAlertDialog(getActivity(), "No Internet Connection",
//                    "You don't have internet connection", false);
//
//        }



        loanProdSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                loanCode = ((TextView) view.findViewById(R.id.code)).getText().toString().trim();
                loanProd = ((TextView) view.findViewById(R.id.desc)).getText().toString().trim();

            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });

        interestTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                intDesc = ((TextView) view.findViewById(R.id.desc)).getText().toString().trim();
                intCode = ((TextView) view.findViewById(R.id.code)).getText().toString().trim();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });


        principalRepayFreqSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                freqCode = ((TextView) view.findViewById(R.id.code)).getText().toString().trim();
                freqDesc = ((TextView) view.findViewById(R.id.desc)).getText().toString().trim();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });


        interestRepayFreqSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                intFreqCode = ((TextView) view.findViewById(R.id.code)).getText().toString().trim();
                intFreqDesc = ((TextView) view.findViewById(R.id.desc)).getText().toString().trim();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing


            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                transAmt = amtEditText.getText().toString();
                tenureInMonths = tenureInMonthEditText.getText().toString();

                if (transAmt.trim().isEmpty()) {
                    amtEditText.requestFocus();
                    amtEditText.setError("Enter loan amount");
                } else if (tenureInMonths.trim().isEmpty()) {
                    tenureInMonthEditText.requestFocus();
                    tenureInMonthEditText.setError("Enter tenure(months)");
                } else {

                    new loanQuotationFireMan().execute();

//                    Intent confirmTransIntent = new Intent(LoanQuotation.this, ConfirmTrans.class);
//                    System.out.println(accountNum);
//                    System.out.println(aliasAccNum);
//                    System.out.println(curr);
//                    confirmTransIntent.putExtra("curr", curr);
//                    confirmTransIntent.putExtra("dbAcc", accountNum);
//                    confirmTransIntent.putExtra("crAcc", aliasAccNum);
//                    confirmTransIntent.putExtra("crAccName", aliasName);
//                    confirmTransIntent.putExtra("amt", transAmt);
//                    confirmTransIntent.putExtra("narration", narration.toUpperCase());
//                    confirmTransIntent.putExtra("token", token);
//                    confirmTransIntent.putExtra("userName", usrName);
//                    confirmTransIntent.putExtra("userId", usrId);
//                    confirmTransIntent.putExtra("mb_acc_details", custType);
//                    confirmTransIntent.putExtra("balances", balances);
//                    confirmTransIntent.putExtra("creationTime", creationTime);
//                    confirmTransIntent.putExtra("beneInternal", beneInternal);
//                    confirmTransIntent.putExtra("beneExternal", beneExternal);
//                    confirmTransIntent.putExtra("questions", questions);
//                    confirmTransIntent.putExtra("codeFlag", codeFlag);
//                    startActivity(confirmTransIntent);
                }

            }
        });



        return view;

    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !_areLecturesLoaded ) {
            ConnectionDetector cd = new ConnectionDetector(getActivity());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new myProductsFireMan().execute();
                _areLecturesLoaded = true;
            } else {

                showAlertDialog(getActivity(), "No Internet Connection",
                        "You don't have internet connection", false);

            }
        }
    }



    private String networkIP() {

        boolean WIFI = false;
        boolean MOBILE = false;
        String IPaddress = null;

        ConnectivityManager CM = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected())
                    WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected())
                    MOBILE = true;
        }
        if (WIFI) {
            IPaddress = GetDeviceipWiFiData();
//			textview.setText(IPaddress);
        }
        if (MOBILE) {
            IPaddress = GetDeviceipMobileData();
//			textview.setText(IPaddress);
        }
        return IPaddress;
    }


    public String productsJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myProductsResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = productsJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getloanproducts", json);

        System.out.println(response);

        return response;
    }


    public class myProductsFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress2 = new ProgressDialog(getActivity());
            progress2.setCancelable(false);
            progress2.setTitle("Please wait");
            progress2.setMessage("Retrieving loan products...");
            progress2.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myProductsResponse(token);
//				System.out.println(response);
                try {
                    if (!response.isEmpty()) {
                        JSONObject jsonObject = new JSONObject(response);
                        mb_response = jsonObject.getString("mb_response");
                        mbAcc = jsonObject.getString("mb_accDetails");

                        System.out.println("mb_response: " + mb_response);
                        System.out.println("mb_accDetails: " + mbAcc);

                        System.out.println(mbAcc);
                    } else {
                        mb_response = "66";
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress2.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();
                    getActivity().finish();
                } else if (mb_response.trim().equals("66")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("99")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error", mbAcc).showDialog();
                } else if (mb_response.trim().equals("11")) {
                    progress2.cancel();
                    new ResponseDialog(getActivity(), "Error !", "Unknown Error").showDialog();
                    confirmButton.setEnabled(false);
                } else if (mb_response.trim().equals("00")) {
                    progress2.cancel();

                    System.out.println(mbAcc);

                    aliasAccDetails = mbAcc.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < aliasAccDetails.length - 1; a++) {
                        StringTokenizer myAccToken = new StringTokenizer(aliasAccDetails[a], "~");
                        loanProduct = myAccToken.nextToken().trim();
                        loanProdCode = myAccToken.nextToken().trim();


                        System.out.println(loanProduct);
                        System.out.println(loanProdCode);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("desc", loanProduct);
                        hm.put("code", loanProdCode);


                        theList.add(hm);

                    }

                    String[] from = {"desc", "code"};
                    int[] to = {R.id.desc, R.id.code};

                    SimpleAdapter adapter = new SimpleAdapter(getActivity(), theList, R.layout.list_loan_quotation, from, to);

                    loanProdSpinner.setAdapter(adapter);


                }

            }
            ConnectionDetector cd = new ConnectionDetector(getActivity());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                new interestTypesFireMan().execute();
            } else {
                showAlertDialog(getActivity(), "No Internet Connection",
                        "You don't have internet connection", false);
            }

        }

    }



    public String getInterestJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myInterestResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getInterestJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getinteresttype", json);

        System.out.println(response);

        return response;
    }


    public class interestTypesFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving interest types...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myInterestResponse(token);

                try {

                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        mb_response = jsonObject.getString("mb_response");
                        interestTypes = jsonObject.getString("mb_accDetails");

                        System.out.println(interestTypes);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "Please create a beneficiary to transfer funds").showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    interestTypesArray = interestTypes.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    System.out.println(aliasAccDetails.length);

                    for (int a = 0; a < interestTypesArray.length; a++) {
                        StringTokenizer accDetToken = new StringTokenizer(interestTypesArray[a], "~");

                        System.out.println(accDetToken.countTokens());

                        interestType = accDetToken.nextToken().trim();
                        interestCode = accDetToken.nextToken().trim();

                        System.out.println(interestType);
                        System.out.println(interestCode);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("interestType", interestType);
                        hm.put("interestCode", interestCode);

                        theList.add(hm);

                    }

                    String[] from = {"interestType", "interestCode"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.desc, R.id.code};

                    SimpleAdapter adapter2 = new SimpleAdapter(getActivity(), theList, R.layout.list_loan_interest, from, to);

                    interestTypeSpinner.setAdapter(adapter2);


                }

                ConnectionDetector cd = new ConnectionDetector(getActivity());
                Boolean isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    new frequencyFireMan().execute();
                } else {
                    showAlertDialog(getActivity(), "No Internet Connection",
                            "You don't have internet connection", false);
                }

            }


        }

    }




    public String getFreqJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public String myFreqResponse(String a1) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getFreqJSON(a1);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "getfreq", json);

        System.out.println(response);

        return response;
    }


    public class frequencyFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Retrieving repay frequencies...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                response = myFreqResponse(token);

                try {

                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        mb_response = jsonObject.getString("mb_response");
                        repayFrequencies = jsonObject.getString("mb_accDetails");

                        System.out.println(repayFrequencies);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "Please create a beneficiary to transfer funds").showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    freqArray = repayFrequencies.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    for (int a = 0; a < freqArray.length; a++) {
                        StringTokenizer accDetToken = new StringTokenizer(freqArray[a], "~");

                        System.out.println(accDetToken.countTokens());

                        principalFreqDesc = accDetToken.nextToken().trim();
                        principalFreqCode = accDetToken.nextToken().trim();

                        System.out.println(principalFreqDesc);
                        System.out.println(principalFreqCode);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("principalFreqDesc", principalFreqDesc);
                        hm.put("principalFreqCode", principalFreqCode);

                        theList.add(hm);

                    }

                    String[] from = {"principalFreqDesc", "principalFreqCode"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.desc, R.id.code};

                    SimpleAdapter adapter2 = new SimpleAdapter(getActivity(), theList, R.layout.list_repay_freq, from, to);

                    principalRepayFreqSpinner.setAdapter(adapter2);


                }

                populateInterestRepayFreq();

            }


        }

    }


    public void populateInterestRepayFreq(){

        interestFreqArray = repayFrequencies.split("`");

        List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

        for (int a = 0; a < interestFreqArray.length; a++) {
            StringTokenizer accDetToken = new StringTokenizer(interestFreqArray[a], "~");

            System.out.println(accDetToken.countTokens());

            principalFreqDesc = accDetToken.nextToken().trim();
            principalFreqCode = accDetToken.nextToken().trim();

            System.out.println(principalFreqDesc);
            System.out.println(principalFreqCode);

            HashMap<String, String> hm = new HashMap<String, String>();

            hm.put("freqDesc", principalFreqDesc);
            hm.put("freqCode", principalFreqCode);

            theList.add(hm);

        }

        String[] from = {"freqDesc", "freqCode"};

        // Ids of views in listview_layout
        int[] to = {R.id.desc, R.id.code};

        SimpleAdapter adapter2 = new SimpleAdapter(getActivity(), theList, R.layout.list_interest_repay_freq, from, to);

        interestRepayFreqSpinner.setAdapter(adapter2);



    }



    public String loanQuotationJSON(String loancode, String transamt, String tenure, String principalFrequency, String interestFreq, String interest,String authToken) throws UnsupportedEncodingException {
        return "loanproduct=" + loancode +
                "&amount=" + transamt +
                "&tenure=" + tenure +
                "&pfreq=" + principalFrequency +
                "&ifreq=" + interestFreq +
                "&itype=" + interest +
                "&authToken=" + authToken +
                "&device_ip=" + URLEncoder.encode(networkIP(), "UTF-8");
    }


    public String myLoanResponse(String a1, String a2, String a3, String a4, String a5, String a6, String a7 ) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = loanQuotationJSON(a1, a2, a3, a4, a5, a6, a7);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "loanquotation", json);

        System.out.println(response);

        return response;
    }


    public class loanQuotationFireMan extends AsyncTask<Void, Void, Void> {

        private Exception e = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setCancelable(false);
            progress.setTitle("Please wait");
            progress.setMessage("Sending request...");
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {


            try {
                System.out.println("loancode: " + loanCode);
                System.out.println(transAmt);
                System.out.println(tenureInMonths);
                System.out.println(freqCode);
                System.out.println(intFreqCode);
                System.out.println(intCode);
                response = myLoanResponse(loanCode, transAmt, tenureInMonths, freqCode, intFreqCode, intCode, token);

                try {

                    if (!response.isEmpty()) {

                        JSONObject jsonObject = new JSONObject(response);
                        mb_response = jsonObject.getString("mb_response");
                        loanSchedule = jsonObject.getString("mb_accDetails");
                        mb_token = jsonObject.getString("mb_authToken");
                        interest = jsonObject.getString("mb_interest");

                        System.out.println("loanschdule: " + loanSchedule);
                        System.out.println("interest: " + interest);

                    } else {
                        mb_response = "66";
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub

            if (e != null) {
                progress.cancel();
                //				new ResponseDialog(MyAccounts.this, "We found an error!", e.getMessage()).showDialog();
            } else {
                super.onPostExecute(result);

                if (mb_response.trim().equals("12")) {
                    System.out.println("Received response code > " + mb_response);

                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Data Found For This User", "Please login again").showDialog();

                } else if (mb_response.trim().equals("66")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No response", "Please try again").showDialog();
                } else if (mb_response.trim().equals("19")) {
                    progress.cancel();
                    new ResponseDialog(getActivity(), "No Beneficiary Found", "Please create a beneficiary to transfer funds").showDialog();

                } else if (mb_response.trim().equals("00")) {
                    progress.cancel();

                    /*freqArray = repayFrequencies.split("`");

                    List<HashMap<String, String>> theList = new ArrayList<HashMap<String, String>>();

                    for (int a = 0; a < freqArray.length; a++) {
                        StringTokenizer accDetToken = new StringTokenizer(freqArray[a], "~");

                        System.out.println(accDetToken.countTokens());

                        principalFreqDesc = accDetToken.nextToken().trim();
                        principalFreqCode = accDetToken.nextToken().trim();

                        System.out.println(principalFreqDesc);
                        System.out.println(principalFreqCode);

                        HashMap<String, String> hm = new HashMap<String, String>();

                        hm.put("principalFreqDesc", principalFreqDesc);
                        hm.put("principalFreqCode", principalFreqCode);

                        theList.add(hm);

                    }

                    String[] from = {"principalFreqDesc", "principalFreqCode"};

                    // Ids of views in listview_layout
                    int[] to = {R.id.desc, R.id.code};

                    SimpleAdapter adapter2 = new SimpleAdapter(getActivity(), theList, R.layout.list_repay_freq, from, to);

                    principalRepayFreqSpinner.setAdapter(adapter2);

*/
                }


            }


        }

    }








    public class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    getActivity().finish();
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }

//	@Override
//	public void onUserInteraction() {
//		super.onUserInteraction();
//		MainMenu menuObject = new MainMenu();
//		menuObject.countDownTimer.cancel();
//		menuObject.countDownTimer.start();
//	}


    //	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		menu.findIte
//		+m(R.id.refresh).setVisible(false);
//		return true;
//	}
//	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //	         NavUtils.navigateUpFromSameTask(this);S
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onBackPressed() {
        //		moveTaskToBack(true);
        getActivity().finish();
    }


}

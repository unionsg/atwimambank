package com.unionsg.blink;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.io.File;


/**
 * Created by Chris A on 19/04/2016.
 */
public class CameraActivity extends AppCompatActivity {

    //    private Handler handler = new Handler();
    private Camera mCamera;
    private CameraPreview mPreview;

    private byte[] picResults = null;
    String token = null;
    int cameraFlag = 0;
    ImageView capturedImageHolder;
    Dialog dialog;
    Dialog dialog2;

    Bitmap photo1;
    Bitmap photo2;

    Button captureButton;
    //    ImageView imageView;
    ImageView imageView;
    Button saveButton;
    Button cancelButton;
    byte[] data1;
    byte[] data2;
    int myCameraFlag = 0;

    Runnable r;

    File file; // the File to save , append increasing numeric counter to prevent files from getting overwritten.

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.framelayout);
        token = getIntent().getExtras().getString("token").trim();
        cameraFlag = getIntent().getExtras().getInt("cameraFlag");
        myCameraFlag = cameraFlag;
        System.out.println("my camera flag is " + myCameraFlag);
        captureButton = (Button) findViewById(R.id.button_capture);
        capturedImageHolder = (ImageView) findViewById(R.id.imageView);


        if (checkCameraHardware(getApplicationContext())) {

            System.out.println("phone has a camera");
            // Create an instance of Camera
            mCamera = getCameraInstance();


            // Create our Preview view and set it as the content of our activity.
//            mPreview = new CameraPreview(getApplicationContext(), mCamera);
            mPreview = new CameraPreview(getApplicationContext(), mCamera);

            //FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview1);
            //preview.addView(mPreview);

            FrameLayout previewLarger = (FrameLayout) findViewById(R.id.camera_preview1);

            previewLarger.addView(mPreview);
//            Box box = new Box(this);
//            addContentView(box, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        } else {
            System.out.println("phone does not have a camera");
        }


        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                        mCamera.takePicture(null, null, mPicture);

                    }
                }
        );
        //call in onCreate
        //  setAppIdleTimeout();
    }


    private Camera.PictureCallback mPicture = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            if (myCameraFlag == 0) {
//                    ChequeDeposit.picByte = data;
                data1 = data;
                photo1 = BitmapFactory.decodeByteArray(data, 0, data.length);


                System.out.println("data is " + data1);
//                    Toast.makeText(getApplication(), "the image data is good in pic place", Toast.LENGTH_SHORT).show();
                dialog = new Dialog(CameraActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pic_dialog);
                dialog.setCancelable(true);
                imageView = (ImageView) dialog.findViewById(R.id.picPreview);
                cancelButton = (Button) dialog.findViewById(R.id.cancel_button);
                saveButton = (Button) dialog.findViewById(R.id.save_button);

                imageView.setImageBitmap(photo1);
//                    cropImage();
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }

                });
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChequeDeposit.picByte = data1;
                        dialog.cancel();
//                            cropImage();
                        Intent intent = new Intent(CameraActivity.this, ChequeDeposit.class);
                        intent.putExtra("cameraFlag", 1);
                        intent.putExtra("token", token);
                        setResult(RESULT_OK, intent);
                        startActivity(intent);
                        CameraActivity.this.finish();
                    }

                });


                dialog.show();

            } else if (myCameraFlag == 1) {
//                    ChequeDeposit.picByte2 = data;

                data2 = data;
                photo2 = BitmapFactory.decodeByteArray(data2, 0, data2.length);
                System.out.println("data is " + data2);
//                    Toast.makeText(getApplication(), "the image data is good in pic place", Toast.LENGTH_SHORT).show();
                dialog = new Dialog(CameraActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pic_dialog);
                dialog.setCancelable(true);
                imageView = (ImageView) dialog.findViewById(R.id.picPreview);
                cancelButton = (Button) dialog.findViewById(R.id.cancel_button);
                saveButton = (Button) dialog.findViewById(R.id.save_button);
                imageView.setImageBitmap(photo2);
                cancelButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }

                });
                saveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ChequeDeposit.picByte2 = data2;
                        dialog.cancel();
                        Intent intent = new Intent(CameraActivity.this, ChequeDeposit.class);
//                intent.putExtra("data", byteArray);
                        intent.putExtra("cameraFlag", 2);
                        intent.putExtra("token", token);
                        setResult(RESULT_OK, intent);
                        startActivity(intent);
                        CameraActivity.this.finish();
                    }

                });
                dialog.show();

            }
        }
    };


    private Bitmap scaleDownBitmapImage(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
        return resizedBitmap;
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(0); // attempt to get a Camera instance
            System.out.println("app opened phone camera");
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
            System.out.println("app could not open phone camera");
        }
        return c; // returns null if camera is unavailable
    }

    /**
     * Check if this device has a camera
     */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //releaseMediaRecorder();       // if you are using MediaRecorder, release it first
        releaseCamera();              // release the camera immediately on pause event
    }

    /*
    private void releaseMediaRecorder(){
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }
    */
    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

//    @Override
//    public void onUserInteraction() {
//        super.onUserInteraction();
//        MainMenu menuObject = new MainMenu();
//        menuObject.countDownTimer.cancel();
//        menuObject.countDownTimer.start();
//    }

}

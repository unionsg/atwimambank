package com.unionsg.imageUtils;

import android.graphics.Bitmap;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by pato7 on 3/17/2016.
 */
public class GeneralUtil {

    public String convertBitmapToBytes (Bitmap photo){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        boolean converted = photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);

        if(converted){
            byte[] bArray = bos.toByteArray();

            return bArray.toString();
        }else{
            return null;
        }

    }


    public String confirmJSON(String userID,String pin, String accNum,String chqNum, String amt,String image1,String image2){
        return "authToken="+userID+
                "&pin="+pin+
                "&accNum="+accNum+
                "&chqNume="+chqNum+
                "&amt="+amt+
                "&image1="+image1+
                "&image2="+image2;
    }

    public String sendDataToWebService(String url, String dataParams)  {

        // Create connection
        URL Real_url = null;
        try {
            Real_url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection)Real_url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }

        //now set connection properties
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Accept", "application/json");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);


        // Send request
        DataOutputStream wr = null;
        try {
            wr = new DataOutputStream(connection.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            wr.writeBytes(dataParams);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
        wr.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
        wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Get Response
        InputStream is = null;
        try {
         is = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer();
        try {
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
        rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String responseStr = null;
        responseStr = response.toString();

        return responseStr;
    }
}

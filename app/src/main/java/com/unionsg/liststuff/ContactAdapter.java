package com.unionsg.liststuff;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.unionsg.blink.R;

import java.util.ArrayList;
import java.util.List;


public class ContactAdapter extends ArrayAdapter<String> {

    private Context context;
//    private ArrayList<Contact> itemArrayList;
    private Context mContext;
    private int id;
    private List <String>items ;

    public ContactAdapter(Context context, int textViewResourceId , List<String> list) {

        super(context, textViewResourceId, list);
        this.mContext = context;
        id = textViewResourceId;
        items = list ;
    }

//    private class ItemViewHolder extends RecyclerView.ViewHolder {
//
//        ImageView textDrawable;
//        TextView textDrawableTitle;
//        TextView name;
//        TextView number;
//
//        private ItemViewHolder(View itemView) {
//            super(itemView);
//            this.textDrawable = (ImageView) itemView.findViewById(R.id.text_drawable);
//            this.textDrawableTitle = (TextView) itemView.findViewById(R.id.text_drawable_title);
////            this.name = (TextView) itemView.findViewById(co.farmerline.cocoalink.R.id.name);
//            this.name = (TextView) itemView.findViewById(R.id.name);
//            this.number = (TextView) itemView.findViewById(R.id.number);
//        }
//    }

    @Override
    public View getView(int position, View v, ViewGroup parent)
    {
        View mView = v ;
        if(mView == null){
            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = vi.inflate(id, null);
        }

        TextView text = (TextView) mView.findViewById(R.id.first_letter);

        Drawable circle = context.getResources().getDrawable(R.drawable.circle_c4);
        circle.setColorFilter(getRandomMaterialColor(), PorterDuff.Mode.SRC_ATOP);
        text.setBackground(circle);

        if(items.get(position) != null )
        {
            text.setTextColor(Color.WHITE);
            text.setText(items.get(position));
            text.setBackgroundColor(Color.RED);
            int color = Color.argb( 200, 255, 64, 64 );
            text.setBackgroundColor( color );

        }

        return mView;
    }



//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_content_item_layout, parent, false);
//        return new ItemViewHolder(v);
//    }
//
//    private Contact getItem(int position) {
//        return itemArrayList.get(position);
//    }
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//
//        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
//        final Contact contact = getItem(position);
//
//        String chip;
//        if(!contact.getName().isEmpty()){
//            chip = contact.getName().substring(0, 1);
//        }else{
//            chip = "?";
//        }
//
//        Drawable circle = context.getResources().getDrawable(R.drawable.circle_c4);
//        circle.setColorFilter(getRandomMaterialColor(), PorterDuff.Mode.SRC_ATOP);
//        itemViewHolder.textDrawable.setImageDrawable(circle);
//
//        itemViewHolder.textDrawableTitle.setText(chip);
//        itemViewHolder.name.setText(contact.getName());
//        itemViewHolder.number.setText(contact.getNumber());
//    }
//
//    @Override
//    public int getItemCount() {
//        return itemArrayList.size();
//    }

    private int getRandomMaterialColor() {
        int returnColor = Color.GRAY;
        int arrayId = context.getResources().getIdentifier("list_item_colors", "array", context.getPackageName());

        if (arrayId != 0) {
            TypedArray colors = context.getResources().obtainTypedArray(arrayId);
            int index = (int) (Math.random() * colors.length());
            returnColor = colors.getColor(index, Color.GRAY);
            colors.recycle();
        }
        return returnColor;
    }
}

package com.unionsg.liststuff;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.blink.R;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class BeneficiaryExtAdapter extends SimpleAdapter{

	TextView beneAccTV;
	TextView beneNameTV;
	TextView beneBankTV;
	TextView firstLetterTV;
	ImageView textBgImageView;
	//	TextView balLedger;
	//	TextView acc_name;
	//	TextView acc_num;

	public BeneficiaryExtAdapter(Context context, List<HashMap<String, String>> items, int resource, String[] from, int[] to) {
		super(context, items, resource, from, to);
	}

	//	Activity d = new Activity();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		textBgImageView = (ImageView) view.findViewById(R.id.text_drawable);
		beneAccTV = (TextView) view.findViewById(R.id.bene_acc);
		beneBankTV = (TextView) view.findViewById(R.id.bene_bank);
		beneNameTV = (TextView) view.findViewById(R.id.bene_name);
		firstLetterTV = (TextView) view.findViewById(R.id.first_letter);
		//		ledgerBal = (TextView) view.findViewById(R.id.ledger_bal);
		//		acc_name = (TextView) view.findViewById(R.id.acc_name);
		//		acc_num = (TextView) view.findViewById(R.id.acc_num);

		//		Typeface thinText = Typeface.createFromAsset(currencyTextView.getContext().getAssets(), "HelveticaThin.otf");
		//		Typeface lightText = Typeface.createFromAsset(d.getAssets(), "HelveticaLight.otf");
		Typeface lato = Typeface.createFromAsset(firstLetterTV.getContext().getAssets(), "Lato-Regular.ttf");
		//		Typeface mediumText = Typeface.createFromAsset(sellTextView.getContext().getAssets(), "HelveticaMedium.otf");


		//      int colorPos = position % colors.length;
		//      view.setBackgroundColor(colors[colorPos]);

		beneNameTV.setTypeface(lato);
		firstLetterTV.setTypeface(lato);
		beneAccTV.setTypeface(lato);
		beneBankTV.setTypeface(lato);

		Random r = new Random();
		int red=r.nextInt(255 + 1);
		int green=r.nextInt(255 + 1);
		int blue=r.nextInt(255 + 1);

		GradientDrawable draw = new GradientDrawable();
		draw.setShape(GradientDrawable.OVAL);
		draw.setColor(Color.rgb(red,green,blue));
		textBgImageView.setBackground(draw);


//
//		if(amount.contains("-")){
//			amtTextView.setTextColor(Color.RED);
//			pic.setImageResource(R.drawable.out);
//		}else {amtTextView.setTextColor(amtTextView.getContext().getResources().getColor(R.color.positive_bal));
//		//		pic.setBackgroundResource(R.drawable.in);
//		}

		return view;
	}


//	private int getRandomMaterialColor(Context context) {
//		int returnColor = Color.GRAY;
//		int arrayId = context.getIdentifier("list_item_colors", "array", getPackageName());
//
//		if (arrayId != 0) {
//			TypedArray colors = BeneficiaryAdapter.this.getResources().obtainTypedArray(arrayId);
//			int index = (int) (Math.random() * colors.length());
//			returnColor = colors.getColor(index, Color.GRAY);
//			colors.recycle();
//		}
//		return returnColor;
//	}
}

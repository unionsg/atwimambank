package com.unionsg.liststuff;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.blink.R;

import java.util.HashMap;
import java.util.List;

public class FxRatesAdapter extends SimpleAdapter{

	TextView currencyTextView;
	TextView buyTextView;
	TextView sellTextView;
	ImageView pic;
	//	TextView balLedger;
	//	TextView acc_name;
	//	TextView acc_num;

	public FxRatesAdapter(Context context, List<HashMap<String, String>> items, int resource, String[] from, int[] to) {
		super(context, items, resource, from, to);
	}

	//	Activity d = new Activity();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		currencyTextView = (TextView) view.findViewById(R.id.cur);
		buyTextView = (TextView) view.findViewById(R.id.buy);
		sellTextView = (TextView) view.findViewById(R.id.sell);
		pic = (ImageView) view.findViewById(R.id.flag);
		//		ledgerBal = (TextView) view.findViewById(R.id.ledger_bal);
		//		acc_name = (TextView) view.findViewById(R.id.acc_name);
		//		acc_num = (TextView) view.findViewById(R.id.acc_num);

		//		Typeface thinText = Typeface.createFromAsset(currencyTextView.getContext().getAssets(), "HelveticaThin.otf");
//				Typeface lightText = Typeface.createFromAsset(d.getAssets(), "HelveticaLight.otf");
		Typeface regularText = Typeface.createFromAsset(buyTextView.getContext().getAssets(), "HelveticaRegular.otf");
//		Typeface mediumText = Typeface.createFromAsset(sellTextView.getContext().getAssets(), "HelveticaMedium.otf");


		//      int colorPos = position % colors.length;
		//      view.setBackgroundColor(colors[colorPos]);

		currencyTextView.setTypeface(regularText);
		buyTextView.setTypeface(regularText);
		sellTextView.setTypeface(regularText);

		String curr = (String) currencyTextView.getText();
//
		if(curr.contains("USD")){
			pic.setImageResource(R.drawable.ic_usd);
		}else if(curr.contains("EUR")) {
			pic.setImageResource(R.drawable.ic_euro);
		}else if(curr.contains("GBP")) {
			pic.setImageResource(R.drawable.ic_gbp);
		}else if(curr.contains("JPY")) {
			pic.setImageResource(R.drawable.ic_jpn);
		}else if(curr.contains("CNY")) {
			pic.setImageResource(R.drawable.ic_cny);
		}else if(curr.contains("SLL")) {
			pic.setImageResource(R.drawable.ic_sll);
		}

		return view;
	}
}

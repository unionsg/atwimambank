package com.unionsg.liststuff;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.unionsg.blink.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class TransDetailsAdapter extends SimpleAdapter{

	TextView summaryTextView;
	TextView transDateTextView;
	TextView amtTextView;
	ImageView pic;
	//	TextView balLedger;
	//	TextView acc_name;
	//	TextView acc_num;

	public TransDetailsAdapter(Context context, List<HashMap<String, String>> items, int resource, String[] from, int[] to) {
		super(context, items, resource, from, to);
	}

	//	Activity d = new Activity();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		summaryTextView = (TextView) view.findViewById(R.id.summary);
		transDateTextView = (TextView) view.findViewById(R.id.trans_date);
		amtTextView = (TextView) view.findViewById(R.id.amt);
		pic = (ImageView) view.findViewById(R.id.pic);
		//		ledgerBal = (TextView) view.findViewById(R.id.ledger_bal);
		//		acc_name = (TextView) view.findViewById(R.id.acc_name);
		//		acc_num = (TextView) view.findViewById(R.id.acc_num);

		//		Typeface thinText = Typeface.createFromAsset(currencyTextView.getContext().getAssets(), "HelveticaThin.otf");
		//		Typeface lightText = Typeface.createFromAsset(d.getAssets(), "HelveticaLight.otf");
		Typeface regularText = Typeface.createFromAsset(amtTextView.getContext().getAssets(), "HelveticaRegular.otf");
		//		Typeface mediumText = Typeface.createFromAsset(sellTextView.getContext().getAssets(), "HelveticaMedium.otf");


		//      int colorPos = position % colors.length;
		//      view.setBackgroundColor(colors[colorPos]);

		transDateTextView.setTypeface(regularText);
		summaryTextView.setTypeface(regularText);
		amtTextView.setTypeface(regularText);
		//		ledgerBal.setTypeface(regularText);
		//		balAvail.setTypeface(regularText);
		//		balLedger.setTypeface(regularText);
		//      acc_num.setTextColor(Color.RED);
		//      String acc_num = (String) ((TextView) view.findViewById(R.id.acc_num)).getText();
		//		ledgerBal.setTextColor(availBal.getContext().getResources().getColor(R.color.current_bal));

		String amount = (String) amtTextView.getText();

		if(amount.contains("-")){
			amtTextView.setTextColor(Color.RED);
			pic.setImageResource(R.drawable.out);
		}else {amtTextView.setTextColor(amtTextView.getContext().getResources().getColor(R.color.positive_bal));
		//		pic.setBackgroundResource(R.drawable.in);
		}

		return view;
	}
}

package com.unionsg.liststuff;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.unionsg.blink.R;

import java.util.HashMap;
import java.util.List;

public class MyAccountsAdapter extends SimpleAdapter {

	TextView availBal;
	TextView ledgerBal;
	TextView balAvail;
	TextView balLedger;
	TextView acc_name;
	TextView acc_num;
	TextView currency;
//	ArrayList<> colors;
	int[] colors = new int[]{R.color.negative_bal, R.color.slcb_blue, R.color.slcb_red, R.color.light_blue,R.color.cream};
	ImageView colorImage;
	
	public MyAccountsAdapter(Context context, List<HashMap<String, String>> items, int resource, String[] from, int[] to) {
		super(context, items, resource, from, to);
	}

//	Activity d = new Activity();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		availBal = (TextView) view.findViewById(R.id.avail_bal);
		balAvail = (TextView) view.findViewById(R.id.bal_avail);
		balLedger = (TextView) view.findViewById(R.id.bal_ledger);
		ledgerBal = (TextView) view.findViewById(R.id.ledger_bal);
		acc_name = (TextView) view.findViewById(R.id.acc_name);
		acc_num = (TextView) view.findViewById(R.id.acc_num);
		currency = (TextView) view.findViewById(R.id.curr);
//		colorImage = (ImageView) view.findViewById(R.id.my_color);

//		colors = new ArrayList<>();
//		colors.add(ResourcesCompat.getColor(colorImage.getResources(), R.color.negative_bal, null));
//		colors.add(ResourcesCompat.getColor(colorImage.getResources(), R.color.slcb_blue, null));
//		colors.add(ResourcesCompat.getColor(colorImage.getResources(), R.color.slcb_red, null));
//		colors.add(ResourcesCompat.getColor(colorImage.getResources(), R.color.light_blue, null));
//		colors.add(ResourcesCompat.getColor(colorImage.getResources(), R.color.cream, null));

//		Typeface thinText = Typeface.createFromAsset(availBal.getContext().getAssets(), "HelveticaThin.otf");
		//		Typeface lightText = Typeface.createFromAsset(d.getAssets(), "HelveticaLight.otf");
		Typeface regularText = Typeface.createFromAsset(availBal.getContext().getAssets(), "HelveticaRegular.otf");
		Typeface mediumText = Typeface.createFromAsset(acc_name.getContext().getAssets(), "HelveticaMedium.otf");

		acc_name.setTypeface(mediumText);
		acc_num.setTypeface(regularText);
		availBal.setTypeface(regularText);
		ledgerBal.setTypeface(regularText);
		balAvail.setTypeface(regularText);
		balLedger.setTypeface(regularText);


//		for (int a = 0;a < colors.length; a++ ){
//			System.out.println("CHOOSE COLOR");
//			colorImage.setBackgroundResource(colors[a]);
//		}
		//      acc_num.setTextColor(Color.RED);
		//      String acc_num = (String) ((TextView) view.findViewById(R.id.acc_num)).getText();
//		ledgerBal.setTextColor(availBal.getContext().getResources().getColor(R.color.current_bal));

//		String availableBal = (String) availBal.getText();
//		String ledgerBalance = (String) ledgerBal.getText();
		
//		String availableBal.substring(0, 2);
		
//		if(ledgerBalance.contains("-")){
//			ledgerBal.setTextColor(Color.RED);
//		}else{ ledgerBal.setTextColor(ledgerBal.getContext().getResources().getColor(R.color.positive_bal));
//			/*String curr = availableBal.substring(0, 3);
//			System.out.println(curr);
//			currency.setText(curr);
//			String amt = availableBal.substring(3);
//			StringTokenizer token = new StringTokenizer(amt, ".");
//			token.nextToken();
//			String decimal = token.nextToken();
//			System.out.println(decimal);*/
////			String decimal = availableBal.replaceAll(regularExpression, replacement)
////			availableBal.
//		}

		
//		if(availableBal.contains("-")){
//			availBal.setTextColor(Color.RED);
//		}else availBal.setTextColor(availBal.getContext().getResources().getColor(R.color.positive_bal));

		return view;
	}
}
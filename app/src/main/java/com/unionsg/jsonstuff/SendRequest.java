package com.unionsg.jsonstuff;


import android.content.Context;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class SendRequest {


    public static final MediaType urlEncoded = MediaType.parse("application/x-www-form-urlencoded");
    OkHttpClient client = new OkHttpClient();
    //	client.setConnectTimeout(20, TimeUnit.SECONDS);
    Context context;


    public String postReq(String url, String json) throws IOException {
        try {
            client.setConnectTimeout(30, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);
            RequestBody body = RequestBody.create(urlEncoded, json);
            Request request = new Request.Builder().url(url).post(body).build();
            Response response = client.newCall(request).execute();

            return response.body().string();

        } catch (Exception e) {
            System.out.println("SEND REQUEST EXCEPTION: " + e.getMessage());
//            GlobalCodes.showAlertDialog(context, "Error!", "No response from server. Please check your Internet connection and try again", false);
//		Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
        return "";

    }



    public String getReq(String url) throws IOException {
        try {
            client.setConnectTimeout(30, TimeUnit.SECONDS);
            client.setReadTimeout(30, TimeUnit.SECONDS);
            RequestBody body = RequestBody.create(urlEncoded, "");
            Request request = new Request.Builder().url(url).get().build();
            Response response = client.newCall(request).execute();

            return response.body().string();

        } catch (Exception e) {
            System.out.println("SEND REQUEST EXCEPTION: " + e.getMessage());
//            GlobalCodes.showAlertDialog(context, "Error!", "No response from server. Please check your Internet connection and try again", false);
//		Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
        return "";

    }



    public String postReqCheque(String url, String json, long time) throws IOException {
        try {
            client.setConnectTimeout(time, TimeUnit.SECONDS);
            client.setReadTimeout(time, TimeUnit.SECONDS);
            RequestBody body = RequestBody.create(urlEncoded, json);
            Request request = new Request.Builder().url(url).post(body).build();
            Response response = client.newCall(request).execute();

            return response.body().string();

        } catch (Exception e) {
//		Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
        return json;

    }




	/*String loginJSON(String phoneNumber, String password){
        return "phoneNumber="+phoneNumber+
				"&password="+password;
	}*/




	/*public String MyResponse (String a1,String a2) throws IOException{
		SendRequest example = new SendRequest();



		String json = example.loginJSON(a1, a2);
		String response = example.post("http://192.168.1.26:9999/MBankingApi/api/v1/authentication/signin", json);
		
		System.out.println(response);

		return response;
	}*/


}

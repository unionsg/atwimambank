package com.unionsg.otherservices;


import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.unionsg.blink.R;

/**
*
* @author Ace Programmer Rbk <Rodney Kwabena Boachie at
*         rbk.unlimited@gmail.com>
* 
*/
public final class ProgDialogCustom  extends ProgressDialog{
	public ProgDialogCustom(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
	}




	private AnimationDrawable animation;
	
	
	

	@Override
	public void show() {
		// TODO Auto-generated method stub
		super.show();
		animation.start();
	}




	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
		 animation.stop();
	}




	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		super.cancel();
		 animation.stop();
	}




	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		 setContentView(R.layout.view_custom_progress_dialog);
		 ImageView la = (ImageView) findViewById(R.id.animation);
		  la.setBackgroundResource(R.drawable.dialog_anim);
		  animation = (AnimationDrawable) la.getBackground();
		 
	}*/

	
}
package com.unionsg.otherservices;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by UNION on 1/11/2018.
 */

public class PasswordValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN = "(.[,~,!,@,#,$,%,^,&,,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?,',`,*,\\\\].*$)";
//            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$]).{8,19})";


    private static final String PASSWORD_NUMBER = "\\d"; /*".*\\d.*";*/

    private static final String PASSWORD_LOWERCASE = "([a-z]+)";

    private static final String PASSWORD_UPPERCASE = "([A-Z ])";

    private static final String PASSWORD_SYMBOL = "(.[,~,!,@,#,$,%,^,&,,(,),-,_,=,+,[,{,],},|,;,:,<,>,/,?,',`,*,\\\\].*$)";

    private static final String PASSWORD_LENGTH = "(.{8,19})";


    public boolean validate(final String password) {
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean validateNumber(final String password) {
        pattern = Pattern.compile(PASSWORD_NUMBER);
        matcher = pattern.matcher(password);
        return matcher.find();
    }

    public boolean validateLowercase(final String password) {
        pattern = Pattern.compile(PASSWORD_LOWERCASE);
        matcher = pattern.matcher(password);
        return matcher.find();
    }

    public boolean validateUppercase(final String password) {
        pattern = Pattern.compile(PASSWORD_UPPERCASE);
        matcher = pattern.matcher(password);
        return matcher.find();
    }

    public boolean validateSymbol(final String password) {
        pattern = Pattern.compile(PASSWORD_SYMBOL);
        matcher = pattern.matcher(password);
        return matcher.find();
    }

    public boolean validateLength(final String password) {
        pattern = Pattern.compile(PASSWORD_LENGTH);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }




}

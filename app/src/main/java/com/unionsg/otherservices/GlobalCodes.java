package com.unionsg.otherservices;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.widget.TextView;

import com.unionsg.blink.R;
import com.unionsg.jsonstuff.SendRequest;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

public final class GlobalCodes {


    public static String currLedger = null;
    public static String amtLedger = null;
    public static String decimalLedger = null;

    public static String currAvail = null;
    public static String amtAvail = null;
    public static String decimalAvail = null;
    public static String numberValue = null;

    public GlobalCodes() {
    }

    //	public static String myUrl ="http://197.251.247.5:8680/MBankingApi/api/v1/authentication/";
//public static String myUrl ="http://41.205.235.98:6580/MBankingApi/api/v1/authentication/";
    public static String myUrl = "http://197.159.129.211:6580/MBankAPI/webresources/unionsys/";
//    public static String myUrl = "http://197.157.233.216:6580/MBankingApi/api/v1/authentication/";
//    public static String myUrl ="http://192.168.1.243:8680/MBankingApi/api/v1/authentication/";
//public static String myUrl ="http://192.168.1.102:8080/MBankingApi/api/v1/authentication/";
    public static String myAccountsUrl = myUrl + "accounts";
    public static String myEntertainmentMoviesUrl = myUrl + "getmovies";
    //	 http://192.168.246.1:8680/MBankingApi/api/v1/authentication/
    // http://192.168.1.26:8680/MBankingApi/api/v1/authentication/
    // http://mbanking.unionsg.com/MBankingApi/api/v1/authentication/
    //	 http://197.251.247.5:8680/MBankingApi/api/v1/authentication/
    public static String mbAccDetails = null;

    public static String getMbAccDetails() {
        return mbAccDetails;
    }

    public static void setMbAccDetails(String mbAccDetails) {
        GlobalCodes.mbAccDetails = mbAccDetails;
    }

    public static String FormatToMoney(String dataToConvert) {
        String ledgeBefore = dataToConvert;

        System.out.println("ledgeBefore = :" + ledgeBefore);
        String ledgeCurrencyCodeOnly = ledgeBefore.replaceAll("\\d+.*", "");
        System.out.println("Currency Code only = :" + ledgeCurrencyCodeOnly);
        String ledgeNumberOnly = dataToConvert.replaceAll("[^\\d.]", "");
        System.out.println("Numbers only = " + ledgeNumberOnly);
        numberValue = ledgeNumberOnly;
        String ledgeFinalFormatted = formatAmounts(ledgeNumberOnly);
        System.out.println("Finally Formatted Number= :" + ledgeFinalFormatted);
        dataToConvert = ledgeCurrencyCodeOnly + ledgeFinalFormatted;
        return dataToConvert;
    }

//public String conv(String num){
//	try {
//		double dbl = Double.parseDouble(num);
//	}
//		catch(NumberFormatException ex){
//			// handle exception
//		}
// }

    public static String formatAmounts(String theNumber) {
        Double amtDouble = 0.00;
        String formattedAmt = null;
        amtDouble = Double.parseDouble(theNumber);
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        formattedAmt = formatter.format(amtDouble);
        return formattedAmt;
    }

    public static void breakUpAmt(String amount) {

        currAvail = amount.substring(0, 3);
        switch (currAvail) {
            case "SLL":
                currAvail = "Le ";
                break;
            case "USD":
                currAvail = "$";
                break;
            case "GBP":
                currAvail = "£";
                break;
            case "GHS":
                currAvail = "¢";
                break;
            case "EUR":
                currAvail = "€";
                break;
            case "CNY":
                currAvail = "¥";
                break;
            case "JPY":
                currAvail = "¥";
                break;
        }
        amtAvail = amount.substring(3);
        StringTokenizer token = new StringTokenizer(amtAvail, ".");
        amtAvail = token.nextToken();
        if (token.hasMoreTokens()) {
            decimalAvail = token.nextToken();
        } else {
            decimalAvail = "00";
        }
    }

    public static void breakUpLedger(String amount) {

        currLedger = amount.substring(0, 3);
        switch (currLedger) {
            case "SLL":
                currLedger = "Le ";
                break;
            case "USD":
                currLedger = "$";
                break;
            case "GBP":
                currLedger = "£";
                break;
            case "GHS":
                currLedger = "¢";
                break;
            case "EUR":
                currLedger = "€";
                break;
            case "CNY":
                currLedger = "¥";
                break;
            case "JPY":
                currLedger = "¥";
                break;
        }
        amtLedger = amount.substring(3);
        StringTokenizer token = new StringTokenizer(amtLedger, ".");
        amtLedger = token.nextToken();
        if (token.hasMoreTokens()) {
            decimalLedger = token.nextToken();
        } else {
            decimalLedger = "00";
        }

    }

    public static String myResponse(String a1, String theUrl) throws IOException {

        SendRequest sendReqObj = new SendRequest();

        String json = fundsTransJSON(a1);
        String response = sendReqObj.postReq(theUrl, json);

        System.out.println(response);

        return response;
    }

    public static String fundsTransJSON(String authToken) {
        return "authToken=" + authToken;
    }


    public static String payEntertainmentResponse(String authToken, String showCode, String accNumCr, String accNumDr,
                                                  String amt, String showType) throws IOException {
        SendRequest sendReqObj = new SendRequest();

        String json = getEntertainmentRequest(authToken, showCode, accNumCr, accNumDr, amt, showType);
        //String json = getAliasRequest(token, aliasAccNum, aliasMaxAmt, aliasAccName);
        String response = sendReqObj.postReq(GlobalCodes.myUrl + "entpay", json);   /*http://mbanking.unionsg.com/MBankingApi/api/v1/authentication/signin*/
        System.out.println(response);

        return response;
    }

    public static String getEntertainmentRequest(String authToken, String showCode, String accNumCr, String accNumDr,
                                                 String amt, String showType) {
        return "authToken=" + authToken +
                "&showCode=" + showCode +
                "&accNumCr=" + accNumCr +
                "&accNumDr=" + accNumDr +
                "&amt=" + amt +
                "&showType=" + showType;
    }

    public static void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.slcb_big : R.drawable.slcb_big);

        // Setting OK Button
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //which = DialogInterface.BUTTON_POSITIVE;
                //dialog = alertDialog;
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static class ResponseDialog extends Builder {

        // Store the passed context
        private Context context;

        // Can be used as a regular builder
        public ResponseDialog(Context context) {
            super(context);
        }

        // Or as a custom builder, which we want
        public ResponseDialog(Context context, String title, String message) {
            super(context);
            // Store context
            this.context = context;
            // Set up everything
            setMessage(message);
            setTitle(title);
            setCancelable(false);
            //			setPadding(0, 5, 0, 5);
            setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss(); // It's just for info so we don't really care what this does
                    //					loginIntent = new Intent(MavenClientActivity.this, MainMenu.class);
                    //					startActivity(loginIntent);
                }
            });
        }

        public void showDialog() {
            // Create and show
            AlertDialog alert = create();
            alert.show();
            // Center align everything
            ((TextView) alert.findViewById(android.R.id.message)).setGravity(Gravity.CENTER);
            ((TextView) alert.findViewById((context.getResources().getIdentifier("alertTitle", "id", "android")))).setGravity(Gravity.CENTER);
        }


    }


}

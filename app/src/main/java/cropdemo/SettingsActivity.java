package cropdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

import com.unionsg.blink.R;

/**
 * Created by Denis on 30.08.2016.
 */
public class SettingsActivity extends AppCompatActivity {
	public static final String ARG_PREVIEW_SIZES = "preview_sizes";
	public static final String ARG_DEFAULT_PREVIEW_SIZE = "default_preview_size";

	// Custom Fragments
	private AppParamsFragment mAppParams;
	private SdkParamsFragment mSdkParams;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		final Intent in = getIntent();

		// Setup app params
		if (savedInstanceState == null) {
			mAppParams = new AppParamsFragment();
			getSupportFragmentManager().beginTransaction().add(R.id.settings_app_params_holder,
					mAppParams).commit();
		} else {
			mAppParams = (AppParamsFragment) getSupportFragmentManager().findFragmentById(
					R.id.settings_app_params_holder);
		}

		// Setup FragmentTabHost
		if (savedInstanceState == null) {
			mSdkParams = new SdkParamsFragment();
			getSupportFragmentManager().beginTransaction().add(R.id.settings_sdk_params_holder,
					mSdkParams).commit();
		} else {
			mSdkParams = (SdkParamsFragment) getSupportFragmentManager().findFragmentById(
					R.id.settings_sdk_params_holder);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.development, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
			return true;
		} else if (id == R.id.action_apply) {
			final CropDemoApp theApp = (CropDemoApp) getApplicationContext();

			// Save camera data
			boolean succeeded = true;

			// Save general params
			if (succeeded && mAppParams != null) {
				succeeded = mAppParams.save(theApp);
			}

			// Save SDK params
			if (succeeded && mSdkParams != null) {
				succeeded = mSdkParams.save(theApp);
			}

			if (succeeded) {
				setResult(RESULT_OK);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	public static void initCheckBox(CheckBox checkBox, boolean value) {
		if (checkBox.isChecked() == value) {
			checkBox.setChecked(!value);
			checkBox.toggle();
		} else {
			checkBox.setChecked(value);
		}
	}
}

package cropdemo;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import android.view.animation.AlphaAnimation;

@SuppressLint("NewApi")
public class EditImageView extends ImageView implements AnimationListener{
	
	//private EditImage parent;
	private int imageWidth;
	private int imageHeight;
	private Bitmap initialBitmap;
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	float oldAngle = 0f;
	private float fitScale;
	private float curScale;
	private float savedScale;
	private float angle = 0f;
	private enum AFTER_ANIM_COMMAND{NONE, NEXT, PREV, NEXT2, PREV2}; 
	AFTER_ANIM_COMMAND afterCommand;

	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	static final int ZOOM_ROTATE = 3;
	int mode = NONE;

	
	public EditImageView(Context context) {
	      super(context);
	      setScaleType(ScaleType.MATRIX); 
	      setFocusable(true);
	      setFocusableInTouchMode(true);
	      try{
		      setLayerType(View.LAYER_TYPE_SOFTWARE,
		    		  null); 
	      }
	      catch(NoSuchMethodError e){
	    	  
	      }
	   }
	public EditImageView(Context context, AttributeSet attrs){
		  super(context, attrs);
		  setScaleType(ScaleType.MATRIX); 
	      setFocusable(true);
	      setFocusableInTouchMode(true);
	      try{
		      setLayerType(View.LAYER_TYPE_SOFTWARE,
		    		  null); 
	      }
	      catch(NoSuchMethodError e){
	    	  
	      }
		}
	//public void setParent(EditImage par){
	//	parent = par;
	//	}
		
	public void setImageBitmap (Bitmap bm, Boolean scale){
		super.setImageBitmap (bm);
		if(bm != null)
		{
			initialBitmap = bm;
			imageWidth = bm.getWidth();
			imageHeight = bm.getHeight();
			if(scale)
			{
				scaleToFit(getWidth(), getHeight());
				//invalidate();
			}
		}
	}
		private void scaleToFit(int w, int h){
		   int w1 = w;
		   int h1 = h;
		   float scale;
		   if(w1 * imageHeight > h1 * imageWidth)
			   scale = (float)h1 / imageHeight;
		   else
			   scale = (float)w1 / imageWidth;
		   if(scale > 1f)
			   scale = 1f;
			   
		   curScale = fitScale = scale;
		   matrix.reset();
		   matrix.postScale(scale, scale);
		   float dx = (w - imageWidth * scale) / 2; 
		   float dy = (h - imageHeight * scale) / 2; 
		   matrix.postTranslate(dx, dy);
		   setImageMatrix(matrix);
		}
	   @Override
	   protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		   super.onSizeChanged(w, h, oldw, oldh);
		   scaleToFit(w, h);
	   }
	   public Bitmap getBitmap(){
		   return initialBitmap;
	   }
	   private float angle(MotionEvent event) {
		   float x = event.getX(0) - event.getX(1);
		   float y = event.getY(0) - event.getY(1);
		   return (float)Math.atan2(y, x);
		}	   
	   private float spacing(MotionEvent event) {
		   float x = event.getX(0) - event.getX(1);
		   float y = event.getY(0) - event.getY(1);
		   return (float)Math.sqrt(x * x + y * y);
		}	   
	   private void midPoint(PointF point, MotionEvent event) {
		   float x = event.getX(0) + event.getX(1);
		   float y = event.getY(0) + event.getY(1);
		   point.set(x / 2, y / 2);
		} 
	   @Override
	   public boolean onTouchEvent(MotionEvent event) {

	      // Handle touch events here...
	      switch (event.getAction() & MotionEvent.ACTION_MASK) {
	      case MotionEvent.ACTION_DOWN:
	    	   angle = 0f;
		          savedMatrix.set(matrix);
		          savedScale = curScale; 
		          start.set(event.getX(), event.getY());
		    	  mode = DRAG;
	          break;
	       case MotionEvent.ACTION_UP:
	       case MotionEvent.ACTION_POINTER_UP:
	    	   if(mode != NONE)
	    		   postTouchEvent();
	    	   //angle = 0f;
	          mode = NONE;
	          return true;
	       case MotionEvent.ACTION_POINTER_DOWN:
	    	   oldDist = spacing(event);
	    	   angle = 0f;
	    	   if (oldDist > 10f) {
		    	  oldAngle = angle(event);
	    	      savedMatrix.set(matrix);
	    	      midPoint(mid, event);
	    	      if(curScale <= fitScale)
	    	    	  mode = ZOOM_ROTATE;
	    	      else
	    	    	  mode = ZOOM;
	    	   }
	    	   break;

	    	case MotionEvent.ACTION_MOVE:
	    	   if (mode == DRAG) {
		             matrix.set(savedMatrix);
		             float dx = event.getX() - start.x;
		             float dy = event.getY() - start.y;
		             if(curScale <= fitScale)
		            	 dy = 0f;
		             matrix.postTranslate(dx,
		            dy);
	    	   }
	    	   else if (mode == ZOOM) {
	    	      float newDist = spacing(event);
	    	      if (newDist > 10f) {
	    	         matrix.set(savedMatrix);
	    	         float scale = newDist / oldDist;
	    	         curScale = savedScale * scale;
    	        	 matrix.postScale(scale, scale, mid.x, mid.y);
	    	      }
	    	   }
	    	   else if (mode == ZOOM_ROTATE) {
		    	      float newDist1 = spacing(event);
		    	      if (newDist1 > 10f) {
		    	         matrix.set(savedMatrix);
		    	         float scale = newDist1 / oldDist;
		    	         curScale = savedScale * scale;
		    	         angle = angle(event) - oldAngle;
	    	        	 matrix.postScale(scale, scale, mid.x, mid.y);
	    	        	 matrix.postRotate(angle * 180f / (float)Math.PI, mid.x, mid.y);
		    	      }
	    	   }
	    	   break;	          
	    }

	      // Perform the transformation
	      setImageMatrix(matrix);

	      return true; // indicate event was handled
	   }
		private void postTouchEvent(){
			if(angle != 0.f)
			{
   	         	matrix.set(savedMatrix);
   	         	float scale = curScale / savedScale;
   	         	if(angle > Math.PI)
   	         		angle = - 2 * (float)Math.PI + angle;
   	         	if(angle < -Math.PI)
   	         		angle = 2 * (float)Math.PI + angle;
	        	matrix.postScale(scale, scale, mid.x, mid.y);
				if(angle < -Math.PI/6)
				{
					Matrix m = new Matrix();
					m.set(matrix);
				   m.postRotate(-90f, mid.x, mid.y);
		   	       setImageMatrix(m);
					//parent.rotate(-90f);
					return;
				}
				else
				if(angle > Math.PI/6)
				{
					Matrix m = new Matrix();
					m.set(matrix);
				   m.postRotate(90f, mid.x, mid.y);
		   	       setImageMatrix(m);
		   	     	invalidate();
					//parent.rotate(90f);
					return;
				}
			}
	         if(curScale < fitScale)
	         {
      			 curScale = fitScale;
      			 int w = getWidth();
      			 int h = getHeight();
      			 matrix.reset();
      			 matrix.postScale(curScale, curScale);
      			 float dx = (w - imageWidth * curScale) / 2; 
      			 float dy = (h - imageHeight * curScale) / 2; 
      			 matrix.postTranslate(dx, dy);
		   	     setImageMatrix(matrix);
	         }
	         else
	         if(curScale > 2f)
	         {
	        	 matrix.postScale(2f/curScale, 2f/curScale, mid.x, mid.y);
	        	 curScale = 2f; 
		   	     setImageMatrix(matrix);
	         }
	         RectF rc = new RectF(0, 0, imageWidth, imageHeight);
	         matrix.mapRect(rc);
	         float dx = 0f, dy = 0f;
	         if(rc.left > 0f)
	        	 dx = -rc.left;
	         if(rc.top > 0f)
	        	 dy = -rc.top;
	         if(rc.right < getWidth())
	        	 dx = getWidth() - rc.right; 
	         if(rc.bottom < getHeight())
	        	 dy = getHeight() - rc.bottom;
	         if(dx == 0f && dy == 0f)
	         {
    	      	setImageMatrix(matrix);
	        	 return;
	         }
	         /*float k;
	         if(curScale <= fitScale)
	        	 k = 4f;
	         else
	        	 k = 2f;
	         if(dx < -getWidth() / k && MDScanActivity.getNumPages() >1)
	         {
	        	 prev(dx, dy);
	        	 return;
	         }
	         if(dx > getWidth() / k && MDScanActivity.getNumPages() >1)
	         {
	        	 next(dx, dy);
	        	 return;
	         }*/
	         if(imageWidth * curScale <= getWidth())
	         {
	        	 float x = (getWidth() - imageWidth * curScale) / 2;
	        	 dx = x - rc.left;
	         }
	         if(imageHeight * curScale <= getHeight())
	         {
	        	 float y = (getHeight() - imageHeight * curScale) / 2;
	        	 dy = y - rc.top;
	         }
        	matrix.postTranslate(dx, dy);
   	      	setImageMatrix(matrix);
		}
		private void next(float dx, float dy){
			//parent.showToast(R.string.loading);
			TranslateAnimation animation = new TranslateAnimation(0f, -(getWidth() - dx) - 10, 0f, 0f);
			animation.setDuration(100);
			animation.setFillAfter(true);
			animation.setAnimationListener(this);
			afterCommand = AFTER_ANIM_COMMAND.NEXT;
			startAnimation(animation);
		}
		private void prev(float dx, float dy){
			//parent.showToast(R.string.loading);
			TranslateAnimation animation = new TranslateAnimation(0f, (getWidth() + dx), 0f, 0f); 
			animation.setDuration(100);
			animation.setFillAfter(true);
			animation.setAnimationListener(this);
			afterCommand = AFTER_ANIM_COMMAND.PREV;
			startAnimation(animation);
		}
		@Override
		public void onAnimationEnd(Animation arg0) {
			if(afterCommand == AFTER_ANIM_COMMAND.NONE)
				setAnimation(null);
			switch(afterCommand)
			{
			case NEXT:
				/*if(parent.isModified())
				{
					setAnimation(null);
					afterCommand = AFTER_ANIM_COMMAND.NONE;	
					parent.next();
					return;
				}*/
				afterCommand = AFTER_ANIM_COMMAND.NEXT2;
				{
					//AlphaAnimation animation = new AlphaAnimation(0f, 1f);
					TranslateAnimation animation = new TranslateAnimation( getWidth(), 0f, 0f, 0f); 
					animation.setDuration(500);
					animation.setFillAfter(true);
					animation.setAnimationListener(this);
					startAnimation(animation);
				}
				break;
			case PREV:
				/*if(parent.isModified())
				{
					setAnimation(null);
					afterCommand = AFTER_ANIM_COMMAND.NONE;	
					parent.next();
					return;
				}*/
				afterCommand = AFTER_ANIM_COMMAND.PREV2;
				{
					//AlphaAnimation animation = new AlphaAnimation(0f, 1f); 
					TranslateAnimation animation = new TranslateAnimation(-getWidth(), 0f, 0f, 0f); 
					animation.setDuration(500);
					animation.setFillAfter(true);
					animation.setAnimationListener(this);
					startAnimation(animation);
				}
				break;
			}
			
		}
		@Override
		public void onAnimationRepeat(Animation arg0) {
			
		}
		@Override
		public void onAnimationStart(Animation arg0) {
			switch(afterCommand)
			{
			case NEXT2:
				afterCommand = AFTER_ANIM_COMMAND.NONE;	
				//parent.next();
				break;
			case PREV2:
				afterCommand = AFTER_ANIM_COMMAND.NONE;	
				//parent.prev();
				break;
			}
		}
		private void baseSetImageBitmap(Bitmap bm)
		{
			super.setImageBitmap(bm);
		}
	   public void rotate(float a){
		   final float ang = a;
		   /*final ProgressDialog progress = ProgressDialog.show(parent, "",
				   parent.getString(R.string.processing), true);*/			   
		   final Handler handler = new Handler() {
			   @Override
			   public void handleMessage(Message msg) {
				   //progress.dismiss();
				   baseSetImageBitmap (initialBitmap);
				   setImageMatrix(matrix);
			   }
		   };
		   new Thread(){
			   public void run(){
				   try
				   {
					   Matrix m = new Matrix();
					   m.postRotate(ang, 0, 0);
					   Bitmap bm = Bitmap.createBitmap(initialBitmap, 0, 0, imageWidth, imageHeight, m, false);
					   imageWidth = bm.getWidth();
					   imageHeight = bm.getHeight();
					   initialBitmap = bm;
					   m.reset();
					   int w = getWidth();
					   int h = getHeight();
					   int w1 = w;
					   int h1 = h;
					   float scale;
					   if(w1 * imageHeight > h1 * imageWidth)
						   scale = (float)h1 / imageHeight;
					   else
						   scale = (float)w1 / imageWidth;
					   curScale = fitScale = scale;
					   m.postScale(scale, scale);
					   float dx = (w - imageWidth * scale) / 2; 
					   float dy = (h - imageHeight * scale) / 2; 
					   m.postTranslate(dx, dy);
					   matrix = m;
					   handler.sendEmptyMessage(0);
				   }
				   catch(Exception e)
				   {
					   handler.sendEmptyMessage(0);
				   }
				   catch(OutOfMemoryError e)
				   {
					   handler.sendEmptyMessage(0);
				   }
			   }
		   }.start();
	   }

}

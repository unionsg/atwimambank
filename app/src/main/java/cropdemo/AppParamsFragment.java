package cropdemo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.unionsg.blink.R;


/**
 * Created by Denis on 17.09.2016.
 */
public class AppParamsFragment extends Fragment implements ISettingsFragment {
	private AppCompatSpinner mSaveFormat;
	// NOTE: Must be in same order as CropDemoApp.SAVE_XXX
	private final String [] mSaveFormatValues = {"System JPEG", "Tiff G4", "PNG", "PDF", "PDF through PNG"};
	private int mSaveFormatIndex = -1;

	private CheckBox mSimulatePages;
	private CheckBox mShareOutput;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		final View root = inflater.inflate(R.layout.fragment_app_params, container, false);

		final CropDemoApp theApp = (CropDemoApp) getContext().getApplicationContext();

		// Setup save format
		mSaveFormat = (AppCompatSpinner) root.findViewById(R.id.settings_save_format);
		final ArrayAdapter<String> saveFormatAdapter = new ArrayAdapter<String>(getContext(),
				android.R.layout.simple_spinner_item, mSaveFormatValues);
		saveFormatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSaveFormat.setAdapter(saveFormatAdapter);
		mSaveFormat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mSaveFormatIndex = position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				mSaveFormatIndex = -1;
			}
		});


		mSimulatePages = (CheckBox) root.findViewById(R.id.settings_simulate_pages);
		mShareOutput = (CheckBox) root.findViewById(R.id.settings_share_output);

		// Initialize from preferences
		if (savedInstanceState == null) {
			mSaveFormat.setSelection(theApp.getSaveFormat());
			mSimulatePages.setChecked(theApp.getSimulatePages());
			mShareOutput.setChecked(theApp.getShareOutput());
		}
		return root;
	}

	@Override
	public boolean save(@NonNull CropDemoApp theApp) {
		if (mSaveFormatIndex != -1) {
			theApp.setSaveFormat(mSaveFormatIndex);
		}
		theApp.setSimulatePages(mSimulatePages.isChecked());
		theApp.setShareOutput(mShareOutput.isChecked());
		return true;
	}
}

package cropdemo.widget.console;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unionsg.blink.R;

import java.util.ArrayList;

import cropdemo.util.IntPair;
import cropdemo.util.Utils;

//import cropdemo.R;

/**
 * Created by Denis on 10.06.2016.
 */
public class ConsoleAdapter extends RecyclerView.Adapter<ConsoleAdapter.ConsoleViewHolder>
		implements ConsoleView.IConsole {

	private final Context mContext;
	private final ArrayList<ConsoleLineImpl> mLines = new ArrayList<>();

	public class ConsoleLineImpl extends ConsoleView.ConsoleLine implements Runnable {
		public ConsoleLineImpl(CharSequence text, long showDelay) {
			super(text, showDelay);
		}

		@Override
		public void run() {
			remove(this);
		}
	}

	class ConsoleViewHolder extends RecyclerView.ViewHolder {
		final ConsoleTextView mConsoleText;
		ConsoleViewHolder(View itemView) {
			super(itemView);
			mConsoleText = (ConsoleTextView) itemView.findViewById(android.R.id.text1);
		}

	}

	int mDockSide = -1;

	public ConsoleAdapter(@NonNull Context context) {
		mContext = context;
	}

	@Override
	public int getItemCount() {
		return mLines.size();
	}

	@Override
	public ConsoleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View itemView = inflater.inflate(R.layout.list_row_console, parent, false);
		return new ConsoleViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ConsoleViewHolder holder, int position) {
		// Reposition text
		ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();
		if (ConsoleView.isVertical(mDockSide)) {
			params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
			params.height = ViewGroup.LayoutParams.MATCH_PARENT;
		} else {
			params.width = ViewGroup.LayoutParams.MATCH_PARENT;
			params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
		}
		holder.itemView.setLayoutParams(params);

		final ConsoleLineImpl line = (ConsoleLineImpl) mLines.get(position);
		holder.mConsoleText.setText(line.text);
		holder.mConsoleText.setDockSide(mDockSide);
	}

	// Array adapter simulation
	private Context getContext() {
		return mContext;
	}
	private void remove(ConsoleView.ConsoleLine line) {
		final int position = mLines.indexOf((ConsoleLineImpl)line);
		if (position != -1) {
			mLines.remove(position);
			notifyItemRemoved(position);
		}
	}

	private int [] removeText(CharSequence text) {
		final ArrayList<Integer> removed = new ArrayList<>();
		for (int i = mLines.size()-1; i >= 0; i--) {
			ConsoleLineImpl line = mLines.get(i);
			if (TextUtils.equals(line.text, text)) {
				mShowHandler.removeCallbacks(line);
				mLines.remove(i);
				removed.add(i);
			}
		}

		int i = 0;
		final int [] indices = new int[removed.size()];
		for (Integer n : removed) {
			indices[i++] = n;
		}
		return indices;
	}

	private ConsoleView.ConsoleLine getItem(int position) {
		return mLines.get(position);
	}

	private final Handler mShowHandler = new Handler();

	@Override
	public ConsoleView.ConsoleLine appendLine(@StringRes int textId, long showDelay) {
		return appendLine(getContext().getString(textId), showDelay);
	}

	@Override
	public ConsoleView.ConsoleLine appendLine(CharSequence text, long showDelay) {
		// Remove line with same text without
		final int [] removed = removeText(text);

		final ConsoleLineImpl line = new ConsoleLineImpl(text, showDelay);
		mLines.add(line);

		if (removed.length > 0) {
			// Notify entire view if removed
			notifyDataSetChanged();
		} else {
			// Only append occurs
			notifyItemInserted(mLines.size()-1);
		}

		if (showDelay != ConsoleView.INFINITE) {
			mShowHandler.postDelayed(line, showDelay);
		}
		return line;
	}

	@Override
	public void removeLine(@StringRes int textId) {
		removeLine(getContext().getString(textId));
	}

	@Override
	public void removeLine(CharSequence text) {
		int [] removed = removeText(text);
		IntPair[] pairs = Utils.collectRanges(removed, removed.length);
		for (IntPair pair : pairs) {
			notifyItemRangeRemoved(pair.first, pair.second - pair.first + 1);
		}
	}

	@Override
	public void removeLine(ConsoleView.ConsoleLine line) {
		mShowHandler.removeCallbacks((ConsoleLineImpl)line);
		remove(line);
	}

	@Override
	public void clear() {
		mShowHandler.removeCallbacksAndMessages(null);
		mLines.clear();
		notifyDataSetChanged();
	}

	public void setDockSide(int dockSide) {
		if (mDockSide != dockSide) {
			mDockSide = dockSide;
			notifyItemRangeChanged(0, getItemCount());
		}
	}
}

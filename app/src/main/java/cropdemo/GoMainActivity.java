package cropdemo;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.unionsg.blink.R;


/**
 * Created by USG0011 on 06/27/17.
 */

public class GoMainActivity extends AppCompatActivity {

    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_main);

        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                intent.setComponent(new ComponentName("com.pixelnetica.cropdemo",
                        "com.pixelnetica.cropdemo.MainActivity"));
                startActivity(intent);



            }
        })
        ;
    }
}

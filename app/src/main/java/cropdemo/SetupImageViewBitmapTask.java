package cropdemo;

import java.lang.ref.WeakReference;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.widget.ImageView;


public class SetupImageViewBitmapTask extends AsyncTask<Bitmap, Void, Bitmap>{
	
	private final WeakReference<CropImageView> imageViewRef;
	private int displayWidth = 0;
	private int displayHeight = 0;
	private float rotationAngle = Float.NaN;
	private float scaleFactor = 1.0f;	// 0f means 1:1 scale
	private final WeakReference<CropData> cropDataRef;

	public static void setImageBitmap(@NonNull CropImageView imageView, final Bitmap displayBitmap, float angle, float zoom, CropData cropData) {
		// Reset image view
		if( displayBitmap == null) {
			imageView.setImageBitmap(null);
			imageView.setRotation(0);
			return;
		}
		
		SetupImageViewBitmapTask task = new SetupImageViewBitmapTask(imageView, angle, zoom, cropData);
		// Serialize calls
		task.execute(displayBitmap);
	}
		
	private SetupImageViewBitmapTask(final CropImageView imageView, float angle, float zoom, CropData cropData) {
		imageViewRef = new WeakReference<CropImageView>(imageView);
		rotationAngle = angle;
		scaleFactor = zoom;
		cropDataRef = new WeakReference<CropData>(cropData);		
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		
		ImageView imageView = imageViewRef.get();
		if (imageView != null) {
			displayWidth = imageView.getWidth();
			displayHeight = imageView.getHeight();		
		}
		
		// Set default values
		// In some layout (e.g. wrap_content) control dimensions may be zero
		if( displayWidth == 0) {
			displayWidth = 500;
		}
		if( displayHeight == 0) {
			displayHeight = 500;
		}

		// Apply zoom
		if (scaleFactor > 0 ) {
			displayWidth *= scaleFactor;
			displayHeight *= scaleFactor;
		}
	}
	
	@Override
	protected Bitmap doInBackground(Bitmap... params) {
		final Bitmap displayBitmap = params[0];
		Bitmap result = null;
		try 	{
			int width, height;
			if (scaleFactor > 0) {
				width = displayWidth;
				height = displayHeight;
			} else {
				width = displayBitmap.getWidth();
				height = displayBitmap.getHeight();
			}
			result = createDisplayBitmap(displayBitmap, width, height, false, rotationAngle);
		} catch(OutOfMemoryError e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	protected void onPostExecute(Bitmap bitmap) {
		super.onPostExecute(bitmap);
		
		if( imageViewRef != null && bitmap != null) {
			final CropImageView imageView = imageViewRef.get();
			if (imageView != null) {
				imageView.setImageBitmap(bitmap);
				imageView.setScaleFactor(scaleFactor);
				imageView.setCropData(cropDataRef.get());
			}
		}
	}
	
    // Returns scaled bitmap inscribed to width and height saving source aspect ratio 
    private Bitmap createDisplayBitmap(@NonNull Bitmap sourceBitmap, int width, int height, boolean makeCopy, float angle) {
	    if (width <= 0) {
		    throw new IllegalArgumentException("Invalid width: " + width);
	    }
	    if (height <= 0) {
		    throw new IllegalArgumentException("Invalid height: " + height);
	    }

	    if (angle == Float.NaN) {
		    throw new IllegalArgumentException("Invalid angle: NaN");
	    }

     	int newWidth, newHeight;
     	final double a = angle * Math.PI / 180;
     	final double cos = Math.abs(Math.cos(a));
     	final double sin =  Math.abs(Math.sin(a));
     	
     	final int srcWidth = sourceBitmap.getWidth();
     	final int srcHeight = sourceBitmap.getHeight();
     	
     	final int dstWidth = (int) (width * cos + height * sin);
     	final int dstHeight = (int) (width * sin + height * cos);
     	
     	if (srcWidth * dstHeight > srcHeight * dstWidth ) {
     		newWidth = dstWidth;
     		newHeight = (newWidth * srcHeight) / srcWidth;
     	} else {
     		newHeight = dstHeight;
     		newWidth = (newHeight * srcWidth) / srcHeight; 
     	}
     	
     	Matrix matrix = new Matrix();
     	matrix.postScale((float)newWidth/srcWidth, (float)newHeight/srcHeight);
     	matrix.postRotate(angle);
     	return Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);
     }
}

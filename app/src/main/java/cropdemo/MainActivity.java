package cropdemo;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.unionsg.blink.ChequeDeposit;
import com.unionsg.blink.R;

import java.io.ByteArrayOutputStream;
import java.io.File;

import cropdemo.camera.CameraActivity;

import static com.unionsg.blink.ChequeDeposit.picByte;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
	
	private static final int PICK_IMAGE = 100;
	private static final int TAKE_PHOTO = 101;

	String token = null;
	int cameraFlag = 0;
	int myCameraFlag = 0;

		
	// Main application
	CropDemoApp theApp;

	// Support toolbar
	Toolbar mToolBar;

	// Main button bar
	ViewGroup buttonBar;
	
	// Crop button bar
	ViewGroup cropButtonBar;

    // Display and size
    cropdemo.CropImageView imageView;
    ProgressBar progressWait;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cam);

		token = getIntent().getExtras().getString("token").trim();
		cameraFlag = getIntent().getExtras().getInt("cameraFlag");
		myCameraFlag = cameraFlag;
		System.out.println("my camera flag is " + myCameraFlag);
 
        // Setup App's main activity
    	theApp = (CropDemoApp) getApplication();
        theApp.mainActivity = this;
	    theApp.loadSettings();

//		theApp.setProcessingProfile(CropDemoApp.NoBinarization);
//		theApp.setForceManualCrop(!theApp.isForceManualCrop());

        // Add buttons to action bar
	    mToolBar = (Toolbar) findViewById(R.id.app_toolbar);
	    if (mToolBar != null) {
		    setSupportActionBar(mToolBar);
	    }

    	// Setup main buttons handlers
        buttonBar = mToolBar;
//        buttonBar.findViewById(R.id.btn_open_image).setOnClickListener(this);
//	    buttonBar.findViewById(R.id.btn_take_photo).setOnClickListener(this);
        buttonBar.findViewById(R.id.btn_crop_image).setOnClickListener(this);
        buttonBar.findViewById(R.id.btn_save_image).setOnClickListener(this);
        
        // Setup crop button bar
        View root = findViewById(R.id.root);
        cropButtonBar = (ViewGroup) root.findViewById(R.id.crop_button_bar);
        cropButtonBar.findViewById(R.id.btn_rotate_left).setOnClickListener(this);
        cropButtonBar.findViewById(R.id.btn_rotate_right).setOnClickListener(this);
        cropButtonBar.findViewById(R.id.btn_revert_selection).setOnClickListener(this);
        cropButtonBar.findViewById(R.id.btn_expand_selection).setOnClickListener(this);

        // Setup display
        imageView = (CropImageView) findViewById(R.id.image_holder);
		progressWait = (ProgressBar) findViewById(R.id.progress_wait);

		onTakePhoto();
		theApp.setProcessingProfile(CropDemoApp.NoBinarization);
    }
    
    @Override
    protected void onDestroy()
    {
    	super.onDestroy();
    	
        // Setup App's main activity
        theApp.mainActivity = null;
    }

    @Override
    protected void onResume()
    {
    	super.onResume();
    	
    	// Set buttons state
	    updateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;// super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
    	//super.onPrepareOptionsMenu(menu);
    	MenuItem item;
    	
    	// Force manual crop
    	item = menu.findItem(R.id.action_force_manual_crop);
    	//item.setChecked(theApp.isForceManualCrop());
	    item.setVisible(theApp.canPerformManualCrop());

    	// Strong shadows
    	item = menu.findItem(R.id.action_strong_shadows);
    	item.setChecked(theApp.isStrongShadows());
    	
    	// Processing profile
    	switch (theApp.getProcessingProfile()) {
		    case CropDemoApp.NoBinarization:
			    item = menu.findItem(R.id.action_profile_nobinarization);
			    item.setChecked(true);
			    break;
	        case CropDemoApp.BWBinarization:
	            item = menu.findItem(R.id.action_profile_bwbinarization);
	            item.setChecked(true);
	            break;
	        case CropDemoApp.GrayBinarization:
	            item = menu.findItem(R.id.action_profile_graybinarization);
	            item.setChecked(true);
	            break;
	        case CropDemoApp.ColorBinarization:
	            item = menu.findItem(R.id.action_profile_colorbinarization);
	            item.setChecked(true);
	            break;
    	}
    	
    	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();
        if (id == R.id.action_force_manual_crop) {
	        // Invert force manual crop
	        theApp.setForceManualCrop(!theApp.isForceManualCrop());
	        theApp.performManualCrop();
	        return true;
        } else if (id == R.id.action_settings) {
	        final Intent intent = new Intent(this, SettingsActivity.class);
	        startActivity(intent);
	        return true;
        } else if (id == R.id.action_strong_shadows) {
        	// Invert strong shadows
        	theApp.setStrongShadows(!theApp.isStrongShadows());
            return true;
        }
        else if (id == R.id.action_profile_nobinarization) {
	        theApp.setProcessingProfile(CropDemoApp.NoBinarization);
	        return true;
        }
        else if (id == R.id.action_profile_bwbinarization) {
        	theApp.setProcessingProfile(CropDemoApp.BWBinarization);
        	return true;
        }
        else if (id == R.id.action_profile_graybinarization) {
        	theApp.setProcessingProfile(CropDemoApp.GrayBinarization);
        	return true;
        }
        else if (id == R.id.action_profile_colorbinarization) {
        	theApp.setProcessingProfile(CropDemoApp.ColorBinarization);
        	return true;
        }
        /*else if (id == R.id.action_about) {
        	// Show advanced processing dialog (old mode)
			onShowAbout();
			return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

	public static CharSequence getFormattedText(Context context, int id, Object... args) {
		for (int i = 0; i < args.length; ++i) {
			args[i] = (args[i] instanceof String) ? TextUtils.htmlEncode((String) args[i]) : args[i];
		}
		return Html.fromHtml(
				String.format(
						Html.toHtml(
								new SpannableString(
										Html.fromHtml(
												context.getText(id).toString()
										)
								)), args));
	}
	private void onShowAbout() {

		String versionName;
		final ApplicationInfo appInfo = getApplicationInfo();
		try {
			final PackageInfo pkgInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionName = pkgInfo.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			versionName = "";
		}

		// Prepare text view for HTML content
		//final SpannableString msg = new SpannableString(Html.fromHtml(getText(R.string.about_message).toString()));
		final CharSequence msg = new SpannableString(getFormattedText(this, R.string.about_message, versionName));

		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.setTitle(R.string.about_title);
		dlg.setIcon(appInfo.icon);
		dlg.setCancelable(true);
		dlg.setMessage(msg);
		dlg.setButton(AlertDialog.BUTTON_POSITIVE, getText(R.string.about_ok), new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		dlg.show();

		// Show links
		final TextView textView = (TextView)dlg.findViewById(android.R.id.message);
		if (textView != null) {
			textView.setMovementMethod(LinkMovementMethod.getInstance());
		}
	}
    
	@Override
	public void onClick(View v) {		
		switch( v.getId() )
		{
		/*case R.id.btn_open_image:
			onOpenImage();
			break;*/
		/*case R.id.btn_take_photo:
			onTakePhoto();
			theApp.setProcessingProfile(CropDemoApp.NoBinarization);
			break;*/
		case R.id.btn_crop_image:
			onCropImage();
			break;
		case R.id.btn_save_image:
			onSaveImage();
			break;
		case R.id.btn_rotate_left:
			imageView.rotateLeft();
			break;
		case R.id.btn_rotate_right:
			imageView.rotateRight();
			break;
		case R.id.btn_revert_selection:
			imageView.revertSelection(theApp.documentCorners);
			break;
		case R.id.btn_expand_selection:
			imageView.expandSelection();
			break;
		}			
	}

	private void selectPicturesGranted(String title, boolean multiple) {
		final Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_PICK);
		if (multiple && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
			intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
		}
		startActivityForResult(Intent.createChooser(intent, title), PICK_IMAGE);
	}

	public void selectPictures(final String title, final boolean multiple) {
		theApp.RuntimePermission.runWithPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
				R.string.permission_query_read_storage, new RuntimePermissions.Callback() {
					@Override
					public void permissionRun(String permission, boolean granted) {
						if (granted) {
							selectPicturesGranted(title, multiple);
						}
					}
				});
	}


	public void selectPictures(int resID, boolean multiple) {
		selectPictures(getString(resID), multiple);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		theApp.RuntimePermission.handleRequestPermissionsResult(requestCode, permissions, grantResults);
	}

	void onOpenImage() {
		selectPictures(R.string.select_picture_title, false);
    }

	void onTakePhoto() {
		/*Intent intent = new Intent(this, CameraActivity.class);
		startActivityForResult(intent, TAKE_PHOTO);*/

		final File fileSink = new File(Environment.getExternalStorageDirectory(), "slcb_images");

		/*ContextWrapper contextWrapper = new ContextWrapper(getApplicationContext());
		File fileSink = contextWrapper.getDir("CropDemo", Context.MODE_PRIVATE);*/
		//myInternalFile = new File(directory , filename);

		System.out.println(fileSink.getAbsolutePath());
		Intent intent = CameraActivity.newIntent(this,
				fileSink.getAbsolutePath(),
				"camera-prefs",
				true);
		startActivityForResult(intent, TAKE_PHOTO);

		/*onCropImage();
		onSaveImage();*/
	}
	
	void onCropImage() {
		// Start crop pipeline		
		theApp.startCropImage(imageView.getCropData());
	}
	
	void onSaveImage() {

//		ChequeDeposit.picByte = data1;

		if (myCameraFlag == 0) {
			Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] imageInByte = baos.toByteArray();

			picByte = imageInByte;
			System.out.println("image pic byte: " + picByte);

			theApp.processImage.setBitmap(null);

			Intent intent = new Intent(MainActivity.this, ChequeDeposit.class);
			intent.putExtra("cameraFlag", 1);
			intent.putExtra("token", token);
			setResult(RESULT_OK, intent);
			startActivity(intent);
			MainActivity.this.finish();
		} else if (myCameraFlag == 1){

			Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] imageInByte = baos.toByteArray();

			ChequeDeposit.picByte2 = imageInByte;
			System.out.println("image pic byte: " + picByte);

			theApp.processImage.setBitmap(null);

			Intent intent = new Intent(MainActivity.this, ChequeDeposit.class);
			intent.putExtra("cameraFlag", 2);
			intent.putExtra("token", token);
			setResult(RESULT_OK, intent);
			startActivity(intent);
			MainActivity.this.finish();

		}

//		theApp.RuntimePermission.runWithPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
//				R.string.permission_query_write_storage, new RuntimePermissions.Callback() {
//					@Override
//					public void permissionRun(String permission, boolean granted) {
//						if (granted) {
//							theApp.onSaveImage();
//						}
//					}
//				});
	}

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data); 
    	
    	switch( requestCode) {
		    case PICK_IMAGE:
		    case TAKE_PHOTO:
			    if (resultCode == RESULT_OK) {
				    Uri selectedImage = data.getData();
				    theApp.onOpenImage(selectedImage);
				    //theApp.onOpenCropPage(selectedImage, mPageListener);
					theApp.setProcessingProfile(CropDemoApp.NoBinarization);
					theApp.startCropImage(imageView.getCropData());
					//theApp.onSaveImage();

			    }
			    break;
	    }
    }

	void updateUI() {
		setButtonsState();
		setDisplayImage();
		setWaitMode();
	}

    public void setButtonsState()
    {
		final boolean manualCropAvailable = theApp.imageMode == CropDemoApp.CropOrigin && theApp.processImage != null && !theApp.waitMode;
		final boolean processingAvailable = (theApp.imageMode == CropDemoApp.Source || theApp.imageMode == CropDemoApp.CropOrigin) && theApp.processImage != null && !theApp.waitMode;
		final boolean resultAvailable = theApp.imageMode == CropDemoApp.Target && theApp.processImage != null && !theApp.waitMode;
    	
    	if (buttonBar != null)
    	{
        	// Open & Camera buttons are always available
    		
    		// Crop button is enabled when we have a source image and no target image
    		// and no current processing
        	setupButtonVisible(buttonBar, R.id.btn_crop_image, processingAvailable);
    		
    		// Save button is enabled when we have a target image
        	setupButtonVisible(buttonBar, R.id.btn_save_image, resultAvailable);
    	}
    	
    	// Show or hide crop button bar
    	setupButtonVisible(findViewById(R.id.root), R.id.crop_button_bar, manualCropAvailable);
    }
    
    private static void setupButtonVisible(@NonNull View container, int id, boolean visible) {

    	View button = (View) container.findViewById(id);
    	if (button == null) {
		    throw new IllegalStateException("Button is null!");
	    }
    	
    	if (visible) {
    		button.setVisibility(View.VISIBLE);
    	} else {
    		button.setVisibility(View.GONE);
    	}
    }
    
    public void setDisplayImage() {
    	float displayAngle = 0;
    	Bitmap displayBitmap = null;
    	if (theApp.processImage != null)
    	{
    		displayBitmap = theApp.processImage.getBitmap();
		    switch (theApp.processImage.getExifOrientation()) {
			    case ExifInterface.ORIENTATION_ROTATE_90:
				    displayAngle = 90f;
				    break;
			    case ExifInterface.ORIENTATION_ROTATE_180:
				    displayAngle = 180f;
				    break;
			    case ExifInterface.ORIENTATION_ROTATE_270:
				    displayAngle = 270f;
				    break;
			    default:
				    displayAngle = 0;
		    }
    	}

	    imageView.setImageBitmap(displayBitmap);
	    //imageView.setScaleFactor(1.0f);
	    if (theApp.documentCrop == null) {
		    imageView.rotateImageToFit(displayAngle);
		    imageView.setCropData(null);
	    } else {
		    imageView.setCropData(theApp.documentCrop);
	    }
		//SetupImageViewBitmapTask.setImageBitmap(imageView, displayBitmap, displayAngle, theApp.manualCropZoom, theApp.documentCrop);
    }

    public void setWaitMode()
    {        	
		if( theApp.waitMode )
		{
			// TODO: setup a better color filter
			imageView.setColorFilter(Color.rgb(128, 128, 128), PorterDuff.Mode.LIGHTEN);
			progressWait.setVisibility(View.VISIBLE);
		}
		else
		{
			imageView.setColorFilter(0, PorterDuff.Mode.DST);
			progressWait.setVisibility(View.INVISIBLE);
		}	
    }
    
	public void showProcessingError(@CropDemoApp.ProcessingError int error) {
		// Show error toast
		switch (error)
		{
		case CropDemoApp.NOERROR:
			Toast.makeText(getApplicationContext(), R.string.msg_processing_complete, Toast.LENGTH_SHORT).show();
			break;
		case CropDemoApp.PROCESSING:
			Toast.makeText(getApplicationContext(), R.string.msg_processing_error, Toast.LENGTH_LONG).show();
			break;
		case CropDemoApp.OUTOFMEMORY:
			Toast.makeText(getApplicationContext(), R.string.msg_out_of_memory, Toast.LENGTH_LONG).show();
			break;
		case CropDemoApp.NODOCCORNERS:
			Toast.makeText(getApplicationContext(), R.string.msg_no_doc_corners, Toast.LENGTH_LONG).show();
			break;
		case CropDemoApp.INVALIDCORNERS:
			Toast.makeText(getApplicationContext(), R.string.msg_invalid_corners, Toast.LENGTH_LONG).show();			
		}
	}


	@Override
	public void onBackPressed() {
		//	   moveTaskToBack(true);
		MainActivity.this.finish();
	}

}

package cropdemo;

import android.app.Application;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.pixelnetica.imagesdk.ImageProcessing;
import com.pixelnetica.imagesdk.ImageSdkLibrary;


/**
 * Created by Denis on 26.11.2016.
 */

public class SdkFactory {

	private final Application mApplication;

	public SdkFactory(@NonNull Application application) {
		mApplication = application;
	}

	public ImageProcessing createSDK(@NonNull Application application) {
		// Default processing
		return new ImageSdkLibrary(application).newProcessingInstance();
	}

	public Bundle createDocCorners() {
		// All-defaults params
		return new Bundle();
	}

	public void loadPreferences() {
		// Dummy
	}
}

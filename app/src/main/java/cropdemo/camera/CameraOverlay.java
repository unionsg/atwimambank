package cropdemo.camera;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.unionsg.blink.R;

import cropdemo.util.MovingAverage;

/**
 * Created by Denis on 28.02.2015.
 */
public class CameraOverlay extends View  implements ICameraOverlay {

	private boolean mShowCorners;
	private boolean mAlertMode;

	// Current corners
	private PointF [] mDocumentCorners;

	// Drawing
	private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private float mFrameWidth = 5;
	private int mFrameColor = Color.WHITE;
	private int mAlertColor = Color.BLUE;
	private Path mEdgeLines = new Path();

	// Average
	private final MovingAverage mAverage = new MovingAverage(8, 20);

	// Animation
	private final Runnable mAnimate = new Runnable() {
		@Override
		public void run() {
			mAverage.duplicate();
			buildCorners(mShowCorners);
		}
	};

	private final Runnable mCancelAlert = new Runnable() {
		@Override
		public void run() {
			mAlertMode = false;
			invalidate();
		}
	};

	public CameraOverlay(Context context) {
		super(context);
		initPaint(null, 0, 0);
	}

	public CameraOverlay(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPaint(attrs, 0, 0);
	}

	public CameraOverlay(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initPaint(attrs, defStyleAttr, 0);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public CameraOverlay(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		initPaint(attrs, defStyleAttr, defStyleRes);
	}

	@Override
	public void onDraw(Canvas canvas) {
		if (mShowCorners && mDocumentCorners != null) {
			mEdgeLines.reset();
			// Top-left start point
			mEdgeLines.moveTo(mDocumentCorners[0].x, mDocumentCorners[0].y);
			// Top-right
			mEdgeLines.lineTo(mDocumentCorners[1].x, mDocumentCorners[1].y);
			// Bottom-RIGHT
			mEdgeLines.lineTo(mDocumentCorners[3].x, mDocumentCorners[3].y);
			// Bottom-left
			mEdgeLines.lineTo(mDocumentCorners[2].x, mDocumentCorners[2].y);
			// Return to start point
			mEdgeLines.lineTo(mDocumentCorners[0].x, mDocumentCorners[0].y);
			// finish
			mEdgeLines.close();

			if (mAlertMode) {
				mPaint.setColor(mAlertColor);
			} else {
				mPaint.setColor(mFrameColor);
				mPaint.setAlpha(mAverage.fullness(Color.alpha(mFrameColor)));
			}
			canvas.drawPath(mEdgeLines, mPaint);
		}
	}

	@Override
	public void showCorners(boolean shown) {
		if (shown != mShowCorners) {
			mShowCorners = shown;
			if (mShowCorners) {
				mAverage.reset();
			}
			invalidate();
		}
	}

	@Override
	public void showAlert(boolean alert, int delay) {
		removeCallbacks(mCancelAlert);
		mAlertMode = alert;
		invalidate();

		if (alert && delay > 0) {
			postDelayed(mCancelAlert, delay);
		}
	}

	@Override
	public void setDocumentCorners(PointF[] points) {
		removeCallbacks(mAnimate);
		updateCorners(points, mShowCorners);
	}

	private void updateCorners(final PointF [] points, boolean animate) {

		if (points != null) {
			final float [] vector = new float[points.length*2];
			for (int i = 0; i < points.length; ++i) {
				vector[i*2] = points[i].x;
				vector[i*2 + 1] = points[i].y;
			}
			mAverage.append(vector);
		} else {
			mAverage.append(null);
		}

		buildCorners(animate);
	}

	void buildCorners(boolean animate) {
		float [] avg = mAverage.average(true);
		if (avg != null) {
			mDocumentCorners = new PointF[avg.length/2];
			for (int i = 0; i < mDocumentCorners.length; ++i) {
				mDocumentCorners[i] = new PointF(Math.round(avg[i * 2]), Math.round(avg[i * 2 + 1]));
			}
		} else {
			mDocumentCorners = null;
		}

		if (animate) {
			postDelayed(mAnimate, 50);
		}

		invalidate();
	}

	private void initPaint(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		final TypedArray ar = getContext().obtainStyledAttributes(attrs, R.styleable.CameraOverlay, defStyleAttr, defStyleRes);
		mFrameWidth = ar.getDimension(R.styleable.CameraOverlay_frameWidth, mFrameWidth);
		mFrameColor = ar.getColor(R.styleable.CameraOverlay_frameColor, mFrameColor);
		mAlertColor = ar.getColor(R.styleable.CameraOverlay_alertColor, mAlertColor);
		ar.recycle();

		mPaint.setColor(mFrameColor);
		mPaint.setStrokeWidth(mFrameWidth);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
	}
}

package cropdemo.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;

import cropdemo.AppLog;
import cropdemo.CropDemoApp;
import cropdemo.DocImageRoutine;
import cropdemo.util.SequentialThread;
import com.pixelnetica.imagesdk.Corners;
import com.pixelnetica.imagesdk.ImageProcessing;
import com.pixelnetica.imagesdk.MetaImage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Find document corners by SDK
 * Created by Denis on 22.03.2015.
 */
public class FindDocCornersThread extends SequentialThread {

	private static final int PROCESS_CORNERS = EXIT_THREAD + 1;

	// Stuff
	private final Context mContext;
	private DocImageRoutine mRoutine;
	private final AutoShotDetector mShotDetector = new AutoShotDetector();

	public interface FindDocCornersListener {
		void documentCornersFound(Task task);
	}

	public class Task {
		// Using weak reference to allow camera be released
		final WeakReference<Camera> refCamera;

		final byte [] pictureBuffer;
		final int pictureFormat;
		final Point pictureSize;

		// Output parameters
		Corners documentCorners;
		boolean shotReady;

		public Task(Camera camera, byte [] buffer, int format, Point size) {
			this.refCamera = new WeakReference<Camera>(camera);
			this.pictureBuffer = buffer;
			this.pictureFormat = format;
			this.pictureSize = size;
		}

		public FindDocCornersThread getThread() {
			return FindDocCornersThread.this;
		}

		public YuvImage createImage() {
			if (pictureFormat == ImageFormat.NV21 || pictureFormat == ImageFormat.YUY2) {
				return new YuvImage(pictureBuffer, pictureFormat, pictureSize.x, pictureSize.y, null);
			} else {
				// Unsupported format
				return null;
			}
		}

		public Bitmap decodeBuffer() {
			YuvImage image = createImage();
			if (image == null) {
				return null;
			}

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			image.compressToJpeg(new Rect(0, 0, pictureSize.x, pictureSize.y), 100, out);

			ByteArrayInputStream input = new ByteArrayInputStream(out.toByteArray());
			return BitmapFactory.decodeStream(input);
		}
	}

	private List<FindDocCornersListener> listeners = new ArrayList<FindDocCornersListener>();

	public FindDocCornersThread(Context context) {
		super("FindDocCornersThread");    // No runnable object
		this.mContext = context;
	}

	public void addListener(FindDocCornersListener obj) {
		assert obj != null;
		if (!listeners.contains(obj)) {
			listeners.add(obj);
		}
	}

	public void removeListener(FindDocCornersListener obj) {
		assert obj != null;
		listeners.remove(obj);
	}

	public Task createTask(Camera camera, byte [] buffer, int format, Point size) {
		return new Task(camera, buffer, format, size);
	}

	public void processDocumentCorners(Task task) {
		addThreadTask(PROCESS_CORNERS, task, SINGLE_TASK, false);
	}

	public void setShotDetectorParams(int stableRadius, int stableDelay, int stableCount) {
		mShotDetector.setParams(stableRadius, stableDelay, stableCount);
	}

	@Override
	protected void onThreadStarted() {
		// Create SDK instance in worker thread
		// NOTE: DocImageSDK.load() MUST be caller early
		final CropDemoApp theApp = (CropDemoApp)mContext.getApplicationContext();
		mRoutine = theApp.createRoutine(false);
	}

	@Override
	protected Runnable handleThreadTask(int what, Object arg) {
		// Main processing
		final Task task = (Task) arg;
		process(mRoutine.sdk, task);
		task.shotReady = mShotDetector.addDetectedCorners(task.documentCorners);
		return new Runnable() {
			@Override
			public void run() {
				complete(task);
			}
		};
	}

	protected void onThreadComplete() {
		if (mRoutine != null) {
			mRoutine.close();
			mRoutine = null;
		}
	}

	// Main task
	private void process(ImageProcessing sdk, Task task) {
		// First, decode picture to bitmap
		Bitmap previewBitmap = task.decodeBuffer();
		if (previewBitmap == null) {
			Log.d(AppLog.TAG, "Cannot decode camera preview frame buffer");
			return;
		}

		if (!sdk.validate()) {
			// Something wrong
			return;
		}

		Bitmap sourceBitmap;
		// Need scale input?
		Point inputSize = sdk.supportImageSize(task.pictureSize);
		if (inputSize == task.pictureSize) {
			// No scale need. Check picture format
			if (previewBitmap.getConfig() == Bitmap.Config.ARGB_8888) {
				sourceBitmap = previewBitmap;
			} else {
				sourceBitmap = previewBitmap.copy(Bitmap.Config.ARGB_8888, true);
			}
		} else {
			// Scale bitmap
			sourceBitmap = Bitmap.createScaledBitmap(previewBitmap.copy(Bitmap.Config.ARGB_8888, true), inputSize.x, inputSize.y, true);
		}

		try {
			MetaImage image = new MetaImage(sourceBitmap);
			Bundle params = new Bundle();
			task.documentCorners = sdk.detectDocumentCorners(image, params);
			if (!params.getBoolean("isSmartCropMode")) {
				// No corners if no smart crop
				task.documentCorners = null;
			}
		} catch (Throwable e) {
			Log.e(AppLog.TAG, "DocImage SDK internal error", e);
		}
	}

	// Notify task complete
	private void complete(Task task) {
		// Notify all listeners
		for (FindDocCornersListener obj : listeners) {
			obj.documentCornersFound(task);
		}
	}
}
